export const BACKEND_SERVER = {
  BASE_URL:
    'https://api.internetmoney.io',

  // EndPoints
  GET_NETWORK_SUPPORT_DETAIL: '/get-network-specific-data',
  SEND_SUPPORT_MAIL: '/send-support-mail',
  GET_IMTOKEN_DETAILS: '/get-imtoken-details',
  GET_SWAP_SUPPORT_NETWORK_LIST: '/get-swap-supported-chainids',
  GET_NETWORKS: '/api/v1/networks',
  POST_LOG: '/api/v1/log',
};
