export const WALLET_CONNECT_PROJECT_ID = '8533702654378ddc839ecab4ca215f0f';
export const WALLET_CONNECT_PROJECT_NAME = 'Internet Money Wallet';
export const WALLET_CONNECT_PROJECT_DESCRIPTION = 'EVM Wallet. Connect to All EVM Chains. First Tokenized Crypto Wallet of It\'s Kind.';
export const WALLET_CONNECT_PROJECT_URL = 'internetmoney.io';