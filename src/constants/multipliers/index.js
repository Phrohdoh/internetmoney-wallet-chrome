import { CHAIN_IDS } from 'constants/common';

/**
 * Fee Multipliers
 */
export const DEFAULT_FEE_MULTIPLIER = 10;
export const FEE_RANGE_LOW_PERCENT_OFFSET = -0.1;
export const FEE_RANGE_HIGH_PERCENT_OFFSET = 0.1;

// Chain-specific multipliers
export const FEE_MULTIPLIERS = {
  [CHAIN_IDS.ETHEREUM]: 3,
};

/**
 * Gas Multipliers
 */
export const DEFAULT_GAS_MULTIPLIER = 1.2;
export const GAS_RANGE_LOW_PERCENT_OFFSET = 0;
export const GAS_RANGE_HIGH_PERCENT_OFFSET = 0;

// Chain-specific multipliers
export const GAS_MULTIPLIERS = {};

/**
 * Gas Price Multipliers
 */
export const DEFAULT_GAS_PRICE_MULTIPLIER = 1.3;
export const GAS_PRICE_RANGE_LOW_PERCENT_OFFSET = -0.15;
export const GAS_PRICE_RANGE_HIGH_PERCENT_OFFSET = 0.38;

// Chain-specific multipliers
export const GAS_PRICE_MULTIPLIERS = {};
