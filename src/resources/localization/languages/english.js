export default {
  APP_NAME: 'Internet Money',

  // Splash Screen
  INTERNET: 'INTERNET',
  MONEY: 'MONEY',
  SPLASH_MSG: 'The Future of\nMoney',

  // Start Screens
  WELCOME_TITLE: 'Welcome to',
  INTERNET_MONEY: 'Internet Money',
  WELCOME_MSG: 'The Future of Finance\nBy the people, for the people',
  NEW_CRYPTO_UPPERCASE: 'NEW TO CRYPTO',
  EXPERIENCE_CRYPTO_UPPERCASE: 'EXPERIENCED WITH CRYPTO',
  ACKNOWLEDGE_MSG: 'I acknowledge that I have read and accept the {0} and {1}',
  CHECK_ACKNOWLEDGE_MSG: 'Please accept the terms and conditions to continue.',

  // Learn Basic Screen
  CONTINUE_UPPERCASE: 'Continue',
  LEARN_BASIC_INFO_LIST: [
    {
      image: 'GET_READY_BG',
      title: 'Get Ready',
      subtitle: 'to embark on a journey of financial freedom',
      description:
        'Our mission is to simplify the crypto space and make it more accessible. \n\n There will be plenty of resources available to help you along the way.',
    },
    {
      image: 'UNDERSTAND_BASIC_BG',
      title: 'Understanding The Basics',
      subtitle: 'What is a Wallet?',
      description: `This app is your wallet. With this wallet, you can create accounts. Those accounts store information about and allow you to interact with your cryptocurrency assets.

It’s kind of like your “crypto bank account”.`,
    },
    {
      title: 'Understanding The Basics',
      subtitle: 'Seed Phrase',
      description: `In a couple of moments you will generate your “Seed Phrase”.

Understanding your seed phrase is one of the most important components of your crypto journey.

Your seed phrase is 24 words that are unique to you. These 24 words are used to store and recover your crypto assets. With these 24 words someone can steal all of your crypto assets. It is very important to store these words safely.

NEVER share this seed phrase with anyone.

If someone asks for your seed phrase, consider it a scam`,
    },
    {
      title: 'Understanding The Basics',
      subtitle: 'Public & Private Keys',
      description: `When your seed phrase is generated, an account will also be generated.

Your account has a public key or “public address”. Anyone can view your public address. It is completely safe. This is how people send you crypto assets.

With each public key you generate, a private key is also generated. If someone knows this private key, they can steal your crypto.

It is very important to NEVER share this private key with anyone.

If someone asks for your private key, consider it a scam.

First rule of crypto, “Not your keys, not your crypto”.
`,
    },
    {
      image: 'PASS_SETUP_BG',
      title: 'Password/Passcode Setup',
      description:
        'Make this something you will remember or can safely store, as this password/passcode cannot be recovered if lost. If you lose this, the only way to access your crypto assets is with your seed phrase.',
    },
  ],
  UNDERSTAND_BASICS: 'I understand the basics',
  UNDERSTAND: 'I Understand',
  CHECK_UNDERSTAND_MSG:
    'Please confirm you understand the basics in order to continue.',
  CHECK_UNDERSTAND_MSG_2: 'Please confirm you understand in order to continue.',
  SETUP_PASSWORD: 'SETUP PASSWORD',
  SETUP_PASSCODE: 'SETUP PASSCODE',

  // Setup Password Screen
  PASS_SETUP: 'Password Setup',
  PASSWORD: 'Password',
  CONF_PASSWORD: 'Confirm Password',
  ENTER_PASSWORD: 'Enter Password',
  ENTER_CONF_PASSWORD: 'Enter Confirm Password',
  GET_START: 'Get Started',
  GET_START_UPPERCASE: 'GET STARTED',
  PASSWORD_NOTES:'Password must contain minimum of 10 characters.',

  // Setup Passcode screen
  PASSCODE_SETUP: 'Passcode Setup',
  PASSCODE: 'Passcode',
  CONF_PASSCODE: 'Confirm Passcode',
  ENTER_VALID_PASSCODE_MSG: 'Your passcode must be 6 digits long.',
  INVALID_PASSCODE_MSG: 'Your passcode is invalid',
  INVALID_PASSWORD_MSG: 'Your password is invalid',
  PASSCODE_NOT_MATCH_MSG:
    'Passcode and confirm passcode do not match, please try again.',

  // ExperiencedCryptoScreen
  THANK_YOU_FOR_CHOOSE: 'Thank you for choosing us',
  EXPERIENCE_CRYPTO_MSG: `We know there a lot of crypto wallets to choose from. We appreciate you giving us the opportunity to earn your trust.

The Internet Money Wallet is a multi-chain EVM wallet that supports all Ethereum based blockchains.

Security is our top priority. Followed by innovation. Inside the app you will find many amazing tools and features to support your crypto journey. We are always building and strive to continue to enhance your experience.

Make sure you learn about the T.I.M.E Dividend (TIME) token. The first token of its kind.

By holding this token, you will earn passive income from transactions that occur in the Internet Money Wallet.`,
  CREATE_NEW_WALLET_UPPERCASE: 'CREATE NEW WALLET',
  IMPORT_WALLET_UPPERCASE: 'Import Wallet',

  // Create wallet screen
  CREATE_WALLET: 'Creating Your',
  WALLET: 'Wallet',
  WALLET_UPPERCASE: 'WALLET',
  WALLET_CREATE_STRINGS: [
    'generating...',
    '24 word...',
    'seedphrase...',
    'encrypting...',
    'in aes 256....',
  ],

  // Create wallet success screen
  CONGRATULATIONS: 'Congratulations!',
  WALLET_CREATE_SUCCESS_MSG: 'Wallet Successfully\nCreated!',
  ALMOST_THERE: 'You’re almost there',
  CONTINUE_INFO_MSG:
    'We know this can be overwhelming. Don’t sweat it. In the app you will find plenty of resources to help you along the way.',

  // Import Wallet Screen
  IMPORT_WALLET_MSG: 'Select the number of words in your BIP39 seed phrase, then enter the words one at a time with a space after each word.',
  ENTER_PHRASE_MSG: 'Enter Seed Phrase',
  PHRASE_VALIDATION_MSG:
    'The number of words entered does not match with your selected count.',
  IMPORT_WALLET_ERROR:
    'Your secret recovery phrase is invalid, please try again.',
  ADD: 'Add',

  // Secret Recovery Phrase Screen
  SEED_PHRASE: 'Seed Phrase',
  RECOVERY_PHRASE_MSG:
    'Reminder, your seed phrase needs to be stored securely. With these 24 words anyone can access your crypto assets. We HIGHLY recommend using a “hardware wallet” to store your seed phrase. Until then, write them down and store them in a secure location.',
  REVEAL_SEED_PHRASE_UPPERCASE: 'Reveal Seed Phrase',
  HIDE_SEED_PHRASE_UPPERCASE: 'HIDE SEED PHRASE',
  SHOW_SEED_PHRASE_UPPERCASE: 'SHOW SEED PHRASE',
  SEED_PHRASE_SCAM_MSG_UPPERCASE: 'SCAMMERS MAY TRY TO STEAL YOUR SEED PHRASE OR BY POSING AS SUPPORT. NO INDIVIDUAL OR SUPPORT WILL EVER ASK FOR THIS INFORMATION. ONLY IMPORT YOUR SEED PHRASE INTO REPUTABLE AND TRUSTED APPLICATIONS. IT IS RECOMMENDED TO AVOID TAKING SCREENSHOTS OF THIS PAGE, AS SCREENSHOTS MAY BE ACCESSED BY MALICIOUS APPS.',
  SEED_PHRASE_24_WORDS_MSG: 'Make sure to save your 24-word seed phrase in the exact order it is displayed below.',
  HIDDEN_SEED_PHRASE_PLACEHOLDER: '**********',
  SEED_PHRASE_PASSCODE_VERIFICATION_MSG: 'Enter your passcode to reveal your seed phrase:',
  SEED_PHRASE_PASSWORD_VERIFICATION_MSG: 'Enter your password to reveal your seed phrase:',

  // Password Verification Screen
  PASSWORD_VERIFICATION_UPPERCASE: 'PASSWORD VERIFICATION',
  PASSCODE_VERIFICATION_UPPERCASE: 'PASSCODE VERIFICATION',
  PRIVATE_KEY_PASSCODE_VERIFICATION_MSG: 'Enter your passcode to reveal your private key:',
  PRIVATE_KEY_PASSWORD_VERIFICATION_MSG: 'Enter your password to reveal your private key:',

  // Your Secret Recovery Phrase Screen
  YOUR_SEED_PHRASE_UPPERCASE: 'Your Seed Phrase',
  RECOVERY_24_WORD_MSG: 'Never share this with ANYONE',
  COPY_CLIPBOARD_UPPERCASE: 'Copy to clipboard',
  HIDE_KEY_UPPERCASE: 'HIDE PRIVATE KEY',
  SHOW_KEY_UPPERCASE: 'SHOW PRIVATE KEY',
  PRIVATE_KEY_SCAM_MSG_UPPERCASE: 'SCAMMERS MAY TRY TO STEAL YOUR PRIVATE KEYS BY POSING AS SUPPORT. NO INDIVIDUAL OR SUPPORT WILL EVER ASK FOR THIS INFORMATION. ONLY IMPORT YOUR PRIVATE KEYS INTO REPUTABLE AND TRUSTED APPLICATIONS. IT IS RECOMMENDED TO AVOID TAKING SCREENSHOTS OF THIS PAGE, AS SCREENSHOTS MAY BE ACCESSED BY MALICIOUS APPS.',
  REVEAL_KEY_MSG: `If someone knows this private key, they can steal your crypto. It is very important to NEVER share this private key with anyone.

If someone asks you for your private key, consider it a scam.`,
  CONFIRM_MSG: '"I confirm that I have securely stored my seed phrase"',
  SLIDE_TO_CONFIRM_UPPERCASE: 'Slide to Confirm',
  CLICK_TO_CONFIRM: 'Click to Confirm',
  CHECK_CONFIRM_MSG:
    'Please confirm you have stored your secret recovery phrase in order to continue',

  // Get start info screen
  INTERNET_MONEY_WALLET: 'Internet Money Wallet',
  GET_START_INFO_MSG: `Be sure to check the resources tab. There you’ll find many of the tools to help you on your crypto journey.

Join our communities for updates and questions! Our Telegram chat is amazing. You can get most questions answered there. Otherwise feel free to submit a support ticket or give us feedback by hitting the bubble in the top right corner of the app.`,
  GET_START_THANKYOU_MSG:
    'Thank you for giving us the opportunity to be your crypto wallet!',

  // Wallet dashboard screen
  ACCOUNTS: 'Accounts',
  TRANSACTIONS: 'Transactions',
  ACCOUNTS_UPPERCASE: 'ACCOUNTS',
  TRANSACTION_UPPERCASE: 'TRANSACTIONS',
  HIDE_BALANCE_UPPERCASE: 'HIDE BALANCES',
  SHOW_BALANCE_UPPERCASE: 'SHOW BALANCES',
  EXPORT_TRANSACTION_UPPERCASE: 'EXPORT TRANSACTIONS',
  TOTAL_VALUE: 'Total Value',
  VALUE: 'Value',
  FILTER_UPPERCASE: 'Filter',
  ACCOUNT: 'Account',
  NO_ACCOUNTS: 'There are no accounts in this wallet. Please use the Add Account button.',

  // Add Account component
  ADD_ACCOUNT: 'ADD ACCOUNT',
  REMOVE_ACCOUNT: 'REMOVE ACCOUNT',
  REMOVE_ACCOUNT_MESSAGE: 'Are you sure you want to remove the account "{0}"?',
  FINISHED_REMOVING_ACCOUNT: 'FINISHED REMOVING',
  CREATE_NEW_ACCOUNT: 'Create New Account',
  IMPORT_ACCOUNT: 'Import Account',
  CREATE_ACCOUNT: 'Create Account',
  RESTORE_ACCOUNT: 'Restore Account',
  ACCOUNT_NAME: 'Account Name',
  ENTER_ACCOUNT_NAME: 'Enter Account Name',
  PRIVATE_KEY: 'Private Key',
  ENTER_PRIVATE_KEY: 'Enter Private Key',
  IMPORT_ACCOUNT_MSG:
    'Note: Imported accounts are not recoverable via the Seed Phrase generated by the Internet Money Wallet',
  ENTER_VALID_ACCOUNT_NAME_MSG: 'Please enter a valid account name.',
  ENTER_VALID_PRIVATE_KEY: 'Please enter a valid private key',
  ACCOUNT_NAME_EXIST_MSG:
    'Account name already exist. Please use a different account name.',

  // Account detail screen
  DAY_FILTER_LIST: ['HOUR', 'DAY', 'WEEK', 'MONTH', 'YEAR', 'ALL'],
  SEND_UPPERCASE: 'SEND',
  RECEIVE_UPPERCASE: 'RECEIVE',
  SWAP_UPPERCASE: 'SWAP',
  SEND: 'Send',
  SEND_TRANSACTION_CONFIRM: 'Send Transaction',
  SWAP: 'Swap',
  DAPP: 'Dapp',
  CLAIM: 'Claim',
  RECEIVE: 'Receive',
  ADD_TOKEN_BUTTON_UPPERCASE: '+ Add Token',
  EDIT_UPPERCASE: 'EDIT',
  HIDE_CHART: 'Hide Chart',
  VIEW_CHART: 'View Chart',
  DELETE: 'Delete',
  REMOVE_TOKEN: 'Remove Token',
  REMOVE_TOKEN_MSG: 'Are you sure you want to remove {0}?',

  // Add Token view
  ADD_TOKEN_UPPERCASE: 'ADD TOKEN',
  ADDED_ALL_POPULAR_TOKENS_MSG: 'You\'ve already added the most popular tokens!',

  // Import Token view
  IMPORT_TOKEN_TAB_UPPERCASE: 'IMPORT TOKEN',
  IMPORT_TOKEN_UPPERCASE: 'Import Token',
  IMPORT_TOKEN_MSG:
    'Note: Anyone can create a token, including creating a fake version of an existing token.',
  TOKEN_ADDRESS: 'Token Address',
  ENTER_HERE: 'Enter Here...',
  TOKEN_SYMBOL: 'Token Symbol',
  TOKEN_PRECISION: 'Token Precision',
  TOKEN_SYMBOL_ERROR: 'Token symbol must be 11 characters or fewer.',
  TOKEN_PRECISION_ERROR: 'Token Precision must be atleast 0, and not over 36.',
  ENTER_VALID_TOKEN_ADDRESS: 'Pleas enter a valid token address.',
  TOKEN_ALREADY_EXIST_MSG:
    'This token is already imported! Please use a different token.',

  // Popular Tokens view
  POPULAR_TOKENS_TAB_UPPERCASE: 'POPULAR TOKENS',
  ADD_TOKEN_MSG: 'Are you sure you want to add {0}?',

  // Send token screen
  SEND_TOKEN_UPPERCASE: 'Send Tokens',
  CURRENT_NETWORK: 'Current Network',
  NEXT_UPPERCASE: 'NEXT',
  CHOOSE_SEND_FROM_ACCOUNT: 'Choose the account to send from:',
  ENTER_SEND_TO_ACCOUNT: 'Enter the account to send to:',
  RECENT_ACCOUNT: 'Recent accounts you\'ve sent to',
  ENTER_PUBLIC_ADDRESS_PLACEHOLDER: 'Public address',
  'BALANCE:': 'Balance: ',
  BALANCE: 'Balance',
  TRANSFER_BETWEEN_ACCOUNTS_UPPERCASE: 'Transfer between accounts',
  CHOOSE_ACCOUNTS_UPPERCASE: 'Choose Account',
  ENTER_PUBLIC_ADDRESS_MSG: 'Please enter public address.',
  CHOOSE_TOKEN: 'Choose token to send:',
  ENTER_AMOUNT: 'Enter amount to send:',
  ACCOUNT_SELECT_ERROR_MSG:
    'This account does not contain any tokens.  Please add some tokens before sending.',
  USD_UPPERCASE: 'USD',
  'VALUE:': ' Value: ',
  SEND_AMOUNT_ERROR_MSG:
    'Your balance is not sufficient to send the entered amount. Please enter a valid amount.',
  SWAP_AMOUNT_ERROR_MSG:
    'Your balance is not sufficient to transfer the entered amount. Please enter valid amount.',
  ENTER_AMOUNT_MSG: 'Please enter the amount to send',
  ESTIMATE_FEE_ERROR_MSG: 'Not enough {0} to pay for the transaction fees.',
  MAX: 'Max',

  // Transfer between accounts
  CHOOSE_TRANSFER_FROM_ACCOUNT: 'Choose the account to transfer from:',
  CHOOSE_TRANSFER_TO_ACCOUNT: 'Choose the account to transfer to:',
  CHOOSE_DIFFERENT_ACCOUNT_MSG:
    'You cannot transfer to the same account. Please select a different account.',

  // Confirm transaction screen
  CONFIRM_TRANSACTION_UPPERCASE: 'Confirm transaction',
  FAILED_TRANSACTION_UPPERCASE: 'Failed transaction',
  PENDING_TRANSACTION_UPPERCASE: 'Pending transaction',
  SUBMITTING_TRANSACTION_UPPERCASE: 'Submitting the Transaction...',
  EXPEDITE_TRANSACTION_NOTICE: 'If your transaction has been pending for an extended period, consider resubmitting it with a higher gas fee to expedite processing.',
  AMOUNT: 'Amount',
  PERCENT: '%',
  FROM_TEXT: 'From:',
  TO_TEXT: 'To:',
  ESTIMATE_GAS_FEE: 'Estimated gas fee:',
  LOW_SEND_TIME: 'Likely more then > 30 Seconds',
  MEDIUM_SEND_TIME: 'Likely in < 30 Seconds',
  HIGH_SEND_TIME: 'Likely in 15 Seconds',
  LIKELY_SECONDS: 'Likely in < 30 Seconds',
  MAX_FEE: 'Max Fee:',
  GAS_PRICE: 'Gas Price',
  TOTAL: 'Total',
  SLICE_TO_SEND_TOKEN_UPPERCASE: 'SLIDE TO SEND TOKEN',
  CLICK_TO_SEND_TOKENS: 'Click to send tokens',

  // GasFee popup
  ESTIMATED_GAS_FEE_UPPERCASE: 'ESTIMATED GAS FEE',
  SAVE_UPPERCASE: 'SAVE',
  RANGE_LIST: ['LOW', 'MEDIUM', 'HIGH'],
  ADVANCE_OPTION: 'Advanced Options',
  GAS_LIMIT: 'Gas Limit',
  MAX_PRIO_FEE: 'Max Priority Fee',
  ESTIMATE_TEXT: 'Estimate',
  'ESTIMATE:': 'Estimate: ',
  GAS_FEE_NOT_ALLOW_MSG: 'Gas fee cannot be less than the minimum gas value',
  MAX_PRIO_FEE_NOT_ALLOW_MSG:
    'Max priority fees cannot be less than minimum priority value.',
  MAX_FEE_NOT_ALLOW_MSG: 'Max fee cannot be less than max priority fee. Please try your transaction again. This error usually resolves itself.',
  MAX_FEE_CHECK_MSG: 'MaxPrioFee should be less than MaxFee',
  MIN_GAS_PRICE_MSG: 'Gas Price is Underpriced',
  MIN_GAS_LIMIT_MSG: 'Gas limit is too low. Given {0}, need at least {1}.',

  // Receive Token Screen
  RECEIVE_TOKENS_UPPERCASE: 'RECEIVE TOKENS',
  RECEIVE_TOKENS_MSG_UPPERCASE: 'Scan or Click Copy to Share Your',
  COPY_UPPERCASE: 'COPY',
  SHARE_UPPERCASE: 'SHARE',

  // Transaction filter component
  APPLY_UPPERCASE: 'APPLY',
  CHOOSE_ACCOUNT_UPPERCASE: 'CHOOSE ACCOUNT',
  CHOOSE_ACCOUNT: 'Choose Account',
  SEND_TRANSACTION: 'Send Transactions',
  RECEIVE_TRANSACTION: 'Receive Transactions',
  SWAP_TRANSACTION: 'Swap Transactions',
  CLAIM_TRANSACTION: 'Claim Transactions',
  DAPP_TRANSACTION: 'Dapp Transactions',
  REFERRAL_EARNINGS: 'Referral Earnings', // Keep this for historical transaction records
  ALL: 'All',

  // Swap Token select token screen
  SWAP_TOKEN_UPPERCASE: 'Swap Tokens',
  CHOOSE_TOKEN_UPPERCASE: 'CHOOSE TOKEN',
  CHOOSE_SWAP_ACCOUNT: 'Choose the account to swap with:',
  ENTER_SWAP_TOKEN_ADDRESS: 'Enter token address here...',
  CHOOSE_FROM_TOKEN_TO_SWAP: 'Choose asset to swap out of:',
  ENTER_AMOUNT_TO_SWAP: 'Enter amount to swap:',
  SLIPPAGE: 'Slippage (%):',
  CHOOSE_TO_TOKEN_TO_SWAP: 'Choose asset to swap into:',
  CHOOSE_DIFFER_TOKEN_MSG:
    'You can not swap the same token. Please choose a different token to swap.',
  ENTER_SWAP_AMOUNT_MSG: 'Please enter the amount to swap',
  WALLET_FEE: 'Wallet fee',

  // swap token get quote screen
  RECEIVE_TOKEN_MSG: 'Here are quotes for',
  RECOMMENDED_OPTION: 'Recommended Option',
  RECEIVING: 'Receiving',
  DEX_FEES: 'Dex Fees',
  WALLET_FEES: 'Wallet Fees',
  SOURCE: 'Source',
  SWAP_NOT_ALLOW_MSG: 'Swap amount not approved. Please approve to get quotes.',
  APPROVE_REQUEST: 'Approve Request',
  GIVE_ALLOWANCES_MSG: 'Give Allowance to Access {0} token',
  APPROVE: 'Approve',
  FEE_DETAILS_TITLE: 'Internet Money Swap Fee',
  FEE_DETAILS: 'Get the best price from the top liquduity sources every time. The fee of 0.729% is automatically calculated based on this quote and paid in {NATIVE_TOKEN}. 100% of this fee is distributed to holders of the TIME token.',
  FEE_FOOTNOTE: 'A fee of 0.729% is calculated based on this quote',

  // swap approvel confirmation screen
  APPROVE_TRANSACTION_UPPERCASE: 'Approve transaction',
  GIVE_PERMISSION_MSG: 'Give permission to access your {0}?',
  GRANT_MSG:
    'By granting permission, you are allowing the following contract to access your funds.',
  IM_SWAP_ROUTER: 'Internet Money Swap Router',
  APPROVAL_AMOUNT: 'Approval amount',
  SLIDE_TO_APPROVE_UPPERCASE: 'SLIDE TO APPROVE',
  CLICK_TO_APPROVE: 'Click to Approve',
  PERMISSON_REQUEST: 'Permission Request',
  PERMISSION_REQUEST: 'Permission request',
  SWAP_PERMISSION_INFO:
    'InternetMoney wallet may access and spend up to this max amount',
  APPROVED_AMOUNT: 'Approved Amount',
  GRANTED_TO: 'Granted to',
  CONTRACT: 'Contract',
  DATA: 'Data',
  FUNCTION: 'Function',

  // Edit permission
  EDIT_PERMISSION_TITLE:'Spend limit permission',
  EDIT_PERMISSION_CONTENT:'Allow Internet Money Wallet to withdraw and spend up to the following amount:',
  PROPOSED_APPROVAL_LIMIT:'Proposed Approval Limit',
  PROPOSED_APPROVAL_CONTENT:'Spend limit requested by Internet Money Wallet',
  CUSTOM_SPEND_LIMIT_TITLE:'Custom Spend Limit',
  CUSTOM_SPEND_LIMIT_CONTENT:'Enter Max Spend Limit',
  CUSTOM_LIMIT_NOT_VALID: 'Amount is less than the Swap Amount.',
  
  // Swap Token
  CLICK_TO_SWAP_TOKENS: 'Click to swap tokens',
  SWAP_SUCCESS_MSG: 'Succesfully submitted transaction: Swap from {0} to {1}',
  ADD_TOKEN: 'Add Token',
  SWAPPING: 'Swapping',
  FOR: 'for',
  EEPECTED_AMOUNT: 'Expected',
  VOLATILITY_ERROR: 'This quote is no longer valid due to price volatility. Consider increasing the maximum slippage, and try again.',

  // Rename account screens
  ACCOUNT_SETTING_UPPERCASE: 'Accounts Setting',
  RENAME_ACCOUNT_MSG:
    'Below you can rename your account or reveal your private key.',
  RENAME_ACCOUNT: 'Rename Account',
  REVEAL_PRIVATE_KEY_UPPERCASE: 'Reveal Private Key',
  SAVE_CHANGES_UPPERCASE: 'Save Changes',
  REVEAL_PRIVATE_KEY_MSG: `If someone knows this private key, they can steal your crypto. It is very important to NEVER share this private key with anyone.

If someone asks you for your private key, consider it a scam.`,

  // Reveal private key screen
  PRIVATE_KEY_UPPERCASE: 'PRIVATE KEY',
  REVEAL_KEY_INFO_MSG: `The private key is a key that allows you to access this single account.

Anyone who has access to the private key can transfer and gain access to all of your funds for this account.`,
  VERIFICATION_UPPERCASE: 'VERIFICATION',
  ACCOUNT_PRIVATE_KEY_UPPERCASE: '{0} PRIVATE KEY',

  // Transaction detail screen
  GAS_LIMIT_FEE: 'Gas Limit (Units)',
  GAS_USE_FEE: 'Gas Used (Units)',
  BASE_FEE: 'Base Fee',
  PRIO_FEE: 'Priority Fee',
  TOTAL_FEE: 'Total Gas Fee',
  MAX_FEE_PER_GAS: 'Max Fee Per Gas',
  GWEI: '(GWEI)',
  VIEW_BLOCK_EXPLORER_UPPERCASE: 'VIEW ON BLOCK EXPLORER',
  COPY_TRANS_ID_UPPERCASE: 'COPY TRANSACTION ID',
  NONCE: 'Nonce',
  CLAIMED_AMOUNT: 'Claimed Dividends',
  CLAIM_DIV: 'Claim Dividend',
  TRANSACTION_CONFIRMED:'Transaction Confirmed',

  // Login Password Verification Screen
  LOGIN_PASSWORD_VERIFY_UPPERCASE: 'Verify Password to Login',
  LOGIN_INVALID_PASSWORD: 'Invalid Password!',
  LOGIN_PASSCODE_VERIFY_UPPERCASE: 'Verify Passcode to Login',
  LOGIN_INVALID_PASSCODE: 'Invalid Passcode!',

  // Network List screen
  NETWORK_UPPERCASE: 'NETWORKS',
  ADD_NETWORK_UPPERCASE: 'Add Network',
  NETWORK_SWITCH: 'Change Network',
  NETWORK_SWITCH_MSG: 'Change network to ',
  NETWORK_SWITCH_TITLE: 'Network Switched',
  NETWORK_CHANGE_SUCCESS_MSG: 'Successfully switched to ',
  HIDE: 'Hide',
  DONE: 'Done',
  REMOVE: 'Remove',

  // Add Network screen
  ADD_NETWORK_MSG:
    'Note: A Malicious network provider can lie about the state of the blockchain and record your network activity. Only add custom networks you trust',
  NETWORK_NAME: 'Network Name',
  RPC_URL: 'RPC Url',
  NEW_RPC_NETWORK: 'New RPC Network',
  CHAIN_ID: 'Chain ID',
  SYMBOL: 'Symbol',
  BLOCK_EXPLORER_URL: 'Block Explorer URL',
  BLOCK_EXPLOR_URL_OPTIONAL: 'Block Explorer URL (Optional)',
  ENTER_NETWORK_NAME_MSG: 'Please enter a network name.',
  ENTER_RPC_URL_MSG: 'Please enter a RPC Url',
  ENTER_CHAIN_ID_MSG: 'Please enter a chainid.',
  ENTER_SYMBOL_NAME_MSG: 'Please enter a symbol name.',
  ENTER_VALID_RPC_MSG: 'Please enter a valid rpc url.',
  ENTER_VALID_BLOCK_URL_MSG: 'Please enter a valid block url',
  NETWORK_ALREADY_EXIST_MSG:
    'This network already exists. Please enter a new chain ID or remove the existing one.',

  // setting screens
  SETTINGS_UPPERCASE: 'SETTINGS',
  GENERAL: 'General',
  ABOUT_IM: 'About',
  CHOOSE_LANGUAGE: 'Choose Language',
  LOCK_WALLET: 'Lock Wallet',
  REFER_FRIEND: 'Refer a Friend',
  CONNECTED_WALLET: 'Connected Wallet',
  THEME_MORE: 'Theme - Dark More',
  SECURITY_PRIVACY: 'Security & Privacy',
  SECRET_RECOVERY_PHRASE: 'Secret Recovery Phrase',
  DATA_COLLECTION: 'Send Logs & Error Reports',
  WALLET_CONNECT: 'Wallet Connect',
  VIEW_CONNECTED_APPS: 'View Connected Apps',
  SCAN_QR_CODE: 'Scan QR Code',
  SUPPORT: 'Support',
  FEATURE_REQUEST: 'Feature Request',
  SUBMIT_BUG: 'Submit a Bug',
  SUBMIT_UPPERCASE: 'SUBMIT',
  CONTACT_US: 'Contact Us',
  LEGAL: 'Legal',
  TERMS_AND_CONDITIONS: 'Terms and Conditions',
  PRIVACY_POLICY: 'Privacy Policy',
  COLLECT_LOGS: 'Collect Logs/Crash Data',

  // ABOUT IM screens,
  ABOUT: 'About',
  APP_VERSION: 'App Version',

  // Change Language  screens
  CURRENT_LANGUAGE_UPPERCASE: 'Current Language',
  CHOOSE_LANGUAGE_MSG:
    'At this time, we only support English. Additional languages will be supported in future versions.',
  LANGUAGE_LIST: [{ title: 'English', key: 'en' }],
  ENTER_VALID_WALLET_URL: 'Wallet url is not valid. Please enter valid URL.',

  CHANGE_THEME: 'Change Theme',

  // Change Password screen
  PASSWORD_SETTING_UPPERCASE: 'PASSWORD SETTINGS',
  PASSCODE_SETTING_UPPERCASE: 'PASSCODE SETTINGS',

  CHANGE_PASSWORD_UPPERCASE: 'Change Password',
  CHANGE_PASSCODE_UPPERCASE: 'Change Passcode',

  CHANGE_PASSWORD_INFO_MSG:
    'Your password MUST be something you will remember or can safely store, as this password/passcode cannot be recovered if lost. If you lose it, the only way to access your crypto assets is with your seed phrase.',
  CHANGE_PASSCODE_INFO_MSG:
    'Your passcode MUST be something you will remember or can safely store, as this password/passcode cannot be recovered if lost. If you lose it, the only way to access your crypto assets is with your seed phrase.',

  OLD_PASSWORD: 'Old Password',
  NEW_PASSWORD: 'New Password',

  OLD_PASSCODE: 'Old Passcode',
  NEW_PASSCODE: 'New Passcode',

  ENTER_OLD_PASSWORD_MSG: 'Please enter old password',
  ENTER_NEW_PASSWORD_MSG: 'Please enter new password',
  CHANGE_PASS_ERROR_MSG: 'Something went wrong, please try again.',

  ENTER_OLD_PASSCODE_MSG: 'Please enter valid old passcode',
  ENTER_NEW_PASSCODE_MSG: 'Please enter valid new passcode',

  INCORRECT_OLD_PASSWORD: 'Incorrect old Password',

  // WD Claim screen
  WD_UPPERCASE: 'TIME',
  WD_BALANCE: 'TIME Balance',
  WALLET_DIVIDENDS_UPPERCASE: 'T.I.M.E Dividends',
  PENDING_WALLET_DIVIDENDS: 'Pending Dividends',
  TOTAL_DIVIDENDS_CLAIMED: 'Total Dividends Claimed',
  CLAIMED_DIVIDENDS: 'Claimed Dividends',
  CLAIMABLE_DIVIDENDS: 'Claimable Dividends',
  SWEEPABLE_AMOUNT: 'Sweepable Dividends',
  NETWORK: 'Network',
  TOKEN: 'Token',
  TOTAL_DIVIDENDS_DISTRIBUTED: 'Total Dividends Distributed',
  CLAIM_WD_UPPERCASE: 'Claim TIME',
  CLAIM_DIVIDENDS_UPPERCASE: 'Claim Dividends',
  SWEEP_DIVIDENDS_UPPERCASE: 'Sweep Dividends',
  LAST_CLAIMED: 'Last Claimed',
  NEVER_CLAIMED: 'No Dividends Claimed',
  TO_WD_ADDRESS: 'To TIMEAddress:',
  DIVIDENDS_AVAILABLE_TO_CLAIM: 'Dividends Available to Claim',
  SLIDE_TO_CLAIM_DIVIDENDS_UPPERCASE: 'SLIDE TO CLAIM DIVIDENDS',
  CLICK_TO_CLAIM_DIVIDENDS_UPPERCASE: 'CLICK TO CLAIM DIVIDENDS',
  CLICK_TO_SWEEP_DIVIDENDS_UPPERCASE: 'CLICK TO SWEEP DIVIDENDS',
  CLAIM_ZERO_BALANCE_ERROR_MSG:
    'Claimable dividend is not large enough to claim.',
  SWEEP_ZERO_BALANCE_ERROR_MSG:
    'Sweepable amount is not large enough.',
  AVAILABLE_CLAIM: 'Available to Claim',
  TIME_NOT_SUPPORTED: 'T.I.M.E. Dividend (TIME) is not yet supported on this network.',
  WHAT_IS_TIME: 'What is TIME? {0}',
  LEARN_MORE: 'Learn More Here',  

  // Support Screen
  SUPPORT_TYPES: [
    {
      title: 'Submit a Feature',
      subTitle: 'Feature Request',
      type: 'sendfeedback',
      detailTitle: 'Got a cool feature in mind?',
      detailMsg: 'Let us know what you would like to see in future versions.',
    },
    {
      title: 'Submit a Bug',
      subTitle: 'Platform Used',
      type: 'submitbug',
      detailTitle: 'Found a Bug?',
      detailMsg:
        'Leaving your contact information is optional. If you want us to follow up, please leave a way to contact you!',
    },
    {
      title: 'Contact Us',
      subTitle: 'Get In Touch',
      type: 'contactus',
      detailTitle: 'We Would Love to Hear From You!',
      detailMsg:
        'Leaving your contact information is optional. If you want us to follow up, please leave a way to contact you!',
    },
  ],
  NAME: 'Name',
  NAME_PLACEHOLDER: 'Name (Optional)',
  ENTER_NAME_MSG: 'Please enter your name.',
  YOUR_MESSAGE: 'Enter your message',
  YOUR_MESSAGE_PLACEHOLDER: 'Message',
  ENTER_MESSAGE: 'Please enter your message.',
  EMAIL_OPT: 'Email (Optional)',
  ENTER_VALID_EMAIL: 'Please enter a valid email address.',
  SUBJECT: 'Subject',
  ENTER_SUBJECT: 'Enter your subject here',
  ENTER_SUBJECT_MSG: 'Please enter subject',
  TELL_ISSUE: 'Tell us your issue',
  PROVIDE_DETAIL: 'Please provide as much detail as possible',
  WHAT_YOU_LIKE: 'What would you like to see',
  TELL_ABOUT_IT: 'Tell us about it',
  HOW_DO_REACH: 'How do we reach you?',
  YOUR_ISSUE_BELOW: 'Describe your issue below',
  ENTER_ISSUE_MSG: 'Please enter your issue.',
  ENTER_ISSUE: 'Enter Describe your issue here',

  // Collect Data screen
  SENDING_LOGS_UPPERCASE: 'SENDING LOGS',
  LOGS_AND_ERROR_REPORTING: 'Logs and Error Reporting',
  DATA_COLLECTION_MSG: 'You may opt in to send logs and error reports to help the Internet Money team in debugging found issues. NO PERSONAL OR IDENTIFYING DATA IS COLLECTED.',
  DATA_COLLECTION_LIST: [
    { title: 'Do not send logs', key: 'disable' },
    { title: 'Send logs', key: 'enable' },
  ],

  // Wallet connect screen
  ENTER_WALLET_URL: 'Enter WalletConnect url',
  REJECT: 'Reject',
  DISCONNECT: 'Disconnect',
  CONNECT: 'Connect',
  VIEW: 'View',
  WALLET_CONNECT_SUCCESS_MSG: 'Wallet connected successfully.',

  // Wallet Connect Modals
  'WC_SESSION_REQUEST_TITLE': 'New Connection Request',
  'WC_SESSION_REQUEST_SELECT_ACCOUNT': 'Select Account to Connect',
  'WC_SESSION_REQUEST_ACCEPT': 'Connect',
  'WC_SESSION_REQUEST_REJECT': 'Reject',
  'WC_SESSION_REQUEST_ERROR': 'An error occurred while connecting with Wallet Connect. Please reject the request and try again.',
  'WC_CHAIN_REQUEST_TITLE': 'Chain Switch Request',
  'WC_CHAIN_REQUEST_ACCEPT': 'Switch',
  'WC_CHAIN_REQUEST_REJECT': 'Reject',
  'WC_CHAIN_REQUEST_DISMISS': 'Dismiss',
  'WC_CHAIN_REQUEST_CHAIN_ID': 'Chain ID',
  'WC_CHAIN_REQUEST_UNABLE_TO_PARSE': 'Unable to parse the requested chain.',
  'WC_CHAIN_REQUEST_UNSUPPORTED_CHAIN': 'Requested chain is not currently enabled in your wallet.',
  'WC_TRANSACTION_REQUEST_TITLE': 'New Transaction Request',
  'WC_TRANSACTION_REQUEST_VIEW': 'View',
  'WC_TRANSACTION_REQUEST_REJECT': 'Reject',

  // sign transaction confirmation screen
  SIGN_REQUEST_UPPERCASE: 'Sign Request',
  CONFIRM_SIGNATURE_UPPERCASE: 'Confirm Signature',
  ORIGIN: 'Origin',
  YOU_ARE_SIGNING: 'You are signing:',
  MESSAGE: 'Message:',
  CANCEL: 'Cancel',
  SIGN: 'Sign',

  // Send Transaction confirmation screen
  DAPP_TRANSACTION_UPPERCASE: 'DAPP TRANSACTION',
  CLICK_TO_SEND_DAPP: 'Click to send dapp',

  // Statistics
  STATISTICS_TITLE: 'Internet Money (IM) Stats',
  CONTRACT_ADDRESS: 'Contract Address',
  CURRENT_PRICE: 'Current Price',
  CURRENT_HOLDERS: 'Current Holders',
  TOTAL_LIQUIDITY: 'Total Liquidity',
  HOURS_PRICE: '$ {0}\n24 hour price change {1}%',
  INTERNET_MONEY_DAY: 'Internet Money Day \n(days since launch)',
  COUNT_UP_HERE: '{0}\n11/30/2021 UTC',

  BSC: 'Binance Smart Chain (BSC)',
  PULSE_CHAIN: 'PulseChain',
  ETHEREUM: 'Ethereum',
  COMING_SOON: 'Coming Soon',

  // Videos
  LEARN_MORE_BUTTON: 'Learn More',
  RELATED_VIDEOS: 'Related Videos',
  RELATED_ARTICLES: 'Related Articles',
  CHOOSE_CATEGORY_UPPERCASE: 'CHOOSE CATEGORY',

  // Tab bar strings
  NETWORK_DASHBOARD_TAB: 'Network',
  CLAIM_DASHBOARD_TAB: 'Dividends',
  WALLET_DASHBOARD_TAB: 'Wallet',
  SETTING_DASHBOARD_TAB: 'Settings',
  EXPAND_DASHBOARD_TAB: 'Expand',
  SWAP_DASHBOARD_TAB: 'Swap',

  // Terms of Service
  TERMS_OF_SERVICE: 'Terms of Service',

  // Common strings
  OK: 'Ok',
  SUCCESS: 'Success',
  ERROR: 'Error',
  YES: 'Yes',
  NO: 'No',
  FINISHED: 'Finished',

  COPIED_SUCCESS: 'Copied successfully',
  ADDRESS_COPIED_SUCCESS: 'Account address copied successfully',
  REFERRAL_CODE_COPIED_SUCCESS: 'Referral code copied successfully',
  SEED_PHRASE_COPIED_SUCCESS: 'Seed phrase copied successfully',
  PRIVATE_KEY_COPIED_SUCCESS: 'Private key copied successfully',
  TRANSACTION_HASH_COPIED_SUCCESS: 'Transaction hash copied successfully',
  FIRST_CLAIM_FREE_PLS_MSG: 'You have to claim free BNB to refer friend.',
  CONTACT_ADDRESS_COPIED_SUCCESS: 'Contact address copied successfully',

  GO_BACK_BUTTON_UPPERCASE: 'GO BACK',

  UNEXPECTED_ERROR: 'We encountered an unexpected error. Please close the wallet and then reopen it. If the problem persists, contact our support team for assistance.',
  SHOW_ERROR: 'Show Error',
  HIDE_ERROR: 'Hide Error',

  // Error & Alert Messages
  ENTER_PASS_MSG:
    'You can not have an empty password, please enter a password.',
  ENTER_VALID_PASS_MSG: 'The password must have a minimum of 10 characters',
  ENTER_CONF_PASS_MSG: 'Please confirm your password.',
  MATCH_PASS_MSG:
    'The password and confirm password fields are not matching, please ensure the entries match in order to continue.',
  PASS_NOT_MATCH_MSG:
    'Your password is invalid. Please enter the correct password.',
  PASSCODE_NOT_MATCH_WITH_PREVIOUS_MSG:
    'Your passcode is invalid. Please enter the correct passcode',
  LOGIN_VERIFY_PASSWORD_NOT_MATCH_MSG:
    'Your password is invalid. Please enter the correct password.',
  LOGIN_VERIFY_PASSCODE_NOT_MATCH_MSG:
    'Your passcode is invalid. Please enter the correct passcode.',
  ENTER_VALID_NEW_PASS_MSG:
    'The new password must be at least 10 characters long.',
  MATCH_NEW_PASS_MSG:
    'The new password and confirm password fields are not matching, please ensure the entries match in order to continue.',
  ENTER_VALID_AMOUNT_MSG: 'Please enter a valid amount.',
  INSUFFICIENT_FUNDS: 'Not enough {0} to pay for the transaction.',

  // Hardware
  HARDWARE_WALLET_TRANSACTION: 'Hardware Wallet Transaction',
  APPROVE_OR_REJECT: 'Please Approve the transaction from your Hardware Wallet',
  USER_REJECTED:
    'You have rejected the transaction on your hardware device.',
  USER_APPROVED:
    'The User Approved the transaction request, Here are the transaction details.',
  TRY_AGAIN: 'Try again',
  CONNECT_HARDWARE_WALLET: 'Connect Hardware Wallet',
  IMPORT_CONNECTED_HARDWARE_WALLET: 'Import Hardware Wallet',
  SELECT_CONNECTED_HARDWARE_WALLET: 'Select Hardware Wallet',
  SELECT_AN_ACCOUNT: 'Select an Account',
  CONNECT_ACCOUNTS: 'Connect Accounts',
  CONNECT_HARDWARE_WALLET_NOTES:
    'If you need help with your hardware wallet, click here to get help setting it up.',
  LEDGER: 'LEDGER',
  TREZOR: 'Trezor',
  DERIVATION_PATH_TITLE: 'Account Derivation Path',
  DERIVATION_PATH_LIST: [
    { title: `BIP44`, key: 'bip44' },
    { title: `Ledger Live`, key: 'ledgerLive' },
    { title: `Legacy`, key: 'legacy' },
  ],
  ADDRESS: 'Address',
  DECLINED: 'Transaction Cancelled',
  OKAY: 'Okay',
  PREVIOUS: '<< PREV',
  NEXT: 'NEXT >>',

  // Hardware Error Handling
  LEDGER_ERROR_06D00: 'Ledger device: INS_NOT_SUPPORTED (0x6d00)',
  LEDGER_ERROR_06B0C: 'Ledger device: UNKNOWN_ERROR (0x6b0c)',
  LEDGER_ERROR_06A15: 'Ledger device: UNKNOWN_ERROR (0x6a15)',
  LEDGER_ERROR_06511: 'Ledger device: UNKNOWN_ERROR (0x6511)',
  PLEASE_OPEN_DESIRED_APP: 'Please open the desired app.',
  TRY_CONNECTING_DEVICE: 'Try connecting the device',
  DEVICE_CONNECTED_BUT_LOCKED: 'Device is connected but locked.',
  IN_WRONG_APP_OPEN_ETHEREUM:
    'Could be connecting to wrong app in Ledger, You need to be in the Ethereum app.',
  DEVICE_IS_ALREADY_OPEN: 'The device is already open.',
  USER_NOT_IN_APP_IN_HOME_SCREEN:
    'Please select the Ethereum app on your Ledger to continue.',
  HARDWARE_ALREADY_CONNECTED: 'Hardware Wallet already connnected',
  HARDWARE_PUBLIC_ADDRESS_NOT_MATCH:
    'Please connect the required hardware and retry your transaction.',
  EXPORT_TRANSACTIONS: 'EXPORT TRANSACTIONS',
  ENTER_VALID_ACCOUNT_ADDRESS: 'Enter the valid address',
  PLEASE_ENTER_TO_ADDRESS: 'Please enter to address',
  TRANSACTION_SUCCESS: 'Transaction successful!',
  SUBMIT_SUCCESS: 'Transaction submitted!',
  REPLACEMENT_TRANSACTION: 'A pending transaction already exists. Please wait for it to confirm, or consider resubmitting it with a higher gas fee to expedite processing.',
};
