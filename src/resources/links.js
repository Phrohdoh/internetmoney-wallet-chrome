export const Links = {
  PRIVACY_POLICY: "https://internetmoney.io/privacy-policy",
  TERMS_OF_SERVICE: "https://internetmoney.io/terms-of-service",
  WHAT_IS_TIME: 'https://internetmoney.io/time',
};
