export const displayAddress = value => {
  if (value) {
    const publicAddressPart1 = value.toString().substr(0, 8);
    const publicAddressPart2 = value
      .toString()
      .substr(value.length - 6, value.length);
    return publicAddressPart1 + '...' + publicAddressPart2;
  }
};
