export const displayDecimal = (value, minDecimal) => {
  let decimalSize = value.toString().split('.');

  if (decimalSize.length > 1) {
    decimalSize = Math.min(decimalSize[1].length, minDecimal);
  } else {
    decimalSize = 0;
  }

  const num = Number(value);

  return num.toFixed(Math.min(9, decimalSize));
};
