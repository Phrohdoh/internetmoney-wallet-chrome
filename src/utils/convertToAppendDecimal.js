import { appendDecimals } from 'web3-layer';
import { formatExponentialNumber } from 'utils/formatExponentialNumber';

export const convertToAppendDecimal = (value, decimals = 9) => {
  return Number(
    appendDecimals(formatExponentialNumber(value.toString()), decimals),
  );
};
