import { ExportToCsv } from 'export-to-csv';
import moment from 'moment';

export const exportTransactions= (transactions) => {

  if((!transactions) || (typeof transactions === 'object' && transactions.length === 0))
    return;

  transactions= JSON.stringify(parseTransactions(transactions));
      
  const options = {
    filename: 'intenet-money-wallet-transactions-' + Date.now(), 
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalSeparator: '.',
    useTextFile: false,
    useBom: true,
    useKeysAsHeaders: true,
  };
      
  const csvExporter = new ExportToCsv(options);
      
  csvExporter.generateCsv(transactions);
};

const getDateString= (date, format) => {
  try{
    return moment(date).utc().format(format);
  }
  catch(e){
    return '';
  }
};

const getTransactionUrl= (transaction) => {

  if(transaction.transactionDetail?.networkDetail?.explore && transaction.receipt?.transactionHash){
    return transaction.transactionDetail?.networkDetail?.explore + 'tx/' + transaction.receipt?.transactionHash;
  }
  else{
    return '';
  }
};


const parseTransactions= (transactions) => {

  const flatTransactions=[];

  transactions.forEach(transaction => {

    const transactionObject = {
      'Memo': '',
      'Date (UTC)': transaction.transactionDetail?.utcTime ? getDateString(transaction.transactionDetail?.utcTime, 'MM-DD-YYYY') : '',
      'Time (UTC)': transaction.transactionDetail?.utcTime ? getDateString(transaction.transactionDetail?.utcTime, 'hh:mm a') : '',
      'Type': transaction.type ? transaction.type[0].toUpperCase() + transaction.type.slice(1) : '',
      'Amount (Tokens)': transaction.transactionDetail?.displayAmount.toString(6) ?? '',
      'Asset Symbol': transaction.transactionDetail?.fromToken?.symbol ? transaction.transactionDetail.fromToken.symbol : '',
      'Token Swapped Into': transaction.transactionDetail?.toToken?.symbol ? transaction.transactionDetail.toToken.symbol : '', // TO TOKEN SYMBOL
      'USD Value': transaction.transactionDetail?.isSupportUSD
        ? `$${transaction.transactionDetail.displayAmount.multiply(transaction.transactionDetail.usdValue).toString(2)}`
        : '',
      'Wallet Fee': transaction.transactionDetail?.walletFee ? transaction.transactionDetail.walletFee.toString(6) : '',
      // 'Gas Fee': '',
      'From (Address)': transaction.transactionDetail?.fromAddress ? transaction.transactionDetail.fromAddress : '',
      'Account Name': transaction.transactionDetail?.accountName ? transaction.transactionDetail.accountName : '',
      'To (Address)': transaction.transactionDetail?.toAddress ? transaction.transactionDetail.toAddress : '',
      'Network (Blockchain)': transaction.transactionDetail?.networkDetail?.networkName ? transaction.transactionDetail.networkDetail.networkName : '',
      'Transaction Hash': transaction.receipt?.transactionHash ? transaction.receipt.transactionHash : '',
      'Transaction Url': getTransactionUrl(transaction),
      'Status': transaction.receipt?.status ? (transaction.receipt.status ? 'Success' : 'Failed') : '',
      
      // transferedAmount: transaction.transactionDetail?.transferedAmount ? transaction.transactionDetail.transferedAmount : '',
      // supportsUSD: transaction.transactionDetail?.isSupportUSD ? transaction.transactionDetail.isSupportUSD : '',
      // claimableRewards: transaction.transactionDetail?.claimableRewards ? transaction.transactionDetail.claimableRewards : '',
      // claimableDiv: transaction.transactionDetail?.claimableDiv ? transaction.transactionDetail.claimableDiv : '',
      // airdropAddress: transaction.transactionDetail?.airdropAddress ? transaction.transactionDetail.airdropAddress : '',
      // networkChainId: transaction.transactionDetail?.networkDetail?.chainId ? transaction.transactionDetail.networkDetail.chainId : '',
      // networkRpc: transaction.transactionDetail?.networkDetail?.rpc ? transaction.transactionDetail.networkDetail.rpc : '',
      // networkExplore: transaction.transactionDetail?.networkDetail?.explore ? transaction.transactionDetail.networkDetail.explore : '',
    };

    flatTransactions.push(transactionObject);
  });

  return flatTransactions;

};
