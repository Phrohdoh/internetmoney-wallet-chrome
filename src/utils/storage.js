export const setItem = (key, value) => {
  try {
    localStorage.setItem(key, value);
    return { error: false };
  } catch (error) {
    /* istanbul ignore next */
    return { error: true };
  }
};

export const getItem = (key) => {
  try {
    const value = localStorage.getItem(key);

    return { data: value, error: !value ? true : false };
  } catch (error) {
    /* istanbul ignore next */
    return { data: null, error: true };
  }
};

export const removeItem = (key) => {
  try {
    localStorage.removeItem(key);
    return { error: false };
  } catch (error) {
    /* istanbul ignore next */
    return { error: true };
  }
};

export const removeAll = () => {
  try {
    localStorage.clear();
    return { error: false };
  } catch (error) {
    /* istanbul ignore next */
    return { error: true };
  }
};

export const setStorageItem = (key, value) => {
  if (chrome.storage) {
    chrome.storage.local.set({ [key]: value });
  } else if (process.env.NODE_ENV === 'development') {
    setItem(key, value);
  }
}

export const getStorageItem = async (key) => {
  if (chrome.storage) {
    const item = await chrome.storage.local.get(key);
    return item[key];
  } else if (process.env.NODE_ENV === 'development') {
    return getItem(key)?.data;
  }
}

export const removeStorageItem = (key) => {
  if (chrome.storage) {
    chrome.storage.local.remove(key);
  } else if (process.env.NODE_ENV === 'development') {
    removeItem(key);
  }
}
