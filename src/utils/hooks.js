import { useCallback, useState } from 'react';
import { IMAGES } from 'themes';
import { Validation } from 'utils/validations';
import { getTokenLogo } from '../web3-layer/web3Layer';

export const useTokenIcons = () => {
  const [tokenIcons, setTokenIcons] = useState(IMAGES);
  const getTokenIcon = useCallback((tokenAddress, chainId) => {
    const imageList = tokenIcons[chainId] ?? {};
    if (Validation.isEmpty(tokenAddress)) {
      return imageList.NATIVE_ICON ?? IMAGES.TOKEN_PLACEHOLDER_ICON;
    }
    const icon = imageList[tokenAddress.toLowerCase()] ?? IMAGES.TOKEN_PLACEHOLDER_ICON;
    if (imageList[tokenAddress.toLowerCase()] === undefined) {
      getTokenLogo(chainId, tokenAddress).then(logo => {
        IMAGES[chainId] = IMAGES[chainId] ?? {};
        IMAGES[chainId][tokenAddress.toLowerCase()] = logo;
        setTokenIcons({...IMAGES});
      });
    }
    return icon;
  }, [tokenIcons]);
  return getTokenIcon;
};
