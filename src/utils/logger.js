import crypto from 'crypto';
import Logger from 'eleventh';

import { STORAGE_KEYS, BACKEND_SERVER } from '../constants';
import { getItem, setItem } from 'utils/storage';
import packageJson from '../../package.json';

Logger.setLogLevel('debug');

let uniqueIdentifier;

let logsEnabled = false;

export const getLogsEnabled = () => {
  return logsEnabled;
};

export const setLogsEnabled = async (enabled) => {
  logsEnabled = enabled;
  setItem(
    STORAGE_KEYS.LOGS_ENABLED,
    JSON.stringify(enabled),
  );
};

const loadLogsEnabled = async () => {
  const { data } = getItem(STORAGE_KEYS.LOGS_ENABLED);
  logsEnabled = data === 'true';
}

loadLogsEnabled();

const getIdentifier = () => {
  if (uniqueIdentifier !== undefined) {
    return uniqueIdentifier;
  }
  const { error, data } = getItem(
    STORAGE_KEYS.UNIQUE_IDENTIFIER,
  );
  if (!error && typeof data === 'string' && data.length === 32) {
    uniqueIdentifier = data;
    return uniqueIdentifier;
  }
  uniqueIdentifier = crypto.randomBytes(16).toString('hex');
  setItem(
    STORAGE_KEYS.UNIQUE_IDENTIFIER,
    uniqueIdentifier,
  );
  return uniqueIdentifier;
};

const logUrl = BACKEND_SERVER.BASE_URL + BACKEND_SERVER.POST_LOG;

Logger.subscribe(async (message) => {
  if (!logsEnabled) {
    return;
  }
  const body = {
    uniqueIdentifier: getIdentifier(),
    message,
  };
  await fetch(logUrl, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
  });
});

const addDefaults = logFunction => (message, values) => {
  logFunction(message, {
    platform: 'chrome',
    version: packageJson.version ?? 'undefined',
    // TODO: add selected network info?
    ...values,
  })
}

export default {
  debug: addDefaults(Logger.debug),
  info: addDefaults(Logger.info),
  warning: addDefaults(Logger.warning),
  error: addDefaults(Logger.error),
  fatal: addDefaults(Logger.fatal),
};
