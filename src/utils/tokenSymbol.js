
import { IMAGES } from 'themes';
import { Validation } from 'utils/validations';
import { getTokenLogo } from '../web3-layer/web3Layer';

export const getTokenSymbol = async (tokenAddress, chainId) => {
  try {
    const imageList = IMAGES[chainId];
    if (Validation.isEmpty(tokenAddress)) {
      return imageList.NATIVE_ICON;
    }
    if (imageList[tokenAddress]) {
      return imageList[tokenAddress];
    }
    return await getTokenLogo(chainId, tokenAddress);
  } catch (e) {
    return IMAGES.TOKEN_PLACEHOLDER_ICON;
  }
};
