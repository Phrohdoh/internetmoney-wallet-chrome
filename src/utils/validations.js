import { log } from './log';


const EMAIL_VALIDATION_KEYSET =
// eslint-disable-next-line
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})|((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}]))|((([a-zA-Z\-0-9]+\.){1,2})[a-zA-Z]{2,}))$/;

// const PASSWORD_VALIDATION_KEYSET =
//   /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{0,10}$/;

const URL_VALIDATION_KEYSET =
  /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/;
  
export const Validation = {
  // check empty , undefined or null string
  isEmpty: function (value = '') {
    value= value+'';
    try {
      if (value) {
        if (value.trim() === '') {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    } catch (e) {
      log('error to check empty string >>>>> ', e);
      return false;
    }
  },

  // Check Email is valide or not
  isValidEmail: function (value='') {
    value= value+'';

    return EMAIL_VALIDATION_KEYSET.test(value.trim());
  },

  // Check Password Validation
  isValidPassword: function (value='') {
    value= value+'';
    const password = value.trim();

    if (password.length < 10) {
      return false;
    }
    return true;
  },

  // Check Email is valide or not
  isValidURL: function (value) {
    const CHECK_URL = URL_VALIDATION_KEYSET;
    return CHECK_URL.test(value.trim());
  },
};
