export const formatExponentialNumber = expoNum => {
  if (Math.abs(expoNum) < 1.0) {
    const e = parseInt(expoNum.toString().split('e-')[1]);
    if (e) {
      expoNum *= Math.pow(10, e - 1);
      expoNum = '0.' + new Array(e).join('0') + expoNum.toString().substring(2);
    }
  } else {
    let e = parseInt(expoNum.toString().split('+')[1]);
    if (e > 20) {
      e -= 20;
      expoNum /= Math.pow(10, e);
      expoNum += new Array(e + 1).join('0');
    }
  }
  return expoNum;
};
