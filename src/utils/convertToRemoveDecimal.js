import { removeDecimals } from 'web3-layer';
import { formatExponentialNumber } from 'utils/formatExponentialNumber';

export const convertToRemoveDecimal = (value, decimals = 9) => {
  return Number(
    removeDecimals(formatExponentialNumber(value.toString()), decimals),
  );
};
