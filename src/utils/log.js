import appConfig from 'config';

export const log = (...data) => {
  /* istanbul ignore else */
  if (appConfig.environment === 'local' || appConfig.environment === 'development') {
    console.log(...data);
  }
};
