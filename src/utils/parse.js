import { log } from 'utils/log';
export const parseObject = data => {
  try {
    const parseData = JSON.parse(data);
    if (parseData) {
      return { success: true, data: parseData };
    } else {
      return { success: false };
    }
  } catch (e) {
    log('parsing error >>>>>', e);
    return { success: false };
  }
};
