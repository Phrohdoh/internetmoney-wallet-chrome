import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'resources';
import { useLocation, useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getTransactionDetails } from 'web3-layer';
import copy from 'copy-to-clipboard';
import toast from 'react-hot-toast';
import moment from 'moment';

//  Utils
import { useTokenIcons } from 'utils/hooks';
import { displayAddress } from 'utils/displayAddress';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import Spinner from 'components/Spinner';

// Images
import sendIcon from 'assets/images/send-icon.svg';
import receiveIcon from 'assets/images/receive-icon.svg';
import swapIcon from 'assets/images/swap-icon.svg';
// import claimIcon from 'assets/images/IMD.svg';
import claimIcon from 'assets/images/time3d.png';
import referralEarningsIcon from 'assets/images/referral-earnings.svg';
import dappIcon from 'assets/images/dapp-transections.svg';
import imIcon from 'assets/images/im-icon.svg';

// Images
import downArrow from 'assets/images/down-arrow.svg';

import './transactions-details.scss';
import IntegerUnits from 'utils/IntegerUnits';
import { useWalletConnect } from 'contexts/WalletConnectContext';
import { getNetwork } from 'redux/selectors';

const GWEI = new IntegerUnits(1000000000, 18);

const getTransactionStatus = (transactionObject) => {
  if (transactionObject === undefined) {
    return 'loading';
  }
  if (transactionObject.pending) {
    return 'pending';
  }
  if (!transactionObject.success) {
    return 'failed';
  }
  return 'success';
};

export default function TransactionsDetails () {

  const navigate = useNavigate();
  const params = useLocation();
  const item = params.state.item;
  const { transactionDetail, receipt } = item;
  const getTokenIcon = useTokenIcons();
  
  const selectedNetwork = useSelector(getNetwork);
  const { sessions } = useWalletConnect();

  let peerMeta;
  if (item.type === 'dapp') {
    const isV2 = typeof transactionDetail.dappTransactionDetail.topic === 'string';
    peerMeta = isV2
      ? sessions.find(s => s.topic === transactionDetail.dappTransactionDetail.topic)?.peer?.metadata
      : transactionDetail.dappTransactionDetail.connection.peerMeta;
    peerMeta = peerMeta ?? {};
  }

  const [transactionObject, setTransactionObject] = useState(undefined);

  const getTransactionObject = async () => {
    setTransactionObject(await getTransactionDetails(
      receipt.transactionHash,
      selectedNetwork,
    ));
  };

  useEffect(() => {
    getTransactionObject();
  }, []);

  const transactionStatus = getTransactionStatus(transactionObject);
  const transactionDetails = transactionObject?.txnDetails ?? {};
  const exploreUrl = transactionObject?.exploreUrl ?? transactionDetails?.txnExploreUrl;

  const statusText = {
    pending: Strings.PENDING_TRANSACTION_UPPERCASE,
    failed: Strings.FAILED_TRANSACTION_UPPERCASE,
    success: Strings.TRANSACTION_CONFIRMED,
  }[transactionStatus];

  const statusColor = {
    pending: 'text-pending-color',
    failed: 'text-failed-color',
    success: 'text-success-color',
  }[transactionStatus];

  const getTransImage = () => {
    if (item.type === 'send') {
      return sendIcon;
    } else if (item.type === 'swap') {
      return swapIcon;
    } else if (item.type === 'claim') {
      return claimIcon;
    } else if (item.type === 'dapp') {
      return dappIcon;
    } else if (item.type === 'referral_claim') {
      return referralEarningsIcon;
    } else {
      return receiveIcon;
    }
  };

  const getTransType = () => {
    if (item.type === 'send') {
      return Strings.SEND;
    } else if (item.type === 'swap') {
      return Strings.SWAP;
    } else if (item.type === 'claim') {
      return Strings.CLAIM_DIV;
    } else if (item.type === 'dapp') {
      return Strings.DAPP;
    } else if (item.type === 'referral_claim') {
      return Strings.REFERRAL_EARNINGS;
    } else {
      return Strings.RECEIVE;
    }
  };

  const copyToClipboard = (e) => {
    e.preventDefault();
    copy(exploreUrl);
    toast(Strings.TRANSACTION_HASH_COPIED_SUCCESS);
  };

  const getAmountTitle = () => {
    switch (item.type) {
    case 'claim':
      return Strings.CLAIMED_AMOUNT;
    case 'referral_claim':
      return Strings.RECEVING_REWARDS;
    default:
      return Strings.AMOUNT;
    }
  };

  const renderDetailsHeader = () => {
    return (
      <div className="details-header">
        <div className="left">
          <img src={ getTransImage() } alt={ item.type } />
        </div>
        <div className="body">
          <h2 className="text-theme-color mb-0">     
            {item.type === 'dapp' ? (             
              getTransType() +
                ' ' +
                peerMeta.name             
            ) : item.type !== 'claim' ? (             
              getTransType() + ' ' + item.transactionDetail.fromToken.symbol             
            ) : (
              getTransType()
            )
            }
          </h2>
          <small className="opacity">
            {transactionDetails.date
              ? moment(transactionDetails.date).format('MMMM DD, YYYY | hh:mm A')
              : ''}
          </small>
        </div>
      </div>  
    );
  };

  const headingStyle = () => {
    return (
      <div className="heading-styles">
        <span className="line" />
        <span className="line" />
        <h6 className={ `text-center uppercase ${statusColor}` }>
          {statusText}
        </h6>
        <span className="line" />
        <span className="line" />
      </div>
    );
  };

  const renderReferralView = () => {
    return (
      <div className="send-container">
        <div className="send-row">
          <div className="body">
            <small>{Strings.FROM_TEXT}</small>
            <h6 className="text-gray-color">{item.transactionDetail.accountName}</h6>
          </div>
          <div className="right text-right">
            <h6 className="uppercase">
              {item.transactionDetail.displayAmount.toString(6)} IM
            </h6>
            {item.transactionDetail.isSupportUSD && (
              <span>
                <small>{Strings.VALUE}:</small>
                ${item.transactionDetail.displayAmount.multiply(item.transactionDetail.usdValue).toString(2)}       
              </span>
            )}
          </div>
          <div className="icon">
            <img src={ imIcon } alt={ 'IM' } />
          </div>
        </div>
        <div className="send-row">
          <div className="body">
            <small>{Strings.TO_TEXT}</small>
            <h6 className="text-gray-color text-eclipse-style">              
              {item.transactionDetail.toAddress}              
            </h6>
          </div>
        </div>
      </div>
    );
  };

  const renderFromAccountView = () => {
    return (
      <div className="send-container">        
        <div className="send-row">
          <div className="body">
            <small>{Strings.FROM_TEXT}</small>
            <h6 className="text-gray-color">{item.transactionDetail.accountName}</h6>
          </div>
          <div className="right text-right">
            <h6 className="uppercase">
              {item.transactionDetail.displayAmount.toString(6)} {item.transactionDetail.fromToken.symbol}
            </h6>
            {item.transactionDetail.isSupportUSD && (
              <span className="flex-row">
                <small>{Strings.VALUE}: </small>
                <span className="left-sm-space">
                  ${item.transactionDetail.displayAmount.multiply(item.transactionDetail.usdValue).toString(2)}
                </span>
              </span>
            )}
          </div>
          <div className="icon">
            <img src={ getTokenIcon(item.transactionDetail.fromToken.address, selectedNetwork.chainId) } alt={ item.transactionDetail.fromToken.symbol } />
          </div>
        </div>
        <div className="send-row">
          <div className="body">
            <small>{Strings.TO_TEXT}</small>
            <h6 className="text-gray-color text-eclipse-style">
              { item.transactionDetail.toAccountName ? item.transactionDetail.toAccountName : displayAddress(item.transactionDetail.toAddress) }
            </h6>
          </div>
        </div>
      </div>
    );
  };

  const renderReceive = () => {
    return (
      <div className="send-container">
        <div className="send-row">
          <div className="body">
            <small>{Strings.FROM_TEXT}</small>
            <h6 className="text-gray-color text-eclipse-style">
              {item.transactionDetail.fromAddress}
            </h6>
          </div>
        </div>
        <div className="send-row">
          <div className="body">
            <small>{Strings.TO_TEXT}</small>
            <h6 className="text-gray-color text-eclipse-style">
              {item.transactionDetail.toAddress}               
            </h6>
          </div>
        </div>
      </div>
    );
  };

  const renderDappFrom = () => {
    return (
      <div className="send-row">
        <div className="body">
          <small>{Strings.FROM_TEXT}</small>
          <h6 className="text-gray-color text-eclipse-style">            
            {item.transactionDetail.fromAddress}           
          </h6>
        </div>
      </div>
    );
  };

  const renderDappTo = () => {
    return (
      <div className="send-row">
        <div className="body">
          <small>{Strings.TO_TEXT}</small>       
          <h6 className="text-gray-color text-eclipse-style">
            {item.transactionDetail.toAddress}            
          </h6>
        </div>
      </div>
    );
  };

  const renderFromTokenView = () => {
    return (
      <div className="send-container">
        <div className="send-row">
          <div className="body">
            <small>{Strings.FROM_TEXT}</small>
            <h6 className="text-gray-color">{item.transactionDetail.fromToken.symbol}</h6>
          </div>
          <div className="right text-right">
            <h6 className="uppercase">
              {item.transactionDetail.displayAmount.toString(6)} {item.transactionDetail.fromToken.symbol}
            </h6>
            {item.transactionDetail.isSupportUSD && (
              <span>
                <small>{Strings.VALUE}:</small>   
                ${item.transactionDetail.displayAmount.multiply(item.transactionDetail.usdValue).toString(2)}
              </span>
            )}
          </div>
          <div className="icon">
            <img src={ getTokenIcon(item.transactionDetail.fromToken.address, selectedNetwork.chainId) } alt={ item.transactionDetail.fromToken.symbol } />
          </div>
        </div>
        <div className="send-row">
          <div className="body">
            <small>{Strings.TO_TEXT}</small>
            <h6 className="text-gray-color">{item.transactionDetail.toToken.symbol}</h6>
          </div>
          <div className="right text-right">
            <h6 className="uppercase">
              {`${item.transactionDetail?.quoteObj?.amountOut?.toString(6)} ${item.transactionDetail.toToken.symbol}`}
            </h6>
          </div>
          <div className="icon">
            <img src={ getTokenIcon(item.transactionDetail.toToken.address, selectedNetwork.chainId) } alt={ item.transactionDetail.toToken.symbol } />
          </div>
        </div>
      </div>
    );
  };
  
  const sendTokenAmount = () => {
    return (
      <div className="send-amount">
        <small>{getAmountTitle()}</small>
        <h2 className="uppercase">
          {item.transactionDetail.displayAmount.toString(6)}
          {' '}
          {item.type === 'referral_claim' ? 'IM' : item.transactionDetail.fromToken.symbol}
          <img src={ item.type === 'referral_claim' ? imIcon : getTokenIcon(item.transactionDetail.fromToken.address, selectedNetwork.chainId) } alt={ item.transactionDetail.fromToken.symbol } />
        </h2>
        {item.transactionDetail.isSupportUSD && ( 
          <span><small>{Strings.VALUE}: </small>
            ${item.transactionDetail.displayAmount.multiply(item.transactionDetail.usdValue).toString(2)}
          </span>
        )}
      </div>
    );
  };

  const renderFeeDetailView = (label, value) => {
    return (
      <div  className="trans-listitem">
        <div className="body">
          {label}
        </div>
        <div className="right text-right">
          <strong>{value}</strong>
        </div>
      </div>
    );
  };

  const renderTotalFeeDetailView = (total, value) => {
    return (
      <div className="trans-listitem">
        <div className="body">
          {total}
        </div>
        <div className="right text-right">
          <strong>{value}</strong>
        </div>
      </div>
    );
  };
  
  const dappDetailHeader = () => {
    return (
      item.type === 'dapp' && (
        <div className="dapp-detail-header">
          <h3>{Strings.DAPP}</h3>
          <p className="text-theme-color">{peerMeta.name}</p>
        </div>
      )
    );
  };

  const renderLoading = () => (
    <Spinner />
  );

  const renderPending = () => (
    <div className="wrapper trans-details">
      {headingStyle()}
      {dappDetailHeader()}
      <div className="expedite-notice text-center text-theme-color">
        {Strings.EXPEDITE_TRANSACTION_NOTICE}
      </div>
      <div className="send-details">
        <div className="from-to-pattern">
          <img src={ downArrow } alt="" className="from-to-arrow" />
        </div>
        {item.type === 'send' && (
          renderFromAccountView()
        )}
        {item.type === 'receive' && (
          renderReceive()
        )}
        {item.type === 'swap' && (
          renderFromTokenView()
        )}
        {item.type === 'dapp' && (
          <div className="send-container">
            {renderDappFrom()}
            {renderDappTo()}
          </div>
        )}
        {item.type === 'claim' && (
          renderFromAccountView()
        )}
        {item.type === 'referral_claim' && (
          renderReferralView()
        )}
      </div>
      { sendTokenAmount() }
    </div>
  );

  const renderFailed = () => (
    <div className="text-center pending-container">
      <h3 className="text-theme-color mb-0">{Strings.PENDING_TRANSACTION_UPPERCASE}</h3>
    </div>
  );

  const renderTransaction = () => (
    <div className="wrapper trans-details">   
      {headingStyle()}
      {dappDetailHeader()}
      <div className="send-details">
        <div className="from-to-pattern">
          <img src={ downArrow } alt="" className="from-to-arrow" />
        </div>
        {item.type === 'send' && (           
          renderFromAccountView()
        )}
        {item.type === 'receive' && (                
          renderReceive()               
        )}
        {item.type === 'swap' && (           
          renderFromTokenView()
        )}
        {item.type === 'dapp' && (
          <div className="send-container">
            {renderDappFrom()}
            {renderDappTo()}
          </div>
        )}
        {item.type === 'claim' && (             
          renderFromAccountView()             
        )}
        {item.type === 'referral_claim' && (
          renderReferralView()
        )}
      </div>
      { sendTokenAmount() }
      {renderFeeDetailView(Strings.GAS_LIMIT_FEE, transactionDetails.gasLimit?.value?.toString())}
      {renderFeeDetailView(Strings.GAS_USE_FEE, transactionDetails.gasUsed?.value?.toString())}
      {renderFeeDetailView(transactionDetails.type !== 0 ? Strings.BASE_FEE : Strings.GAS_PRICE, transactionDetails.baseFee?.divide(GWEI)?.toString(6) + ' ' + Strings.GWEI)}
      {transactionDetails.type !== 0 ? (
        <React.Fragment>
          {renderFeeDetailView(Strings.PRIO_FEE, transactionDetails.priorityFee?.divide(GWEI)?.toString(6) + ' ' + Strings.GWEI)}
          {renderFeeDetailView(Strings.TOTAL_FEE, transactionDetails.totalGasFee?.multiply(transactionDetails.gasLimit)?.toString(6) + ' ' + selectedNetwork.sym)}
          {renderFeeDetailView(Strings.MAX_FEE_PER_GAS, transactionDetails.maxFeePerGas?.divide(GWEI)?.toString(6) + ' ' + Strings.GWEI)}                  
        </React.Fragment>
      ) : null
      }
      {item.type === 'swap' &&
          renderFeeDetailView(
            Strings.WALLET_FEE,
            (item.transactionDetail.walletFee?.toString(6) ?? '0') +
            ' ' +
            selectedNetwork.sym,
          )}
      {renderFeeDetailView(Strings.NONCE, transactionDetails.nonce?.toString())}
       
      {renderTotalFeeDetailView(
        Strings.TOTAL,
        (
          transactionDetails.type !== 0
            ? transactionDetails.totalGasFee?.multiply(transactionDetails.gasUsed)
            : transactionDetails.baseFee?.multiply(transactionDetails.gasUsed)
        )?.toString(6) +
          ' ' +
          selectedNetwork.sym,
      )}          
    </div>
  );

  return (
    <div className="container">
      <Header
        support
        onClickBack={() => {
          navigate('/wallet', {
            state:{
              tab: 'tab2',
            }
          });
        }}
      />
      {renderDetailsHeader()}
      <div className="content-background ds-flex-col full-flex">
        {transactionStatus === 'loading' && renderLoading()}
        {transactionStatus === 'pending' && renderPending()}
        {transactionStatus === 'failed' && renderFailed()}
        {transactionStatus === 'success' && renderTransaction()}
        {transactionStatus !== 'loading' && (
          <div className="footer-bottom footer-bottom-small">
            <Button
              label={ Strings.VIEW_BLOCK_EXPLORER_UPPERCASE }
              outline
              target="_blank" 
              onClick={ (e) => {
                e.preventDefault();
                try {
                  window.open(exploreUrl);
                } catch (e) {
                  console.log('e >>>', e);
                }
              } }
            />
            <Button
              label={ Strings.COPY_TRANS_ID_UPPERCASE }
              outline
              onClick={ (e) => copyToClipboard(e) }
            />
          </div>
        )}
      </div>
    </div>
  );
}

TransactionsDetails.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  subtitle: PropTypes.string,
  title: PropTypes.string,
  children: PropTypes.object,
  onClick: PropTypes.func,
  icon: PropTypes.string,
  tokensList: PropTypes.array,
  testId: PropTypes.string,
};
