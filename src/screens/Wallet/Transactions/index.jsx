import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'resources';

// Components
import Modal from 'components/Modal';
import TransactionsList from 'components/TransactionsList';
import TransactionsFilter from 'components/TransactionsFilter';
import { exportTransactions } from 'utils/transactions';

// Images
import downArrow from 'assets/images/down-arrow-black.svg';

import './transactions.scss';

export default function Transactions (props) {
  const [isShowFilter, setIsShowFilter] = useState(false);
  const [filterTypeArray, setFilterTypeArray] = useState([]);
  const [filterAccount, setFilterAccount] = useState(undefined);
  const [filterAccountName, setFilterAccountName] = useState(undefined);
  const [filteredList, setFilteredList] = useState([]);

  useEffect(() => {
    getFilteredList();
  }, [filterTypeArray, filterAccount]);

  const getFilteredList = () => {
    let tempFilteredList = [];
    if (filterTypeArray.length > 0 && filterAccount) {
      tempFilteredList = props.transactionsList.filter(
        item =>
          item.transactionDetail.fromAddress === filterAccount &&
          filterTypeArray.includes(item.type),
      );
    } else if (filterTypeArray.length > 0) {
      tempFilteredList = props.transactionsList.filter(
        item => filterTypeArray.includes(item.type),
      );
    } else if (filterAccount) {
      tempFilteredList = props.transactionsList.filter(
        item => item.transactionDetail.fromAddress === filterAccount,
      );
    } else {
      tempFilteredList = props.transactionsList;
    }

    // const filteredListReverse = tempFilteredList.slice().reverse();
    const sortArray = tempFilteredList
      .slice()
      .reverse()
      .sort((a, b) => parseFloat(b.blockNumber) - parseFloat(a.blockNumber));
    setFilteredList(sortArray);

  };

  return (
    <React.Fragment>
      <div className="wrapper pb-0">
        <div className="filter-header">
          <span className="filter-btn noselect" onClick={ () => setIsShowFilter(true) }>
            {Strings.FILTER_UPPERCASE}
            <img src={ downArrow } alt={ Strings.FILTER_UPPERCASE } />
          </span>

          <span className="filter-btn noselect" onClick={ () => exportTransactions(filteredList) }>
            {Strings.EXPORT_TRANSACTIONS}
          </span>
        </div>
        <TransactionsList transactionsList={ filteredList } />
        {isShowFilter ?
          <Modal
            isOpen={ isShowFilter }
            closeModal={ () => setIsShowFilter(false) }
          >
            <div className="modal-body">
              <TransactionsFilter 
                transactionsList={ props.transactionsList }
                selectedFilterType={ filterTypeArray }
                selectedFilterAccount={ filterAccount }
                accountName={ filterAccountName }
                onSelectFilter={ (type, account, accountName) => {
                  setFilterTypeArray(type);
                  setFilterAccount(account);
                  setFilterAccountName(accountName);
                  setIsShowFilter(false);
                } }
              />
            </div>
          </Modal>
          : null }           
      </div>
    </React.Fragment>
  );
}

Transactions.propTypes = {
  transactionsList: PropTypes.array,
  isModalVisible: PropTypes.bool,
  title: PropTypes.string,
  children: PropTypes.object
};
