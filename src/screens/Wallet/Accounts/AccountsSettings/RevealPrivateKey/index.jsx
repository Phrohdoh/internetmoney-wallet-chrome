import React, { useState, useEffect } from 'react';
import { Strings } from 'resources';
import { useLocation, useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { retrievePrivateKeyOf } from 'web3-layer';
import copy from 'copy-to-clipboard';
import toast from 'react-hot-toast';

// Components
import Header from 'components/Header';
import Button from 'components/Button';

import './reveal-private-key.scss';

const HIDDEN_PRIVATE_KEY_PLACEHOLDER = '****************************************';

export default function RevealPrivateKey () {

  const navigate = useNavigate();
  const params = useLocation();  
  const wallet = useSelector(state => state.wallet);

  const [privateKey, setPrivateKey] = useState('');
  const [isHidden, setIsHidden] = useState(true);
  
  useEffect(() => {
    getAccountPrivateKey();
  }, []);

  const getAccountPrivateKey = async () => {
    const privateKeyObj = await retrievePrivateKeyOf(
      params.state.accountDetail.publicAddress,
      wallet.walletDetail.walletObject,
      params.state.password,
    );

    /* istanbul ignore else */
    if (privateKeyObj.success) {
      setPrivateKey(privateKeyObj.privateKey);
    }
  };

  const copyToClipboard = (e) => {
    e.preventDefault();
    copy(privateKey);
    toast(Strings.PRIVATE_KEY_COPIED_SUCCESS);
  };
  
  return (
    <React.Fragment>
      <div className="container">
        <Header 
          onClickBack={ () => {
            navigate(-1);
          } }
        />
        <div className="content-background ds-flex-col full-flex">
          <div className="wrapper">
            <div className="full-flex">
              <h2 className="text-theme-color">
                {Strings.formatString(Strings.ACCOUNT_PRIVATE_KEY_UPPERCASE, [params.state.accountDetail.name])}
              </h2>
              <p className="opacity scam-alert-msg">{Strings.PRIVATE_KEY_SCAM_MSG_UPPERCASE}</p>             
              <div className="private-key-container">
                <span>{!isHidden ? privateKey : HIDDEN_PRIVATE_KEY_PLACEHOLDER}</span>
              </div>  
            
              
              <Button
                onClick={ (e) => copyToClipboard(e) }
                label={ Strings.COPY_CLIPBOARD_UPPERCASE }
                className="btn-full"
              />
             
              <br />
              
              <Button
                outline
                onClick={ (e) => {
                  e.preventDefault();
                  setIsHidden(!isHidden);}
                }
                label={ isHidden ? Strings.SHOW_KEY_UPPERCASE : Strings.HIDE_KEY_UPPERCASE }
                className="btn-full"
              />

              <br />

              <Button
                outline
                onClick={ (e) => {
                  e.preventDefault();
                  navigate(-3); // back to account details
                } }
                label={ Strings.GO_BACK_BUTTON_UPPERCASE }
                className="btn-full"
              />
            </div>          
          </div>
          
        </div>
      </div>
    </React.Fragment>
  );
}
