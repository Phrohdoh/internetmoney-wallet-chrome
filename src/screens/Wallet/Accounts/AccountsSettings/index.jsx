import React, { useState } from 'react';
import { Strings } from 'resources';
import { useLocation, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { STORAGE_KEYS } from '../../../../constants';
import { setItem } from 'utils/storage';

import { AccountAction } from 'redux/slices/accountSlice';

//  Utils
import { Validation } from 'utils/validations';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import InputField from 'components/InputField';
import Dropdown from 'components/Dropdown';

// Images

import './accounts-settings.scss';

export default function AccountsSettings () {
  const navigate = useNavigate();
  const params = useLocation();
  const dispatch = useDispatch();

  const initialValues = {
    accountName: params.state.accountDetail.name,
    accountDetail: params.state.accountDetail,
  };

  const accounts = useSelector((state) => state.account);

  const [errorAccountName, setErrorAccountName] = useState('');
  const [values, setValues] = useState(initialValues);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  // check all validation
  const checkValidation = () => {
    const isEmptyAccountName = Validation.isEmpty(values.accountName);
    const isNameExist = accounts.accountList.find(
      (accObj) => accObj.name === values.accountName.trim()
    );

    const isSameName =
      params.state.accountDetail.name === values.accountName.trim();

    if (isSameName && !isEmptyAccountName) {
      navigate(-1);
      return false;
    } else if (!isEmptyAccountName && !isNameExist) {
      return true;
    } else {
      /* istanbul ignore else */
      if (isEmptyAccountName) {
        setErrorAccountName(Strings.ENTER_VALID_ACCOUNT_NAME_MSG);
      } else if (isNameExist) {
        setErrorAccountName(Strings.ACCOUNT_NAME_EXIST_MSG);
      }

      return false;
    }
  };

  const _onPressContinue = async (e) => {
    e.preventDefault();
    if (checkValidation()) {
      const accountList = [...accounts.accountList];
      const index = accountList.findIndex(
        (x) =>
          x.publicAddress.toLowerCase() ===
          params.state.accountDetail.publicAddress.toLowerCase()
      );

      const tempAccObj = { ...accountList[index] };
      tempAccObj.name = values.accountName.trim();
      accountList[index] = tempAccObj;

      dispatch(AccountAction.updateAccountObject(accountList));
      setItem(STORAGE_KEYS.ACCOUNT_LIST, JSON.stringify(accountList));
      navigate('/wallet');
    }
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header headerTitle={ Strings.ACCOUNT_SETTING_UPPERCASE } support />
        <div className="content-background ds-flex-col full-flex">
          <div className="wrapper">
            <div className="full-flex">
              <p className="opacity">{Strings.RENAME_ACCOUNT_MSG}</p>
              <InputField
                label={ Strings.RENAME_ACCOUNT }
                value={ values.accountName }
                onChange={ handleInputChange }
                error={ errorAccountName }
                name="accountName"
                dataTestid="account-name"
              />
              {values?.accountDetail?.isHardware === false && (
                <Dropdown
                  dataTestid="send-token-choose-account-button"
                  value={ Strings.REVEAL_PRIVATE_KEY_UPPERCASE }
                  onClick={ (e) => {
                    e.preventDefault();
                    navigate('/reveal-private-key-verification', {
                      state: {
                        accountName: params.state.accountName,
                        password: params.state.password,
                        accountDetail: params.state.accountDetail,
                      },
                    });
                  } }
                  className="text-color-theme round-button"
                />
              )}
            </div>
            <Button
              onClick={ (e) => _onPressContinue(e) }
              label={ Strings.SAVE_CHANGES_UPPERCASE }
              className="btn-full"
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
