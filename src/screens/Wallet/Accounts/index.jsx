import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Strings } from 'resources';

// Components
import Modal from 'components/Modal';
import AccountListCell from 'components/AccountListCell';
import AddAccountsView from 'components/AddAccountsView';

import './accounts.scss';
import { getTotalValue, getAccountList } from 'redux/selectors';

export default function Account (props) {
  const accountList = useSelector(getAccountList);
  const [isShowAddAccount, setIsShowAddAccount] = useState(false);
  const [toggleBalance, setToggleBalance] = useState(true);
  const [removingAccount, setRemovingAccount] = useState(false);

  const totalValue = useSelector(getTotalValue);

  const renderAccountList = (accountList) => {
    if (accountList.length === 0) {
      return (
        <p className="no-accounts">{Strings.NO_ACCOUNTS}</p>
      )
    }
    return (
      <ul className="list-container is-add-button">
        {accountList.map((item, index) => (
          <AccountListCell
            key={ index + item + 'accList' }
            item={ item }
            index={ index }
            isShowAddress={ toggleBalance }
            isShowBalance={ toggleBalance }
            isRemoving={removingAccount}
            { ...props }
          />
        ))}
      </ul>
    );
  };

  return (
    <React.Fragment>
      <div className="wrapper pb-0">
        <div className="total-value">
          <p>{Strings.TOTAL_VALUE}</p>
          <h2 className="text-theme-color wrap-text">
            {toggleBalance ? `$${totalValue.toString()}` : '--'}            
          </h2>
        </div>
        <div className="filter-header">
          <span
            className="filter-btn noselect"
            data-testid="add-button"
            onClick={ () => setIsShowAddAccount(true) }
          >
            {Strings.ADD_ACCOUNT}
          </span>
          <span
            className="filter-btn noselect"
            data-testid="remove-button"
            onClick={ () => setRemovingAccount(!removingAccount) }
          >
            {
              removingAccount
                ? Strings.FINISHED_REMOVING_ACCOUNT
                : Strings.REMOVE_ACCOUNT
            }
          </span>
          <span
            className="filter-btn noselect"
            onClick={ () => setToggleBalance(!toggleBalance) }
          >
            {toggleBalance
              ? Strings.HIDE_BALANCE_UPPERCASE
              : Strings.SHOW_BALANCE_UPPERCASE}
          </span>
        </div>
        {renderAccountList(accountList)}

        {isShowAddAccount ? (
          <Modal
            isOpen={ isShowAddAccount }
            closeModal={ () => setIsShowAddAccount(false) }
          >
            <div className="modal-body">
              <AddAccountsView
                createAccountSuccess={ () => setIsShowAddAccount(false) }
                importAccountSuccess={ () => setIsShowAddAccount(false) }
                connectHardwareWalletSuccess={ () => setIsShowAddAccount(false) }
                restoreAccountSuccess={() => setIsShowAddAccount(false) }
              />
            </div>
          </Modal>
        ) : null}
      </div>
    </React.Fragment>
  );
}

Account.propTypes = {
  title: PropTypes.string,
  isModalVisible: PropTypes.bool,
  children: PropTypes.object,
  onClick: PropTypes.func,
  content: PropTypes.string,
  accountList: PropTypes.array,
};
