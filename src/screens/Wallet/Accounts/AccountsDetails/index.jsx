
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'resources';
import { useLocation, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { NetworksAction } from 'redux/slices/networksSlice';
import { getBalances, getNetwork, getPopularTokens, getTokensByAddress, getTotalValueByAddress, getUsdPrices } from 'redux/selectors';

import { displayAddress } from 'utils/displayAddress';
import { setItem, getItem } from 'utils/storage';
import { parseObject } from 'utils/parse';

// Components
import Header from 'components/Header';
import Modal from 'components/Modal';
import AddToken from 'components/AddToken';
import TokenListCell from 'components/TokenListCell';
import AlertModel from 'components/AlertModel';
import StyledSpinner from './StyledSpinner';

// Images
import chart from 'assets/images/chart.svg';
import calenderButtons from 'assets/images/calender-buttons.svg';
import sendIcon from 'assets/images/send-icon.svg';
import receiveIcon from 'assets/images/receive-icon.svg';
import swapIcon from 'assets/images/swap-icon.svg';
import refreshIcon from 'assets/images/refresh-icon.png';

import './details.scss';

import { STORAGE_KEYS } from '../../../../constants';

import copyIcon from 'assets/images/copy-yellow.svg';
import toast from 'react-hot-toast';
import copy from 'copy-to-clipboard';
import IntegerUnits from 'utils/IntegerUnits';

export default function AccountsDetails (props) {

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const params = useLocation();
  const data = params.state;
  const account = params.state.item;
  const accountAddress = account.publicAddress.toLowerCase();

  const networks = useSelector(state => state.networks);
  const selectedNetwork = useSelector(getNetwork);

  const [expand, setExpand] = useState(true);
  const [error, setError] = useState();
  const [isShowAddToken, setIsShowAddToken] = useState(false);
  const [isShowAddAlert, setIsShowAddAlert] = useState(false);
  const [isShowDeleteAlert, setIsShowDeleteAlert] = useState(false);
  const [selectedToken, setSelectedToken] = useState(undefined);
  const [isRemoveEnable, setIsRemoveEnable] = useState(false);
  const [accountPopularTokens, setAccountPopularTokens] = useState();

  const tokens = useSelector(getTokensByAddress)[data.item.publicAddress.toLowerCase()];
  const totalValue = useSelector(getTotalValueByAddress)[data.item.publicAddress.toLowerCase()];
  
  const popularTokens = useSelector(getPopularTokens)[accountAddress];
  const chainBalances = useSelector(getBalances)[selectedNetwork.chainId];
  const usdPrices = useSelector(getUsdPrices)[selectedNetwork.chainId];
  
  const displayAccountBalance = totalValue !== undefined;

  useEffect(() => {
    if (chainBalances && usdPrices) {
      const balances = chainBalances[accountAddress];
      const accountPopularTokens = popularTokens.map((popularToken) => {
        const address = popularToken.address.toLowerCase();
    
        const defaultValues = {
          fetching: false,
          value: new IntegerUnits(0),
          lastFetched: Date.now(),
        };
    
        const balance = balances[address]?.value
          ? {
              ...balances[address],
              value: new IntegerUnits(
                balances[address].value.value,
                balances[address].value.decimals
              ),
            }
          : defaultValues;
    
        const usdPrice = usdPrices[address]?.value
          ? {
              ...usdPrices[address],
              value: new IntegerUnits(
                usdPrices[address].value.value,
                usdPrices[address].value.decimals
              ),
            }
          : defaultValues;
    
        return {
          ...popularToken,
          balance,
          usdPrice,
        };
      });

      setAccountPopularTokens(accountPopularTokens);
    }
  }, [chainBalances, usdPrices]);

  useEffect(() => {
    if (isShowAddToken) setIsRemoveEnable(false);
  }, [isShowAddToken, setIsRemoveEnable]);

  const refreshAccountBalance = () => {
    dispatch(NetworksAction.refreshBalances());
  };

  const copyToClipboard = (copyInfo) => {
    console.log(copyInfo);
    copy(copyInfo);
    toast(Strings.ADDRESS_COPIED_SUCCESS);
  };

  // Add Token
  const addToken = async () => {
    const networkTokens = networks.tokens ? [...networks.tokens] : [];
    
    const accountTokens = networkTokens
      .find(({ id }) => id.toLowerCase() === account.publicAddress.toLowerCase());

    if (accountTokens) {
      const tokenIndex = networkTokens.indexOf(accountTokens);
      const tokenList = [...accountTokens.tokens];
      tokenList.push(selectedToken);
      const newAccountTokens = { ...accountTokens };
      newAccountTokens.tokens = tokenList;
      networkTokens[tokenIndex] = newAccountTokens;
    } else {
      const newAccountTokens = {
        id: account.publicAddress,
        tokens: [selectedToken]
      };
      networkTokens.push(newAccountTokens);
    }

    
    const { data } = getItem(STORAGE_KEYS.CHAIN_TOKENS);
    const parsedData = parseObject(data);
    
    const chainTokens = parsedData.success ? parsedData.data : [];
    const chain = chainTokens.find((chainToken) => chainToken.chainId == selectedNetwork.chainId);
    
    if (chain) {
      const chainIndex = chainTokens.indexOf(chain);
      chain.accountTokens = networkTokens;
      chainTokens[chainIndex] = chain;
    } else {
      chainTokens.push({
        chainId: selectedNetwork.chainId,
        accountTokens: networkTokens,
      });
    }

    dispatch(NetworksAction.saveTokens(networkTokens));
    setItem(STORAGE_KEYS.CHAIN_TOKENS, JSON.stringify(chainTokens))

    setIsShowAddAlert(false);
    setSelectedToken(undefined);
  };

  // Remove Token
  const removeToken = async () => {
    const networkTokens = networks.tokens ? [...networks.tokens] : [];

    const accountTokens = networks.tokens.find(
      token =>
        token.id.toLowerCase() === params.state.item.publicAddress.toLowerCase(),
    );

    if (accountTokens) {
      const tokenIndex = networkTokens.indexOf(accountTokens);
      const tokenList = [...accountTokens.tokens];
      const idx = tokenList.findIndex(
        x => x.address.toLowerCase() === selectedToken.address.toLowerCase(),
      );
      tokenList.splice(idx, 1);
      const newAccountTokens = { ...accountTokens };
      newAccountTokens.tokens = tokenList;
      networkTokens[tokenIndex] = newAccountTokens;
    }

    const { data } = getItem(STORAGE_KEYS.CHAIN_TOKENS);
    const parsedData = parseObject(data);

    const chainTokens = parsedData.success ? parsedData.data : [];
    const chain = chainTokens.find((chainToken) => chainToken.chainId == selectedNetwork.chainId);

    if (chain) {
      const chainIndex = chainTokens.indexOf(chain);
      chain.accountTokens = networkTokens;
      chainTokens[chainIndex] = chain;
    } else {
      chainTokens.push({
        chainId: selectedNetwork.chainId,
        accountTokens: networkTokens,
      });
    }

    dispatch(NetworksAction.saveTokens(networkTokens));
    setItem(STORAGE_KEYS.CHAIN_TOKENS, JSON.stringify(chainTokens));

    setIsShowDeleteAlert(false);
    setSelectedToken(undefined);
  };

  const renderDetailsHeader = () => {
    return (
      <div className="details-header">
        <div className="body">
          <h2 className="text-theme-color mb-0">
            {data.item.name}
          </h2>
          <div className="small-text">
            {displayAddress(data.item.publicAddress)}
            <a className="button noselect copy-button-details" onClick={ () => copyToClipboard(data.item.publicAddress) }>
              <img src={ copyIcon } />
            </a>
          </div>
        </div>
       
        <div>
     
          <div className="right text-right">
            { displayAccountBalance && (
              <div>
                
                <p onClick={() => refreshAccountBalance()} style={{ cursor: 'pointer' }} className="mb-0">
                  <img style={{ margRight: '20px' }} src={refreshIcon} width="20px" alt="refresh" />
                  {' '}
                  {Strings.VALUE}
                </p>
                
                <h2 className="headingTextStyle text-theme-color mb-0">
                  ${totalValue.toString()}
                </h2>
              </div>
            )}
            { !displayAccountBalance &&
              <StyledSpinner styles={{ height: '50px', width: '50px', marginRight: '25px', margin: 0 }} />
            }
          </div>

        </div>

      </div>
    );
  };

  // eslint-disable-next-line no-unused-vars
  const renderChartArea = () => {
    return (
      <div className="chart-container">
        <button className={ `char-header ${expand ? 'open' : 'close'}` } onClick={ () => setExpand(!expand) }>
          <h6>{expand ? Strings.HIDE_CHART : Strings.VIEW_CHART}</h6>
        </button>
        {expand && (
          <div className="chart-area">
            <img src={ chart } alt="Chart" />
            <img src={ calenderButtons } alt="calenderButtons" />
          </div>
        )}
      </div>
    );
  };

  const renderArrowCircleButton = (testId, onClick, title, icon) => {
    return (
      <div className="circle-buttons" data-testid={ testId } onClick={ onClick }>
        <div className={ 'arrow-circle-button' }>
          <img src={ icon } alt={ title } />
        </div>
        {title && <span>{title}</span>}
      </div>
    );
  };

  const renderArrowCircleButtons = () => {
    const onClickSendToken = () => {
      setError(false);
      navigate('/send-token',
        {
          state: {
            accountDetail: data.item,
            accountIndex: params.state.accountIndex,
          }
        });
    };

    const onClickReceiveToken = () => {
      navigate('/receive-token',
        {
          state: {
            accountDetail: data.item,
          }
        });
    };

    const onClickSwapToken = () => {
      setError(false);
      navigate('/swap-token',
        {
          state: {
            accountDetail: data.item,
          }
        });
    };

    return (
      <div className="arrow-circle-buttons">
        {renderArrowCircleButton('send-token', onClickSendToken, Strings.SEND, sendIcon)}
        {renderArrowCircleButton('receive-token', onClickReceiveToken, Strings.RECEIVE, receiveIcon)}
        {networks.wdSupportObject.success && networks.wdSupportObject.swapRouterV3 ?
          renderArrowCircleButton('swap-token', onClickSwapToken, Strings.SWAP, swapIcon) :
          null
        }
      </div>
    );
  };

  const renderTokensList = (tokensList) => {
    return (
      !!tokensList &&
      <div className="overflow-auto">
        <div className="list-style">
          {
            tokensList.map((item, index) => (
              <TokenListCell
                key={ index }
                index={ index }
                item={ item }
                networkObj={ selectedNetwork }
                accountAddress={ data.item }
                isRemoveEnable={ isRemoveEnable }
                onClickDelete={ (item) => {
                  setIsShowDeleteAlert(true);
                  setSelectedToken(item);
                } }

              />
            ))
          }
        </div>
      </div>
    );
  };

  const onClickEdit = (e) => {
    e.preventDefault();
    navigate('/accounts-settings',
      {
        state: {
          accountDetail: data.item,
        }
      });
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header
          networkHeader
          support
          onSettingClick={ onClickEdit }
          onClickBack={ () => {
            navigate('/wallet');
          } }
        />
        {renderDetailsHeader()}
        <div className="content-background ds-flex-col full-flex">
          <div className="padder">
            {/* {renderChartArea()} */}
            {error ? <span className="error">{Strings.ACCOUNT_SELECT_ERROR_MSG}</span> : null}
            {renderArrowCircleButtons()}
            <div className="footer-bottom space-around pb-0">
              <span data-testid="add-token-btn" onClick={ (_e) => setIsShowAddToken(true) } className="footer-button">
                {Strings.ADD_TOKEN_BUTTON_UPPERCASE}
              </span>
              <span data-testid="remove-token-btn" onClick={ (_e) => setIsRemoveEnable(!isRemoveEnable) } className="footer-button">
                {isRemoveEnable ? Strings.FINISHED : Strings.REMOVE_TOKEN}
              </span>
            </div>
          </div>
          {tokens && tokens.length > 0 ? renderTokensList(tokens) : null}
        </div>
        {isShowAddToken ?
          <Modal
            isOpen={ isShowAddToken }
            closeModal={ () => setIsShowAddToken(false) }
          >
            <AddToken
              onImportTokenSuccess={ () => setIsShowAddToken(false) } 
              setIsShowAddAlert={setIsShowAddAlert}
              setIsShowAddToken={setIsShowAddToken}
              setSelectedToken={setSelectedToken}
              popularTokens={accountPopularTokens}
            />
          </Modal> : null
        }
        {isShowAddAlert &&
          <AlertModel
            alertTitle={ Strings.ADD_TOKEN }
            isShowAlert={ isShowAddAlert }
            onPressSuccess={ () => addToken() }
            successBtnTitle={ Strings.YES }
            onPressCancel={ () => {
              setIsShowAddAlert(false);
              setSelectedToken(undefined);
            } }
            className="text-center"
          >
            <p>{selectedToken && Strings.formatString(Strings.ADD_TOKEN_MSG, [selectedToken.symbol])}</p>
          </AlertModel>
        }
        {isShowDeleteAlert && (
          <AlertModel
            alertTitle={ Strings.REMOVE_TOKEN }
            isShowAlert={ isShowDeleteAlert }
            onPressSuccess={ () => removeToken() }
            successBtnTitle={ Strings.YES }
            onPressCancel={ () => {
              setIsShowDeleteAlert(false);
              setSelectedToken(undefined);
            } }
            className="text-center"
          >
            <p>{Strings.formatString(Strings.REMOVE_TOKEN_MSG, selectedToken && [selectedToken.symbol])}</p>
          </AlertModel>
        )}
      </div>
    </React.Fragment>
  );
}

AccountsDetails.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  subtitle: PropTypes.string,
  title: PropTypes.string,
  isModalVisible: PropTypes.bool,
  children: PropTypes.object,
  onClick: PropTypes.func,
  icon: PropTypes.string,
  tokensList: PropTypes.array,
  testId: PropTypes.string,
};
