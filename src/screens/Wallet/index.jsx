import React, { useState } from 'react';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import { Strings } from 'resources';
import { useSelector, useDispatch } from 'react-redux';
import { getItem, setItem } from 'utils/storage';
import { STORAGE_KEYS } from '../../constants';
import { parseObject } from 'utils/parse';
import moment from 'moment';
import { log } from 'utils/log';
import { getUsersReceiveTransactions } from 'web3-layer';
import { TransactionsAction } from 'redux/slices/transactionsSlice';

import './wallet.scss';

// Components 
import Accounts from 'screens/Wallet/Accounts';
import Transactions from 'screens/Wallet/Transactions';
import Navbar from 'components/Navbar';
import Spinner from 'components/Spinner';
import StyledSpinner from './StyledSpinner';

// Images
import coin from 'assets/images/im3d.png';
import support from 'assets/images/support.svg';
import refreshIcon from 'assets/images/refresh-icon.png';
import { NetworksAction } from 'redux/slices/networksSlice';
import { getNetwork, getTotalValue, getTransactionsList } from 'redux/selectors';

export default function Wallet () {

  const params = useLocation();

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const accounts = useSelector(state => state.account);
  const transactions = useSelector((state) => state.transactions);
  const accountList = useSelector(state => state.account.accountList);
  const transactionsList = useSelector(getTransactionsList);

  const networks = useSelector((state) => state.networks);
  const selectedNetwork = useSelector(getNetwork);
  const totalValue = useSelector(getTotalValue);
  const displayAccountsData = totalValue !== undefined;

  const [activeTab, setActiveTab] = useState(params.state?.tab ?? 'tab1');

  const refreshAccountsData = async () => {
    dispatch(NetworksAction.refreshBalances());
  };

  const checkReceiveCallEligible = async () => {
    const receiveSupportNetwork = ['1', '56', '97', '137', '250', '43114'];
    if (receiveSupportNetwork.includes(selectedNetwork.chainId.toString())) {
      {
        const {error, data} = await getItem(STORAGE_KEYS.LAST_RECEIVE_CALL_COUNT);

        if (!error) {
          const parseCount = parseObject(data);

          return {
            isEligible: true,
            date: moment().format('YYYY-MM-DD'),
            count: parseCount.data,
          };
        }
      }
      return {
        isEligible: true,
        date: moment().format('YYYY-MM-DD'),
        count: 0,
      };
    } else {
      return { isEligible: false };
    }
  };

  const filterReceiveTransactionObject = async () => {
    try {
      const checkEligbility = await checkReceiveCallEligible();
      if (checkEligbility.isEligible) {
        const { error, data } = await getItem(STORAGE_KEYS.BLOCK_NO);
        let tempBlockNo = {};

        if (!error) {
          const parseBlock = parseObject(data);
          if (parseBlock.success) {
            tempBlockNo = { ...parseBlock.data };
          }
        }
        const accountListObj = accounts.accountList.map(
          item => item.publicAddress,
        );
        const tokenArray = [
          ...new Set(
            networks.tokens
              .map(value => value['tokens'].map(value => value['address']))
              .flat(1),
          ),
        ];

        let currentBlockNo = 0;
        if (tempBlockNo[selectedNetwork.chainId]) {
          currentBlockNo = tempBlockNo[selectedNetwork.chainId];
        }

        const receiveObj = [
          selectedNetwork.chainId,
          currentBlockNo,
          accountListObj,
          tokenArray,
        ];
        const receiveTransactions = await getUsersReceiveTransactions(
          ...receiveObj,
        );

        if (receiveTransactions.aReceiveTransactions) {
          saveReceiveTransactions(receiveTransactions.aReceiveTransactions);
        }
        if (receiveTransactions.success) {
          tempBlockNo[selectedNetwork.chainId] =
            receiveTransactions.nLatestBlockNumber;
          await setItem(STORAGE_KEYS.LAST_RECEIVE_CALL_TIME, JSON.stringify(checkEligbility.date));
          await setItem(STORAGE_KEYS.LAST_RECEIVE_CALL_COUNT, JSON.stringify(checkEligbility.count + 1));
          await setItem(STORAGE_KEYS.BLOCK_NO, JSON.stringify(tempBlockNo));          
        }
      }
    } catch (e) {
      log('get receive transaction error >>>', e);
    }
  };

  const saveReceiveTransactions = async receiveTrans => {
    try {
      const tempTransList = [...transactions.transactionsList, ...receiveTrans ?? []];

      const { data } = await getItem(STORAGE_KEYS.TRANSACTIONS);
      
      let parseTransactions;

      const parseObj = parseObject(data);
      /* istanbul ignore else */
      if (parseObj.success) {
        parseTransactions = parseObj.data;
      }
      let tempTransactions = [];
      /* istanbul ignore else */
      if (parseTransactions) {
        tempTransactions = parseTransactions;
      }
      const transObj = tempTransactions.find(
        chain => chain.chainId === selectedNetwork.chainId,
      );
      if (transObj) {
        const transIndex = tempTransactions.indexOf(transObj);
        transObj.transactions = tempTransList;
        tempTransactions[transIndex] = transObj;
      } else {
        tempTransactions.push({
          chainId: selectedNetwork.chainId,
          transactions: tempTransList,
        });
      }

      dispatch(TransactionsAction.saveTransactions(tempTransList));
      await setItem(STORAGE_KEYS.TRANSACTIONS, JSON.stringify(tempTransactions));
      refreshAccountsData();
    } catch (e) {
      log('e >>>', e);
    }
  };

  return (          
    <React.Fragment>
      <div className="main-container">
        <Navbar />
        <div className="container">
          <div className="main-content">
            <div className="dash-header">
              <div className="left">
                <img src={ coin } alt="Coin" />
              </div>
              <div className="body">
                <h1 className="text-theme-color mb-0">{Strings.APP_NAME}</h1>
              </div>                
              <div className="right">
            { displayAccountsData && (
              <div>
                <p onClick={refreshAccountsData} style={{ cursor: 'pointer' }} className="mb-0">
                  <img style={{ margRight: '20px' }} src={refreshIcon} width="20px" alt="refresh" />
                </p>
              </div>
            )}
            { !displayAccountsData &&
              <StyledSpinner />
            } 
                <Link to={ { pathname: '' } }
                  onClick={ (e) => {
                    e.preventDefault();
                    navigate('/support', {
                      state: {
                        supportType: 'contactus',
                        title: Strings.CONTACT_US,
                      }
                    });
                  } } className="header-icon">
                  <img src={ support } alt="Support" />
                </Link>
              </div>
            </div> 
            <div className="content-background ds-flex-col full-flex">
              <div className="tabs">
                <div className="tab-nav">
                  <div onClick={ () => setActiveTab('tab1') } data-testid="accounts-tab" className={ `tabStyle tab-first ${activeTab === 'tab1' ? 'active' : ''}` }>
                    <span className="noselect">{Strings.ACCOUNTS}</span>
                  </div>
                  <div onClick={ () => 
                  {
                    setActiveTab('tab2');
                    filterReceiveTransactionObject();
                  }
                  }  data-testid="transactions-tab" className={ `tabStyle tab-last ${activeTab === 'tab2' ? 'active' : ''}` }>
                    <span className="noselect">{Strings.TRANSACTIONS}</span>
                  </div>
                  {activeTab && 
                    <div className="tab-highlight"></div>
                  }
                </div>                    
              </div>    
              <div className="full-flex relative">
                {
                  activeTab === 'tab1' && displayAccountsData &&
                    <Accounts accountList={ accountList } />
                }
                {
                  activeTab === 'tab2' && displayAccountsData &&
                    <Transactions transactionsList={ transactionsList } />
                }
                {
                  !displayAccountsData && <Spinner />
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
