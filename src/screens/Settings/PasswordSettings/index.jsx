import React from 'react';
import { Strings } from 'resources';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import './password-settings.scss';

// Components 
import Header from 'components/Header';
import Button from 'components/Button';

export default function PasswordSettings () {

  const navigate = useNavigate();
  const wallet = useSelector(state => state.wallet);

  const onContinue = (e) => {
    e.preventDefault();    
    navigate('/change-password');    
  };

  return (
    <React.Fragment>
      <div className="container">  
        <Header
          support
        />
        <div className="content-background ds-flex-col full-flex">
          <div className="wrapper">
            <h2 className="text-theme-color uppercase">
              {
                wallet.isPasscode
                  ? Strings.PASSCODE_SETTING_UPPERCASE
                  : Strings.PASSWORD_SETTING_UPPERCASE
              }
            </h2>
            <p className="opacity">
              {
                wallet.isPasscode
                  ? Strings.CHANGE_PASSCODE_INFO_MSG
                  : Strings.CHANGE_PASSWORD_INFO_MSG
              }
            </p>
            <br />
            
            <Button
              label={ Strings.CONTINUE_UPPERCASE }
              onClick={ (e) => onContinue(e, false) }
            />


          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
