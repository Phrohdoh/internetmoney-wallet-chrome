import React, { useState } from 'react';
import { Strings } from 'resources';
import { useLocation } from 'react-router-dom';
import { BACKEND_SERVER } from '../../../constants';

import './support.scss';

import { Validation } from 'utils/validations';

// Components 
import Header from 'components/Header';
import Button from 'components/Button';
import InputField from 'components/InputField';
import TextareaField from 'components/TextareaField';
import Spinner from 'components/Spinner';
import { GlobalAction } from 'redux/slices/globalSlice';
import { useDispatch } from 'react-redux';

export default function Support () {
  const dispatch = useDispatch();
  const params = useLocation();

  const supportTypes = Strings.SUPPORT_TYPES;
  const selectedSupportType = supportTypes.find(
    type => type.type === params.state.supportType,
  );

  const [isLoading, setIsLoading] = useState(false);

  const initialValues = {
    name: '',
    email: '',
    subject: '',
    yourMessage: '',
    issue: '',
  };

  const [values, setValues] = useState(initialValues);
  const [errorName, setErrorName] = useState('');
  const [errorEmail, setErrorEmail] = useState('');
  const [errorSubject, setErrorSubject] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [errorIssue, setErrorIssue] = useState('');

  const resetError = () => {
    setErrorName('');
    setErrorEmail('');
    setErrorSubject('');
    setErrorMessage('');
    setErrorIssue('');
  };
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const reset = () => {
    setValues(initialValues);
  };

  const handelSubmit = (e) => {
    e.preventDefault();
    if (checkValidation()) {
      sendSupportMail();
    }
  };

  const checkValidation = () => {
    resetError();
    const isValidEmail =
      Validation.isEmpty(values.email) || Validation.isValidEmail(values.email);

    const isEmptySubject = Validation.isEmpty(values.subject);
    const isEmptyMessage = Validation.isEmpty(values.yourMessage);
    const isEmptyIssue = Validation.isEmpty(values.issue);

    if (isValidEmail) {
      if (isEmptyIssue && params.state.supportType === 'contactus') {
        setErrorIssue(Strings.ENTER_MESSAGE);
        return false;
      }
      else if (isEmptySubject && params.state.supportType !== 'contactus') {
        setErrorSubject(Strings.ENTER_SUBJECT_MSG);
        return false;
      }
      else if (params.state.supportType !== 'contactus' && isEmptyMessage) {
        setErrorMessage(Strings.ENTER_MESSAGE);
        return false;
      }
      return true;
    }
    else {
      setErrorEmail(Strings.ENTER_VALID_EMAIL);
    }
  };

  const sendSupportMail = async () => {
    const baseUrl = BACKEND_SERVER.BASE_URL + BACKEND_SERVER.SEND_SUPPORT_MAIL;

    const body = {
      subject: params.state.supportType === 'contactus'
        ? selectedSupportType.subTitle
        : values.subject,
      message: params.state.supportType === 'contactus'
        ? values.issue
        : values.yourMessage
    };
    if (!Validation.isEmpty(values.name)) {
      <p>{values.name}</p>;
    }
    if (!Validation.isEmpty(values.email)) {
      <p>{values.email}</p>;
    }
    if (!Validation.isEmpty(values.issue)) {
      <p>{values.issue}</p>;
    }

    setIsLoading(true);

    try{
      const response = await fetch(baseUrl, {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      });

      const data = await response.json();
      /* istanbul ignore else */
      setIsLoading(false);
      if (data.success) {
        dispatch(
          GlobalAction.showAlert(
            Strings.SUCCESS,
            data.message,
          )
        );
        reset();
      } else {
        dispatch(
          GlobalAction.showAlert(
            Strings.ERROR,
            data.message,
          )
        );
      }
    } catch (error) {
      dispatch(
        GlobalAction.showAlert(
          Strings.ERROR,
          'Something went wrong!',
        )
      );
      setIsLoading(false);
    }
  };

  return (
    <React.Fragment>
      <div className="container support-container">
        <Header
          headerTitle={ Strings.SUPPORT }
          support
        />
        <div className="padder">
          <h2 className="text-theme-color uppercase">{selectedSupportType.detailTitle}</h2>
          <p className="opacity">{selectedSupportType.detailMsg}</p>
        </div>
        <div className="content-background ds-flex-col full-flex">
          <div className="wrapper">
            <h3 className="text-theme-color text-center">{selectedSupportType.subTitle}</h3>
            <br />
            {params.state.supportType === 'contactus' && (
              <React.Fragment>
                <InputField
                  label={ Strings.NAME }
                  value={ values.name }
                  name="name"
                  onChange={ handleInputChange }
                  placeholder={ Strings.NAME_PLACEHOLDER }
                  error={ errorName }
                />
                <TextareaField
                  name="issue"
                  label={ Strings.YOUR_MESSAGE }
                  value={ values.issue }
                  onChange={ handleInputChange }
                  placeholder={ Strings.YOUR_MESSAGE_PLACEHOLDER }
                  error={ errorIssue }
                />
              </React.Fragment>
            )}
            <InputField
              name="email"
              label={ Strings.HOW_DO_REACH }
              value={ values.email }
              onChange={ handleInputChange }
              error={ errorEmail }
              placeholder={ Strings.EMAIL_OPT }
            />
            {params.state.supportType === 'sendfeedback' ||
              params.state.supportType === 'submitbug' ?
              <React.Fragment>
                <InputField
                  name="subject"
                  label={ Strings.SUBJECT }
                  value={ values.subject }
                  onChange={ handleInputChange }
                  placeholder={ Strings.ENTER_SUBJECT }
                  error={ errorSubject }
                />
                <TextareaField
                  name="yourMessage"
                  label={ params.state.supportType === 'submitbug' ? Strings.TELL_ISSUE : Strings.WHAT_YOU_LIKE }
                  value={ values.yourMessage }
                  onChange={ handleInputChange }
                  error={ errorMessage }
                  placeholder={ params.state.supportType === 'submitbug' ? Strings.PROVIDE_DETAIL : Strings.TELL_ABOUT_IT }
                />
              </React.Fragment>
              : null
            }
            <div>
              {!isLoading ?
                <Button
                  onClick={ (e) => handelSubmit(e) }
                  label={ Strings.SUBMIT_UPPERCASE }
                  className="btn-full"
                /> : <Spinner />
              }
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
