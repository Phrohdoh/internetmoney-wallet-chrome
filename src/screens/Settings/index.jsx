import React from 'react';
import PropTypes from 'prop-types';
import { Links, Strings } from 'resources';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import {log} from 'utils/log';

import './settings.scss';

// Components 
import Header from 'components/Header';
import Navbar from 'components/Navbar';

// Images
import rightArrow from 'assets/images/right-arrow.svg';

export default function Settings () {

  const navigate = useNavigate();

  const wallet = useSelector(state => state.wallet);
  
  const sectionData = [
    {
      title: Strings.GENERAL,
      data: [
        { 
          title: Strings.ABOUT, 
          navigateTo: '/about'
        },
        { title: Strings.CHOOSE_LANGUAGE, navigateTo: '/choose-language' },
        { title: Strings.LOCK_WALLET, navigateTo: '/lock-wallet' },
      ],
    },
    {
      title: Strings.SECURITY_PRIVACY,
      data: [
        { title: wallet.isPasscode ? Strings.CHANGE_PASSCODE_UPPERCASE : Strings.CHANGE_PASSWORD_UPPERCASE, navigateTo: '/password-settings' },
        {
          title: Strings.SEED_PHRASE,
          navigateTo: '/settings-secret-recovery-phrase',
        },
        {
          title: Strings.DATA_COLLECTION,
          navigateTo: '/data-collection',
        },
      ],
    },
    {
      title: Strings.WALLET_CONNECT,
      data: [
        { title: Strings.VIEW_CONNECTED_APPS, navigateTo: '/wallet-connected' },
      ],
    },
    {
      title: Strings.SUPPORT,
      data: [
        {
          title: Strings.FEATURE_REQUEST,
          navigateTo: '/support',
          params: { supportType: 'sendfeedback' },
        },
        {
          title: Strings.SUBMIT_BUG,
          navigateTo: '/support',
          params: { supportType: 'submitbug' },
        },
        {
          title: Strings.CONTACT_US,
          navigateTo: '/support',
          params: { supportType: 'contactus' },
        },
      ],
    },
    {
      title: Strings.LEGAL,
      data: [
        {
          title: Strings.TERMS_AND_CONDITIONS,
          onClick: (_) => window.open(Links.TERMS_OF_SERVICE)
        },
        {
          title: Strings.PRIVACY_POLICY,
          onClick: (_) => window.open(Links.PRIVACY_POLICY)
        },
      ],
    }
  ];

  const renderOption = (item, index) => {
    return (
      <div
        key={ index }
        className={ 'option-button' }
        data-testid={ item.title }
        onClick={ () => {
          try {
    
            if (item.onClick) {
              item.onClick();
            }
            if (item.navigateTo) {
              if (item.params)
                navigate(item.navigateTo, {
                  state: item.params
                });
              else
                navigate(item.navigateTo);
            }
         
          } catch (e) {
            log('');
          }
        } }
      >
        <div className="body">          
          <span>{item.title}</span>          
        </div>
        <img src={ rightArrow } />
      
      </div>
    );
  };

  const OptionsGroup = (section, index) => {
    return (
      <div
        key={ index }
        data-testid={ section.title }
      >
        <div className="options-group">
          <div className="options-header">{section.title}</div>        
          {section.data.map((item, index) => renderOption(item, index))}
        </div>
      </div>
    );
  };

  const renderSettingOptions = () => {
    return (
      <div className='options-container'>       
        {sectionData.map((section, index) => OptionsGroup(section, index))}        
      </div>
    );
  };

  return (
    <React.Fragment>
      <div className="main-container">
        <Navbar /> 
        <div className="container">          
          <div className="main-content">
            <Header
              headerTitle={ Strings.SETTINGS_UPPERCASE }
              support
              className="navigator-header"
            />
            <div className="content-background ds-flex-col full-flex">
              <div className="wrapper">
                {renderSettingOptions()}                
              </div>              
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

Settings.propTypes = {
  title: PropTypes.string,
  children: PropTypes.object,
  content: PropTypes.string,
  className: PropTypes.string,
};
