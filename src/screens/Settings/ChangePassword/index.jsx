import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Strings } from 'resources';
import { changePassword, checkPasswordSecurely } from 'web3-layer';

import { STORAGE_KEYS } from '../../../constants';
import { WalletAction } from 'redux/slices/walletSlice';

import './change-password.scss';

import { setItem } from 'utils/storage';

// Components 
import Header from 'components/Header';
import InputField from 'components/InputField';
import Button from 'components/Button';
import Spinner from 'components/Spinner';
import Passcode from 'components/Passcode';
import AlertModel from 'components/AlertModel';

// Images
import visibleIcon from 'assets/images/visible.svg';
import invisibleIcon from 'assets/images/invisible.svg';
import { getUserFriendlyErrorMessage } from 'utils/errors';

export default function ChangePassword () {
  
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const wallet = useSelector(state => state.wallet);

  const { isPasscode } = wallet;

  const [oldPasscode, setOldPasscode] = useState('');
  const [newPasscode, setNewPasscode] = useState('');
  const [confPasscode, setConfPasscode] = useState('');

  const [oldPasswordError, setOldPasswordError] = useState('');
  const [newPasswordError, setNewPasscodeError] = useState('');
  const [confPasswordError, setConfPasscodeError] = useState('');

  
  const [selectedType, setSelectedType] = useState('old');

  const [oldPasswordType, setOldPasswordType] = useState('password');
  const [newPasswordType, setNewPasswordType] = useState('password');
  const [confirmPasswordType, setConfirmPasswordType] = useState('password');
  
  const [isLoading, setIsLoading] = useState(false);

  const initialValues = {
    oldPassword: '',
    newPassword: '',
    confirmPassword: '',
  };

  const [values, setValues] = useState(initialValues);

  const [showAlert, setShowAlert] = useState(false);
  const [error, setError] = useState('');

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const handlePasscode = (item, code, setCode) => {
    const tempCode = code.split('');
    if (item.value === 'remove') {
      if (tempCode.length) {
        tempCode.pop();
      }
    } else {
      if (tempCode.length < 6) {
        tempCode.push(item.value);
      }
    }
    const strPasscode = tempCode.join('');
    setCode(strPasscode);
  };

  const checkPasscodeValidation = () => {
    const isValidOldPasscode = oldPasscode.length === 6;
    const isValidNewPasscode = newPasscode.length === 6;
    const matchPasscode = newPasscode === confPasscode;

    if (selectedType === 'old') {
      if (isValidOldPasscode) {
        setSelectedType('new');
      } else {
        setShowAlert(true);
        setError(Strings.ENTER_OLD_PASSCODE_MSG);        
      }
    } else if (selectedType === 'new') {
      if (isValidNewPasscode) {
        setSelectedType('conf');
      } else {
        setShowAlert(true);
        setError(Strings.ENTER_NEW_PASSCODE_MSG);
      }
    } else {
      if (matchPasscode) {
        return true;
      } else {
        setShowAlert(true);
        setError(Strings.PASSCODE_NOT_MATCH_MSG);
      }
    }

    return false;
  };

  const handelSubmit = async (e) => {
    e.preventDefault();
    if (isPasscode ? checkPasscodeValidation() : checkPasswordValidation()) {
      setIsLoading(true);
      if (
        checkOldPasswordIsValid(
          wallet.walletDetail,
          isPasscode ? oldPasscode : values.oldPassword,
        )
      ) {
        const changePassObj = await changePassword(
          wallet.walletDetail.walletObject,
          wallet.walletDetail.mnemonic,
          isPasscode ? oldPasscode : values.oldPassword,
          isPasscode ? newPasscode : values.newPassword,
        );
        setIsLoading(false);
        if (changePassObj.success) {
          const tempWalletDetail = { ...wallet.walletDetail };
          tempWalletDetail.walletObject = changePassObj.walletObject;
          tempWalletDetail.mnemonic = changePassObj.mnemonic;
          tempWalletDetail.passwordHash = changePassObj.passwordHash;
          // Move on another screen
          setItem(STORAGE_KEYS.WALLET_OBJECT, JSON.stringify(tempWalletDetail));
          dispatch(WalletAction.initApp(
            tempWalletDetail,
            isPasscode ? newPasscode : values.newPassword,
            isPasscode,
          ));
          navigate('/wallet');
        }
        else {
          setShowAlert(true);
          setError(Strings.INCORRECT_OLD_PASSWORD);
        }
      }
    }
  };
  
  const checkPasswordValidation = () => {
    
    const minLengthRegExp = /.{10,}/;
    const minLengthPassword = minLengthRegExp.test(values.newPassword);
    
    if (!values.oldPassword) {
      setOldPasswordError(Strings.ENTER_OLD_PASSWORD_MSG);
    }
    if (!values.newPassword) {
      setNewPasscodeError(Strings.ENTER_NEW_PASSWORD_MSG);
    } 
    else if (!minLengthPassword) {
      setNewPasscodeError(Strings.ENTER_VALID_NEW_PASS_MSG);
    } 
    else if (!values.confirmPassword) {
      setConfPasscodeError(Strings.ENTER_PASSWORD);
    }
    else if (values.newPassword !== values.confirmPassword) {
      setConfPasscodeError('Confirm password is not matched');
    } 

    if (values.oldPassword && minLengthPassword && values.newPassword === values.confirmPassword){
      return true;
    } else{
      return false;
    }    
  };

  const checkOldPasswordIsValid = async (walletObj, password) => {
    const checkPass = await checkPasswordSecurely(
      password,
      walletObj.passwordHash,
    );
    return checkPass;   
  };

  const toggleOldPassword = () => {
    if (oldPasswordType === 'password') {
      setOldPasswordType('text');
    }
    else
      setOldPasswordType('password');
  };

  const toggleNewPassword = () => {
    if (newPasswordType === 'password') {
      setNewPasswordType('text');
    }
    else
      setNewPasswordType('password');
  };

  const toggleConfirmPassword = () => {
    if (confirmPasswordType === 'password') {
      setConfirmPasswordType('text');
    }
    else
      setConfirmPasswordType('password');
  };
  const reset = () => {
    setValues(initialValues);
    setNewPasscode('');
    setOldPasscode('');
    setConfPasscode('');
    setValues(initialValues);
  };
  const onClickSuccess = () => {
    setShowAlert(false);
    if (selectedType === 'conf') {
      setSelectedType('old');
    }
    reset();
  };

  const getBtnText = () => {
    let btnString = Strings.SAVE_CHANGES_UPPERCASE;
    if (isPasscode) {
      if (selectedType === 'old' || selectedType === 'new') {
        btnString = Strings.NEXT_UPPERCASE;
      }
    }

    return btnString;
  };

  const renderPassComponent = () => {
    return (
      <React.Fragment>
        <h2 className="text-theme-color uppercase">{Strings.CHANGE_PASSWORD_UPPERCASE}</h2>
        <InputField
          type={ oldPasswordType === 'password' ? 'password' : 'text' }
          dataTestid="old-password"
          label={ Strings.OLD_PASSWORD }
          value={ values.oldPassword }
          onChange={ handleInputChange }
          error={ oldPasswordError }
          name="oldPassword"
          onIconClick={ toggleOldPassword }
          icon={ oldPasswordType === 'password' ? <img src={ visibleIcon } alt="visible" /> : <img src={ invisibleIcon } alt="invisible" /> }
        />
        <InputField
          type={ newPasswordType === 'password' ? 'password' : 'text' }
          dataTestid="new-password"
          label={ Strings.NEW_PASSWORD }
          value={ values.newPassword  }
          onChange={ handleInputChange }
          error={ newPasswordError }
          name="newPassword"
          onIconClick={ toggleNewPassword }
          icon={ newPasswordType === 'password' ? <img src={ visibleIcon } alt="visible" /> : <img src={ invisibleIcon } alt="invisible" /> }
        />
        <InputField
          type={ confirmPasswordType === 'password' ? 'password' : 'text' }
          dataTestid="conf-password"
          label={ Strings.CONF_PASSWORD }
          value={ values.confirmPassword  }
          onChange={ handleInputChange }
          error={ confPasswordError }
          name="confirmPassword"
          onIconClick={ toggleConfirmPassword }
          icon={ confirmPasswordType === 'password' ? <img src={ visibleIcon } alt="visible" /> : <img src={ invisibleIcon } alt="invisible" /> }
        />
      </React.Fragment>
    );
  };

  const renderPasscodeComponent = () => {
    switch (selectedType) {
    case 'old':
      return (
        <Passcode
          title={ Strings.OLD_PASSCODE }
          passcode={ oldPasscode }
          onClickKey={ item =>
            handlePasscode(item, oldPasscode, setOldPasscode)
          }
        />
      );
    case 'new':
      return (
        <Passcode
          title={ Strings.NEW_PASSCODE }
          passcode={ newPasscode }
          onClickKey={ item =>
            handlePasscode(item, newPasscode, setNewPasscode)
          }
        />
      );
    default:
      return (
        <Passcode
          title={ Strings.CONF_PASSCODE }
          passcode={ confPasscode }
          onClickKey={ item =>
            handlePasscode(item, confPasscode, setConfPasscode)
          }
        />
      );
    }
  };

  return (
   
    <div className="container">  
      <Header
        support
      />
      <div className="content-background ds-flex-col full-flex">
        <div className="wrapper">    
          <div className="full-flex">
            {isPasscode ? (
              <React.Fragment>
                <h2 className="text-theme-color uppercase">{Strings.CHANGE_PASSCODE_UPPERCASE}</h2>
                {renderPasscodeComponent()}
              </React.Fragment>
            ): (
              renderPassComponent()
            )}

          </div>
          {!isLoading ?
            <Button 
              onClick={ (e) => handelSubmit(e) }
              label={ getBtnText() }
              className="btn-full"
            />
            : /* istanbul ignore next */ <Spinner />
          }
          {showAlert && (
            <AlertModel
              isShowAlert={ showAlert }
              onPressSuccess={ () => onClickSuccess() }
              className="text-center"
            >
              <p>{getUserFriendlyErrorMessage(error)}</p>
            </AlertModel>
          )}
        </div>
      </div>
    </div>
  );
}

