import { Strings } from 'resources';

// Components
import Header from 'components/Header';
import packageJson from '../../../../../package.json';
import './about.scss';

export default function About () {

  return (
    <div className="container">
      <Header 
        headerTitle={ Strings.ABOUT }
        support
      />     
      <div className="content-background ds-flex-col full-flex">
        <div className="wrapper">
          <div className="content-row">
            <h5 className="text-theme-color">{Strings.APP_VERSION}</h5>
            <p className="opacity">{packageJson.version}</p>
          </div>
        </div>
      </div>      
    </div>
  );
}
