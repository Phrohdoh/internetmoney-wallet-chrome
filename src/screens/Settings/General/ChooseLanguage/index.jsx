import React, { useState } from 'react';
import { Strings } from 'resources';

// Components
import Header from 'components/Header';
import Dropdown from 'components/Dropdown';
import Modal from 'components/Modal';

import './choose-language.scss';

export default function ChooseLanguage () {

  const [isShowLanguageList, setIsShowLanguageList] = useState(false);
  const [languages] = useState(Strings.LANGUAGE_LIST);
  const [selectedLanguage] = useState(languages[0]);

  const renderTitleView = (title, subtitle) => {
    return (
      <div className="title-header">
        <h5 className="text-theme-color uppercase">{title}</h5>
        <span className="text-style">{subtitle}</span>
      </div>
    );
  };

  // render Create Account View
  const renderLanguages = (languages) => {
    return (
      <React.Fragment>
        <div className="modal-header">
          <h6 className="text-center">{Strings.CHOOSE_LANGUAGE}</h6>
        </div>
        <div className="modal-body">
          <div className="languages-list">
            {
              languages.map((item, index) => (
                <div 
                  key={ item.key }
                  data-testid={ `languages_${index}` } 
                  onClick={ () => setIsShowLanguageList(false) }
                  className={ 'listitem-control noselect' }
                >
                  <div className="body">
                    <h6>{item.title}</h6>
                  </div>
                </div>
              ))
            }
          </div>
        </div>
      </React.Fragment>

    );
  };

  return (
    <div className="container">
      <Header 
        headerTitle={ Strings.CHOOSE_LANGUAGE }
        support
      />
      <div className="content">            
        <div className="content-background ds-flex-col full-flex">
          <div className="wrapper">
            {renderTitleView(Strings.CURRENT_LANGUAGE_UPPERCASE, Strings.CHOOSE_LANGUAGE_MSG)}            
            <Dropdown
              dataTestid="general-settings"
              value={ selectedLanguage.title }
              className="text-gray-theme"
              onClick={ (e) => { 
                e.preventDefault(); 
                setIsShowLanguageList(true); 
              } }
            />            
          </div>
          {isShowLanguageList ?
            <Modal
              isOpen={ isShowLanguageList }
              closeModal={ () => setIsShowLanguageList(false) }
            >
              {renderLanguages(languages)}
            </Modal>
            : null}
        </div>               
      </div>    
    </div>
  );
}
