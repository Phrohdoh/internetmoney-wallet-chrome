import { Strings } from 'resources';

// Components
import Header from 'components/Header';

import './general.scss';

export default function General () {

  const renderTitleView = (title, subtitle) => {
    return (
      <div className="title-header">
        <h5 className="text-theme-color uppercase">{title}</h5>
        <span className="text-style">{subtitle}</span>
      </div>
    );
  };
  
  return (
    <div className="container">
      <Header 
        headerTitle={ Strings.ABOUT_IM }
        support
      />
      <div className="content">            
        <div className="content-background ds-flex-col full-flex">
          <div className="wrapper">
            {renderTitleView(Strings.ABOUT, Strings.formatString(Strings.APP_VERSION, [1.1]))}
          </div>        
        </div>               
      </div>    
    </div>
  );
}
