import React from 'react';
import { Strings } from 'resources';

// Components
import Header from 'components/Header';

export default function PrivacyPolicy () {
  return (
    <React.Fragment>
      <div className="container">
        <Header />
        <div className="content">            
          <div className="single-content">
            <h2 className="text-theme-color mb-0 uppercase">{Strings.PRIVACY_POLICY}</h2>
            <p>Last Updated and Effective as of: September 3, 2022</p>
            <p>Decentralized Innovations, LLC (“Company,” “we,” “our,” or “us”) understands that privacy is important to our users. Protecting your private information is our priority. This Privacy Policy discloses our practices regarding information collection and usage for InternetMoney.io (the “Website”), The Internet Money Wallet (the “App”), our Google Chrome extension, and any other services provided by us or our affiliates (collectively, the “Services”).</p>
            <ol>
              <li><p><strong>OVERVIEW</strong> </p>
                <ul>
                  <li>We collect Personal Information (as defined below) from users of our Services when it is voluntarily provided to us and use it for the purposes for which it was provided (see <strong>Information We Collect Directly From You and How We Use Your Information</strong>). </li>
                  <li>We automatically collect information from users of our Services for analytical purposes and in order to provide the Services (see <strong>Information We Collect Automatically and How We Use Your Information</strong>). </li>
                  <li>We will not use or share information we collect from you with anyone except as described in this Privacy Policy.</li>
                </ul>
                <p>By accessing or using the Services, you agree to this Privacy Policy. If you do not agree with our policies and practices, you should not use the Services, or otherwise provide us with Personal Information.</p>
                <p>Our Website and Services are provided from the United States and all servers that make it available reside in the United States. You understand that data stored in the United States may be subject to lawful requests by the courts or law enforcement authorities in the United States.</p>
                <p>If you have questions, you can always contact us using the information in the section below titled <strong>Contact.</strong></p>
              </li>
              <li>
                <p><strong>INFORMATION WE COLLECT DIRECTLY FROM YOU</strong></p>
                <p>Company’s Services are non-custodial and decentralized by nature, and accordingly, we do not require that you provide us with any Personal Information. However, we may collect information, including Personal Information, when you provide such information to us directly, such as when you contact us over email or social media, or otherwise interact with us in connection with the Services. Your provision of your Personal Information to us is completely voluntary. “Personal Information” is information that specifically identifies you. We do not collect Personal Information unless you submit that information to us. </p>
                <p>You may directly provide us with your email address if you contact us via email or agree to receive direct marketing. While we will never collect or store your social media information, if you contact us on social media, you understand that you are providing us with any Personal Information that is made publicly available on your social media account. We may also collect other information that you directly provide us, including marketing and communications data (e.g., your preferences in receiving marketing from us). </p>
                <p>You may also choose to provide your phone number over the App in order to claim free crypto. If you do, your number will be de-identified through a one-way encryption algorithm, and not stored as Personal Information. We will never retain your actual phone number on our servers.</p>
                <p>We do not directly collect your payment information over the Services. Payment information goes directly to our third-party payment processor</p>
                <p>Registering for the Services will require you to generate a seed phrase or import a seed phrase from an existing wallet. We do not store this information. Please note that with the exception of your initial registration, we will never ask you to share your private keys or wallet seed. Never trust any person, company, or website that asks you to enter your private keys or wallet seed.</p>
              </li>
              <li>
                <p><strong>INFORMATION WE COLLECT AUTOMATICALLY</strong></p>
                <p>We, our service providers, or our third-party partners may also automatically collect information from you when you use our Services. Such information does not directly identify you as an individual and may include:</p>
                <ul>
                  <li>
                    <strong>Cookies and Similar Technology</strong> <br />
                    <p>We or our third-party partners may store some information on your device or device hard drive as a cookie or similar type of file (such as clear gifs, web beacons, tags, and similar technologies that work on mobile devices) to collect data related to usage of our Services. "Cookies" are pieces of information that may be placed on your computer by a website for the purpose of collecting data to facilitate and enhance your communication and interaction with that website. Such data may include, without limitation, the address of the websites you visited before and after you visited the Website, the type of browser you are using, your Internet Protocol (IP) address, what pages in the Website you visit and what links you clicked on, the region where your device is located, and geo-IP data. We may use cookies to customize your visit to the Website and for other purposes to make your visit more convenient or to enable us to enhance our Services.</p>
                  </li>
                  <li>
                    <strong>Clickstream Data</strong> <br />
                    <p>As you use the Internet, a trail of electronic information is left at each website you visit. This information, which is sometimes referred to as "clickstream data," can be collected and stored by a website's server. Clickstream data can tell us the type of computer and browsing software you use and the address of the website from which you linked to the Website. We may collect and use clickstream data as a form of aggregate information to anonymously determine how much time visitors spend on each page of our Website, how visitors navigate throughout the Website and how we may tailor our web pages to better meet the needs of visitors. This information will be used to improve our Website and our Services. </p>
                  </li>
                  <li>
                    <strong>Site and Platform Analytics</strong> <br />
                    <p>We may work with third-party service providers who use the technologies described in this section to conduct website analytics to help us track and understand how visitors use our Services. One such provider is Google Analytics, a web analytics service provided by Google, Inc. (“Google”). Google Analytics uses cookies to help analyze how users use the Services. The information generated by these cookies about your use (including your IP address) will be transmitted to and stored by Google on servers in the United States. Google will use this information for the purpose of evaluating your use of the Services, compiling reports on activity for its staff, and providing other services relating to web page activity and internet usage. Google may also transfer this information to third parties where required to do so by law, or where such third parties process the information on Google's behalf. You may refuse the use of  4258641.3 034162-9999-000 cookies by selecting the appropriate settings in your browser. By using the Services and accepting cookies, you consent to the processing of data about you by Google in the manner and for the purposes set out above. Please refer to the currently available opt-outs for Google Analytics by visiting https://tools.google.com/dlpage/gaoptout/. </p>
                  </li>
                </ul>
              </li>
              <li>
                <p><strong>HOW WE USE YOUR INFORMATION</strong></p>
                <p>We may use Personal Information and other information as described herein including for the following purposes:</p>
                <ul>
                  <li>To contact you about and provide you with our services</li>
                  <li>To create, maintain, customize, and secure your account with us;</li>
                  <li>To operate the Services, and to provide you with any specific services that you have requested;</li>
                  <li>To deliver content and product and service offerings relevant to your interests;</li>
                  <li>To identify trends and make inferences about you and your interactions with us or our affiliates or our business partners, such as to analyze your behavior and preferences, and to evaluate and improve the products and services of our affiliates and business partners;</li>
                  <li>To market Company goods and services, or goods and services of those of our affiliates, business partners, and other third parties;</li>
                  <li>To provide you advertising for products and services that may be of interest to you;</li>
                  <li>To respond to law enforcement requests and as required by applicable law, court order, legal process, or governmental regulation, including financial and anti-money laundering laws;</li>
                  <li>To help maintain the safety, security, and integrity of our Services, databases and other technology assets, and business;</li>
                  <li>For internal research; for technological development and demonstration; and to improve, upgrade, or enhance our Services;</li>
                  <li>For detecting security incidents; protecting against malicious, deceptive, fraudulent, or illegal activity; and prosecuting those responsible for that activity;</li>
                  <li>To investigate suspected fraud, harassment, or other violations of any law, rule, or regulation, or the policies for the Services;</li>
                  <li>In connection with a bankruptcy proceeding or the sale, merger, or change of control of the Company or the division responsible for the services with which your information is associated;</li>
                  <li>To respond to your direct inquiries;</li>
                  <li>To add you to our mailing lists and send you emails from time to time; and</li>
                  <li>For any additional purposes that you specifically consent to. </li>
                </ul>
                <p>We reserve the right to supplement your Personal Information with information we gather from other sources which may include online and offline sources. We also reserve the right to use and disclose information that is not Personal Information in any manner permitted by law except as otherwise provided in this Privacy Policy. Finally, we may permit our vendors and subcontractors to access your Personal Information, but they are only permitted to do so in connection with performing services for us. They are not authorized by us to use the information for their own benefit.</p>
                <p>As noted above, we may collect information that is not Personal Information (“Non-Personal Information”), including information lawfully made available from federal, state, or local government records, or aggregate or de-identified information. Because Non-Personal Information does not personally identify you, we may collect, use, and disclose such information for any purpose permitted by  4258641.3 034162-9999-000 law. In some instances, we may combine Non-Personal Information with Personal Information. If we combine any Non-Personal Information with Personal Information, the combined information will be treated by us as Personal Information.</p>
                <p>Additionally, we may analyze public blockchain data to ensure parties utilizing our Services are not engaged in illegal or prohibited activity under our Terms, and to analyze transaction trends for research and development purposes.</p>
              </li>
              <li>
                <p><strong>HOW WE SHARE YOUR INFORMATION</strong></p>
                <p>Besides using your Personal Information ourselves, we may send your Personal Information and other information to other companies, affiliates, and third parties in the following instances:</p>
                <ul>
                  <li><em>Service Providers.</em> We may share your Personal Information with third parties who may use your information to provide us services such as website hosting, data analysis, infrastructure provision, information technology services, customer service, email delivery services, auditing, and antifraud monitoring. These third parties may have access to Personal Information that is necessary to perform their functions</li>
                  <li><em>Legal Compliance and to Defend Our Rights.</em> We may disclose Personal Information and other information as we believe necessary or appropriate: (a) under applicable law (including financial and anti-money laundering laws), which may include laws outside your country of residence; (b) to comply with legal process; (c) to respond to requests from public and government authorities including public and government authorities outside your country of residence; (d) to enforce our Terms and Conditions; (e) to protect our operations or those of any of our affiliates; (f) to protect our rights, privacy, safety or property, and/or that of our affiliates, you, or others; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
                  <li><em>Business Transfers.</em> We may share your Personal Information and other information with third parties in connection with a corporate restructuring, acquisition, or divestiture.</li>
                  <li><em>Cross Device Matching.</em> We may now or in the future have the ability to match your devices using the data collected, making educated predictions, and, in some cases, using deterministic data (e.g., unique identifiers) or other content across devices. We may then, subject to the limitations otherwise set forth in this Privacy Policy and applicable law, display targeted advertisements to you across your devices unless it is an Opted-Out Device as further described in the section titled <strong>Opt-Out</strong> below.</li>
                  <li><em>Aggregated or Non-Personal Information.</em> We may share aggregated information or Non-Personal Information with unaffiliated third parties, such as business partners, manufacturers, distributors, and retailers, in a form in which the shared information will not contain nor be linked to any Personal Information, to improve and enhance your experience using the Services, and for our market research activities. </li>                
                </ul>
                <p>Please note that if you specifically consent to additional uses of your Personal Information, we may use your Personal Information in a manner consistent with that consent. </p>              
              </li>
              <li>
                <p><strong>INTEREST-BASED ADVERTISING</strong></p>
                <p>We may use information we collect (alone or in combination with information provided by third parties and service providers) to deliver targeted advertising (about Company or other third party products and services) to you when you visit our Website or other websites. Information about you may be used in this process. For example, if you are searching for information on a particular product, we may use that information to cause an advertisement to appear on other websites you view with information on that product. We may, now or in the future, have the ability to engage in “cross-device matching” to display targeted advertisements to you across browsers and devices (as described in the section <em>How We Share Your Information</em> above). </p>
                <p>To further clarify, we partner with third parties that collect information across various channels, including offline and online, for purposes of delivering more relevant advertising to you or your business. Our partners may place or recognize a cookie on your computer, device, or directly in our emails/communications, and we may share Personal Information with them if you have submitted such information to us. Our partners use this information to recognize you across different channels and platforms, including but not limited to, computers, mobile devices, and Smart TVs, over time for advertising, analytics, attribution, and reporting purposes. </p>
                <p>If you would like to opt-out of these interest-based advertisements or “cross-device matching,” please see the section titled <strong>Opt-Out</strong>, below.</p>
              </li>
              <li>
                <p><strong>THIRD-PARTY AND SOCIAL NETWORKING SERVICES</strong></p>
                <p>As a convenience to you, we may provide links to third-party websites, apps, or services from within the Services. We are not responsible for the privacy practices or content of these third-party sites, and by providing a link, we are not endorsing or promoting such third-party sites.</p>
                <p>You may choose to access and use the Services through our Google Chrome extension. While our operation of this extension is subject to the terms of this Privacy Policy, your use of the Chrome browser and other Google services is subject to Google’s terms and policies. Please refer to the currently available privacy policy for Google Chrome by visiting https://www.google.com/chrome/privacy/. </p>
                <p>The Services may also integrate with social networking services. We do not control such services and are not liable for the manner in which they operate. While we may provide you with the ability to use such services in connection with our Services, we are doing so merely as an accommodation and, like you, are relying upon those third-party services to operate properly and fairly. You should be aware that Personal Information which you voluntarily include and transmit online in a publicly accessible blog, social network, or otherwise online may be viewed and used by others.</p>
              </li>
              <li>
                <p><strong>SECURITY</strong></p>
                <p>The security of your information is very important to us. We attempt to provide for the secure transmission of your information from your devicesto our servers by utilizing Secure Sockets Layer (“SSL”) encryption, when reasonably available. However, due to the inherent open nature of the Internet, we cannot guarantee that communications between you and the Company, or information stored on the Services or our servers, will be free from unauthorized access by third parties such as hackers, and your use of the Services demonstrates your assumption of this risk. We have put in place reasonable physical, electronic, and managerial procedures to safeguard the information we collect. Only those employees who need access to your information in order to perform their duties are authorized to have access to  4258641.3 034162-9999-000 your Personal Information. If you have reason to believe that your interaction with us is no longer secure, please immediately notify us of the problem by contacting us at <a href="mailto:privacy@InternetMoney.io">privacy@InternetMoney.io</a>.</p>
              </li>
              <li>
                <p><strong>DATA RETENTION</strong></p>
                <p>We will retain your information for as long as your inquiry is active or as needed to provide you with the Services, and for a reasonable time thereafter in accordance with our standard procedures or as necessary to comply with our legal obligations, to resolve disputes, and to enforce our agreements. Even if we delete some or all of your information, we may continue to retain and use anonymous or aggregated data previously collected. Please note that we will not be liable for disclosures of your data due to errors or unauthorized acts of third parties.</p>
              </li>
              <li>
                <p><strong>PROTECTION FOR CHILDREN</strong></p>
                <p>Our Website and Services are intended for users ages 18 and over. We do not knowingly collect Personal Information from minors. When we become aware that Personal Information (or other information that is protected under applicable law) from a child under 16 has been collected, we will use all reasonable efforts to delete such information from our databases. If you believe we might have any Personal Information from or about a child under 16, please contact us at <a href="mailto:privacy@InternetMoney.io">privacy@InternetMoney.io</a>.</p>
              </li>
              <li>
                <p><strong>OPT-OUT</strong></p>
                <p><strong>Opting Out of Messages.</strong> To opt out of any future promotional messages from us, you should send an unsubscribe request to us at <a href="mailto:privacy@InternetMoney.io">privacy@InternetMoney.io</a>. We will process your request within a reasonable time after receipt. Please note that if you opt out in this manner, certain aspects of our services may no longer be available to you</p>
                <p><strong>Cookies.</strong> If you want to stop or restrict the placement of cookies or flush any cookies that may already be on your computer or device, please refer to and adjust your web browser preferences. Further information on cookies is available at www.allaboutcookies.org. By deleting our cookies or disabling future cookies, you may not be able to access certain areas or features of our Services, and some of the Services’ functionality may be affected.</p>
                <p><strong>Interest-Based Advertising Opt-Out and Do Not Track Signals.</strong> With respect to "do not track" (a/k/a “DNT”) signals or similar mechanisms transmitted by web browsers, the Website does not respond to or honor such signals or mechanisms. This means that third parties, such as ad networks, web analytics companies, and social networking platforms (some of whom are discussed elsewhere in this Privacy Policy), may collect information about your online activities over time and across our Website and other third-party online properties or services. These companies may use information about your visits to our Website and other sites, and general geographic information derived from your IP address, in order to provide advertisements about goods and services of interest to you. For more information about third-party advertisers and how to prevent them from using your information, please visit http://www.networkadvertising.org/choices/. This is a site offered by the Network Advertising Initiative ("NAI") that includes information on how consumers can opt out from receiving interest-based advertising from some or all of NAI's members. You can also visit http://www.aboutads.info/choices, which is a site offered by the Digital Advertising Alliance ("DAA") that includes information on how consumers can optout from receiving internet-based advertising from some or all of DAA's participating companies. Opting out of interest-based advertising does not mean that you will no longer see any advertisements. Rather, you will still see advertisements that are general and not tailored to your specific interests and activities. 4258641.3 034162-9999-000 Further, cookie-based opt-outs must be performed on each device and browser that you wish to have opted-out. For example, if you have opted out on your computer browser, that opt-out will not necessarily be effective on your mobile device. In the event we are performing cross-device matching (as described earlier in the Privacy Policy), once you have opted out on one device (“Opted-Out Device”), we will not use any new data from the Opted-Out Device to identify you on another device for interest-based advertising purposes and we will not use data from another device for interest-based advertising purposes on the Opted-Out Device.</p>              
              </li>
              <li>
                <p><strong>CALIFORNIA “SHINE THE LIGHT”</strong></p>
                <p>Under California Civil Code Section 1798.83 (“Shine the Light”), California residents have the right to request in writing from businesses with whom they have an established business relationship: (a) a list of the categories of personal information, as defined under Shine the Light, such as name, email address and mailing address and the type of services provided to the customer that a business has disclosed to third parties (including affiliates that are separate legal entities) during the immediately preceding calendar year for the third parties’ direct marketing purposes; and (b) the names and addresses of all such third parties. To request the above information, please contact us by email at <a href="mailto:privacy@InternetMoney.io">privacy@InternetMoney.io</a>. If you do not want your personal information shared with any third party who may use such information for direct marketing purposes, then you may opt out of such disclosures by sending an email to us at <a href="mailto:privacy@InternetMoney.io">privacy@InternetMoney.io</a>.</p>
              </li>
              <li>
                <p><strong>NEVADA PRIVACY DISCLOSURES</strong></p>
                <p>If you are a Nevada resident, you have the right to request that we do not sell your covered information (as those terms are defined in N.R.S. 603A) that we have collected, or may collect, from you. We do not sell your covered information, however, if you would like to make such a request you may do so by contacting us at <a href="mailto:privacy@InternetMoney.io">privacy@InternetMoney.io</a>.</p>
              </li>
              <li>
                <p><strong>ADDITIONAL EUROPEAN DISCLOSURES</strong></p>
                <p>IF YOU ARE SITUATED IN THE EUROPEAN ECONOMIC AREA, SWITZERLAND, OR THE UNITED KINGDOM, THIS SECTION APPLIES TO OUR COLLECTION, USE, AND DISCLOSURE OF YOUR PERSONAL DATA AND ADDITIONAL RIGHTS YOU HAVE UNDER APPLICABLE LAW.</p>
                <p><strong>Legal Basis:</strong></p>
                <p>We will only use your personal data, as that term is defined under the General Data Protection Regulation (“GDPR”), when the law allows us to. Most commonly, we will use your personal data in the following circumstances:</p>
                <ul>
                  <li>Where we need to perform the contract we are about to enter into or have entered into with you.</li>
                  <li>Where it is necessary for our legitimate interests (or those of a third party) and your interests and fundamental rights do not override those interests.</li>
                  <li>Where you have consented to a certain use of your personal data.</li>
                  <li>Where we need to comply with a legal or regulatory obligation.</li>
                </ul>
                <p>To the extent permitted under applicable laws, we will also process, transfer, disclose, and preserve personal data when we have a good faith belief that doing so is necessary. </p>
                <p><strong>Data controller:</strong></p>
                <p>Company is the data controller of all personal data collected through our Services. To contact us, please see the section titled <em>Contact</em>.</p>
                <p>If you are situated in the EEA, Switzerland, or the UK and have any complaints regarding our privacy practices, you have the right to make a complaint at any time to your local supervisory authority. We would, however, appreciate the chance to deal with your concerns before you approach your supervisory authority, so please contact us in the first instance. If you have a complaint, please contact our privacy manager here: <a href="mailto:privacy@InternetMoney.io">privacy@InternetMoney.io</a>.</p>
                <p><strong>Provision of personal data and failure to provide personal data:</strong></p>
                <p>Where we need to collect personal data by law or under the terms of a contract we have with you, and you fail to provide that data when requested, we may not be able to perform the contract we have or are trying to enter into with you (for example, to provide you with our services). In this case, we may not be able to provide certain services to you.</p>
                <p><strong>Collection of personal data from third-party sources:</strong></p>
                <p>We may obtain personal data and other information about you public sources and through our third-party partners who help us provide our products and services to you.</p>
                <p><strong>Withdrawing your consent:</strong></p>
                <p>If we are relying on your consent to process your personal data, you have the right to withdraw your consent at any time by contacting us at <a href="mailto:privacy@InternetMoney.io">privacy@InternetMoney.io</a>.</p>
                <p><strong>Data Transfer:</strong></p>
                <p>We may transfer personal data from the EEA, Switzerland, and the UK to the USA and other countries, some of which have not been determined by the European Commission or the UK to have an adequate level of data protection. Where we use certain service providers, we may use specific contracts approved by the European Commission or the UK Secretary of State which give personal data the same protection it has in Europe. For more information about how we transfer your data, please contact us at <a href="mailto:privacy@InternetMoney.io">privacy@InternetMoney.io</a>.</p>
                <p><strong>Use of your personal data for marketing purposes:</strong></p>
                <p>We strive to provide you with choices regarding certain personal data uses, particularly around marketing and advertising:</p>
                <ul>
                  <li>Promotional offers from us: We may use your personal data to form a view on what we think you may want or need, or what may be of interest to you. This is how we decide which products, services and offers may be relevant for you (we call this marketing). You will receive marketing communications from us if you have requested information from us or used our services and, in each case, you have consented to our use of your personal data for marketing purposes.</li>
                </ul>
                <p><strong>Data Subject Rights:</strong></p>
                <p>If you are situated in the EEA, Switzerland, or the UK, under the GDPR, as a data subject, you have the right to: </p>
                <ul>
                  <li><strong>Request access</strong> to your personal data (commonly known as a "data subject access request"). This enables you to receive a copy of the personal data we hold about you and to check that we are lawfully processing it.</li>
                  <li><strong>Request correction</strong> of the personal data that we hold about you. This enables you to have any incomplete or inaccurate data we hold about you corrected, though we may need to verify the accuracy of the new data you provide to us.</li>
                  <li><strong>Request erasure</strong> of your personal data. This enables you to ask us to delete or remove personal data where there is no good reason for us continuing to process it. You also have the right to ask us to delete or remove your personal data where you have successfully exercised your right to object to processing (see below), where we may have processed your information unlawfully, or where we are required to erase your personal data to comply with local law. Note, however, that we may not always be able to comply with your request of erasure for specific legal reasons which will be notified to you, if applicable, at the time of your request.</li>
                  <li><strong>Object to processing</strong> of your personal data where we are relying on a legitimate interest (or those of a third party) and there is something about your particular situation which makes you want to object to processing on this ground as you feel it impacts on your fundamental rights and freedoms. You also have the right to object where we are processing your personal data for direct marketing purposes. In some cases, we may demonstrate that we have compelling legitimate grounds to process your information which override your rights and freedoms.</li>
                  <li><strong>Request restriction of processing</strong> of your personal data. This enables you to ask us to suspend the processing of your personal data in the following scenarios: (a) if you want us to establish the data's accuracy; (b) where our use of the data is unlawful but you do not want us to erase it; (c) where you need us to hold the data even if we no longer require it as you need it to establish, exercise, or defend legal claims; or (d) you have objected to our use of your data but we need to verify whether we have overriding legitimate grounds to use it.</li>
                  <li><strong>Request the transfer</strong> of your personal data to you or to a third party. We will provide to you, or a third party you have chosen, your personal data in a structured, commonly used, machinereadable format. Note that this right only applies to automated information which you initially provided consent for us to use or where we used the information to perform a contract with you.</li>
                  <li><strong>Withdraw consent at any time</strong> where we are relying on consent to process your personal data. However, this will not affect the lawfulness of any processing carried out before you withdraw your consent. If you withdraw your consent, we may not be able to provide certain products or services to you. We will advise you if this is the case at the time you withdraw your consent.</li>
                </ul>
                <p>To exercise your rights under the GDPR, please contact us at <a href="mailto:privacy@InternetMoney.io">privacy@InternetMoney.io</a>. Please note that in order for you to assert these rights, we may need to verify your identity to confirm your right to access your personal data. This is a security measure to ensure that personal data is not disclosed to any person 4258641.3 034162-9999-000 who has no right to receive it. In order to verify your identity, we may need to gather more personal data from you than we currently have.</p>
              </li>
              <li>
                <p><strong>CHANGES TO POLICY</strong></p>
                <p>We reserve the right, at our discretion, to change, modify, add, or remove portions from this Privacy Policy at any time, provided that any such modifications will only be applied prospectively. We encourage you to periodically review the Website and App for the latest information on our privacy practices. Your continued use of the Services following the posting of any changes to this policy means you accept such changes.</p>
              </li>
              <li>
                <p><strong>CONTACT</strong></p>
                <p>For questions or concerns relating to privacy, we can be contacted at: <a href="mailto:privacy@InternetMoney.io">privacy@InternetMoney.io</a>.</p>
              </li>
            </ol>
          </div>                
        </div>    
      </div>
    </React.Fragment>
  );
}
