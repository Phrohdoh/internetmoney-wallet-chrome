/* istanbul ignore file */
import React, { useState } from 'react';
import { Strings } from 'resources';

// Components 
import Header from 'components/Header';
import InputField from 'components/InputField';
import Spinner from 'components/Spinner';
import Dropdown from 'components/Dropdown';
import Modal from 'components/Modal';
import AccountListitem from 'components/AccountListitem';

import { pair, unpair } from 'utils/walletConnect';
import { setItem } from 'utils/storage';

import { useSelector, useDispatch } from 'react-redux';
import { WalletConnectAction } from 'redux/slices/walletConnectSlice';

import { STORAGE_KEYS } from '../../../constants';

import './wallet-connect.scss';
import { getAccountList, getSelectedWalletConnectAccount } from 'redux/selectors';
import { useWalletConnect } from 'contexts/WalletConnectContext';

export default function WalletConnected (_props) {

  const initialValues = {
    walletAddress: '',
  };

  const dispatch = useDispatch();

  const accountList = useSelector(getAccountList);
  const selectedAccount = useSelector(getSelectedWalletConnectAccount);
  const { sessions } = useWalletConnect();

  const [isValidatingAddress, setIsValidatingAddress] = useState(false);
  const [values, setValues] = useState(initialValues);

  const [isShowAccountList, setIsShowAccountList] = useState(false);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const _selecteAccount = async item => {
    setIsShowAccountList(false);
    await setItem(
      STORAGE_KEYS.WALLET_CONNECTED_ACCOUNT,
      JSON.stringify(item.publicAddress),
    );
    dispatch(WalletConnectAction.updateSelectedAccount(item.publicAddress));
  };

  const onPaste= async (uri) => {
    await pair(uri);
  };

  const renderSession = (session, index) => {
    return (
      <div key={ index }  className="con-in">
        <div className="icon">
          <img src={ session.peer.metadata.icons?.[0] } alt="" />
        </div>
        <div className="body">
          <span className='title-in'>{session.peer.metadata.name}</span>
        </div>
        <button className='btn-in button' onClick={ () => unpair(session.topic) }>{Strings.DISCONNECT}</button>
      </div>
    );
  };

  const renderSessions= () => {
    return (
      <div className="connected-list">
        <h3 className="text-theme-color">{Strings.CONNECTED_WALLET}</h3>
        {sessions.filter(session => !session.disconnected).map(renderSession)}
      </div>
    );
  };

  // render Create Account View
  const renderAccounts = (lists) => {
    return (
      <React.Fragment>
        <div className="modal-header">
          <h6 className="text-center">{Strings.CHOOSE_ACCOUNTS_UPPERCASE}</h6>
        </div>
        <div className="modal-body hide-right-details">
          {
            lists.map((item, index) => (
              <AccountListitem
                dataTestid={ `reedem-account-cell-${index}` }
                key={ index }
                item={ item }
                index={ index }
                onSelectAccount={ (item, index) =>
                  _selecteAccount(item, index)
                }
              />
            ))
          }
        </div>
      </React.Fragment>
    );
  };

  return (
    <div className="container">
      <Header
        headerTitle={ Strings.VIEW_CONNECTED_APPS }
        support
      />
      <div className="content-background ds-flex-col full-flex">
        <div className="wrapper">
          <Dropdown
            dataTestid="gift-reedem-choose-account-button"
            label={ Strings.CHOOSE_ACCOUNTS_UPPERCASE }
            value={ selectedAccount?.name }
            onClick={ (e) => { e.preventDefault(); setIsShowAccountList(true); } }
          />
          <InputField 
            placeholder={ Strings.ENTER_WALLET_URL }
            name="walletAddress"
            value={ values.walletAddress }
            onChange={ handleInputChange }
            // onBlur={ (_text) => onPaste(values.walletAddress) }
            icon={ isValidatingAddress ? <Spinner /> : null }
            className="frm-with-loader form-connect"
            rightButtonLabel={ Strings.CONNECT }
            rightButtonClick={ (_text) => onPaste(values.walletAddress) }
          >
          </InputField>
          {renderSessions()}
        </div>
      </div>
      {isShowAccountList ?
        <Modal
          isOpen={ isShowAccountList }
          dataTestid="accounts-modal"
          closeModal={ () => setIsShowAccountList(false) }
        >
          <div className="modal-body">
            {renderAccounts(accountList)}
          </div>
        </Modal>
        : null}
    </div>
  );
}
