import React, { useState } from 'react';
import { Strings } from 'resources';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { checkPasswordSecurely } from 'web3-layer';

//  Utils
import { Validation } from 'utils/validations';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import InputField from 'components/InputField';
import Passcode from 'components/Passcode';

// Images
import visibleIcon from 'assets/images/visible.svg';
import invisibleIcon from 'assets/images/invisible.svg';

import './reveal-seed-phrase-verification.scss';
import { GlobalAction } from 'redux/slices/globalSlice';

export default function RevealSeedPhraseVerification () {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const wallet = useSelector(state => state.wallet);
  
  const { isPasscode } = wallet;
  const [passwordType, setPasswordType] = useState('password');
  const [passcode, setPasscode] = useState('');

  const initialValues = {
    password: '',
  };
  const [values, setValues] = useState(initialValues);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const togglePassword = () => {
    if (passwordType === 'password') {
      setPasswordType('text');
      return;
    }
    setPasswordType('password');
  };
  
  const checkPasscodeValidation = () => {
    if (passcode.length < 6) {
      dispatch(
        GlobalAction.showAlert(
          Strings.ERROR,
          Strings.ENTER_VALID_PASSCODE_MSG,
        )
      );
      return false;
    } else {
      return true;
    }
  };
  // check all validation
  const checkPasswordValidation = () => {
    const isValidPassword = Validation.isValidPassword(values.password);

    const matchPassword = values.password === wallet.password;
    if (isValidPassword && matchPassword) {
      return true;
    } else {
      /* istanbul ignore else */
      if (Validation.isEmpty(values.password)) {
        dispatch(
          GlobalAction.showAlert(
            Strings.ERROR,
            Strings.ENTER_PASSWORD,
          )
        );
      } else if (!isValidPassword) {
        dispatch(
          GlobalAction.showAlert(
            Strings.ERROR,
            Strings.ENTER_VALID_PASS_MSG,
          )
        );
      } else if (!matchPassword) {
        dispatch(
          GlobalAction.showAlert(
            Strings.ERROR,
            Strings.LOGIN_VERIFY_PASSWORD_NOT_MATCH_MSG,
          )
        );
      }
      return false;
    }
  };
  
  // checkPassword
  const ischeckPassword = async (walletObj, password) => {   
    const checkPass = await checkPasswordSecurely(
      password,
      walletObj.passwordHash,
    );
    return checkPass;
  };


  const _onPressContinue = async (e) => {
    e.preventDefault();
    if (isPasscode ? checkPasscodeValidation() : checkPasswordValidation()) {
      const checkPass = await ischeckPassword(
        wallet.walletDetail,
        isPasscode ? passcode : values.password,
      );
      if (checkPass === true) {
        navigate('/your-secret-seed-phrase',
          {
            state: {
              wallet: wallet.walletDetail,
              password: isPasscode ? passcode : values.password,              
              isSetting: true,
            }
          });
      } else {
        dispatch(
          GlobalAction.showAlert(
            Strings.ERROR,
            isPasscode
              ? Strings.INVALID_PASSCODE_MSG
              : Strings.INVALID_PASSWORD_MSG,
          )
        );
      }
    }  
    setPasscode('');
  };

  const handlePasscode = item => {
    const tempCode = passcode.split('');
    if (item.value === 'remove') {
      if (tempCode.length) {
        tempCode.pop();
      }
    } else {
      if (tempCode.length < 6) {
        tempCode.push(item.value);
      }
    }
    const strPasscode = tempCode.join('');
    setPasscode(strPasscode);
  };

  const renderPasswordVerification = () => (
    <div className="content-area">
      <h2 className="text-theme-color uppercase mb-0">{Strings.REVEAL_PRIVATE_KEY_UPPERCASE}</h2>
      <p className="opacity scam-alert-msg">{Strings.SEED_PHRASE_SCAM_MSG_UPPERCASE}</p>
      <p className="opacity">{Strings.SEED_PHRASE_PASSWORD_VERIFICATION_MSG}</p>
      <InputField
        type={ passwordType === 'password' ? 'password' : 'text' }
        label={ Strings.PASSWORD }
        value={ values.password }
        onChange={ handleInputChange }
        name="password"
        dataTestid='password'
        onIconClick={ togglePassword }
        icon={ passwordType === 'password' ? <img src={ visibleIcon } alt="visible" /> : <img src={ invisibleIcon } alt="invisible" /> }
      /> 
    </div>
  );

  const renderPasscodeVerification = () => {
    return (
      isPasscode && (
        <React.Fragment>
          <Passcode
            title={ Strings.PASSCODE }
            passcode={ passcode }
            onClickKey={ item => handlePasscode(item) }
          />
        </React.Fragment>
      ));
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header />
        
        <div className="wrapper">
          <div className="full-flex">
            {isPasscode ? renderPasscodeVerification() : renderPasswordVerification()}
          </div>
          <Button
            onClick={ (e) => _onPressContinue(e) }
            label={ Strings.CONTINUE_UPPERCASE }
            className="btn-full"
          />          
        </div>
      </div>
    </React.Fragment>
  );
}
