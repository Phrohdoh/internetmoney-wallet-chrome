import React, { useState } from 'react';
import { Strings } from 'resources';
import Logger, { getLogsEnabled, setLogsEnabled } from 'utils/logger';

// Components
import Header from 'components/Header';
import Dropdown from 'components/Dropdown';
import Modal from 'components/Modal';

import './data-collection.scss';

export default function DataCollection () {
  const [showList, setShowList] = useState(false);
  const [options] = useState(Strings.DATA_COLLECTION_LIST);
  const [selectedOption, setSelectedOption] = useState(getLogsEnabled() ? options[1] : options[0]);

  const renderTitleView = (title, subtitle) => {
    return (
      <div className="title-header">
        <h5 className="text-theme-color uppercase">{title}</h5>
        <span className="text-style">{subtitle}</span>
      </div>
    );
  };

  const renderOptions = (options) => {
    return (
      <React.Fragment>
        <div className="modal-header">
          <h6 className="text-center">{Strings.LOGS_AND_ERROR_REPORTING}</h6>
        </div>
        <div className="modal-body">
          <div className="options-list">
            {
              options.map((item, index) => (
                <div 
                  key={ item.key }
                  data-testid={ `data_collection_option_${index}` } 
                  onClick={ () => {
                    setShowList(false);
                    setSelectedOption(item);
                    setLogsEnabled(item.key === 'enable')
                   } }
                  className={ 'listitem-control noselect' }
                >
                  <div className="body">
                    <h6>{item.title}</h6>
                  </div>
                </div>
              ))
            }
          </div>
        </div>
      </React.Fragment>

    );
  };

  return (
    <div className="container">
      <Header 
        headerTitle={ Strings.SENDING_LOGS_UPPERCASE }
        support
      />
      <div className="content">            
        <div className="content-background ds-flex-col full-flex">
          <div className="wrapper">
            {renderTitleView(Strings.LOGS_AND_ERROR_REPORTING, Strings.DATA_COLLECTION_MSG)}
            <Dropdown
              dataTestid="general-settings"
              value={ selectedOption.title }
              className="text-gray-theme"
              onClick={ (e) => { 
                e.preventDefault(); 
                setShowList(true); 
              } }
            />            
          </div>
          {showList ?
            <Modal
              isOpen={ showList }
              closeModal={ () => setShowList(false) }
            >
              {renderOptions(options)}
            </Modal>
            : null}
        </div>               
      </div>    
    </div>
  );
}
