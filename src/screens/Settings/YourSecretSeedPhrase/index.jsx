import React, { useState, useEffect } from 'react';

import { useLocation, useNavigate } from 'react-router-dom';
import { decryptMnemonicSecurely } from 'web3-layer';

import { Strings } from 'resources';

import copy from 'copy-to-clipboard';
import toast from 'react-hot-toast';

// Components
import Header from 'components/Header';
import Button from 'components/Button';

import './your-secret-seed-phrase.scss';

const HIDDEN_SEED_PHRASE_PLACEHOLDER = '**********';

export default function YourSecretSeedPhrase () {

  const [isHidden, setIsHidden] = useState(true);
  const [phraseArray, setSecretPhrase] = useState([]);

  const navigate = useNavigate();
  const params = useLocation();

  useEffect(() => {
    getSecretPhrase();
  }, []);
  
  const getSecretPhrase = async () => {
    const mnemonicObject = await decryptMnemonicSecurely(
      params.state.wallet.mnemonic,
      params.state.password,
    );
    const phrases = mnemonicObject.mnemonic.split(' ');
    setSecretPhrase(phrases);
  };

   
  const copyToClipboard = (e) => {
    e.preventDefault();
    copy(phraseArray.join(' '));
    toast(Strings.SEED_PHRASE_COPIED_SUCCESS);
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header />
        <div className="content">            
          <div className="content-area">
            <h2 className="text-theme-color uppercase mb-0">{Strings.YOUR_SEED_PHRASE_UPPERCASE}</h2>
            <p className="scam-alert-msg">{Strings.SEED_PHRASE_24_WORDS_MSG}</p>
            {phraseArray && 
              <div className="tags">
                {phraseArray.map((item, index) => (
                  <div key={ index } className="tag">
                    <span>{index+ 1}</span>
                    <div className="tagStyle"><div>{isHidden ? HIDDEN_SEED_PHRASE_PLACEHOLDER : item}</div></div>
                  </div>
                ))}
              </div>
            }
          </div>                
        </div>
        <div className="content-footer">

          <Button
            className="btn-full"
            label={ Strings.COPY_CLIPBOARD_UPPERCASE }
            onClick={ (e) => copyToClipboard(e) }
          />

          <br />

          <Button
            className="btn-full"
            label={ isHidden ? Strings.SHOW_SEED_PHRASE_UPPERCASE : Strings.HIDE_SEED_PHRASE_UPPERCASE }
            onClick={ (e) => {
              e.preventDefault();
              setIsHidden(!isHidden);
            } }
          />

          <br />

          <Button
            outline
            className="btn-full"
            label={ Strings.GO_BACK_BUTTON_UPPERCASE }
            onClick={ (e) => {
              e.preventDefault();
              navigate(-3); // back to settings
            } }
          />
        </div>
      </div>
    </React.Fragment>
  );
}
