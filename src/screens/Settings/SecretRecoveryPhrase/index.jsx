import React from 'react';
import { Strings } from 'resources';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

// Components
import Header from 'components/Header';
import Button from 'components/Button';

import './secret-recovery-phrase.scss';

export default function SecretRecoveryPhrase () {

  const wallet = useSelector(state => state.wallet);
   
  const navigate = useNavigate();

  const onClickPasswordVerification = (e) => {
    e.preventDefault();
    navigate('/reveal-seed-phrase-verification',
      {
        state: {
          password: wallet.password,
          wallet: wallet.walletDetail,
          isSetting: true,
        }
      });
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header />
        <div className="content">            
          <div className="content-area">
            <h2 className="text-theme-color uppercase">{Strings.SEED_PHRASE}</h2>
            <p className="opacity">{Strings.RECOVERY_PHRASE_MSG}</p>
            <p className="opacity scam-alert-msg">{Strings.SEED_PHRASE_SCAM_MSG_UPPERCASE}</p>
          </div>                
        </div>
        <div className="content-footer">
          <Button 
            label={ Strings.REVEAL_SEED_PHRASE_UPPERCASE }
            className="btn-full" 
            onClick={ (e) => onClickPasswordVerification(e) }
          />
        </div>
      </div>
    </React.Fragment>
  );
}
