/* istanbul ignore file */
import { useEffect, useState } from 'react';
import { Strings } from 'resources';

// utils
import { useTokenIcons } from 'utils/hooks';
import { setItem } from 'utils/storage';
import {
  approveTransactionRequest,
  rejectTransaction,
} from 'utils/walletConnect';
import { displayAddress } from 'utils/displayAddress';
import moment from 'moment';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import Spinner from 'components/Spinner';
import Modal from 'components/Modal';
import EstimatedGasFee from 'components/EstimatedGasFee';
import AlertModel from 'components/AlertModel';

import { useLocation, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { TransactionsAction } from 'redux/slices/transactionsSlice';
import { STORAGE_KEYS } from '../../../constants';
import './dapp-transactions.scss';

import {
  getSingleAccount,
  handleDappTxnObject,
  calculateEstimateFee,
  calculateMaxFee,
  updateTxnLists,
  createAndSendTxn,
  WalletTransactionTypes,
} from 'web3-layer';

// Images
import downArrow from 'assets/images/down-arrow.svg';
import dropIcon from 'assets/images/drop.svg';
import {
  getHardwareDeviceDetails,
} from 'web3-layer/hardwareWeb3Layer';
import IntegerUnits from 'utils/IntegerUnits';
import { useWalletConnect } from 'contexts/WalletConnectContext';
import { getNetwork, getSelectedWalletConnectAccount } from 'redux/selectors';
import { getUserFriendlyErrorMessage } from 'utils/errors';

export default function DappTransactions (_props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const params = useLocation();
  const getTokenIcon = useTokenIcons();

  const wallet = useSelector((state) => state.wallet);
  const accounts = useSelector((state) => state.account);
  const transactions = useSelector((state) => state.transactions);
  const selectedAccount = useSelector(getSelectedWalletConnectAccount);
  const { sessions } = useWalletConnect();
  const selectedNetwork = useSelector(getNetwork);


  const transaction = params.state.transaction;
  const isV2 = typeof transaction.topic === 'string';
  const peerMeta = isV2
    ? sessions.find(s => s.topic === transaction.topic).peer.metadata
    : transaction.connection.peerMeta;

  const [selectedRange, setSelectedRange] = useState(1);
  const [txnObject, setTxnObject] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [gasRange, setGasRange] = useState({});
  const [accountBalance, setAccountBalance] = useState(0);

  const [error, setError] = useState(false);
  const [isShowAlert, setIsShowAlert] = useState(false);
  const [isShowEstimatedGasFee, setIsShowEstimatedGasFee] = useState(false);
  const [isHardwareWallet, setIsHardwareWallet] = useState(undefined);

  useEffect(() => {
    getTxnObject();
    getAccountBalance();
    getHardwareAccountObject();
  }, []);

  const getAccountBalance = async () => {
    const balance = await getSingleAccount(
      selectedAccount.publicAddress,
      selectedNetwork
    );
    setAccountBalance(balance.account.value);
  };

  const getTxnObject = async () => {
    setIsLoading(true);
    const dappObject = await handleDappTxnObject(
      transaction,
      selectedAccount.publicAddress,
      wallet.walletDetail.walletObject,
      wallet.password,
      selectedNetwork
    );
    if (dappObject.success) {
      setGasRange(dappObject.range);
      setIsLoading(false);
      setTxnObject(dappObject.txnObject);
    } else {
      navigate(-1);
    }
  };

  const getHardwareAccountObject = () => {
    const isExistAccounts = accounts.accountList.find(
      (accObj) =>
        selectedAccount.publicAddress.toLowerCase() ===
        accObj.publicAddress.toLowerCase()
    );
    if (isExistAccounts.isHardware) {
      const getHardwareObject = getHardwareDeviceDetails(
        isExistAccounts.isHardware
      );
      setIsHardwareWallet({
        ...getHardwareObject,
        isHardware: isExistAccounts.isHardware,
      });
    }
  };

  const updateTxnObjectTypeTwo = (gas, maxPrioFee, maxFeePerGas) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.maxPriorityFeePerGas = maxPrioFee;
    tempTxObj.maxFeePerGas = maxFeePerGas;
    setTxnObject(tempTxObj);
  };

  const updateTxnObjectTypeOne = (gas, gasPrice) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.gasPrice = gasPrice;
    setTxnObject(tempTxObj);
  };

  const getEstimateFee = () => {
    if (!txnObject) return '';
    const estimateFee = calculateEstimateFee(txnObject, selectedNetwork.txnType);
    return `${estimateFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getMaxFee = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getTotal = () => {
    if (!txnObject) return '';
    return `${txnObject.value?.toString(6)} ${selectedNetwork.sym} + ${getEstimateFee()}`;
  };

  const getMaxTotal = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${txnObject.value?.toString(6)} ${selectedNetwork.sym} + ${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const clickToSendDapp = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const isHardware = !!selectedAccount.isHardware;

    try {
      const txnResult = await createAndSendTxn(
        WalletTransactionTypes.DAPP,
        txnObject,
        wallet,
        selectedAccount,
        accountBalance,
        selectedNetwork
      );

      if (txnResult.success) {
        const { tempTxns, tempTxnsList } = updateTxnLists(
          IntegerUnits.serialize({
            type: WalletTransactionTypes.DAPP,
            transactionDetail: {
              accountName: selectedAccount.name,
              displayAmount: txnObject.value,
              displayDecimal: 18,
              transferedAmount: txnObject.value,
              toAddress: txnObject.to,
              fromAddress: txnObject.from,
              isSupportUSD: false,
              networkDetail: selectedNetwork,
              dappObject: txnObject,
              fromToken: { symbol: selectedNetwork.sym },
              dappTransactionDetail: transaction,
              utcTime: moment.utc().format(),
            },
            receipt: txnResult.receipt,
            blockNumber: txnResult.blockNumber,
          }),
          transactions,
          selectedNetwork
        );

        dispatch(TransactionsAction.saveTransactions(tempTxnsList));
        setItem(STORAGE_KEYS.TRANSACTIONS, JSON.stringify(tempTxns));
        setIsLoading(false);
        approveTransactionRequest(
          transaction,
          txnResult.receipt.transactionHash,
        );
        navigate(-1);
      } else {
        setIsLoading(false);
        if (!isHardware) {
          setError(txnResult.error);
          setIsShowAlert(true);
        } else {
          if (
            txnResult.error ===
            'Ledger device: Condition of use not satisfied (denied by the user?) (0x6985)' ||
            txnResult.error === 'Action cancelled by user'
          ) {
            setError('');
            rejectTransaction(transaction);
          } else {
            setIsLoading(false);
            setError(txnResult.error);
            setIsShowAlert(true);
          }
        }
      }
    } catch (err) {
      setIsLoading(false);
      console.error(err);
    }
  };

  const onClickSuccess = () => {
    setIsShowAlert(false);
    navigate(-1);
  };

  const renderContentHeader = () => {
    return (
      <div className="send-token-header text-center">
        <p>{Strings.CURRENT_NETWORK}</p>
        <h6 className="mb-0">{selectedNetwork.networkName}</h6>
      </div>
    );
  };

  const headingStyle = () => {
    return (
      <div className="heading-styles heading-dashed-styles">
        <span className="line" />
        <span className="line" />
        <h6 className="text-center uppercase">
          {Strings.CONFIRM_TRANSACTION_UPPERCASE}
        </h6>
        <span className="line" />
        <span className="line" />
      </div>
    );
  };

  const renderAccountView = () => {
    return (
      <div className="send-details">
        <div className="from-to-pattern">
          <img src={ downArrow } alt="" className="from-to-arrow" />
        </div>
        <div className="send-row">
          <div className="body">
            <small>{Strings.FROM_TEXT}</small>
            <h6 className="text-gray-color">{peerMeta.name}</h6>
          </div>
          <div className="right text-right">
            <h6 className="uppercase">{selectedNetwork.sym}</h6>
            <span>
              <small>{Strings.BALANCE}</small> {accountBalance.toString(6)}
            </span>
          </div>
          <div className="icon">
            <img src={ getTokenIcon('', selectedNetwork.chainId) } alt={ '' } />
          </div>
        </div>
        <div className="send-row">
          <div className="body">
            <small>{Strings.TO_TEXT}</small>
            <h6 className="text-gray-color text-eclipse-style">
              {displayAddress(txnObject?.to)}
            </h6>
          </div>
        </div>
      </div>
    );
  };

  const dappDetailHeader = () => {
    return (
      <div className="dapp-detail-header">
        <h3>{Strings.DAPP}</h3>
        <p className="text-theme-color">{peerMeta.name}</p>
      </div>
    );
  };

  const SendTransferAmount = () => {
    return (
      <div className="send-amount">
        <small>{Strings.AMOUNT}</small>
        <h2 className="uppercase">
          {txnObject?.value?.toString(6)}
          {' '}
          {selectedNetwork.sym}
          <img src={ getTokenIcon('', selectedNetwork.chainId) } alt="" />
        </h2>
      </div>
    );
  };

  const SendTokenEstimated = () => {
    return (
      <div className="send-estimated">
        <div className="send-row">
          <div className="body">
            <h6>{Strings.ESTIMATE_GAS_FEE}</h6>
            <small className="text-green-color opacity">
              {Strings.LIKELY_SECONDS}
            </small>
          </div>
          <div className="right text-right">
            <h6
              className="uppercase text-theme-color"
              onClick={ () => setIsShowEstimatedGasFee(true) }
            >
              {getEstimateFee()} <img src={ dropIcon } alt="" />
            </h6>
            {selectedNetwork.txnType == 2 && (
              <span>
                <small className="text-gray-color3">{Strings.MAX_FEE}</small>{' '}
                {getMaxFee()}
              </span>
            )}
          </div>
        </div>
        <div className="send-row">
          <div className="body">
            <h6>{Strings.TOTAL}</h6>
          </div>
          <div className="right text-right">
            <h5 className="uppercase">{getTotal()}</h5>
            {selectedNetwork.txnType == 2 && (
              <span>
                <small className="text-gray-color3">{Strings.MAX_FEE}</small>{' '}
                {getMaxTotal()}
              </span>
            )}
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="container">
      <Header headerTitle={ Strings.DAPP_TRANSACTION_UPPERCASE } support />
      {renderContentHeader()}
      <div className="shape-rounded"></div>
      <div className="wrapper ct-wrapper">
        <div className="ct-container">
          <div className="content">
            {headingStyle()}
            {dappDetailHeader()}
            {renderAccountView()}
            {SendTransferAmount()}
            {SendTokenEstimated()}
          </div>
          {isShowAlert && (
            <AlertModel
              isShowAlert={ isShowAlert }
              onPressSuccess={ () => onClickSuccess() }
              className="text-center"
            >
              <p>{getUserFriendlyErrorMessage(error)}</p>
            </AlertModel>
          )}
        </div>
      </div>
      <div className="content-footer">
        {!isLoading ? (
          <Button
            onClick={ (e) => clickToSendDapp(e) }
            label={ Strings.CLICK_TO_SEND_DAPP }
          />
        ) : (
          <Spinner />
        )}
      </div>
      {isShowEstimatedGasFee ? (
        <Modal
          isOpen={ isShowEstimatedGasFee }
          closeModal={ () => setIsShowEstimatedGasFee(false) }
        >
          <EstimatedGasFee
            selectedRange={ selectedRange }
            gasRange={ gasRange }
            txnObject={ txnObject }
            network={ selectedNetwork }
            onClickSave={ (updatedObject) => {
              setIsShowEstimatedGasFee(false);

              if (selectedNetwork.txnType != 0) {
                updateTxnObjectTypeTwo(
                  updatedObject.gas,
                  updatedObject.maxPrioFee,
                  updatedObject.maxFeePerGas
                );
                setSelectedRange(selectedRange);
              } else {
                updateTxnObjectTypeOne(
                  updatedObject.gas,
                  updatedObject.gasPrice
                );
              }
            } }
          />
        </Modal>
      ) : null}
    </div>
  );
}
