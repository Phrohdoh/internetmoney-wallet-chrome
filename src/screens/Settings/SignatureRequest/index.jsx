/* istanbul ignore file */
import { useEffect, useState } from 'react';
import { Strings } from 'resources';
import { useLocation, useNavigate } from 'react-router-dom';

// utils
import { useTokenIcons } from 'utils/hooks';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import Spinner from 'components/Spinner';

import { useDispatch, useSelector } from 'react-redux';
import './signature-request.scss';
import {
  rejectTransaction,
  approveTransactionRequest,
} from 'utils/walletConnect';
import { getSingleAccount, getSignMsg, handleDappTxnObject } from 'web3-layer';
import {
  getHardwareDeviceDetails,
  handleDappTxnObjectInHDW,
} from 'web3-layer/hardwareWeb3Layer';
import { useWalletConnect } from 'contexts/WalletConnectContext';
import { getNetwork, getSelectedWalletConnectAccount } from 'redux/selectors';
import { GlobalAction } from 'redux/slices/globalSlice';

const getMeta = (transaction, sessions) => {
  return sessions.find(s => s.topic === transaction.topic)?.peer.metadata;
};

export default function SignatureRequest (_props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const params = useLocation();

  const wallet = useSelector((state) => state.wallet);
  const selectedAccount = useSelector(getSelectedWalletConnectAccount);
  const accounts = useSelector((state) => state.account);
  const { sessions } = useWalletConnect();
  const selectedNetwork = useSelector(getNetwork);

  const transaction = params.state.transaction;
  const peerMeta = getMeta(transaction, sessions);
  const getTokenIcon = useTokenIcons();

  const [signMsg, setSignMsg] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [accountBalance, setAccountBalance] = useState();
  const [isHardwareWallet, setIsHardwareWallet] = useState(undefined);

  useEffect(() => {
    getSignMsgDetails();
    getAccountBalance();
    getHardwareAccountObject();
  }, []);

  const getAccountBalance = async () => {
    const balance = await getSingleAccount(
      selectedAccount.publicAddress,
      selectedNetwork
    );
    setAccountBalance(balance.account.value);
  };

  const getHardwareAccountObject = async () => {
    const isExistAccounts = accounts.accountList.find(
      (accObj) =>
        selectedAccount.publicAddress.toLowerCase() ===
        accObj.publicAddress.toLowerCase()
    );
    if (isExistAccounts.isHardware) {
      const getHardwareObject = await getHardwareDeviceDetails(
        isExistAccounts.isHardware
      );
      setIsHardwareWallet({
        ...getHardwareObject,
        isHardware: isExistAccounts.isHardware,
      });
    }
  };

  const getSignMsgDetails = async () => {
    if (typeof transaction.message === 'string') {
      setSignMsg(transaction.message);
      return;
    }
    setIsLoading(true);
    const signObject = await getSignMsg(
      transaction.params.request,
      selectedNetwork,
    );
    if (signObject.success) {
      setSignMsg(signObject.signMsg);
    } else {
      setSignMsg(signObject.error);
    }
    setIsLoading(false);
  };

  const signHardwareTransaction = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const dappObject = await handleDappTxnObjectInHDW(
      transaction,
      isHardwareWallet.isHardware,
      selectedNetwork
    );
    setIsLoading(false);
    if (dappObject.success) {
      approveTransactionRequest(
        transaction,
        dappObject.signResult,
      );
      navigate(-1);
    } else {
      if (
        dappObject.error ===
        'Ledger device: Condition of use not satisfied (denied by the user?) (0x6985)' ||
        dappObject.error === 'Action cancelled by user'
      ) {
        rejectTransaction(transaction);
        navigate(-1);
        return;
      }
      dispatch(
        GlobalAction.showAlert(
          Strings.ERROR,
          dappObject.error,
        )
      );
    }

  };

  const signTransaction = async (e) => {
    e.preventDefault();
    setIsLoading(true);

    const dappObject = await handleDappTxnObject(
      transaction,
      selectedAccount.publicAddress,
      wallet.walletDetail.walletObject,
      wallet.password,
      selectedNetwork
    );
    setIsLoading(false);
    if (dappObject.success) {
      approveTransactionRequest(
        transaction,
        dappObject.signResult,
      );
    }
    navigate(-1);
  };

  const renderContentHeader = () => {
    return (
      <div className="send-token-header text-center">
        <p>{Strings.CURRENT_NETWORK}</p>
        <h6 className="mb-0">{selectedNetwork.networkName}</h6>
      </div>
    );
  };

  const headingStyle = () => {
    return (
      <div className="heading-styles heading-dashed-styles">
        <span className="line" />
        <span className="line" />
        <h6 className="text-center uppercase">
          {Strings.CONFIRM_SIGNATURE_UPPERCASE}
        </h6>
        <span className="line" />
        <span className="line" />
      </div>
    );
  };

  const renderAccountView = () => {
    return (
      <div className="send-details render-acc-details">
        <div className="send-row">
          <div className="body">
            <small>{Strings.FROM_TEXT}</small>
            <h6 className="text-gray-color">
              {selectedAccount.name}
            </h6>
          </div>
          <div className="right text-right">
            <h6 className="uppercase">{selectedNetwork.sym}</h6>
            <span>
              <small>{Strings.BALANCE}</small> {accountBalance?.toString(6)}
            </span>
          </div>
          <div className="icon">
            <img src={ getTokenIcon('', selectedNetwork.chainId) } alt={ '' } />
          </div>
        </div>
      </div>
    );
  };

  const renderOriginView = () => {
    return (
      <div className="send-amount origin-container">
        <h4 className="mb-0">
          <span>{`${Strings.ORIGIN}`}</span>
          <img src={ peerMeta.icons?.[0] } alt="" />
          <span className="text-theme-color">{peerMeta.url}</span>
        </h4>
      </div>
    );
  };

  const renderMessageView = () => {
    return (
      <div className="send-estimated msg-container ">
        <div className="send-row">
          <div className="body">
            <h4 className="text-theme-color text-center">
              {Strings.YOU_ARE_SIGNING}
            </h4>
            <h5>{Strings.MESSAGE}</h5>
            <p className="text-gray-color opacity">{signMsg}</p>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="container">
      <Header headerTitle={ Strings.SIGN_REQUEST_UPPERCASE } support />
      {renderContentHeader()}
      <div className="shape-rounded"></div>
      <div className="wrapper ct-wrapper">
        <div className="ct-container">
          <div className="content">
            {headingStyle()}
            {renderAccountView()}
            {renderOriginView()}
            {renderMessageView()}
          </div>
        </div>
      </div>

      <div className="footer-bottom inline-buttons">
        <Button
          label={ Strings.CANCEL }
          outline
          onClick={ (e) => {
            e.preventDefault();
            rejectTransaction(transaction);
            navigate(-1);
          } }
        />
        {!isLoading ? (
          <Button
            label={ Strings.SIGN }
            onClick={ (e) => {
              if (isHardwareWallet) {
                signHardwareTransaction(e);
              } else {
                signTransaction(e);
              }
            } }
          />
        ) : (
          <Spinner />
        )}
      </div>
    </div>
  );
}
