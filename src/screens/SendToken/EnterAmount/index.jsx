import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Strings } from 'resources';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { getMaxAmount } from 'web3-layer';

import { STORAGE_KEYS } from '../../../constants';

// utils
import { Validation } from 'utils/validations';
import { useTokenIcons } from 'utils/hooks';
import { setItem } from 'utils/storage';

// Components
import Header from 'components/Header';
import ProgressBarNumber from 'components/ProgressBarNumber';
import Dropdown from 'components/Dropdown';
import CurrencyInputField from 'components/CurrencyInputField';
import Button from 'components/Button';
import Modal from 'components/Modal';
import SelectTokenListCell from 'components/SelectTokenListCell';
import SegmentController from 'components/SegmentController';
import NetworkSelectionModal from 'components/NetworkSelectionModal';

import { NetworksAction } from 'redux/slices/networksSlice';
import { WalletAction } from 'redux/slices/walletSlice';
import { getNetwork, getTokensByAddress } from 'redux/selectors';

import './enter-amount.scss';
import IntegerUnits, { sanitizeFloatingPointString } from 'utils/IntegerUnits';
import { GlobalAction } from 'redux/slices/globalSlice';

export default function EnterAmount () {
  const navigate = useNavigate();
  const params = useLocation();
  const dispatch = useDispatch();
  const getTokenIcon = useTokenIcons();

  const wallet = useSelector(state => state.wallet);
  const selectedNetwork = useSelector(getNetwork);

  const steps = [1, 2, 3];
  const selectedSteps = 2;

  const accountDetail = params.state.fromAccount;
  const [currentIndex, setCurrentIndex] = useState(0);
  
  const [isShowChooseToken, setIsShowChooseToken] = useState(false);

  const [isShowNetworkList, setIsShowNetworkList] = useState(false);

  const [amountError, setAmountError] = useState('');

  const tokens = useSelector(getTokensByAddress)[accountDetail.publicAddress.toLowerCase()];
  const [selectedToken, setSelectedToken] = useState(tokens?.[0]);

  const tokenBalance = selectedToken?.balance?.value;
  const displayedBalance = tokenBalance === undefined
    ? ''
    : tokenBalance.toString(6, true);

  const usdValue = selectedToken?.usdPrice?.value;
  const isSupportUSD = usdValue !== undefined;

  const initialValues = {
    amount: '',
  };

  const [values, setValues] = useState(initialValues);

  const isUSD = currentIndex !== 0;

  const handleInputChange = (value) => {
    if(!value){
      setValues({
        amount: '',
      });
      return;
    }
    const result = sanitizeFloatingPointString(value);
    setValues({
      ...values,
      amount: result,
    });
    return result;
  };

  const getConvertedValue = () => {
    try {
      const amount = IntegerUnits.fromFloatingPoint(values.amount);
      if (!isUSD) {
        return `${Strings.USD_UPPERCASE} ${Strings.VALUE}: $${amount.multiply(usdValue).toString(2)}`;
      }
      return `${selectedToken.symbol} ${Strings.VALUE}: ${amount.divide(usdValue).toString(6)}`;
    } catch (e) {
      // Error
    }
  };

  const selectToken = token => {
    setIsShowChooseToken(false);
    setSelectedToken(token);
    return token;
  };

  const handleNext = async (e) => {
    e.preventDefault();
    try {
      if (!Validation.isEmpty(values.amount) && parseFloat(values.amount) > 0) {
        let sendAmount = IntegerUnits.fromFloatingPoint(values.amount);
        if (isUSD) {
          sendAmount = sendAmount.divide(usdValue, true);
        }
        sendAmount = sendAmount.toPrecision(selectedToken.decimals, true);

        if (sendAmount.gt(tokenBalance)) {
          setAmountError(Strings.SEND_AMOUNT_ERROR_MSG);
        } else {
          navigate('/confirm-transaction', {
            state: {
              fromAccount: accountDetail,
              fromAccountName: params.state.fromAccountName,
              toAccountAddress: params.state.toAccountAddress,
              toAccountName: params.state.toAccountName,
              selectedToken,
              transferAmount: sendAmount,
              isSupportUSD: isSupportUSD,
              usdValue: isSupportUSD ? usdValue : '',
              accountIndex: params.state.accountIndex,
            },
          });
        }
      } else {
        setAmountError(Strings.ENTER_VALID_AMOUNT_MSG);
      }
    } catch (e) {
      console.log('err >>>', e);
    }
  };

  const onClickMax = async (e) => {
    e.preventDefault();
    setValues({amount:''});
    const maxAmount = await getMaxAmount(
      1,
      selectedToken.address,
      selectedToken.decimals,
      tokenBalance,
      selectedNetwork,
    );

    if (maxAmount.success) {
      const baseMax = maxAmount.maxBalance;

      if (!isUSD) {
        setValues({
          amount: baseMax.toString(6, true, true)
        });
      } else {
        setValues({
          amount: baseMax.multiply(usdValue).toString(6, true, true)
        });
      }

      setAmountError('');
    } else {
      setAmountError(maxAmount.error);
    }
  };

  const changeNetwork = async choosedNetwork => {
    await setItem(
      STORAGE_KEYS.SELECTED_NETWORK,
      JSON.stringify(choosedNetwork),
    );
    dispatch(NetworksAction.saveSelectedNetworkObject(choosedNetwork));
    dispatch(WalletAction.resetData(wallet.walletDetail, choosedNetwork));

    setTimeout(() => {
      dispatch(
        GlobalAction.showAlert(
          Strings.NETWORK_SWITCH_TITLE,
          `${Strings.NETWORK_CHANGE_SUCCESS_MSG}${choosedNetwork.networkName}`,
        ),
      );
    }, 400);
  };

  const renderContentHeader = () => {
    return (
      <div className="send-token-header text-center">
        <div className="pointer" onClick={ () => setIsShowNetworkList(true) }>
          <p>{Strings.CURRENT_NETWORK}</p>
          <h6>{selectedNetwork.networkName}</h6>
        </div>
        <ProgressBarNumber steps={ steps } selectedSteps={ selectedSteps } />
      </div>
    );
  };

  // render Choose Token View
  const renderChooseToken = (tokenList) => {
    return (
      <React.Fragment>
        <div className="modal-header">
          <h6 className="text-center">{Strings.CHOOSE_TOKEN_UPPERCASE}</h6>
        </div>
        <div className="modal-body">
          {/* {accountSelectErrorMsg ? <span className="error text-center">{accountSelectErrorMsg}</span> : null} */}
          {tokenList.map((item, index) => (
            <SelectTokenListCell
              dataTestid={ `choose-token-${index}` }
              key={ index }
              item={ item }
              index={ index }
              networkObj={ selectedNetwork }
              accountAddress={ accountDetail }
              subtext={ Strings.BALANCE }
              onSelectToken={ (item, index) => selectToken(item, index) }
            />
          ))}
        </div>
      </React.Fragment>
    );
  };

  const renderAmountView = () => {
    const segmentList = [{ title: selectedToken.symbol }];
    if (isSupportUSD) {
      segmentList.push({ title: Strings.USD_UPPERCASE });
    }

    return (
      <div className="form-group">
        <div className="label-and-max">
          <label>{Strings.ENTER_AMOUNT}</label>
          <div className="max-btn text-right">
            <Button
              onClick={ (e) => onClickMax(e) }
              label={ Strings.MAX }
              className="button-inline btn-sm-h mt-0"
            />
          </div>
        </div>
        <div className="segment-controller">
          <SegmentController
            segments={ segmentList }
            currentIndex={ currentIndex }
            onChangeIndex={ index => { setCurrentIndex(index); setValues({amount: ''}); } }
            className={ 'currentIndex' }
          />
          <div className="enter-amount-frm">
            <CurrencyInputField
              dataTestid={ 'enter-amount-to-send' }
              name="amount"
              value={ values.amount.toString() }
              onChange={ handleInputChange }
              placeholder={ Strings.AMOUNT }
              isUSD={ isUSD }
              decimalsLimit={ isUSD ? 6 : selectedToken.decimals }
            />
          </div>
        </div>
        {isSupportUSD && (
          <em className="get-converted-value text-right opacity">
            {getConvertedValue()}
          </em>
        )}
        {amountError ? <span className="error">{amountError}</span> : null}
      </div>
    );
  };

  const balanceString = `${tokenBalance?.toString(6) ?? ''} ${selectedToken.symbol ?? ''}`;
  const valueString = isSupportUSD
    ? `$${tokenBalance?.multiply?.(usdValue)?.toString(2)}`
    : '';

  return (
    <div className="container">
      <Header headerTitle={ Strings.SEND_TOKEN_UPPERCASE } support />
      {renderContentHeader()}
      <div className="content-background ds-flex-col full-flex">
        <div className="wrapper">
          <Dropdown
            dataTestid="choose-token-to-send-button"
            icon={ getTokenIcon(
              selectedToken.address,
              selectedNetwork.chainId
            ) }
            label={ Strings.CHOOSE_TOKEN_UPPERCASE }
            value={ balanceString }
            subtext={ valueString }
            onClick={ () => setIsShowChooseToken(true) }
            smallValue
            themeColorSubtext
          />
          {renderAmountView()}
        </div>
        <div className="content-footer">
          <Button
            onClick={ (e) => handleNext(e) }
            label={ Strings.NEXT_UPPERCASE }
          />
        </div>
      </div>
      {isShowChooseToken ? (
        <Modal
          isOpen={ isShowChooseToken }
          closeModal={ (_e) => setIsShowChooseToken(false) }
        >
          {renderChooseToken(tokens)}
        </Modal>
      ) : null}
      {isShowNetworkList && (
        <NetworkSelectionModal
          isOpen={ isShowNetworkList }
          closeModal={ (_e) => setIsShowNetworkList(false) }
          onSelectNetwork={ item => {
            setIsShowNetworkList(false);
            if (item.chainId !== selectedNetwork.chainId) {
              changeNetwork(item);
            }
          } }
        />
      )}
    </div>
  );
}

EnterAmount.propTypes = {
  item: PropTypes.object,
  index: PropTypes.string,
  subtitle: PropTypes.string,
  title: PropTypes.string,
  isModalVisible: PropTypes.bool,
  children: PropTypes.object,
  lists: PropTypes.array,
};
