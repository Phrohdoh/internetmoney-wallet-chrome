import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'resources';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import {
  getSendTxnObject,
  getSingleAccount,
  updateTxnLists,
  createAndSendTxn,
  WalletTransactionTypes,
  calculateEstimateFee,
  calculateMaxFee,
} from 'web3-layer';
import { TransactionsAction } from 'redux/slices/transactionsSlice';
import { STORAGE_KEYS } from '../../../constants';

// utils
import { useTokenIcons } from 'utils/hooks';
import { Validation } from 'utils/validations';

// Components
import Header from 'components/Header';
import ProgressBarNumber from 'components/ProgressBarNumber';
import Button from 'components/Button';
import Spinner from 'components/Spinner';
import Modal from 'components/Modal';
import EstimatedGasFee from 'components/EstimatedGasFee';
import AlertModel from 'components/AlertModel';

// Images
import downArrow from 'assets/images/down-arrow.svg';
import dropIcon from 'assets/images/drop.svg';

import { setItem } from 'utils/storage';
import moment from 'moment';

import './confirm-transaction.scss';
import IntegerUnits from 'utils/IntegerUnits';
import { GlobalAction } from 'redux/slices/globalSlice';
import { getNetwork } from 'redux/selectors';

export default function ConfirmTransaction () {
  const navigate = useNavigate();
  const params = useLocation();
  const dispatch = useDispatch();
  const getTokenIcon = useTokenIcons();

  const steps = [1, 2, 3];
  const selectedSteps = 3;

  const networks = useSelector((state) => state.networks);
  const wallet = useSelector((state) => state.wallet);
  const transactions = useSelector((state) => state.transactions);
  const selectedNetwork = useSelector(getNetwork);
  const selectedToken = params.state.selectedToken;

  const [txnObject, setTxnObject] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);

  const [isShowEstimatedGasFee, setIsShowEstimatedGasFee] = useState(false);

  const [gasRange, setGasRange] = useState({});
  const [selectedRange, setSelectedRange] = useState(1);
  const [accountBalance, setAccountBalance] = useState(new IntegerUnits(0));
  const [isGetTxnObject, setIsGetTxnObject] = useState(false);

  // hardware wallet
  const [isShowHardwareModal, setIsShowHardwareModal] = useState(false);
  const [isShowHardwareAcceptRejectModal, setIsShowHardwareAcceptRejectModal] =
    useState(false);
  const [hardwareError, setHardwareError] = useState('');
  const [acceptHardwareData, setAcceptHardwareData] = useState('');
  const [isShowDeclinePopup, setIsShowDeclinePopup] = useState(false);

  useEffect(() => {
    getTxnObject();
    getAccountBalance();
  }, []);

  const getAccountBalance = async () => {
    const balance = await getSingleAccount(
      params.state.fromAccount.publicAddress,
      selectedNetwork
    );

    setAccountBalance(balance.account.value);
  };

  const getTxnObject = async () => {
    setIsLoading(true);
    const isNetworkTokenMatch = Validation.isEmpty(selectedToken.address);

    const parm = [
      !isNetworkTokenMatch,
      !isNetworkTokenMatch ? selectedToken.address : '',
      params.state.fromAccount.publicAddress,
      params.state.toAccountAddress,
      params.state.transferAmount.toPrecision(selectedToken.decimals),
      selectedNetwork,
    ];
    const txObject = await getSendTxnObject(...parm);
    if (txObject.success) {
      setGasRange(txObject.range);
      setIsLoading(false);
      setTxnObject(txObject.txnObject);
      setIsGetTxnObject(true);
    } else {
      dispatch(
        GlobalAction.showAlert(
          Strings.ERROR,
          txObject.error,
        ),
      );
    }
  };

  const updateTxnObjectTypeTwo = (gas, maxPrioFee, maxFeePerGas) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.maxPriorityFeePerGas = maxPrioFee;
    tempTxObj.maxFeePerGas = maxFeePerGas;
    setTxnObject(tempTxObj);
  };

  const updateTxnObjectTypeOne = (gas, gasPrice) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.gasPrice = gasPrice;
    setTxnObject(tempTxObj);
  };

  const getEstimateFee = () => {
    if (!txnObject) return '';
    const estimateFee = calculateEstimateFee(txnObject, selectedNetwork.txnType);
    return `${estimateFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getMaxFee = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getTotal = () => {
    return `${params.state.transferAmount.toString(6)} ${selectedToken.symbol} + ${getEstimateFee()}`;
  };

  const getMaxTotal = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${params.state.transferAmount.toString(6)} ${selectedToken.symbol} + ${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const clickToSendTokens = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const isHardware = !!params.state.fromAccount.isHardware;
    if (isHardware) setIsShowHardwareAcceptRejectModal(true);

    try {
      const txnResult = await createAndSendTxn(
        WalletTransactionTypes.SEND,
        txnObject,
        wallet,
        params.state.fromAccount,
        accountBalance,
        selectedNetwork,
        {
          address: selectedToken.address,
          transferAmount: params.state.transferAmount
        }
      );

      if (txnResult.success) {
        const tempRecentAddresses = [...transactions.recentAddresses];
        tempRecentAddresses.push(params.state.toAccountAddress);

        const { tempTxns, tempTxnsList } = updateTxnLists(
          IntegerUnits.serialize({
            type: WalletTransactionTypes.SEND,
            transactionDetail: {
              accountName: params.state.fromAccountName,
              displayAmount: params.state.transferAmount,
              displayDecimal: selectedToken.decimals,
              fromAddress: params.state.fromAccount.publicAddress,
              fromToken: selectedToken,
              isSupportUSD: params.state.isSupportUSD,
              networkDetail: selectedNetwork,
              toAccountName: params.state.toAccountName,
              toAddress: params.state.toAccountAddress,
              transferedAmount: params.state.transferAmount,
              usdValue: params.state.usdValue,
              utcTime: moment.utc().format(),
            },
            receipt: txnResult.receipt,
            blockNumber: txnResult.blockNumber,
          }),
          transactions,
          selectedNetwork
        );

        dispatch(TransactionsAction.saveTransactions(tempTxnsList));
        dispatch(TransactionsAction.saveRecentAddresses(tempRecentAddresses));

        setItem(STORAGE_KEYS.TRANSACTIONS, JSON.stringify(tempTxns));
        setItem(
          STORAGE_KEYS.RECENT_ADDRESSES,
          JSON.stringify(tempRecentAddresses)
        );

        setIsLoading(false);
        if (isHardware) setIsShowHardwareAcceptRejectModal(false);
        dispatch(
          GlobalAction.showAlert(
            Strings.SUCCESS,
            Strings.SUBMIT_SUCCESS,
          ),
        );
        navigate('/accounts-details', {
          state: {
            item: params.state.fromAccount,
            accountIndex: params.state.accountIndex,
          },
        });
      } else {
        setIsLoading(false);
        if (!isHardware) {
          dispatch(
            GlobalAction.showAlert(
              Strings.ERROR,
              txnResult.error,
            ),
          );
        } else {
          if (txnResult.error.toLowerCase().includes('insufficient funds')) {
            setHardwareError(
              Strings.formatString(Strings.INSUFFICIENT_FUNDS, [
                selectedToken.symbol
              ])
            );
          } else {
            setHardwareError(txnResult.error);
          }
          setIsShowHardwareAcceptRejectModal(false);
          setIsShowHardwareModal(true);
          if (
            txnResult.error ===
            'Ledger device: Condition of use not satisfied (denied by the user?) (0x6985)' ||
            txnResult.error === 'Action cancelled by user'
          ) {
            setHardwareError('');
            setIsShowHardwareModal(false);
            setIsShowDeclinePopup(true);
          }
        }
      }
    } catch (err) {
      setIsLoading(false);
      if (isHardware) setIsShowHardwareAcceptRejectModal(false);
      console.error(err);
    }
  };

  const renderContentHeader = () => {
    return (
      <div className="send-token-header text-center">
        <p>{Strings.CURRENT_NETWORK}</p>
        <h6>{selectedNetwork.networkName}</h6>
        <ProgressBarNumber steps={ steps } selectedSteps={ selectedSteps } />
      </div>
    );
  };

  const headingStyle = () => {
    return (
      <div className="heading-styles">
        <span className="line" />
        <span className="line" />
        <h6 className="text-center uppercase">
          {Strings.CONFIRM_TRANSACTION_UPPERCASE}
        </h6>
        <span className="line" />
        <span className="line" />
      </div>
    );
  };

  const SendTokenAccounts = () => {
    return (
      <div className="send-details">
        <div className="from-to-pattern">
          <img src={ downArrow } alt="" className="from-to-arrow" />
        </div>
        <div className="send-row">
          <div className="body">
            <small>{Strings.FROM_TEXT}</small>
            <h6 className="text-gray-color">{params.state.fromAccountName}</h6>
          </div>
          <div className="right text-right">
            <h6 className="uppercase">{selectedNetwork.sym + ' '}</h6>
            <span>
              <small>{Strings.BALANCE}</small>
              {' '}
              {accountBalance.toString(6, true)}
            </span>
          </div>
          <div className="icon">
            <img
              src={ getTokenIcon('', selectedNetwork.chainId) }
              alt={ selectedToken?.symbol }
            />
          </div>
        </div>
        <div className="send-row">
          <div className="body">
            <small>{Strings.TO_TEXT}</small>
            <h6 className="text-gray-color text-eclipse-style">
              {params.state.toAccountName ? params.state.toAccountName : params.state.toAccountAddress}              
            </h6>
          </div>
        </div>
      </div>
    );
  };

  const SendTokenAmount = () => {
    return (
      <div className="send-amount">
        <small>{Strings.AMOUNT}</small>
        <h2 className="uppercase">
          {params.state.transferAmount.toString(6, true)} {selectedToken.symbol}
          <img
            src={ getTokenIcon(selectedToken.address, selectedNetwork.chainId) }
            alt={ selectedToken.symbol }
          />
        </h2>
        <span className="flex-row">
          {params.state.isSupportUSD && (
            <React.Fragment>
              <small>{Strings.VALUE}: </small>
              <span className="left-sm-space">
                ${params.state.usdValue.multiply(params.state.transferAmount).toString(2)}
              </span>
            </React.Fragment>
          )}
        </span>
      </div>
    );
  };

  const SendTokenEstimated = () => {
    return (
      <div className="send-estimated">
        <div className="send-row">
          <div className="body">
            <h6>{Strings.ESTIMATE_GAS_FEE}</h6>
            <small className="text-green-color opacity">
              {Strings.LIKELY_SECONDS}
            </small>
          </div>
          <div className="right text-right">
            <h6
              className="uppercase text-theme-color"
              onClick={ () => {
                if (isGetTxnObject) {
                  setIsShowEstimatedGasFee(true);
                }
              } }
            >
              {getEstimateFee()}
              <img src={ dropIcon } alt="" />
            </h6>
            {selectedNetwork.txnType == 2 && (
              <span>
                <small className="text-gray-color3">{Strings.MAX_FEE}</small>{' '}
                {getMaxFee()}
              </span>
            )}
          </div>
        </div>
        <div className="send-row">
          <div className="body">
            <h6>{Strings.TOTAL}</h6>
          </div>
          <div className="right text-right">
            <h5 className="uppercase">{getTotal()}</h5>
            {selectedNetwork.txnType == 2 && (
              <span>
                <small className="text-gray-color3">{Strings.MAX_FEE}</small>{' '}
                {getMaxTotal()}
              </span>
            )}
          </div>
        </div>
      </div>
    );
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header headerTitle={ Strings.SEND_TOKEN_UPPERCASE } support />
        {renderContentHeader()}
        <div className="shape-rounded"></div>
        <div className="wrapper ct-wrapper">
          <div className="ct-container">
            <div className="content">
              {headingStyle()}
              {SendTokenAccounts()}
              {SendTokenAmount()}
              {SendTokenEstimated()}
            </div>
          </div>
        </div>
        <div className="content-footer">
          {!isLoading ? (
            <Button
              onClick={ (e) => clickToSendTokens(e) }
              label={ Strings.CLICK_TO_SEND_TOKENS }
            />
          ) : (
            <Spinner />
          )}
        </div>
        {isShowEstimatedGasFee ? (
          <Modal
            isOpen={ isShowEstimatedGasFee }
            closeModal={ () => setIsShowEstimatedGasFee(false) }
          >
            <EstimatedGasFee
              selectedRange={ selectedRange }
              gasRange={ gasRange }
              txnObject={ txnObject }
              network={ selectedNetwork }
              onClickSave={ (updatedObject) => {
                setIsShowEstimatedGasFee(false);
                if (selectedNetwork.txnType != 0) {
                  updateTxnObjectTypeTwo(
                    updatedObject.gas,
                    updatedObject.maxPrioFee,
                    updatedObject.maxFeePerGas
                  );
                  setSelectedRange(selectedRange);
                } else {
                  updateTxnObjectTypeOne(
                    updatedObject.gas,
                    updatedObject.gasPrice
                  );
                }
              } }
            />
          </Modal>
        ) : null}
        {isShowHardwareModal && (
          <Modal
            isOpen={ isShowHardwareModal }
            closeModal={ () => {
              setIsShowHardwareModal(!isShowHardwareModal);
              setAcceptHardwareData('');
            } }
          >
            <div className="modal-body">
              {hardwareError && (
                <div>
                  <label className="label-error-form">{hardwareError}</label>
                  <Button
                    onClick={(e) => {
                      e.preventDefault();
                      setIsShowHardwareModal(!isShowHardwareModal);
                      setAcceptHardwareData('');
                      navigate('/accounts-details', {
                        state: {
                          item: params.state.fromAccount,
                          accountIndex: params.state.accountIndex,
                        },
                      });
                    }}
                    label={ Strings.GO_BACK_BUTTON_UPPERCASE }
                  />
                </div>
              )}

              {acceptHardwareData && (
                <label className="label-error-form">{acceptHardwareData}</label>
              )}
            </div>
          </Modal>
        )}
        {isShowHardwareAcceptRejectModal && (
          <Modal isOpen={ isShowHardwareAcceptRejectModal }>
            <div className="spinner-container-hardware">
              <div className="loading-spinner-hardware" />
            </div>
            <p className="label-approve-reject-form">
              {Strings.APPROVE_OR_REJECT}
            </p>
          </Modal>
        )}
        {isShowDeclinePopup && (
          <AlertModel
            alertTitle={ Strings.DECLINED }
            isShowAlert={ isShowDeclinePopup }
            onPressSuccess={ () => {
              setIsShowDeclinePopup(!isShowDeclinePopup);
              navigate('/accounts-details', {
                state: {
                  item: params.state.fromAccount,
                  accountIndex: params.state.accountIndex,
                },
              });
            } }
            className="text-center"
            successBtnTitle={ Strings.OKAY }
          >
            <p>{Strings.USER_REJECTED}</p>
          </AlertModel>
        )}
      </div>
    </React.Fragment>
  );
}

ConfirmTransaction.propTypes = {
  subtitle: PropTypes.string,
  title: PropTypes.string,
  isModalVisible: PropTypes.string,
  children: PropTypes.string,
  onItemClicked: PropTypes.func,
  isActive: PropTypes.bool,
  onClickFunc: PropTypes.func,
  message: PropTypes.string,
  label: PropTypes.string,
  rightText: PropTypes.string,
  submessage: PropTypes.string,
};
