import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'resources';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';

import './transfer-between-accounts.scss';

import { STORAGE_KEYS } from '../../../constants';

// utils
import { setItem } from 'utils/storage';

// Components
import Header from 'components/Header';
import ProgressBarNumber from 'components/ProgressBarNumber';
import Dropdown from 'components/Dropdown';
import Button from 'components/Button';
import Modal from 'components/Modal';
import SendTokenAccountListCell from 'components/SendTokenAccountListCell';
import NetworkSelectionModal from 'components/NetworkSelectionModal';

import { NetworksAction } from 'redux/slices/networksSlice';
import { WalletAction } from 'redux/slices/walletSlice';
import { getAccountList, getNetwork, getTokensByAddress } from 'redux/selectors';
import { GlobalAction } from 'redux/slices/globalSlice';

export default function TransferBetweenAccounts () {
  const navigate = useNavigate();
  const params = useLocation();
  const dispatch = useDispatch();

  const wallet = useSelector(state => state.wallet);
  const accountList = useSelector(getAccountList);

  const steps = [1, 2, 3];
  const selectedSteps = 1;

  const selectedNetwork = useSelector(getNetwork);

  const [selectedAccount, setSelectedAccount] = useState(
    params.state.accountDetail
  );

  const [selectedToAccount, setSelectedToAccount] = useState(
    params.state.accountDetail
  );

  const [isSelectToAccount, setIsSelectToAccount] = useState(false);
  const [isShowChooseAccount, setIsShowChooseAccount] = useState(false);
  const [accountSelectErrorMsg, setAccountSelectErrorMsg] = useState('');
  const [toAccountSelectErrorMsg, setToAccountSelectErrorMsg] = useState('');

  const [isShowNetworkList, setIsShowNetworkList] = useState(false);

  const tokens = useSelector(getTokensByAddress)[selectedAccount.publicAddress.toLowerCase()];
  const nativeToken = tokens?.[0];

  const _selecteAccount = (item, _index) => {
    setIsShowChooseAccount(false);
    if (isSelectToAccount) {
      setSelectedToAccount(item);
    } else {
      setSelectedAccount(item);
    }
  };

  const handleNext = async (e) => {
    e.preventDefault();
    setAccountSelectErrorMsg('');
    if (
      selectedAccount.publicAddress.toLowerCase() ===
      selectedToAccount.publicAddress.toLowerCase()
    ) {
      setToAccountSelectErrorMsg(Strings.CHOOSE_DIFFERENT_ACCOUNT_MSG);
    } else {
      navigate('/enter-amount', {
        state: {
          fromAccount: selectedAccount,
          fromAccountName: selectedAccount.name,
          toAccountAddress: selectedToAccount.publicAddress,
          toAccountName: selectedToAccount.name,
          accountIndex: params.state.accountIndex,
        },
      });
    }
  };

  const changeNetwork = async choosedNetwork => {
    await setItem(
      STORAGE_KEYS.SELECTED_NETWORK,
      JSON.stringify(choosedNetwork),
    );
    dispatch(NetworksAction.saveSelectedNetworkObject(choosedNetwork));
    dispatch(WalletAction.resetData(wallet.walletDetail, choosedNetwork));

    setTimeout(() => {
      dispatch(
        GlobalAction.showAlert(
          Strings.NETWORK_SWITCH_TITLE,
          `${Strings.NETWORK_CHANGE_SUCCESS_MSG}${choosedNetwork.networkName}`,
        ),
      );
    }, 400);
  };

  const renderContentHeader = () => {
    return (
      <div className="send-token-header text-center">
        <div className="pointer" onClick={ () => setIsShowNetworkList(true) }>
          <p>{Strings.CURRENT_NETWORK}</p>
          <h6>{selectedNetwork.networkName}</h6>
        </div>
        <ProgressBarNumber steps={ steps } selectedSteps={ selectedSteps } />
      </div>
    );
  };

  // render Create Account View
  const renderChooseAccounts = (props) => {
    return (
      <React.Fragment>
        <div className="modal-header">
          <h6 className="text-center">{Strings.CHOOSE_ACCOUNTS_UPPERCASE}</h6>
        </div>
        <div className="modal-body">
          {props.lists.map((item, index) => (
            <SendTokenAccountListCell
              dataTestid={ `choose-account-${index}` }
              key={ index }
              item={ item }
              index={ index }
              subtext={ Strings.BALANCE }
              onSelectAccount={ (item, index) => _selecteAccount(item, index) }
            />
          ))}
        </div>
      </React.Fragment>
    );
  };

  return (
    <div className="container">
      <Header headerTitle={ Strings.SEND_TOKEN_UPPERCASE } support />
      {renderContentHeader()}
      <div className="content-background ds-flex-col full-flex">
        <div className="wrapper">
          <Dropdown
            dataTestid="choose-the-account-to-transfer-from"
            label={ Strings.CHOOSE_TRANSFER_FROM_ACCOUNT }
            value={ selectedAccount.name }
            subtext={ 'Balance:' }
            subvalue={ `${nativeToken.balance?.value?.toString(6, true) ?? ''} ${nativeToken.symbol}` }
            error={ accountSelectErrorMsg }
            onClick={ (e) => {
              e.preventDefault();
              setIsShowChooseAccount(true);
              setIsSelectToAccount(false);
            } }
          />
          <Dropdown
            dataTestid="enter-the-account-to-transfer-to"
            label={ Strings.CHOOSE_TRANSFER_TO_ACCOUNT }
            value={ selectedToAccount.name }
            onClick={ (e) => {
              e.preventDefault();
              setIsShowChooseAccount(true);
              setIsSelectToAccount(true);
            } }
          />
          {toAccountSelectErrorMsg ? (
            <span className="error">{toAccountSelectErrorMsg}</span>
          ) : null}
        </div>
        <div className="content-footer">
          <Button
            onClick={ (e) => {
              handleNext(e);
            } }
            label={ Strings.NEXT_UPPERCASE }
          />
        </div>
      </div>
      {isShowChooseAccount ? (
        <Modal
          isOpen={ isShowChooseAccount }
          closeModal={ () => setIsShowChooseAccount(false) }
        >
          {renderChooseAccounts({ lists: accountList })}
        </Modal>
      ) : null}
      {isShowNetworkList && (
        <NetworkSelectionModal
          isOpen={ isShowNetworkList }
          closeModal={ (_e) => setIsShowNetworkList(false) }
          onSelectNetwork={ item => {
            setIsShowNetworkList(false);
            if (item.chainId !== selectedNetwork.chainId) {
              changeNetwork(item);
            }
          } }
        />
      )}
    </div>
  );
}

TransferBetweenAccounts.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  subtitle: PropTypes.string,
  title: PropTypes.string,
  isModalVisible: PropTypes.bool,
  children: PropTypes.object,
  lists: PropTypes.array,
};
