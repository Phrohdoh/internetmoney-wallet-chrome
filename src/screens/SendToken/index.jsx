import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'resources';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { checkValidENSOrToAddress } from 'web3-layer';

import './send-token.scss';

import { STORAGE_KEYS } from '../../constants';

// utils
import { Validation } from 'utils/validations';
import { setItem } from 'utils/storage';

// Components
import Header from 'components/Header';
import ProgressBarNumber from 'components/ProgressBarNumber';
import Dropdown from 'components/Dropdown';
import InputField from 'components/InputField';
import RecentSentCell from 'components/RecentSentCell';
import Button from 'components/Button';
import Modal from 'components/Modal';
import SendTokenAccountListCell from 'components/SendTokenAccountListCell';
import Spinner from 'components/Spinner';
import NetworkSelectionModal from 'components/NetworkSelectionModal';

import { NetworksAction } from 'redux/slices/networksSlice';
import { WalletAction } from 'redux/slices/walletSlice';
import { getAccountList, getNetwork, getTokensByAddress } from 'redux/selectors';
import { GlobalAction } from 'redux/slices/globalSlice';

export default function SendToken () {
  const navigate = useNavigate();
  const params = useLocation();
  const dispatch = useDispatch();
  
  const wallet = useSelector(state => state.wallet);
  const networks = useSelector((state) => state.networks);
  const transactions = useSelector((state) => state.transactions);
  const accountList = useSelector(getAccountList);

  const steps = [1, 2, 3];
  const selectedSteps = 1;

  const selectedNetwork = useSelector(getNetwork);

  const [recentSentList, setRecentSentList] = useState([]);
  const [selectedAccount, setSelectedAccount] = useState(
    params.state.accountDetail
  );
  const [isShowChooseAccount, setIsShowChooseAccount] = useState(false);
  const [accountSelectErrorMsg, setAccountSelectErrorMsg] = useState('');
  const [isValidatingEns, setIsValidatingEns] = useState(false);
  const [errorPublicAddress, setErrorPublicAddress] = useState('');

  const [isShowNetworkList, setIsShowNetworkList] = useState(false);

  const initialValues = {
    publicAddress: '',
  };

  const [values, setValues] = useState(initialValues);

  const tokens = useSelector(getTokensByAddress)[selectedAccount.publicAddress.toLowerCase()];
  const nativeToken = tokens?.[0];

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  useEffect(() => {
    filterRecentAccountList();
  }, [networks]);

  const filterRecentAccountList = () => {
    const uniqueToAddress = [
      ...new Set(transactions.recentAddresses.map(item => item)),
    ];

    setRecentSentList(uniqueToAddress.filter(n => n));
  };

  const changeNetwork = async choosedNetwork => {
    await setItem(
      STORAGE_KEYS.SELECTED_NETWORK,
      JSON.stringify(choosedNetwork),
    );
    dispatch(NetworksAction.saveSelectedNetworkObject(choosedNetwork));
    dispatch(WalletAction.resetData(wallet.walletDetail, choosedNetwork));

    setTimeout(() => {
      dispatch(
        GlobalAction.showAlert(
          Strings.NETWORK_SWITCH_TITLE,
          `${Strings.NETWORK_CHANGE_SUCCESS_MSG}${choosedNetwork.networkName}`,
        ),
      );
    }, 400);
  };

  const renderContentHeader = () => {
    return (
      <div className="send-token-header text-center">
        <div className="pointer" onClick={ () => setIsShowNetworkList(true) }>
          <p>{Strings.CURRENT_NETWORK}</p>
          <h6>{selectedNetwork.networkName}</h6>
        </div>
        <ProgressBarNumber steps={ steps } selectedSteps={ selectedSteps } />
      </div>
    );
  };

  const renderRecentAccounts = () => {
    return (
      <div className="list-style">
        {recentSentList.map((item, index) => (
          <RecentSentCell
            key={ index }
            item={ item ? item : '' }
            index={ index }
            dataTestid={ `recent-account-${index}` }
            accounts={ accountList }
            onSelectRecentAccount={ (item) => _selectRecentAccount(item) }
          />
        ))}
      </div>
    );
  };

  const _selecteAccount = (item, _index) => {
    setIsShowChooseAccount(false);
    setSelectedAccount(item);
  };

  // render Create Account View
  const renderAccounts = () => {
    return (
      <React.Fragment>
        <div className="modal-header">
          <h6 className="text-center">{Strings.CHOOSE_ACCOUNTS_UPPERCASE}</h6>
        </div>
        <div className="modal-body">
          {accountList.map((item, index) => (
            <SendTokenAccountListCell
              dataTestid={ `choose-account-${index}` }
              key={ index }
              item={ item }
              index={ index }
              subtext={ Strings.BALANCE }
              onSelectAccount={ (item, index) => _selecteAccount(item, index) }
            />
          ))}
        </div>
      </React.Fragment>
    );
  };

  const handleNext = async (e) => {
    e.preventDefault();
    setAccountSelectErrorMsg('');
    if (!Validation.isEmpty(values.publicAddress)) {
      setIsValidatingEns(true);
      const isValidENS = await checkValidENSOrToAddress(values.publicAddress);
      setIsValidatingEns(false);
      if (isValidENS.success) {
        navigate('/enter-amount', {
          state: {
            fromAccount: selectedAccount,
            fromAccountName: selectedAccount.name,
            toAccountAddress: isValidENS.address,
            accountIndex: params.state.accountIndex,
          },
        });
      } else {
        setErrorPublicAddress(isValidENS.error);
      }
    } else {
      setErrorPublicAddress(Strings.ENTER_PUBLIC_ADDRESS_MSG);
    }
  };

  const handleTransferBetweenAccounts = async (e) => {
    e.preventDefault();
    navigate('/transfer-between-accounts', {
      state: {
        accountDetail: params.state.accountDetail,
        accountIndex: params.state.accountIndex,
      },
    });
  };

  const _selectRecentAccount = (item) => {
    if (selectedAccount.publicAddress.toLowerCase() !== item.toLowerCase()) {
      console.log('select', selectedAccount.publicAddress.toLowerCase() !== item.toLowerCase());
      navigate('/enter-amount', {
        state: {
          fromAccount: selectedAccount,
          fromAccountName: selectedAccount.name,
          toAccountAddress: item,
          accountIndex: params.state.accountIndex,
        },
      });
    }
  };

  return (
    <div className="container">
      <Header headerTitle={ Strings.SEND_TOKEN_UPPERCASE } support />
      {renderContentHeader()}
      <div className="content-background ds-flex-col full-flex hide-scrollbar">
        <div className="wrapper">
          <Dropdown
            dataTestid="send-token-choose-account-button"
            label={ Strings.CHOOSE_SEND_FROM_ACCOUNT }
            value={ selectedAccount.name }
            subtext={ 'Balance:' }
            subvalue={ `${nativeToken?.balance?.value?.toString(6, true) ?? ''} ${nativeToken?.symbol}` }
            onClick={ (e) => {
              e.preventDefault();
              setIsShowChooseAccount(true);
            } }
            error={ accountSelectErrorMsg }
          />
          <InputField
            dataTestid={ 'enter-the-account-to-send-to' }
            label={ Strings.ENTER_SEND_TO_ACCOUNT }
            placeholder={ Strings.ENTER_PUBLIC_ADDRESS_PLACEHOLDER }
            name="publicAddress"
            value={ values.publicAddress }
            onChange={ handleInputChange }
            error={ errorPublicAddress }
          />
          <div className="form-group">
            <label>{Strings.RECENT_ACCOUNT}</label>
            {renderRecentAccounts()}
          </div>
        </div>
        <div className="content-footer">
          <Button
            onClick={ (e) => handleTransferBetweenAccounts(e) }
            label={ Strings.TRANSFER_BETWEEN_ACCOUNTS_UPPERCASE }
            outline
          />
          {!isValidatingEns ? (
            <Button
              onClick={ (e) => handleNext(e) }
              label={ Strings.NEXT_UPPERCASE }
            />
          ) : (
            <Spinner />
          )}
        </div>
      </div>
      {isShowChooseAccount ? (
        <Modal
          isOpen={ isShowChooseAccount }
          closeModal={ () => setIsShowChooseAccount(false) }
        >
          {renderAccounts()}
        </Modal>
      ) : null}
      {isShowNetworkList && (
        <NetworkSelectionModal
          isOpen={ isShowNetworkList }
          closeModal={ (_e) => setIsShowNetworkList(false) }
          onSelectNetwork={ item => {
            setIsShowNetworkList(false);
            if (item.chainId !== selectedNetwork.chainId) {
              changeNetwork(item);
            }
          } }
        />
      )}
    </div>
  );
}

SendToken.propTypes = {
  item: PropTypes.string,
  index: PropTypes.string,
  subtitle: PropTypes.string,
  title: PropTypes.string,
  isModalVisible: PropTypes.string,
  children: PropTypes.object,
  lists: PropTypes.array,
};
