import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'resources';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import {
  getSwapTxnObject,
  getSingleAccount,
  updateTxnLists,
  createAndSendTxn,
  WalletTransactionTypes,
  calculateEstimateFee,
  calculateMaxFee,
} from 'web3-layer';
import { TransactionsAction } from 'redux/slices/transactionsSlice';
import { STORAGE_KEYS } from '../../../constants';
import moment from 'moment';

// utils
import { useTokenIcons } from 'utils/hooks';

// Components
import Header from 'components/Header';
import ProgressBarNumber from 'components/ProgressBarNumber';
import Button from 'components/Button';
import Spinner from 'components/Spinner';
import Modal from 'components/Modal';
import EstimatedGasFee from 'components/EstimatedGasFee';
import AlertModel from 'components/AlertModel';

// Images
import downArrow from 'assets/images/down-arrow.svg';
import dropIcon from 'assets/images/drop.svg';

import { setItem } from 'utils/storage';

import './swap-confirm-transaction.scss';
import { getNativeToken, getNetwork } from 'redux/selectors';
import IntegerUnits from 'utils/IntegerUnits';
import { GlobalAction } from 'redux/slices/globalSlice';
import { getUserFriendlyErrorMessage } from 'utils/errors';

export default function SwapConfirmTransaction () {
  const navigate = useNavigate();
  const params = useLocation();
  const dispatch = useDispatch();
  const getTokenIcon = useTokenIcons();

  const steps = [1, 2, 3];
  const selectedSteps = 3;

  const networks = useSelector((state) => state.networks);
  const wallet = useSelector((state) => state.wallet);
  const transactions = useSelector((state) => state.transactions);

  const selectedNetwork = useSelector(getNetwork);
  const wdSupportObject = networks.wdSupportObject;

  const [txnObject, setTxnObject] = useState(undefined);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(false);
  const [isShowAlert, setIsShowAlert] = useState(false);

  const [isShowEstimatedGasFee, setIsShowEstimatedGasFee] = useState(false);
  const [gasRange, setGasRange] = useState({});
  const accountDetail = params.state.accountDetail;
  const { toToken, fromToken, quoteObj, walletFee } = params.state;
  const nativeToken = useSelector(getNativeToken);
  const nativeUsdValue = nativeToken.usdPrice?.value;
  const walletFeeUsd = (nativeUsdValue === undefined || walletFee === undefined)
    ? undefined
    : `$${walletFee.multiply(nativeUsdValue).toString(2)}`;
  const [selectedRange, setSelectedRange] = useState(1);
  const [accountBalance, setAccountBalance] = useState(new IntegerUnits(0));
  const [isGetTxnObject, setIsGetTxnObject] = useState(false);

  // hardware wallet
  const [isShowHardwareModal, setIsShowHardwareModal] = useState(false);
  const [isShowHardwareAcceptRejectModal, setIsShowHardwareAcceptRejectModal] =
    useState(false);
  const [hardwareError, setHardwareError] = useState('');
  const [isShowDeclinePopup, setIsShowDeclinePopup] = useState(false);

  useEffect(() => {
    Promise.all([
      getTxnObject(),
      getAccountBalance(),
    ]).then(() => {
      setIsLoading(false);
    });
  }, []);

  const getAccountBalance = async () => {
    const balance = await getSingleAccount(
      accountDetail.publicAddress,
      selectedNetwork
    );

    setAccountBalance(balance.account.value);
  };

  const getTxnObject = async () => {
    const parm = [
      wdSupportObject.swapRouterV3,
      quoteObj,
      params.state.slippage,
      accountDetail.publicAddress,
      selectedNetwork,
    ];
    const txObject = await getSwapTxnObject(...parm);

    if (txObject.success) {
      setGasRange(txObject.range);
      setTxnObject(txObject.txnObject);
      setIsGetTxnObject(true);
    } else {
      setError(Strings.VOLATILITY_ERROR);
      setIsShowAlert(true);
    }
  };

  const updateTxnObjectTypeTwo = (gas, maxPrioFee, maxFeePerGas) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.maxPriorityFeePerGas = maxPrioFee;
    tempTxObj.maxFeePerGas = maxFeePerGas;
    setTxnObject(tempTxObj);
  };

  const updateTxnObjectTypeOne = (gas, gasPrice) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.gasPrice = gasPrice;
    setTxnObject(tempTxObj);
  };

  const getEstimateFee = () => {
    if (!txnObject) return '';
    const estimateFee = calculateEstimateFee(txnObject, selectedNetwork.txnType);
    return `${estimateFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getMaxFee = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getTotal = () => {
    return `${params.state.transferAmount.toString(6)} ${fromToken.symbol} + ${getEstimateFee()}`;
  };

  const getMaxTotal = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${params.state.transferAmount.toString(6)} ${fromToken.symbol} + ${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const clickToSwapTokens = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const isHardware = !!accountDetail.isHardware;
    if (isHardware) setIsShowHardwareAcceptRejectModal(true);

    try {
      const txnResult = await createAndSendTxn(
        WalletTransactionTypes.SWAP,
        txnObject,
        wallet,
        accountDetail,
        accountBalance,
        selectedNetwork,
        {
          address: fromToken.address,
          transferAmount: params.state.transferAmount
        }
      );

      if (txnResult.success) {
        const { tempTxns, tempTxnsList } = updateTxnLists(
          IntegerUnits.serialize({
            type: WalletTransactionTypes.SWAP,
            transactionDetail: {
              accountName: accountDetail.name,
              displayAmount: params.state.transferAmount,
              displayDecimal: fromToken.decimals,
              fromAddress: accountDetail.publicAddress,
              fromToken: fromToken,
              isSupportUSD: params.state.isSupportUSD,
              networkDetail: selectedNetwork,
              quoteObj: quoteObj,
              toToken: toToken,
              transferedAmount: params.state.transferAmount,
              usdValue: params.state.usdValue,
              utcTime: moment.utc().format(),
              walletFee: walletFee,
            },
            receipt: txnResult.receipt,
            blockNumber: txnResult.blockNumber,
          }),
          transactions,
          selectedNetwork
        );

        dispatch(TransactionsAction.saveTransactions(tempTxnsList));
        setItem(STORAGE_KEYS.TRANSACTIONS, JSON.stringify(tempTxns));
        setIsLoading(false);
        if (isHardware) setIsShowHardwareAcceptRejectModal(false);
        dispatch(
          GlobalAction.showAlert(
            Strings.SUCCESS,
            Strings.SUBMIT_SUCCESS,
          )
        );
        navigate('/accounts-details', { state: { item: accountDetail } });
      } else {
        setIsLoading(false);
        if (!isHardware) {
          setError(txnResult.error);
          setIsShowAlert(true);
        } else {
          if (txnResult.error.toLowerCase().includes('insufficient funds')) {
            setHardwareError(
              Strings.formatString(Strings.INSUFFICIENT_FUNDS, [
                fromToken.symbol
              ])
            );
          } else {
            setHardwareError(txnResult.error);
          }
          setIsShowAlert(false);
          setIsShowHardwareAcceptRejectModal(false);
          setIsShowHardwareModal(true);
          if (
            txnResult.error ===
            'Ledger device: Condition of use not satisfied (denied by the user?) (0x6985)' ||
            txnResult.error === 'Action cancelled by user'
          ) {
            setHardwareError('');
            setIsShowHardwareModal(false);
            setIsShowDeclinePopup(true);
          }
        }
      }
    } catch (err) {
      setIsLoading(false);
      if (isHardware) setIsShowHardwareAcceptRejectModal(false);
      console.error(err);
    }
  };

  const onClickSuccess = () => {
    setIsShowAlert(false);
    navigate('/accounts-details', { state: { item: accountDetail } });
  };

  const renderContentHeader = () => {
    return (
      <div className="send-token-header text-center">
        <ProgressBarNumber steps={ steps } selectedSteps={ selectedSteps } />
      </div>
    );
  };

  const headingStyle = () => {
    return (
      <div className="heading-styles">
        <span className="line" />
        <span className="line" />
        <h6 className="text-center uppercase">
          {Strings.CONFIRM_TRANSACTION_UPPERCASE}
        </h6>
        <span className="line" />
        <span className="line" />
      </div>
    );
  };

  const SendTokenAccounts = () => {
    return (
      <div className="send-details">
        <div className="from-to-pattern">
          <img src={ downArrow } alt="" className="from-to-arrow" />
        </div>
        <div className="send-row">
          <div className="body">
            <small>{Strings.FROM_TEXT}</small>
            <h6 className="text-gray-color">
              <img
                src={ getTokenIcon(fromToken.address, selectedNetwork.chainId) }
                alt={ fromToken.symbol }
              />
              {fromToken.symbol}
            </h6>
          </div>
          <div className="right text-right">
            <h6 className="uppercase">{fromToken.symbol + ' '}</h6>           
            <span>
              <small>{Strings.AMOUNT}</small> {params.state.transferAmount.toString(6)}
            </span>
          </div>
          <div className="icon">
            <img
              src={ getTokenIcon(fromToken.address, selectedNetwork.chainId) }
              alt={ fromToken.symbol }
              className="sm-icon"
            />
          </div>
        </div>
        <div className="send-row">
          <div className="body">
            <small>{Strings.TO_TEXT}</small>
            <h6 className="text-gray-color">{toToken.symbol}</h6>
          </div>
          <div className="right text-right">
            <h6 className="uppercase">{toToken.symbol + ' '}</h6>
            <span>
              <small>{Strings.EEPECTED_AMOUNT + ' '}</small> 
              {quoteObj.amountOut.toString(6)}
            </span>
          </div>
          <div className="icon">
            <img
              src={ getTokenIcon(toToken.address, selectedNetwork.chainId) }
              alt={ toToken.symbol }
              className="sm-icon"
            />
          </div>
        </div>
      </div>
    );
  };

  const SendTokenAmount = () => {
    return (
      <div className="send-amount">
        <small>{Strings.AMOUNT}</small>          
        <h2>
          
          {`${params.state.transferAmount.toString(6)} ${fromToken.symbol}`}
          <img
            src={ getTokenIcon(fromToken.address, selectedNetwork.chainId) }
            alt={ fromToken.symbol }
            className="sm-icon"
          />
        </h2>        
        {params.state.isSupportUSD && (
          <span>
            <small>{Strings.VALUE}: </small>
            {`$${params.state.transferAmount.multiply(params.state.usdValue).toString(2)}`}
          </span>
        )}
      </div>
    );
  };

  const SendTokenEstimated = () => {
    return (
      <div className="send-estimated">
        <div className="send-row">
          <div className="body">
            <h6>{Strings.ESTIMATE_GAS_FEE}</h6>
            <small className="text-green-color opacity">
              {Strings.LIKELY_SECONDS}
            </small>
          </div>
          <div className="right text-right">
            <h6
              className="uppercase text-theme-color"
              onClick={ () => {
                if (isGetTxnObject) {
                  setIsShowEstimatedGasFee(true);
                }
              } }
            >
              {getEstimateFee()}
              <img src={ dropIcon } alt="" />
            </h6>
            {selectedNetwork.txnType == 2 && (
              <span>
                <small className="text-gray-color3">{Strings.MAX_FEE}</small>{' '}
                {getMaxFee()}
              </span>
            )}
          </div>
        </div>
        <div className="send-row top">
          <div className="body">
            <h6>{Strings.WALLET_FEE}</h6>
          </div>
          <div className="right text-right">
            {walletFeeUsd !== undefined && (
              <span>
                <small className="text-gray-color3">{Strings.VALUE}: </small>
                {walletFeeUsd}
              </span>
            )}
            <h5 className="uppercase">
              {`${walletFee?.toString(6) ?? 0} ${selectedNetwork.sym}`}
            </h5>
          </div>
        </div>
        <div className="send-row">
          <div className="body">
            <h6>{Strings.TOTAL}</h6>
          </div>
          <div className="right text-right">
            <h5 className="uppercase">{getTotal()}</h5>
            {selectedNetwork.txnType == 2 && (
              <span>
                <small className="text-gray-color3">{Strings.MAX_FEE}</small>
                {' '}
                {getMaxTotal()}
              </span>
            )}
          </div>
        </div>
      </div>
    );
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header headerTitle={ Strings.SWAP_TOKEN_UPPERCASE } support />
        {renderContentHeader()}
        <div className="shape-rounded"></div>
        <div className="wrapper ct-wrapper">
          <div className="ct-container">
            <div className="content">
              {headingStyle()}             
              {SendTokenAccounts()}
              {SendTokenAmount()}
              {SendTokenEstimated()}
            </div>
            {isShowAlert && (
              <AlertModel
                isShowAlert={ isShowAlert }
                onPressSuccess={ () => onClickSuccess() }
                className="text-center"
              >
                <p>{getUserFriendlyErrorMessage(error)}</p>
              </AlertModel>
            )}
          </div>
        </div>
        <div className="content-footer">
          {!isLoading ? (
            <Button
              onClick={ (e) => clickToSwapTokens(e) }
              label={Strings.CLICK_TO_SWAP_TOKENS}
            />
          ) : (
            <Spinner />
          )}
        </div>
        {isShowEstimatedGasFee ? (
          <Modal
            isOpen={ isShowEstimatedGasFee }
            closeModal={ () => setIsShowEstimatedGasFee(false) }
          >
            <EstimatedGasFee
              selectedRange={ selectedRange }
              gasRange={ gasRange }
              txnObject={ txnObject }
              network={ selectedNetwork }
              onClickSave={ (updatedObject) => {
                setIsShowEstimatedGasFee(false);
                if (selectedNetwork.txnType != 0) {
                  updateTxnObjectTypeTwo(
                    updatedObject.gas,
                    updatedObject.maxPrioFee,
                    updatedObject.maxFeePerGas
                  );
                  setSelectedRange(selectedRange);
                } else {
                  updateTxnObjectTypeOne(
                    updatedObject.gas,
                    updatedObject.gasPrice
                  );
                }
              } }
            />
          </Modal>
        ) : null}
        {isShowHardwareModal && (
          <Modal
            isOpen={ isShowHardwareModal }
            closeModal={ () => {
              setIsShowHardwareModal(!isShowHardwareModal);
            } }
          >
            <div className="modal-body">
              {hardwareError && (
                <div>
                  <label className="label-error-form">{hardwareError}</label>
                  <Button
                    onClick={(e) => {
                      e.preventDefault();
                      setIsShowHardwareModal(!isShowHardwareModal)
                    }}
                    label={ Strings.TRY_AGAIN }
                  />
                </div>
              )}
            </div>
          </Modal>
        )}
        {isShowHardwareAcceptRejectModal && (
          <Modal isOpen={ isShowHardwareAcceptRejectModal }>
            <div className="spinner-container-hardware">
              <div className="loading-spinner-hardware" />
            </div>
            <p className="label-approve-reject-form">
              {Strings.APPROVE_OR_REJECT}
            </p>
          </Modal>
        )}
        {isShowDeclinePopup && (
          <AlertModel
            alertTitle={ Strings.DECLINED }
            isShowAlert={ isShowDeclinePopup }
            onPressSuccess={ () => {
              setIsShowDeclinePopup(!isShowDeclinePopup);
              navigate('/accounts-details', { state: { item: accountDetail } });
            } }
            className="text-center"
            successBtnTitle={ Strings.OKAY }
          >
            <p>{Strings.USER_REJECTED}</p>{' '}
          </AlertModel>
        )}
      </div>
    </React.Fragment>
  );
}

SwapConfirmTransaction.propTypes = {
  subtitle: PropTypes.string,
  title: PropTypes.string,
  isModalVisible: PropTypes.string,
  children: PropTypes.string,
  onItemClicked: PropTypes.func,
  isActive: PropTypes.bool,
  onClickFunc: PropTypes.func,
  message: PropTypes.string,
  label: PropTypes.string,
  rightText: PropTypes.string,
  submessage: PropTypes.string,
};
