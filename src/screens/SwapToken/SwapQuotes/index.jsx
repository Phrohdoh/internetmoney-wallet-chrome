import React, { useCallback, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'resources';
import { useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { getQuotes, checkAllowance, getUSDPrice } from 'web3-layer';

// Components
import Header from 'components/Header';
import ProgressBarNumber from 'components/ProgressBarNumber';
import Spinner from 'components/Spinner';
import AlertModel from 'components/AlertModel';

// Images
import rightArrow from 'assets/images/right-arrow.svg';
import support from 'assets/images/support.svg';

import './swap-quotes.scss';
import Modal from 'components/Modal';
import { getUserFriendlyErrorMessage } from 'utils/errors';
import { getNetwork } from 'redux/selectors';

export default function SwapQuotes () {
  const navigate = useNavigate();
  const params = useLocation();

  const steps = [1, 2, 3];
  const selectedSteps = 2;

  const networks = useSelector((state) => state.networks);
  
  const selectedNetwork = useSelector(getNetwork);
  const wdSupportObject = networks.wdSupportObject;
  const accountDetail = params.state.accountDetail;
  const [quoteList, setQuoteList] = useState([]);
  const [checkAllowanceObj, setCheckAllowanceObj] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [isShowAlert, setIsShowAlert] = useState(false);
  const [isMessage, setIsMessage] = useState('');
  const [displayFeeDetails, setDisplayFeeDetails] = useState(false);
  const [isShowApproveRequestPopup, setIsShowApproveRequestPopup] = useState(false);
  const [error, setError] = useState('');
  const [toUSDPrice, setToUSDPrice] = useState();

  const fetchToUSDPrice = useCallback(async () => {
    const priceResult = await getUSDPrice(params.state.selectedToToken.address, selectedNetwork);
    if (priceResult.success) {
      setToUSDPrice(priceResult.usdPrice);
    }
  }, [params.state.selectedToToken.address]);

  useEffect(() => {
    fetchToUSDPrice();
  }, [fetchToUSDPrice]);

  useEffect(() => {
    allowanceCheck();
  }, []);

  const onClickSuccess = () => {
    setIsShowAlert(false);
    navigate(-1);
  };

  const allowanceCheck = async () => {
    setIsLoading(true);
    const allowanceObj = await checkAllowance(
      params.state.transferAmount,
      params.state.selectedFromToken.decimals,
      accountDetail.publicAddress,
      wdSupportObject.swapRouterV3,
      params.state.selectedFromToken.address,
      selectedNetwork,
    );
    setCheckAllowanceObj(allowanceObj);

    if (allowanceObj.success) {
      if (allowanceObj.status) {
        getQuotesObj();
      } else {
        setIsShowApproveRequestPopup(true);
        setIsLoading(false);
      }
    } else {
      setIsLoading(false);
      setError(allowanceObj.error);
    }
  };

  const getQuotesObj = async () => {
    try {
      const trsAmount = params.state.transferAmount.toPrecision(params.state.selectedFromToken.decimals);

      const parm = [
        wdSupportObject.swapRouterV3,
        params.state.selectedFromToken.address,
        params.state.selectedFromToken.decimals,
        trsAmount,
        params.state.selectedToToken.address,
        params.state.selectedToToken.decimals,
        selectedNetwork,
        params.state.slippage,
        params.state.accountDetail.publicAddress,
      ];

      const quoteObj = await getQuotes(...parm);
      quoteObj.slippage = params.state.slippage;
      setIsLoading(false);
      if (quoteObj.success) {
        setQuoteList(quoteObj.quotes);
      } else {
        setError(quoteObj.error);
      }
      if (quoteObj.message) {
        setIsShowAlert(true);
        setIsMessage(quoteObj.message);
      }
    } catch (e) {
      console.log('e >>', e);
    }
  };

  const renderContentHeader = () => {
    return (
      <div className="send-token-header text-center">
        <ProgressBarNumber steps={ steps } selectedSteps={ selectedSteps } />
      </div>
    );
  };

  const swapQuotesListing = (props) => {
    const quotes = props.lists;
    const listItems = quotes.map((item, index) => (
      <li
        key={ index }
        data-testid={ `quote-list-item-${index}` }
        onClick={ (_e) => {
          navigate('/swap-confirm-transaction', {
            state: {
              accountDetail: params.state.accountDetail,
              quoteObj: item,
              fromToken: params.state.selectedFromToken,
              toToken: params.state.selectedToToken,
              transferAmount: params.state.transferAmount,
              isSupportUSD: params.state.isSupportUSD,
              usdValue: params.state.usdValue,
              slippage: params.state.slippage,
              walletFee: item.walletFee,
              accountName: params.state.accountName,
            },
          });
        } }
      >
        {
          index === 0
            ? <div className="recommended text-center">{Strings.RECOMMENDED_OPTION}</div>
            : null
        }
        <div className="quote-item">
          <div className="source">
            <label className={ `${Strings.RECEIVING}` }>
              {`${Strings.RECEIVING} (${params.state.selectedToToken.symbol})`}
            </label>
            <strong>
              {item.amountOut.toString(6)}
            </strong>
            {
              toUSDPrice === undefined
                ? null
                : (
                  <span className="quote-usd">
                    {Strings.USD_UPPERCASE} {Strings.VALUE}: ${toUSDPrice.multiply(item.amountOut).toString(2)}
                  </span>
                )
            }
          </div>
          <div className="hex text-right">
            <label className={ `${Strings.SOURCE}` }>{Strings.SOURCE}</label>
            <strong className=" text-theme-color">{item.dexName}</strong>
          </div>
          <div className="right-icon">
            <img src={ rightArrow } alt={ '' } />
          </div>
        </div>
      </li>
    ));
    return (
      <ul className="swap-quotes-list">
        {listItems}
        <div
          className="text-center fee-details"
          onClick={() => setDisplayFeeDetails(true)}
        >
          {Strings.FEE_FOOTNOTE}
          <img
            src={ support }
            alt="learn more"
            style={{ position: 'relative', top: 6, left: 6 }}
          />
        </div>
      </ul>
    );
  };

  const renderLoadingView = () => {
    return isLoading && <Spinner />;
  };

  return (
    <div className="container">
      <Header headerTitle={ Strings.SWAP_TOKEN_UPPERCASE } support />
      {renderContentHeader()}
      <div className="content-background ds-flex-col full-flex">
        {renderLoadingView()}
        {!isLoading && checkAllowanceObj.status && (
          <React.Fragment>
            <div className="quotes-header">
              <h5>
                {Strings.RECEIVE_TOKEN_MSG}
                <strong className="text-theme-color">
                  {`${params.state.transferAmount.toString(6)} ${params.state.selectedFromToken.symbol} -> ${params.state.selectedToToken.symbol}`}
                </strong>
              </h5>
            </div>
            {error ? <span className="error-text text-gray-color3 text-center">{error}</span> : null}
            <div className="full-flex overflow-auto">
              {swapQuotesListing({ lists: quoteList })}
            </div>
          </React.Fragment>
        )}
      </div>
      <Modal
        isOpen={ displayFeeDetails }
        closeModal={ () => setDisplayFeeDetails(false) }
      >
        <div className="modal-body">
          <div className="text-center text-theme-color">{Strings.FEE_DETAILS_TITLE}</div>
          <div className="text-center">{Strings.FEE_DETAILS.replace('{NATIVE_TOKEN}', selectedNetwork.sym)}</div>
        </div>
      </Modal>
      {isShowApproveRequestPopup && (
        <AlertModel
          alertTitle={ Strings.APPROVE_REQUEST }
          isShowAlert={ isShowApproveRequestPopup }
          successBtnTitle={ Strings.APPROVE }
          cancelBtnTitle={ Strings.REJECT }
          onPressSuccess={ (e) => {
            e.preventDefault();
            setIsShowApproveRequestPopup(false);
            navigate('/approve-transaction', {
              state: {
                accountDetail: params.state.accountDetail,
                selectedFromToken: params.state.selectedFromToken,
                selectedToToken: params.state.selectedToToken,
                transferAmount: params.state.transferAmount,
                isSupportUSD: params.state.isSupportUSD,
                usdValue: params.state.isSupportUSD
                  ? params.state.usdValue
                  : '',
                accountName: params.state.accountName,
              },
            });
          } }
          onPressCancel={ () => {
            setIsShowApproveRequestPopup(false);
            navigate(-1);
          } }
          className="text-center"
        >
          <p>
            {Strings.formatString(Strings.GIVE_ALLOWANCES_MSG, [
              params.state.selectedFromToken.symbol,
            ])}
          </p>
        </AlertModel>
      )}
      {isShowAlert && (
        <AlertModel
          isShowAlert={ isShowAlert }
          onPressSuccess={ () => onClickSuccess() }
          className="text-center"
        >
          <p>{getUserFriendlyErrorMessage(isMessage)}</p>
        </AlertModel>
      )}
    </div>
  );
}

SwapQuotes.propTypes = {
  lists: PropTypes.array,
};
