import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Strings } from 'resources';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import {
  getMaxAmount,
  checkIfWrapperSwap,
  isTokenAddressValid,
} from 'web3-layer';

import { STORAGE_KEYS } from 'constants';
// utils
import { Validation } from 'utils/validations';
import { useTokenIcons } from 'utils/hooks';
import { getItem, setItem } from 'utils/storage';

// Components 
import Header from 'components/Header';
import ProgressBarNumber from 'components/ProgressBarNumber';
import Dropdown from 'components/Dropdown';
import InputField from 'components/InputField';
import CurrencyInputField from 'components/CurrencyInputField';
import Button from 'components/Button';
import Modal from 'components/Modal';
import SelectTokenListCell from 'components/SelectTokenListCell';
import SegmentController from 'components/SegmentController';
import NetworkSelectionModal from 'components/NetworkSelectionModal';

import { NetworksAction } from 'redux/slices/networksSlice';
import { WalletAction } from 'redux/slices/walletSlice';
import { getAccountList, getNetwork, getTokensByAddress } from 'redux/selectors';

import './swap-token.scss';
import PercentageInput from 'components/PercentageInput';
import { parseObject } from 'utils/parse';
import IntegerUnits, { sanitizeFloatingPointString } from 'utils/IntegerUnits';
import { GlobalAction } from 'redux/slices/globalSlice';
import SendTokenAccountListCell from 'components/SendTokenAccountListCell';

export default function SwapToken () {
  const navigate = useNavigate();
  const params = useLocation();
  const dispatch = useDispatch();
  const getTokenIcon = useTokenIcons();

  const networks = useSelector(state => state.networks);
  const wallet = useSelector(state => state.wallet);

  const steps = [1, 2, 3];
  const selectedSteps = 1;
  
  const selectedNetwork = useSelector(getNetwork);
  const accountDetail = params.state?.accountDetail;

  const accountList = useSelector(getAccountList);

  const [selectedAccount, setSelectedAccount] = useState(accountDetail ?? accountList[0]);
  const [showSelectAccount, setShowSelectAccount] = useState(false);

  const [isSelectToToken, setIsSelectToToken] = useState(false);

  const [tokenAddress, setTokenAddress] = useState('');
  const [currentIndex, setCurrentIndex] = useState(0);
  const [isShowChooseToken, setIsShowChooseToken] = useState(false);
  const [amountError, setAmountError] = useState('');
    
  const [isShowNetworkList, setIsShowNetworkList] = useState(false);

  const tokensByAddress = useSelector(getTokensByAddress);
  const tokens = tokensByAddress[selectedAccount.publicAddress.toLowerCase()];
  const [selectedFromTokenId, setSelectedFromTokenId] = useState(tokens?.[0].address ?? '');
  const selectedFromToken = tokens.find(token => token.address.toLowerCase() === selectedFromTokenId.toLowerCase()) ?? {};
  const [selectedToTokenId, setSelectedToTokenId] = useState(tokens?.[0].address ?? {});
  const selectedToToken = tokens.find(token => token.address.toLowerCase() === selectedToTokenId.toLowerCase()) ?? {};

  const usdValue = selectedFromToken?.usdPrice?.value;
  const isSupportUSD = usdValue !== undefined;
  const toUsdValue = selectedToToken?.usdPrice?.value;

  const tokenBalance = selectedFromToken.balance?.value;
  const toTokenBalance = selectedToToken.balance?.value;

  const nativeToken = tokens?.[0];

  const initialValues = {
    amount: '',
  };

  const [values, setValues] = useState(initialValues);
  
  const [slippage, setSlippage] = useState(1);

  const isUSD = currentIndex !== 0;

  const selectAccount = (item) => {
    setShowSelectAccount(false);
    setSelectedAccount(item);
    setSelectedFromTokenId(tokensByAddress[item.publicAddress.toLowerCase()]?.[0].address);
    setSelectedToTokenId(tokensByAddress[item.publicAddress.toLowerCase()]?.[0].address);
  };

  // const onChangeTokenAddress = async (e) => {
  const onSubmitTokenAddress = async (tokenAddress) => {
    // e.preventDefault();
    // const tokenAddress = e.target.value;

    if (tokenAddress === '') return;

    // const filteredTokens = tokens.filter((token) => {
    //   console.log(token);
    //   return true;
    // });

    const tokenAlreadyImported = tokens
      .some(token => token.address.toLowerCase() === tokenAddress.toLowerCase());
      
    if (tokenAlreadyImported) {
      // setImportError(false);
      await selectTokenAddress(tokenAddress);
      setTokenAddress('');
    } else {
      const checkIsValidTokenAddress = await isTokenAddressValid(
        tokenAddress.trim(),
        selectedNetwork,
      );
      // setImportError(!checkIsValidTokenAddress.success);
      if (checkIsValidTokenAddress.success) {
        const { tokenDetails } = checkIsValidTokenAddress;
        await importToken(tokenDetails, tokenAddress);
        setTokenAddress('');
      }
    }
  }

  const onChangeTokenAddress = async (e) => {
    e.preventDefault();
    const tokenAddress = e.target.value;
    setTokenAddress(tokenAddress);
    await onSubmitTokenAddress(tokenAddress);
  };

  const importToken = async (tokenDetails, tokenAddress) => {
    let tempTokens = [];

    /* istanbul ignore else */
    if (networks.tokens) {
      tempTokens = [...networks.tokens];
    }

    const tokenObj = tempTokens.find(
      token => token.id.toLowerCase() === selectedAccount.publicAddress.toLowerCase(),
    );
    
    if (tokenObj) {
      const tokenIndex = tempTokens.indexOf(tokenObj);
      const tokenList = [...tokenObj.tokens];
      tokenList.push({ ...tokenDetails, address: tokenAddress.trim() });
      const tempTokenObj = { ...tokenObj };
      tempTokenObj.tokens = tokenList;
      tempTokens[tokenIndex] = tempTokenObj;
    } else {
      const tempTokenObj = {
        id: selectedAccount.publicAddress,
        tokens: [{ ...tokenDetails, address: tokenAddress.trim() }],
      };
      tempTokens.push(tempTokenObj);
    }

    const { data } = getItem(STORAGE_KEYS.CHAIN_TOKENS);
    const parseObj = parseObject(data);

    let tempChainTokens = [];
    if (parseObj.success) {
      tempChainTokens = parseObj.data;
    }      
    const chainObj = tempChainTokens.find(
      chain => chain.chainId === selectedNetwork.chainId,
    );

    if (chainObj) {
      const chainIndex = tempChainTokens.indexOf(chainObj);        
      chainObj['accountTokens'] = tempTokens;
      tempChainTokens[chainIndex] = chainObj;
    } else {
      tempChainTokens.push({
        chainId: selectedNetwork.chainId,
        accountTokens: tempTokens,
      });
    }
    dispatch(NetworksAction.saveTokens(tempTokens));
    setItem(STORAGE_KEYS.CHAIN_TOKENS, JSON.stringify(tempChainTokens));

    const token = tempTokens
      .find(tokens => tokens.id.toLowerCase() === selectedAccount.publicAddress.toLowerCase())
      .tokens
      .find(token => token.address.toLowerCase() === tokenAddress.toLowerCase());
    selectToken({
      ...token,
      isDefault: false
    });
  };

  const selectTokenAddress = async (tokenAddress) => {
    const token = tokens.find(token => (
      token.address.toLowerCase() === tokenAddress.toLowerCase()
    ));
    selectToken({
      ...token,
      isDefault: false
    });
  };

  const handlePercentageChange = (event) => {
    const inputValue = event.target.value;
    if (inputValue >= 0 && inputValue <= 100) {
      setSlippage(parseFloat(inputValue, 10));
    }
  };

  const handleInputChange = (value) => {
    if(!value){
      setValues({
        amount: '',
      });
      return;
    }
    const sanitized = sanitizeFloatingPointString(value);
    setValues({
      ...values,
      amount: sanitized,
    });
    return sanitized;
  };

  const getConvertedValue = () => {
    try {
      const amount = IntegerUnits.fromFloatingPoint(values.amount);
      if (!isUSD) {
        return `${Strings.USD_UPPERCASE} ${Strings.VALUE}: $${amount.multiply(usdValue).toString(2)}`;
      }
      return `${selectedFromToken.symbol} ${Strings.VALUE}: ${amount.divide(usdValue).toString(6)}`;
    } catch (e) {
      // Error
    }
  };


  const selectToken = (item) => {
    setIsShowChooseToken(false);
    if (isSelectToToken) {
      setSelectedToTokenId(item.address);
    } else {
      setSelectedFromTokenId(item.address);
    }
  };

  const goNext = async (e) => {
    e.preventDefault();    
    if (!Validation.isEmpty(values.amount) && parseFloat(values.amount) > 0) {
      if (
        selectedFromToken.address.toLowerCase() ===
        selectedToToken.address.toLowerCase()
      ) {
        setAmountError(Strings.CHOOSE_DIFFER_TOKEN_MSG);
        return;
      }

      let swapAmount = IntegerUnits.fromFloatingPoint(values.amount);
      if (isUSD) {
        swapAmount = swapAmount.divide(usdValue, true);
      }
      swapAmount = swapAmount.toPrecision(selectedFromToken.decimals, true);
      
      if (swapAmount.gt(tokenBalance)) {
        setAmountError(Strings.SWAP_AMOUNT_ERROR_MSG);        
      } else {
        const isWrapperSwap = checkIfWrapperSwap(
          selectedFromToken.address,
          selectedToToken.address,
          swapAmount,
          selectedNetwork,
        );
        if (isWrapperSwap.success) {
          if (isWrapperSwap.isWrapperSwap) {
            navigate('/swap-confirm-transaction', {
              state: {
                accountDetail: selectedAccount,
                quoteObj: isWrapperSwap.quoteObj,
                fromToken: selectedFromToken,
                toToken: selectedToToken,
                transferAmount: swapAmount,
                isSupportUSD: isSupportUSD,
                usdValue: isSupportUSD ? usdValue : '',
                slippage: slippage,
              }
            });
          } else {
            navigate('/swap-quotes', {
              state: {
                accountDetail: selectedAccount,
                selectedFromToken: selectedFromToken,
                selectedToToken: selectedToToken,
                transferAmount: swapAmount,
                isSupportUSD: isSupportUSD,
                usdValue: isSupportUSD ? usdValue : '',
                slippage: slippage,
              }
            });
          }
        } else {
          setAmountError(isWrapperSwap.error);   
        }
      }
    } else {
      setAmountError(Strings.ENTER_VALID_AMOUNT_MSG);   
    }
  };
  
  const onGetMaxAmount = async (e) => {
    e.preventDefault();
    setValues({ amount: '' });
    const maxAmount = await getMaxAmount(
      2,
      selectedFromToken.address,
      selectedFromToken.decimals,
      tokenBalance,
      selectedNetwork,
      selectedAccount,
    );
    
    if (maxAmount.success) {
      const baseMax = maxAmount.maxBalance;

      if (!isUSD) {
        setValues({
          amount: baseMax.toString(6, true, true)
        });
      } else {
        setValues({
          amount: baseMax.multiply(usdValue).toString(6, true, true)
        });
      }

      setAmountError('');
    } else {
      setAmountError(maxAmount.error);
    }
  };

  const changeNetwork = choosedNetwork => {
    setItem(
      STORAGE_KEYS.SELECTED_NETWORK,
      JSON.stringify(choosedNetwork),
    );
    dispatch(NetworksAction.saveSelectedNetworkObject(choosedNetwork));
    dispatch(WalletAction.resetData(wallet.walletDetail, choosedNetwork));

    setTimeout(() => {
      dispatch(
        GlobalAction.showAlert(
          Strings.NETWORK_SWITCH_TITLE,
          `${Strings.NETWORK_CHANGE_SUCCESS_MSG}${choosedNetwork.networkName}`,
        ),
      );
    }, 400);
  };

  const renderContentHeader = () => {
    return (
      <div className="send-token-header text-center">   
        <div className="pointer" onClick={ () => setIsShowNetworkList(true) }>
          <p>{Strings.CURRENT_NETWORK}</p>
          <h6>{selectedNetwork.networkName}</h6>    
        </div>
        <ProgressBarNumber steps={ steps } selectedSteps={ selectedSteps } />
      </div>
    );
  };

  // render Choose Token View
  const renderChooseToken = (lists) => {
    return (
      <React.Fragment>
        <div className="modal-header">
          <h6 className="text-center">{Strings.CHOOSE_TOKEN_UPPERCASE}</h6>
        </div>
        <InputField
          label={ Strings.TOKEN_ADDRESS }
          value={ tokenAddress }
          onChange={ onChangeTokenAddress }
          name="tokenAddress"
          placeholder={ Strings.ENTER_SWAP_TOKEN_ADDRESS }
        />
        <div className="modal-body">
          {/* {accountSelectErrorMsg ? <span className="error text-center">{accountSelectErrorMsg}</span> : null} */}
          {
            lists.map((item, index) => (
              <SelectTokenListCell
                dataTestid={ `choose-token-${index}` }
                key={ index }
                item={ item }
                index={ index }
                networkObj={ selectedNetwork }
                accountAddress={ selectedAccount }
                subtext={ Strings.BALANCE }
                onSelectToken={ (item, index) =>
                  selectToken(item, index)
                }
              />
            ))
          }
        </div>
      </React.Fragment>

    );
  };

  const renderSelectAccount = () => {
    return (
      <React.Fragment>
        <div className="modal-header">
          <h6 className="text-center">{Strings.CHOOSE_ACCOUNTS_UPPERCASE}</h6>
        </div>
        <div className="modal-body">
          {accountList.map((item, index) => (
            <SendTokenAccountListCell
              dataTestid={ `choose-account-${index}` }
              key={ index }
              item={ item }
              index={ index }
              subtext={ Strings.BALANCE }
              onSelectAccount={ selectAccount }
            />
          ))}
        </div>
      </React.Fragment>
    );
  };

  const renderAmountView = () => {
    const segmentList = [{ title: selectedFromToken.symbol }];
    if (isSupportUSD) {
      segmentList.push({ title: Strings.USD_UPPERCASE });
    }

    return (
      <div className="form-group amount-form">
        <div className="label-and-max">
          <label>{Strings.ENTER_AMOUNT_TO_SWAP}</label>
          <div className="max-btn text-right">
            <Button
              onClick={ (e) => onGetMaxAmount(e) }
              label={ Strings.MAX }
              className="button-inline btn-sm-h mt-0"
            />
          </div>
        </div>
        <div className="segment-controller">
          <SegmentController
            segments={ segmentList }
            currentIndex={ currentIndex }
            onChangeIndex={ index => { setCurrentIndex(index); setValues({amount: ''}); } }
            className={ 'currentIndex' }
          />
          <div className="enter-amount-frm">
            <CurrencyInputField
              dataTestid={ 'enter-amount-to-send' }
              name="amount"
              value={ values.amount.toString() }
              onChange={ handleInputChange }
              placeholder={ Strings.AMOUNT }
              isUSD={ isUSD }
              decimalsLimit={ isUSD ? 6 : selectedFromToken.decimals }
            />
          </div>
        </div>
        {isSupportUSD && (
          <em className="get-converted-value text-right opacity">
            {getConvertedValue()}
          </em>
        )}
        {amountError ? <span className="error">{amountError}</span> : null}
      </div>
    );
  };

  const fromBalanceString = `${tokenBalance?.toString(6) ?? ''} ${selectedFromToken.symbol ?? ''}`;
  const fromValueString = isSupportUSD
    ? `$${tokenBalance?.multiply?.(usdValue)?.toString(2)}`
    : '';

  const toBalanceString = `${toTokenBalance?.toString(6) ?? ''} ${selectedToToken.symbol ?? ''}`;
  const toValueString = toUsdValue === undefined
    ? ''
    : `$${toTokenBalance?.multiply?.(toUsdValue)?.toString(2)}`;

  return (
    <div className="container">
      <Header
        headerTitle={ Strings.SWAP_TOKEN_UPPERCASE }
        support
      />
      {renderContentHeader()}

      <div className="content-background ds-flex-col full-flex">
        <div className="full-flex overflow-auto">
          <div className="wrapper swap-header-from">
            <Dropdown
              dataTestid="swap-token-choose-account-button"
              label={ Strings.CHOOSE_SWAP_ACCOUNT }
              value={ selectedAccount.name }
              subtext={ 'Balance:' }
              subvalue={ `${nativeToken?.balance?.value?.toString(6, true) ?? ''} ${nativeToken?.symbol}` }
              onClick={ (e) => {
                e.preventDefault();
                setShowSelectAccount(true);
              } }
            />
            <div className="full-flex token-inputs">
              <Dropdown
                dataTestid="choose-token-to-swap-from"
                icon={ getTokenIcon(selectedFromToken.address, selectedNetwork.chainId) }
                label={ Strings.CHOOSE_FROM_TOKEN_TO_SWAP }
                value={ fromBalanceString }
                subtext={ fromValueString }
                className="full-flex token-input"
                onClick={ (e) => {
                  e.preventDefault();
                  setIsShowChooseToken(true);
                  setIsSelectToToken(false);
                } }
                smallValue
                themeColorSubtext
              />
              <Dropdown
                dataTestid="choose-token-to-swap-to"
                icon={ getTokenIcon(selectedToToken.address, selectedNetwork.chainId) }
                label={ Strings.CHOOSE_TO_TOKEN_TO_SWAP }
                value={ toBalanceString }
                subtext={ toValueString }
                className="full-flex token-input"
                onClick={ (e) => {
                  e.preventDefault();
                  setIsShowChooseToken(true);
                  setIsSelectToToken(true);
                } }
                smallValue
                themeColorSubtext
              />
            </div>
            {renderAmountView()}
            <PercentageInput onChange={ handlePercentageChange } />
          </div>
        </div>
        <div className="content-footer">
          <Button
            onClick={ (e) => goNext(e) }
            label={ Strings.NEXT_UPPERCASE }
            testId="swap-token-submit-btn"
          />
        </div>
      </div>
      {isShowChooseToken ?        
        <Modal
          isOpen={ isShowChooseToken }
          closeModal={ (_e) => setIsShowChooseToken(false) }
        >
          {renderChooseToken(tokens)}
        </Modal>          
        : null}

      {showSelectAccount ? (
        <Modal
          isOpen={ showSelectAccount }
          closeModal={ () => setShowSelectAccount(false) }
        >
          {renderSelectAccount()}
        </Modal>
      ) : null}

      {isShowNetworkList && (
        <NetworkSelectionModal
          isOpen={ isShowNetworkList }
          isShowSwapSupported={ true }
          closeModal={ (_e) => setIsShowNetworkList(false) }          
          onSelectNetwork={ item => {
            setIsShowNetworkList(false);
            if (item.chainId !== selectedNetwork.chainId) {
              changeNetwork(item);
            }
          } }
        />
      )}
    </div>
  );
}

SwapToken.propTypes = {
  item: PropTypes.object,
  index: PropTypes.string,
  subtitle: PropTypes.string,
  title: PropTypes.string,
  isModalVisible: PropTypes.bool,
  children: PropTypes.object,
  lists: PropTypes.array,
};
