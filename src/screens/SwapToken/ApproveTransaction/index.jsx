import React, { useCallback, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'resources';
import { useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import {
  getSingleAccount,
  getTokenApproveTxnObj,
  calculateEstimateFee,
  WalletTransactionTypes,
  createAndSendTxn,
  calculateMaxFee,
} from 'web3-layer';

// utils
import { useTokenIcons } from 'utils/hooks';

// Components
import Header from 'components/Header';
import ProgressBarNumber from 'components/ProgressBarNumber';
import Button from 'components/Button';
import Spinner from 'components/Spinner';
import Modal from 'components/Modal';
import EstimatedGasFee from 'components/EstimatedGasFee';
import AlertModel from 'components/AlertModel';
import CurrencyInputField from 'components/CurrencyInputField';

// Images
import dropIcon from 'assets/images/drop.svg';

import './approve-transaction.scss';
import IntegerUnits, { sanitizeFloatingPointString } from 'utils/IntegerUnits';
import { getUserFriendlyErrorMessage } from 'utils/errors';
import { getNetwork } from 'redux/selectors';

const MAX_256 = (2n ** 256n) - 1n;

export default function ApproveTransaction () {
  const navigate = useNavigate();
  const params = useLocation();
  const getTokenIcon = useTokenIcons();

  const steps = [1, 2, 3];
  const selectedSteps = 2;

  const networks = useSelector((state) => state.networks);
  const wallet = useSelector((state) => state.wallet);

  const selectedNetwork = useSelector(getNetwork);
  const wdSupportObject = networks.wdSupportObject;

  const [txnObject, setTxnObject] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [isShowAlert, setIsShowAlert] = useState(false);

  const [isShowEstimatedGasFee, setIsShowEstimatedGasFee] = useState(false);
  const [gasRange, setGasRange] = useState({});
  const accountDetail = params.state.accountDetail;
  const { selectedFromToken } = params.state;

  const [selectedRange, setSelectedRange] = useState(1);
  const [accountBalance, setAccountBalance] = useState(0);
  const [isSelectCustom, setIsSelectCustom] = useState(false);
  
  // hardware wallet
  const [isShowHardwareModal, setIsShowHardwareModal] = useState(false);
  const [isShowHardwareAcceptRejectModal, setIsShowHardwareAcceptRejectModal] =
        useState(false);
  const [hardwareError, setHardwareError] = useState('');
  const [isShowDeclinePopup, setIsShowDeclinePopup] = useState(false);

  const [isEditPermission, setIsEditPermission] = useState(false);
  const [customAmountError, setCustomAmountError] = useState('');

  const [customAmount, setCustomAmount] = useState(params.state.transferAmount);
  const [customAmountText, setCustomAmountText] = useState(params.state.transferAmount.toString(6, false, true));

  const approvalAmount = isSelectCustom
    ? customAmount
    : new IntegerUnits(MAX_256, selectedFromToken.decimals);

  const handleInputChange = useCallback(text => {
    text = sanitizeFloatingPointString(text);
    setCustomAmount(IntegerUnits.fromFloatingPoint(text).toPrecision(selectedFromToken.decimals));
    setCustomAmountText(text);
    setCustomAmountError('');
    return text;
  }, [setCustomAmount, setCustomAmountText]);

  const getAccountBalance = async () => {
    const balance = await getSingleAccount(
      accountDetail.publicAddress,
      selectedNetwork
    );
    if (balance.success) {
      setAccountBalance(balance.account.value);
    }
  };

  const getTxnObject = useCallback(async () => {
    setIsLoading(true);
    const tokenObj = await getTokenApproveTxnObj(
      wdSupportObject.swapRouterV3,
      accountDetail.publicAddress,
      selectedFromToken.address,
      selectedNetwork,
      approvalAmount,
    );
    setIsLoading(false);

    if (tokenObj.success) {
      setGasRange(tokenObj.range);
      setTxnObject(tokenObj.txnObject);
    }
  }, [approvalAmount.value]);
  
  useEffect(() => {
    getTxnObject();
    getAccountBalance();
  }, [getTxnObject]);

  const updateTxnObjectTypeTwo = (gas, maxPrioFee, maxFeePerGas) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.maxPriorityFeePerGas = maxPrioFee;
    tempTxObj.maxFeePerGas = maxFeePerGas;
    setTxnObject(tempTxObj);
  };

  const updateTxnObjectTypeOne = (gas, gasPrice) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.gasPrice = gasPrice;
    setTxnObject(tempTxObj);
  };

  const getEstimateFee = () => {
    if (!txnObject) return '';
    const estimateFee = calculateEstimateFee(txnObject, selectedNetwork.txnType);
    return `${estimateFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getMaxFee = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const clickToApproveSwap = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const isHardware = !!accountDetail.isHardware;
    if (isHardware) setIsShowHardwareAcceptRejectModal(true);

    try {
      const txnResult = await createAndSendTxn(
        WalletTransactionTypes.SWAP,
        txnObject,
        wallet,
        accountDetail,
        accountBalance,
        selectedNetwork,
        {
          isApprove: true,
          address: selectedFromToken.address,
          transferAmount: params.state.transferAmount
        }
      );

      if (txnResult.success) {
        setIsLoading(false);
        if (isHardware) setIsShowHardwareAcceptRejectModal(false);
        navigate(-1);
      } else {
        setIsLoading(false);
        if (!isHardware) {
          setError(txnResult.error);
          setIsShowAlert(true);
        } else {
          setHardwareError(txnResult.error);
          setIsShowAlert(false);
          setIsShowHardwareAcceptRejectModal(false);
          setIsShowHardwareModal(true);
          if (
            txnResult.error ===
              'Ledger device: Condition of use not satisfied (denied by the user?) (0x6985)' ||
            txnResult.error === 'Action cancelled by user'
          ) {
            setHardwareError('');
            setIsShowHardwareModal(false);
            setIsShowDeclinePopup(true);
          }
        }
      }
    } catch (err) {
      setIsLoading(false);
      if (isHardware) setIsShowHardwareModal(false);
      console.error(err);
    }
  };

  const onClickSuccess = () => {
    setIsShowAlert(false);
  };

  const onClickSubmit = (e) => {
    e.preventDefault();
    if (isSelectCustom) {
      if (params.state.transferAmount.gt(customAmount)) {
        setCustomAmountError(Strings.CUSTOM_LIMIT_NOT_VALID);
        return;
      } else {
        getTxnObject();
      }
    } else {
      getTxnObject();
    }
    setIsEditPermission(false);
  };

  const renderContentHeader = () => {
    return (
      <div className="send-token-header text-center">
        <ProgressBarNumber steps={ steps } selectedSteps={ selectedSteps } />
      </div>
    );
  };

  const headingStyle = () => {
    return (
      <div className="heading-styles">
        <span className="line" />
        <span className="line" />
        <h6 className="text-center uppercase">
          {Strings.APPROVE_TRANSACTION_UPPERCASE}
        </h6>
        <span className="line" />
        <span className="line" />
      </div>
    );
  };

  const sendPermissionContent = () => {
    return (
      <div className="permission-details text-center">
        <h3>
          {Strings.formatString(Strings.GIVE_PERMISSION_MSG, [
            selectedFromToken.symbol,
          ])}
        </h3>
        <p>{Strings.GRANT_MSG}</p>
      </div>
    );
  };

  const renderAccountInfo = () => {
    return (
      <div className={ 'form-group' }>
        <div className="rounded-control noselect">
          <div className="body">
            <h6 className="uppercase">{Strings.CONTRACT}</h6>
            <span className="text-theme-color">
              {`${Strings.IM_SWAP_ROUTER} ${wdSupportObject.swapRouterV3}`}
            </span>
          </div>
        </div>
      </div>
    );
  };

  const sendPermissionAmount = () => {
    return (
      <div className="send-amount">
        <small>{Strings.AMOUNT}</small>
        <h2>
          {!isSelectCustom
            ? '∞' + ` ${selectedFromToken.symbol}` 
            : `${approvalAmount.toString(6)} ${selectedFromToken.symbol}`
          }
          <img
            src={ getTokenIcon(
              selectedFromToken.address,
              selectedNetwork.chainId
            ) }
            alt={ selectedFromToken.symbol }
          />
        </h2>
        <span className="flex-row">
          {params.state.isSupportUSD && (
            <React.Fragment>
              <small>{Strings.VALUE}: </small>
              <span className="left-sm-space">
                {`$${params.state.transferAmount.multiply(params.state.usdValue).toString(2)}`}
              </span>
            </React.Fragment>
          )}
        </span>
      </div>
    );
  };

  const sendPermissionEstimated = () => {
    return (
      <div className="send-estimated permission-estimated">
        <div className="send-row">
          <div className="body">
            <h6>{Strings.ESTIMATE_GAS_FEE}</h6>
          </div>
          <div className="right text-right">
            <h6
              className="uppercase text-theme-color"
              onClick={ () => setIsShowEstimatedGasFee(true) }
            >
              {getEstimateFee()}
              <img src={ dropIcon } alt="" />
            </h6>
            {selectedNetwork.txnType == 2 && (
              <span>
                <small className="text-gray-color3">{Strings.MAX_FEE}</small>{' '}
                {getMaxFee()}
              </span>
            )}
          </div>
        </div>
        <div className="send-row">
          <div className="body">
            <div className="row-item">
              <h6>{Strings.PERMISSION_REQUEST}</h6>
              <span>
                <small className="text-gray-color3">
                  {Strings.SWAP_PERMISSION_INFO}
                </small>
              </span>
            </div>
            <div className="row-item">
              <span>
                <small className="text-gray-color3">
                  {Strings.APPROVED_AMOUNT}
                </small>
              </span>
              <h6>
                {!isSelectCustom
                  ? '∞' + ` ${selectedFromToken.symbol}`
                  : `${approvalAmount.toString(6)} ${selectedFromToken.symbol}`
                }
              </h6>
            </div>
            <div className="row-item">
              <span>
                <small className="text-gray-color3">{Strings.GRANTED_TO}</small>
              </span>
              <h6>{`${Strings.CONTRACT} (${wdSupportObject.swapRouterV3})`}</h6>
            </div>
            <div className="row-item">
              <h6>{Strings.DATA}</h6>
            </div>
            <div className="row-item">
              <span>
                <small className="text-gray-color3">{Strings.FUNCTION}</small>
              </span>
              <h6>{Strings.APPROVE}</h6>
            </div>
            <h6>{txnObject?.data}</h6>
          </div>
          <div className="right text-right float-right">
            <h6
              className="uppercase text-theme-color pointer"
              onClick={ () => setIsEditPermission(true) }
            >
              {Strings.EDIT_UPPERCASE}
            </h6>
          </div>
        </div>
      </div>
    );
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header headerTitle={ Strings.SWAP_TOKEN_UPPERCASE } support />
        {renderContentHeader()}
        <div className="shape-rounded"></div>
        <div className="wrapper ct-wrapper">
          <div className="ct-container">
            <div className="content">
              {!isLoading ? (
                <React.Fragment>
                  {headingStyle()}
                  {sendPermissionContent()}
                  {renderAccountInfo()}
                  {sendPermissionAmount()}
                  {sendPermissionEstimated()}
                </React.Fragment>
              ) : (
                <Spinner />
              )}
            </div>
            {isShowAlert && (
              <AlertModel
                isShowAlert={ isShowAlert }
                onPressSuccess={ () => onClickSuccess() }
                className="text-center"
              >
                <p>{getUserFriendlyErrorMessage(error)}</p>
              </AlertModel>
            )}
          </div>
        </div>
        {!isLoading ? (
          <div className="content-footer">
            <Button
              onClick={ (e) => clickToApproveSwap(e) }
              label={ Strings.CLICK_TO_APPROVE }
            />
          </div>
        ) : (
          <Spinner />
        )}

        {isShowEstimatedGasFee ? (
          <Modal
            isOpen={ isShowEstimatedGasFee }
            closeModal={ () => setIsShowEstimatedGasFee(false) }
          >
            <EstimatedGasFee
              selectedRange={ selectedRange }
              gasRange={ gasRange }
              txnObject={ txnObject }
              network={ selectedNetwork }
              onClickSave={ (updatedObject) => {
                setIsShowEstimatedGasFee(false);
                if (selectedNetwork.txnType != 0) {
                  updateTxnObjectTypeTwo(
                    updatedObject.gas,
                    updatedObject.maxPrioFee,
                    updatedObject.maxFeePerGas
                  );
                  setSelectedRange(selectedRange);
                } else {
                  updateTxnObjectTypeOne(
                    updatedObject.gas,
                    updatedObject.gasPrice
                  );
                }
              } }
            />
          </Modal>
        ) : null}
        {isEditPermission ? (
          <Modal
            isOpen={ isEditPermission }
            closeModal={ () => setIsEditPermission(false) }
          >
            <div className="edit-modal">
              <div className="edit-header border-bottom">
                <h3 className="text-center">{Strings.EDIT_PERMISSION_TITLE}</h3>
                <p className="text-center">{Strings.EDIT_PERMISSION_CONTENT}</p>
              </div>
              <div>
                <div className="edit-row">
                  <button className={ `radio-btn ${!isSelectCustom ? 'active' : null}` } onClick={ () => setIsSelectCustom(false) }>
                    <span className="icon"></span>
                    <div className="radio-content">
                      <strong className={ `${!isSelectCustom ? 'text-theme-color' : null}` }>{Strings.PROPOSED_APPROVAL_LIMIT}</strong>
                      <small>{Strings.PROPOSED_APPROVAL_CONTENT}</small>                     
                      <div className="mt-05"><strong> {'∞ ' + selectedFromToken.symbol}</strong></div>
                    </div>
                  </button>                                 
                </div>
                <div className="edit-row">
                  <button className={ `radio-btn ${isSelectCustom ? 'active' : null}` } onClick={ () => setIsSelectCustom(true) }>
                    <span className="icon"></span>
                    <div className="radio-content">
                      <strong className={ `${isSelectCustom ? 'text-theme-color' : null}` }>{Strings.CUSTOM_SPEND_LIMIT_TITLE}</strong>
                      <small>{Strings.CUSTOM_SPEND_LIMIT_CONTENT}</small>
                    </div>
                  </button>
                  <div className="radio-content">
                    <CurrencyInputField
                      dataTestid={ 'enter-amount-to-send' }
                      name="customAmount"
                      placeholder=""
                      value={ customAmountText }
                      onChange={ handleInputChange }
                      className={ `${!isSelectCustom && ('disable')}` }
                      decimalsLimit={ selectedFromToken.decimals }
                    />
                    {customAmountError ? <span className="error">{customAmountError}</span> : null}
                  </div>
                </div>
              </div>
              <div className="edit-footer">
                <Button
                  label={ Strings.SUBMIT_UPPERCASE }
                  className="btn-full"
                  onClick={ (e) => onClickSubmit(e) }
                />
              </div>
            </div>
          </Modal>
        ) : null}
        {isShowHardwareModal && (
          <Modal
            isOpen={ isShowHardwareModal }
            closeModal={ () => {
              setIsShowHardwareModal(!isShowHardwareModal);
            } }
          >
            <div className="modal-body">
              {hardwareError && (
                <div>
                  <label className="label-error-form">{hardwareError}</label>
                  <Button
                    onClick={(e) => {
                      e.preventDefault();
                      setIsShowHardwareModal(!isShowHardwareModal);
                      navigate('/accounts-details', { state: { item: accountDetail } });
                    }}
                    label={ Strings.GO_BACK_BUTTON_UPPERCASE }
                  />
                </div>
              )}
            </div>
          </Modal>
        )}
        {isShowHardwareAcceptRejectModal && (
          <Modal isOpen={ isShowHardwareAcceptRejectModal }>
            <div className="spinner-container-hardware">
              <div className="loading-spinner-hardware" />
            </div>
            <p className="label-approve-reject-form">
              {Strings.APPROVE_OR_REJECT}
            </p>
          </Modal>
        )}
        {isShowDeclinePopup && (
          <AlertModel
            alertTitle={ Strings.DECLINED }
            isShowAlert={ isShowDeclinePopup }
            onPressSuccess={ () => {
              setIsShowDeclinePopup(!isShowDeclinePopup);
              navigate('/accounts-details', {
                state: { item: params.state.accountDetail },
              });
            } }
            className="text-center"
            successBtnTitle={ Strings.OKAY }
          >
            <p>{Strings.USER_REJECTED}</p>
          </AlertModel>
        )}
      </div>
    </React.Fragment>
  );
}

ApproveTransaction.propTypes = {
  subtitle: PropTypes.string,
  title: PropTypes.string,
  isModalVisible: PropTypes.string,
  children: PropTypes.string,
  onItemClicked: PropTypes.func,
  isActive: PropTypes.bool,
  onClickFunc: PropTypes.func,
  message: PropTypes.string,
  label: PropTypes.string,
  rightText: PropTypes.string,
  submessage: PropTypes.string,
};
