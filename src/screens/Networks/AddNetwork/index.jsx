import React, { useState } from 'react';
import { Strings } from 'resources';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { runNetworkValidation, addNewBlockchainNetwork } from 'web3-layer';
import { STORAGE_KEYS } from '../../../constants';

import './add-network.scss';

import { NetworksAction } from 'redux/slices/networksSlice';

// utils 
import { Validation } from 'utils/validations';
import { setItem } from 'utils/storage';

// Components 
import Header from 'components/Header';
import InputField from 'components/InputField';
import Button from 'components/Button';
import Spinner from 'components/Spinner';
import { getNetworks } from 'redux/selectors';

export default function AddNetworks () {

  const navigate = useNavigate();
  const dispatch = useDispatch();
  
  const networks = useSelector(state => state.networks);
  const networksList = useSelector(getNetworks);

  const [isLoading, setIsLoading] = useState(false);
  const [errorNetworkName, setErrorNetworkName] = useState('');
  const [errorRpcUrl, setErrorRpcUrl] = useState('');
  const [errorChainId, setErrorChainId] = useState('');
  const [errorSymbol, setErrorSymbol] = useState('');
  const [errorBlockUrl, setErrorBlockUrl] = useState('');

  const initialValues = {
    networkName: '',
    rpcUrl: '',
    chainId: '',
    symbol: '',
    blockUrl: '',
  };

  const [values, setValues] = useState(initialValues);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const checkValidation = () => {
    const isEmptyNetworkName = Validation.isEmpty(values.networkName);
    const isEmptyRpcUrl = Validation.isEmpty(values.rpcUrl);
    const isEmptyChainId = Validation.isEmpty(values.chainId);
    const isEmptySymbol = Validation.isEmpty(values.symbol);
    const isEmptyBlockUrl = Validation.isEmpty(values.blockUrl);

    const isValidRpcUrl = Validation.isValidURL(values.rpcUrl);
    const isValidBlockUrl = Validation.isValidURL(values.blockUrl);

    const networkExists = networksList.some(network => network.chainId === parseInt(values.chainId, 10));

    if (isEmptyNetworkName) {
      setErrorNetworkName(Strings.ENTER_NETWORK_NAME_MSG);
    } else{
      setErrorNetworkName('');
    }
    if (isEmptyRpcUrl) {
      setErrorRpcUrl(Strings.ENTER_RPC_URL_MSG);
    } else if (!isValidRpcUrl && !isEmptyRpcUrl) {
      setErrorRpcUrl(Strings.ENTER_VALID_RPC_MSG);
    } else {
      setErrorRpcUrl('');
    }

    if (isEmptyChainId) {
      setErrorChainId(Strings.ENTER_CHAIN_ID_MSG);
    } else if (networkExists) {
      setErrorChainId(Strings.NETWORK_ALREADY_EXIST_MSG);
    } else {
      setErrorChainId('');
    }

    if (isEmptySymbol) {
      setErrorSymbol(Strings.ENTER_SYMBOL_NAME_MSG);
    } else {
      setErrorSymbol('');
    }
    
    if (!isEmptyBlockUrl && !isValidBlockUrl) {
      setErrorBlockUrl(Strings.ENTER_VALID_BLOCK_URL_MSG);
    } else {
      setErrorBlockUrl('');
    }
    
    if (
      !isEmptyNetworkName &&
      !isEmptyRpcUrl &&
      !isEmptyChainId &&
      !isEmptySymbol &&
      (isEmptyBlockUrl || isValidBlockUrl) &&
      !networkExists &&
      isValidRpcUrl
    ) {
      return true;
    }
    else {
      return false;
    }
  };

  const _onPressAddNetwork = async (e) => {
    e.preventDefault();
    if (checkValidation()) {
      setIsLoading(true);
      const networkValidation = await runNetworkValidation(
        values.rpcUrl,
        values.chainId,
      );

      if (networkValidation.success) {
        const addNetworkObject = await addNewBlockchainNetwork(
          values.networkName,
          values.rpcUrl,
          values.chainId,
          values.symbol,
          values.blockUrl,
        );

        setIsLoading(false);

        if (addNetworkObject.success) {     
          
          const newNetworks = [...networks.networks, addNetworkObject.networkDetails];
          await setItem(
            STORAGE_KEYS.NETWORKS,
            JSON.stringify(newNetworks),
          );
          dispatch(NetworksAction.saveNetworks(newNetworks));
          navigate(-1);
        }
        else{
          setErrorBlockUrl(addNetworkObject.error);
        }
      } else {
        setIsLoading(false);
        setErrorBlockUrl(networkValidation.error);
      }
    }
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header
          headerTitle={ Strings.ADD_NETWORK_UPPERCASE }
          support
        />
        <div className="content-background ds-flex-col full-flex">
          <div className="wrapper">
            <div className="inner-content-background">
              <p className="text-sm italic opacity text-center">{Strings.ADD_NETWORK_MSG}</p>
              <InputField
                label={ Strings.NETWORK_NAME }
                dataTestid="add-network-network_name"
                value={ values.networkName }
                onChange={ handleInputChange }
                error={ errorNetworkName }
                name="networkName"
                placeholder={ Strings.NETWORK_NAME }
              />
              <InputField
                label={ Strings.RPC_URL }
                dataTestid="add-network-rpc_url"
                value={ values.rpcUrl }
                onChange={ handleInputChange }
                name="rpcUrl"
                placeholder={ Strings.NEW_RPC_NETWORK }
                error={ errorRpcUrl }
              />
              <InputField
                label={ Strings.CHAIN_ID }
                dataTestid="add-network-chain_id"
                value={ values.chainId }
                onChange={ handleInputChange }
                name="chainId"
                placeholder={ Strings.CHAIN_ID }
                error={ errorChainId }
              />
              <InputField
                label={ Strings.SYMBOL }
                dataTestid="add-network-symbol"
                value={ values.symbol }
                onChange={ handleInputChange }
                name="symbol"
                placeholder={ Strings.SYMBOL }
                error={ errorSymbol }
              />
              <InputField
                label={ Strings.BLOCK_EXPLORER_URL }
                dataTestid="add-network-block_explorer_url"
                value={ values.blockUrl }
                onChange={ handleInputChange }
                name="blockUrl"
                placeholder={ `${Strings.BLOCK_EXPLOR_URL_OPTIONAL}` }
                error={ errorBlockUrl }
              />
              {!isLoading ?
                <Button
                  testId="add-network-button"
                  onClick={ (e) => _onPressAddNetwork(e) }
                  label={ Strings.ADD_NETWORK_UPPERCASE }
                  className="btn-full"
                /> 
                : 
                <Spinner />
              }
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
