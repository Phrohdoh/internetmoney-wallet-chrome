import React, { useState } from 'react';
import { Strings } from 'resources';
import { useSelector, useDispatch, useStore } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { STORAGE_KEYS } from '../../constants';

import './networks.scss';

import { NetworksAction } from 'redux/slices/networksSlice';
import { WalletAction } from 'redux/slices/walletSlice';
import { GlobalAction } from 'redux/slices/globalSlice';
import { getNetwork, getNetworks } from 'redux/selectors';

// utils
import { setItem } from 'utils/storage';

// Components 
import Header from 'components/Header';
import NetworkListCell from 'components/NetworkListCell';
import AlertModel from 'components/AlertModel';
import Navbar from 'components/Navbar';

export default function Networks () {
  
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const store = useStore();

  const wallet = useSelector(state => state.wallet);
  const networksList = useSelector(getNetworks);
  const selectedNetwork = useSelector(getNetwork);

  const [choosedNetwork, setChoosedNetwork] = useState({});
  const [isShowConfirmationPopup, setIsShowConfirmationPopup] = useState(false);

  const [isRemoveEnable, setIsRemoveEnable] = useState(false);

  const changeNetwork = async () => {
    setIsShowConfirmationPopup(false);
    await setItem(
      STORAGE_KEYS.SELECTED_NETWORK,
      JSON.stringify(choosedNetwork),
    );
    dispatch(NetworksAction.saveSelectedNetworkObject(choosedNetwork));
    dispatch(WalletAction.resetData(wallet.walletDetail, choosedNetwork));

    setTimeout(() => {
      dispatch(
        GlobalAction.showAlert(
          Strings.NETWORK_SWITCH_TITLE,
          `${Strings.NETWORK_CHANGE_SUCCESS_MSG}${choosedNetwork.networkName}`,
        ),
      );
    }, 200);
  };

  // Remove Network
  const removeNetwork = async item => {
    const tempNetworksList = [...networksList];
    if (tempNetworksList.length > 1) {
      const idx = tempNetworksList.findIndex(x => x.chainId === item.chainId);
      tempNetworksList.splice(idx, 1);
      await setItem(
        STORAGE_KEYS.NETWORKS,
        JSON.stringify(tempNetworksList),
      );
      dispatch(NetworksAction.saveNetworks(tempNetworksList));
      if (item.chainId === selectedNetwork.chainId) {
        await setItem(
          STORAGE_KEYS.SELECTED_NETWORK,
          JSON.stringify(tempNetworksList[0]),
        );
        dispatch(NetworksAction.saveSelectedNetworkObject(tempNetworksList[0]));
        dispatch(WalletAction.resetData());        
      }

      if (tempNetworksList.length <= 1) {
        setIsRemoveEnable(false);
      }
    }    
    if (item.isDefault) {
      dispatch(NetworksAction.removeDefaultNetwork(item.chainId));
      await setItem(
        STORAGE_KEYS.REMOVED_DEFAULT_NETWORKS,
        JSON.stringify(store.getState().networks.removedDefaultNetworks)
      );
    }
  };

  const renderRecentAccounts = () => {
    return (
      <div className="list-style network-list-style">
        {
          networksList.map((item, index) => (
            <NetworkListCell
              key={ index }
              item={ item }
              index={ index }
              selectedNetwork={ selectedNetwork }
              dataTestid={ `network-list-cell-${index}` }
              isRemoveEnable={ isRemoveEnable && networksList.length > 1 }     
              onSelectNetwork={ item => {
                if (item.chainId !== selectedNetwork.chainId) {
                  setChoosedNetwork(item);
                  setIsShowConfirmationPopup(true);
                }
              } }
              onClickDelete={ item => {
                removeNetwork(item);
              } }
            />
          ))
        }
      </div>

    );
  };
  
  return (
    <React.Fragment>
      <div className="main-container">
        <Navbar /> 
        <div className="container">          
          <div className="main-content">
            <Header
              headerTitle={ Strings.NETWORK_UPPERCASE }
              support
              className="navigator-header"
            />
            <div className="content-background ds-flex-col full-flex">
              <div className="wrapper p-20">
                <div className="filter-header">
                  <span
                    data-testid="add-network-btn"
                    className="filter-btn"
                    onClick={ () => navigate('/add-network') }
                  >
                    + {Strings.ADD_NETWORK_UPPERCASE}
                  </span>
                  <span
                    data-testid="remove-network-btn"
                    className="filter-btn"
                    onClick={ () => {
                      if (networksList.length > 1) {
                        setIsRemoveEnable(!isRemoveEnable);
                      }
                    } }
                  >
                    {!isRemoveEnable
                      ? Strings.REMOVE
                      : Strings.DONE}
                  </span>                  
                </div>
                {renderRecentAccounts()}
              </div>
            </div>
            {isShowConfirmationPopup && (
              <AlertModel
                alertTitle={ Strings.NETWORK_SWITCH }
                isShowAlert={ isShowConfirmationPopup }
                onPressSuccess={ () => changeNetwork() }
                successBtnTitle={ Strings.YES }
                onPressCancel={ () => setIsShowConfirmationPopup(false) }
                className="text-center"
              > 
                <p>
                  {Strings.NETWORK_SWITCH_MSG}
                  <strong>
                    {choosedNetwork.networkName}
                  </strong>
                </p>
              </AlertModel>
            )}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
