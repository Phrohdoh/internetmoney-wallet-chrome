import React from 'react';
import { useLocation } from 'react-router-dom';
import QRCode from 'react-qr-code';
import copy from 'copy-to-clipboard'; 
import toast from 'react-hot-toast';

import { Strings } from 'resources';

import './receive-token.scss';

// utils

// Components 
import Header from 'components/Header';
import Button from 'components/Button';

// Images
import copyIcon from 'assets/images/copy.svg';

export default function ReceiveToken () {

  
  const params = useLocation();
  const publicAddress = params.state.accountDetail.publicAddress;

  const copyToClipboard = (e) => {
    e.preventDefault();
    copy(publicAddress);
    toast(Strings.ADDRESS_COPIED_SUCCESS);
  };


  return (
    <React.Fragment>
      <div className="container">                
        <Header
          headerTitle={ Strings.RECEIVE_TOKENS_UPPERCASE }
          support
        />
      
        <div className="content-background ds-flex-col full-flex">
          <div className="wrapper">
            <div className="rt-container">
              <div className="qr-container">
                <QRCode value={ publicAddress } size={ 180 } />
                <p className="text-theme-color text-center">
                  <span className="publicAddress text-theme-color">
                    {publicAddress}
                  </span>
                </p>
              </div>
              <h6 className="text-center opacity">{Strings.RECEIVE_TOKENS_MSG_UPPERCASE}</h6>
              <div className="buttons">
                <Button     
                  icon={ copyIcon }
                  onClick={ (e) => copyToClipboard(e) }                
                  label={ Strings.COPY_UPPERCASE }
                  testId="copy-btn"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
