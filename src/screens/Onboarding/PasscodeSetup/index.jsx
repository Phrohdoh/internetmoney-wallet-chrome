import React, { useState } from 'react';
import { Strings } from 'resources';
import { useNavigate, useLocation } from 'react-router-dom';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import Passcode from 'components/Passcode';

import '../onboarding.scss';
import { useDispatch } from 'react-redux';
import { GlobalAction } from 'redux/slices/globalSlice';

export default function PasscodeSetup () {
  const dispatch = useDispatch();
  const params = useLocation();
  const navigate = useNavigate();
  
  const [passcode, setPasscode] = useState('');
  const [confPasscode, setConfPasscode] = useState('');
  const [isConfPass, setIsConfPass] = useState(false);

  // check all validation
  const onPressContinuePasscode = (e) => {
    e.preventDefault();
    if (passcode.length < 6) {
      dispatch(
        GlobalAction.showAlert(
          Strings.ERROR,
          Strings.ENTER_VALID_PASSCODE_MSG,
        )
      );
    } else {
      setIsConfPass(true);
    }
  };

  const onPressContinueConfPasscode = (e) => {
    e.preventDefault();
    
    const tempPasscode = passcode;
    const tempConfPasscode = confPasscode;

    if (confPasscode.length < 6) {
      dispatch(
        GlobalAction.showAlert(
          Strings.ERROR,
          Strings.ENTER_VALID_PASSCODE_MSG,
        )
      );
    } else if (tempPasscode !== tempConfPasscode) {
      dispatch(
        GlobalAction.showAlert(
          Strings.ERROR,
          Strings.PASSCODE_NOT_MATCH_MSG,
        )
      );
    } else {
      if (params.state.isFromImportWallet) {
        navigate('/import-wallet', {
          state: {
            password: passcode,
            isPasscode: true,
          }
        });
      } else {
        navigate('/create-wallet', {
          state: {
            password: passcode,
            isPasscode: true,
          }
        });
      }
    }
  };
  
  const handlePasscode = (item, code, setCode) => {
    const tempCode = code.split('');
    if (item.value === 'remove') {
      if (tempCode.length) {
        tempCode.pop();
      }
    } else {
      if (tempCode.length < 6) {
        tempCode.push(item.value);
      }
    }

    const strPasscode = tempCode.join('');
    setCode(strPasscode);
  };
  
  return (
    <React.Fragment>
      <div className="container">
        <Header expand/>       
        <div className="content">         
          <h2 className="text-theme-color text-center">{Strings.PASSCODE_SETUP}</h2>  
          <Passcode 
            title={ isConfPass ? Strings.CONF_PASSCODE : Strings.PASSCODE }
            passcode={ isConfPass ? confPasscode : passcode }
            onClickKey={ item => {
              if (isConfPass) {
                handlePasscode(item, confPasscode, setConfPasscode);
              } else {
                handlePasscode(item, passcode, setPasscode);
              }
            } }
          />
        </div>
        <div className="content-footer text-center">
          <Button
            label={ Strings.CONTINUE_UPPERCASE }
            onClick={ (e) => {
              if(isConfPass){
                onPressContinueConfPasscode(e);
              } else{
                onPressContinuePasscode(e);
              }
            } }
          />
        </div>
      </div>
    </React.Fragment>
  );
}
