import React, { useState } from 'react';
import { Strings } from 'resources';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
const bip39 = require('bip39');

import { createNewWallet } from 'web3-layer';
import { setItem } from 'utils/storage';
import { WalletAction } from 'redux/slices/walletSlice';
import { getNetwork } from 'redux/selectors';
import { STORAGE_KEYS } from '../../../constants';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import TextareaField from 'components/TextareaField';
import SelectCount from 'components/SelectCount';

import '../onboarding.scss';

const sanitizePhrase = (phrase) => (
  phrase.replace(/[^a-zA-Z ]/g, '') // filter for characters and spaces
);

const wordCountList = [12, 15, 18, 21, 24];

export default function ImportWallet () {
  const params = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const selectedNetwork = useSelector(getNetwork);
  const [selectedCount, setSelectedCount] = useState(24);
  const [phrase, setPhrase] = useState('');
  const [error, setError] = useState('');

  const words = phrase
    .split(/\s+/)
    .filter(word => word.length > 0);

  const onChangeValue = (e) => {
    e.preventDefault();
    setPhrase(sanitizePhrase(e.target.value));
  };

  const checkPhraseValidation = () => words.length === selectedCount;

  // Import wallet
  const getStarted = async (e) => {

    e.preventDefault();
    setError('');
    if (checkPhraseValidation()) {
      const intialDetails = {
        mnemonic: words.join(' '),
        password: params.state.password,
        numberOfWords: selectedCount,
      };

      const wallet = await createNewWallet(intialDetails, selectedNetwork);
            
      if (!wallet.success) {
        return setError(wallet.error);                
      }            

      setItem(STORAGE_KEYS.WALLET_OBJECT, JSON.stringify(wallet));
      setItem(STORAGE_KEYS.IS_LOGIN, true);
      setItem(STORAGE_KEYS.IS_PASSCODE, params.state.isPasscode ? 'true' : 'false');
      setItem(STORAGE_KEYS.IS_UPDATED_HASH, 'true');
      setItem(STORAGE_KEYS.ACCOUNT_LIST, JSON.stringify([
        {
          publicAddress: wallet.publicAddress,
          isHardware: false,
          isImported: false, // false because we imported the entire seed phrase, not just this one account
          isArchived: false,
          isPopularTokens: true,
          name: 'Account 1',
        },
      ]));

      dispatch(WalletAction.initApp(wallet, params.state.password, params.state.isPasscode));
      setTimeout(() => {
        navigate('/wallet');
      }, 10);      
    } 
    else{
      setError(Strings.PHRASE_VALIDATION_MSG);
    }
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header expand/>
        <div className="content">            
          <div className="content-area">
            <h2 className="text-theme-color mb-0">{Strings.IMPORT_WALLET_UPPERCASE}</h2>
            <p className="opacity">{Strings.IMPORT_WALLET_MSG}</p>
            <SelectCount
              // label={ Strings.CHOOSE_COUNT_MSG }
              items={ wordCountList }
              selectedCount={ selectedCount }
              onPressCount={ count => {
                setSelectedCount(count);
              } }
            />
            <TextareaField
              placeholder={ Strings.ENTER_PHRASE_MSG }
              value={ phrase }
              onChange={ onChangeValue }
              name="add-tag"
              error={ error }
              autoFocus
              multiline
            />
          </div>                
        </div>
        <div className="content-footer"> 
          <Button
            label={ Strings.GET_START }
            onClick={ (e) => getStarted(e) }
          />
        </div>
      </div>
    </React.Fragment>
  );
}
