import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { Strings } from 'resources';
import {
  checkPassword,
  checkPasswordSecurely,
  createAccountFromPrivateKey,
  createNewWallet,
  decryptMnemonicSecurely,
  generateAccountFromMnemonicOf,
  getUpdatedHashes,
} from 'web3-layer';

import { AccountAction } from 'redux/slices/accountSlice';
import { WalletAction } from 'redux/slices/walletSlice';

import { log } from 'utils/log';
import Logger from 'utils/logger';
import { getItem, setStorageItem, setItem, getStorageItem } from 'utils/storage';

import { STORAGE_KEYS } from '../../../constants';

// Components
import AlertModel from 'components/AlertModel';
import Button from 'components/Button';
import InputField from 'components/InputField';
import Passcode from 'components/Passcode';
import Spinner from 'components/Spinner';

import '../onboarding.scss';

import imLogo from 'assets/images/im3d.png';
import invisibleIcon from 'assets/images/invisible.svg';
import visibleIcon from 'assets/images/visible.svg';
import { parseObject } from 'utils/parse';
import { getUserFriendlyErrorMessage } from 'utils/errors';
import { getNetwork } from 'redux/selectors';

const MINUTES_BEFORE_LOCKOUT = 60; // 1 hour
const LOCKOUT_IN_MILLISECONDS = MINUTES_BEFORE_LOCKOUT * 60 * 1000;
const PASSWORD_CHECK_INTERVAL_MINUTES = 10;

export default function LoginPasswordVerification () {
    
  const params = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  let { walletDetail } = params.state;
  const isPasscode = params.state.isPasscode;
  const [passwordType, setPasswordType] = useState('password');
  const [error, setError] = useState('');
  const [password, setPassword] = useState('');
  const [showAlert, setShowAlert] = useState(false);
  const [passcode, setPasscode] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isVerifyingLogin, setIsVerifyingLogin] = useState(true);
  const selectedNetwork = useSelector(getNetwork);

  const isVerifiedLogin = getItem(STORAGE_KEYS.IS_VERIFIED_LOGIN)?.data === 'true';
  const lastLoginTime = getItem(STORAGE_KEYS.LAST_LOGIN_TIME)?.data;

  const logout = () => {
    setItem(STORAGE_KEYS.IS_VERIFIED_LOGIN, false);
    setItem(STORAGE_KEYS.LAST_LOGIN_TIME, undefined);
  };

  useEffect(() => {
    if (isVerifiedLogin && lastLoginTime) {
      const loginElapsed = Date.now() - lastLoginTime;
      if (loginElapsed >= LOCKOUT_IN_MILLISECONDS) {
        logout();
        setIsVerifyingLogin(false);
      } else {
        validatePassword();
      }
    } else {
      setIsVerifyingLogin(false);
    }
  }, []);

  const onClickSuccess = () => {
    setShowAlert(false);
    setIsLoading(false);
  };

  // checkPassword
  const isCheckPassword = async (walletObj, password) => {
    try {
      const { data } = getItem(STORAGE_KEYS.IS_UPDATED_HASH);
      if (data) {
        const checkPass = await checkPasswordSecurely(
          password,
          walletObj.passwordHash,
        );
        return checkPass;      
      } else {
        const checkPass = checkPassword(
          password,
          walletObj.passwordHash,
        );
        if (checkPass){
          const updatedHash = await getUpdatedHashes(
            password,
            walletObj.mnemonic,
          );

          if(updatedHash.success){
            const tempWalletDetail = { ...walletDetail };
            tempWalletDetail.mnemonic = updatedHash.mnemonic;
            tempWalletDetail.passwordHash = updatedHash.passwordHash;
            setItem(STORAGE_KEYS.IS_UPDATED_HASH, 'true');
            setItem(STORAGE_KEYS.WALLET_OBJECT, JSON.stringify(tempWalletDetail));
            setItem(STORAGE_KEYS.IS_LOGIN, true);

            return checkPass;
          }
          else
            return false;
          
        } else {
          return checkPass;
        }
      }
    } catch (error) {
      log('login error >>>', error);
      return false;
    }     
  };

  const regenerateWallet = async (walletDetail, password) => {
    const mnemonicObject = await decryptMnemonicSecurely(
      walletDetail.mnemonic,
      password,
    );

    const initialDetails = {
      mnemonic: mnemonicObject.mnemonic,
      password,
      numberOfWords: mnemonicObject.mnemonic.split(' ').length,
    };
    
    const newWalletDetail = await createNewWallet(
      initialDetails,
      selectedNetwork,
    );

    const accountListData = getItem(STORAGE_KEYS.ACCOUNT_LIST)
    if (!accountListData) {
      throw new Error('Unable to retrieve account list from storage');
    }

    const accountListParsed = parseObject(accountListData.data);
    if (!accountListParsed.success) {
      throw new Error('Unable to parse account list from storage');
    }

    const accountList = accountListParsed.data;

    if (accountList[0].publicAddress.toLowerCase() !== `0x${newWalletDetail.walletObject[0].address}`) {
      throw new Error('First generated address does not match the first account list address');
    }

    const accountsToAdd = [];
    const newAccountList = [accountList[0]];

    for (let i = 1; i < accountList.length; i += 1) {
      const { address, privateKey } = await generateAccountFromMnemonicOf(
        i,
        mnemonicObject.mnemonic
      );
      const accountFromList = accountList.find(account => account.publicAddress.toLowerCase() === address.toLowerCase());
      if (accountFromList) {
        accountsToAdd.push({address, privateKey});
        newAccountList.push(accountFromList);
      }
    }

    const encryptedAccounts = await Promise.all(
      accountsToAdd.map(account => (
        createAccountFromPrivateKey(account.privateKey, password)
      ))
    );

    newWalletDetail.walletObject = [
      ...newWalletDetail.walletObject,
      ...encryptedAccounts,
    ];

    setItem(STORAGE_KEYS.WALLET_OBJECT, JSON.stringify(newWalletDetail));
    setItem(STORAGE_KEYS.ACCOUNT_LIST, JSON.stringify(newAccountList));
    dispatch(WalletAction.saveWalletDetail(newWalletDetail));
    dispatch(AccountAction.updateAccountObject(newAccountList));
    return newWalletDetail;
  };

  const validatePassword = async () => {
    const savedPass = await getStorageItem(STORAGE_KEYS.WALLET_PASS);

    if (isVerifiedLogin && !savedPass) {
      logout();
      setIsVerifyingLogin(false);
      return;
    }

    const walletPass = isVerifiedLogin ? savedPass : isPasscode ? passcode : password;

    const checkPass = await isCheckPassword(walletDetail, walletPass);

    if (checkPass === true) {
      Logger.debug('Password checked successfully. Current state.', {
        typeOfWalletObject: typeof walletDetail.walletObject,
        typeOfMnemonic: typeof walletDetail.mnemonic,
      });

      if (walletDetail.walletObject === undefined) {
        try {
          walletDetail = await regenerateWallet(
            walletDetail,
            isPasscode ? passcode : password,
          );
        } catch (e) {
          // TODO: what to do with this space
        }
      }

      dispatch(WalletAction.initApp(walletDetail, walletPass, isPasscode));

      let loginTime;
      if (!isVerifiedLogin) {
        loginTime = Date.now();
        setItem(STORAGE_KEYS.IS_VERIFIED_LOGIN, true);
        setItem(STORAGE_KEYS.LAST_LOGIN_TIME, loginTime);
        setStorageItem(STORAGE_KEYS.WALLET_PASS, isPasscode ? passcode : password);
      }

      const createPasswordCheckAlarm = () => {
        chrome.alarms?.create('clearPassword', {
          delayInMinutes: PASSWORD_CHECK_INTERVAL_MINUTES
        });
      };

      const updateWalletLastOpen = () => setStorageItem(STORAGE_KEYS.WALLET_LAST_OPEN, Date.now());

      // Create first password check alarm
      updateWalletLastOpen();
      createPasswordCheckAlarm();

      // Set password check interval
      setInterval(() => {
        updateWalletLastOpen();
        createPasswordCheckAlarm();
      }, 60 * 1000);

      const lastLoginTime = loginTime || getItem(STORAGE_KEYS.LAST_LOGIN_TIME)?.data;
      const loginElapsed = Date.now() - lastLoginTime;

      if (loginElapsed >= LOCKOUT_IN_MILLISECONDS) {
        logout();
        setIsVerifyingLogin(false);
      } else {
        // Set lockout timer
        setTimeout(() => {
          logout();
        }, LOCKOUT_IN_MILLISECONDS - loginElapsed);
      }

      setTimeout(() => {
        setIsVerifyingLogin(false);
        setIsLoading(false);
        navigate('/wallet');
      }, 100);
    }
    else {
      setIsVerifyingLogin(false);
      setIsLoading(false);
      setItem(STORAGE_KEYS.IS_VERIFIED_LOGIN, false);
      if (!isPasscode){
        setError(Strings.LOGIN_INVALID_PASSWORD);
        setShowAlert(true);
      }
      else{
        setError(Strings.LOGIN_INVALID_PASSCODE);
        setShowAlert(true);
      }
    }
  };

  const onChangePassword = (e) => {
    setPassword(e.target.value);
  };

  const handlePasscode = item => {
    const tempCode = passcode.split('');
    if (item.value === 'remove') {
      if (tempCode.length) {
        tempCode.pop();
      }
    } else {
      if (tempCode.length < 6) {
        tempCode.push(item.value);
      }
    }
    const strPasscode = tempCode.join('');
    setPasscode(strPasscode);
  };

  const handleLoginVerification = (evnt) => {
    evnt.preventDefault();
    setIsLoading(true);
    validatePassword(); 
    setPasscode('');
    setPassword('');
  };

  const togglePassword = () => {
    if (passwordType === 'password') {
      setPasswordType('text');
      return;
    }
    setPasswordType('password');
  };

  const renderPassVerification = () => {
    return (
      !isPasscode && (
        <div className="password-frm">
          <h2 className="text-theme-color uppercase">{Strings.LOGIN_PASSWORD_VERIFY_UPPERCASE}</h2>
          <form onSubmit={ handleLoginVerification }>
            <InputField
              type={ passwordType === 'password' ? 'password' : 'text' }
              label={ Strings.PASSWORD }
              value={ password }
              onChange={ onChangePassword }
              name="password"
              onIconClick={ togglePassword }
              icon={ passwordType === 'password' ? <img src={ visibleIcon } alt="visible" /> : <img src={ invisibleIcon } alt="invisible" /> }
              autoFocus
            />
            <div>
              {!isLoading ?
                <button 
                  className={ `button fullwidth ${isPasscode ? /* istanbul ignore next */ 'btn-md' : null}` }
                >
                  {Strings.CONTINUE_UPPERCASE}
                </button>
                :
                <Spinner />
              }
            </div>
          </form>
        </div>
      ));
  };

  const renderPasscodeVerification = () => {
    return (
      isPasscode && (
        <div className="passcode-frm">
          <h2 className="text-theme-color uppercase text-center">{Strings.LOGIN_PASSCODE_VERIFY_UPPERCASE}</h2>   
          <Passcode
            title={ Strings.PASSCODE }
            passcode={ passcode }
            onClickKey={ item => handlePasscode(item) }
          />
          <div>
            {!isLoading ?
              <Button
                label={ Strings.CONTINUE_UPPERCASE }
                onClick={ (e) => handleLoginVerification(e) }
                className='btn-md'
              />
              :
              <Spinner />
            }
          </div>
        </div>
      ));
  };
  
  return (
    <React.Fragment>
      <div className="container">
        {!isVerifyingLogin ? (
          <div className="wrapper p-20">
            <div
              className={`logo text-center ${
                isPasscode ? "passcode-logo" : "password-logo"
              }`}
            >
              <img src={imLogo} alt="Internet Money" />
              <h1 className="text-theme-color">{Strings.INTERNET_MONEY}</h1>
            </div>
            <div className="login-content">
              {renderPassVerification()}
              {renderPasscodeVerification()}
            </div>
          </div>
        ) : (
          <Spinner />
        )}
        {showAlert && (
          <AlertModel
            isShowAlert={ showAlert }
            successBtnTitle={ Strings.TRY_AGAIN }
            onPressSuccess={ () => onClickSuccess() }
            className="text-center"

          >
            <p>{getUserFriendlyErrorMessage(error)}</p>
          </AlertModel>
        )}
      </div>
    </React.Fragment>
  );
}
