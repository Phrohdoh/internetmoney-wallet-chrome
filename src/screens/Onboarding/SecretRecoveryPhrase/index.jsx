import React from 'react';
import { Strings } from 'resources';
import { useNavigate, useLocation } from 'react-router-dom';

// Components
import Header from 'components/Header';
import Button from 'components/Button';

import '../onboarding.scss';

export default function SecretRecoveryPhrase () {

  const params = useLocation();    
  const navigate = useNavigate();
  const sendCreateWallet = (e) => {
    e.preventDefault();
    navigate('/your-secret-recovery-phrase',
      {
        state: {
          password: params.state.password,
          wallet: params.state.wallet,
          isPasscode: params.state.isPasscode,
          isSetting: params.state.isSetting,
        }
      });
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header expand/>
        <div className="content">            
          <div className="content-area">
            <h2 className="text-theme-color">{Strings.SEED_PHRASE}</h2>
            <p>{Strings.RECOVERY_PHRASE_MSG}</p>
            <div className="scam-alert">
              <p>{Strings.SEED_PHRASE_SCAM_MSG_UPPERCASE}</p>
            </div>
          </div>
        </div>
        <div className="content-footer">
          <Button 
            label={ Strings.REVEAL_SEED_PHRASE_UPPERCASE }
            className="btn-full" 
            onClick={ (e) => sendCreateWallet(e) }                
          />
        </div>
      </div>
    </React.Fragment>
  );
}
