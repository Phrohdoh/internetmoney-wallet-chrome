import React from 'react';

import { Strings } from 'resources';
import { IMAGES } from 'themes';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import LearnBasisInfo from 'components/LearnBasisInfo';

import '../onboarding.scss';
import { useNavigate } from 'react-router-dom';

export default function LearnStepOne () {
  
  const learnBasicsInfo = Strings.LEARN_BASIC_INFO_LIST[0];
  const navigate = useNavigate();

  return (
    <React.Fragment>
      <div className="container">
        <Header 
          containerClassName="trans-header"
          changeBackIcon={ IMAGES.BLACK_BACK_ICON }
          expandBlack
          onClickBack={() => {
            navigate('/');
          }}
        />
        <LearnBasisInfo item={ learnBasicsInfo }/>        
        <div className="content-footer">
          <Button
            link="/learn-step-two"
            label={ Strings.CONTINUE_UPPERCASE }
          />
        </div>
      </div>
    </React.Fragment>
  );
}
