import React, { useState } from 'react';
import { Strings } from 'resources';
import {  useNavigate } from 'react-router-dom';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import Checkbox from 'components/Checkbox';
import LearnBasisInfo from 'components/LearnBasisInfo';

import '../onboarding.scss';
import { GlobalAction } from 'redux/slices/globalSlice';
import { useDispatch } from 'react-redux';

export default function LearnStepFour () {
  const dispatch = useDispatch();
  const learnBasicsInfo = Strings.LEARN_BASIC_INFO_LIST[3];
  const navigate = useNavigate();

  const [checked, setChecked] = useState(false);
  const onChange = () => {
    setChecked(!checked);
  };

  const onContinue = (e) => {
    e.preventDefault();
    if (!checked) {
      dispatch(
        GlobalAction.showAlert(
          Strings.ERROR,
          Strings.CHECK_UNDERSTAND_MSG,
        )
      );
    } else {
      navigate('/learn-step-five', {
        state: {
          isFromImportWallet: false
        }
      });
    }
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header expand/>
        <LearnBasisInfo item={ learnBasicsInfo } />
        <div className="content-footer">
          <Checkbox
            id="checkbox"
            label={ Strings.UNDERSTAND_BASICS }
            value={ checked } 
            onChange={ onChange }
          />
          <Button
            label={ Strings.CONTINUE_UPPERCASE }
            onClick={ (e) => onContinue(e) }
          />
        </div>
      </div>
    </React.Fragment>
  );
}
