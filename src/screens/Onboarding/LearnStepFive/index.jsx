import React, { useState } from 'react';
import { Strings } from 'resources';
import { useNavigate, useLocation } from 'react-router-dom';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import Checkbox from 'components/Checkbox';
import LearnBasisInfo from 'components/LearnBasisInfo';

import '../onboarding.scss';
import { useDispatch } from 'react-redux';
import { GlobalAction } from 'redux/slices/globalSlice';

export default function LearnStepFive () {
  const dispatch = useDispatch();
  const learnBasicsInfo = Strings.LEARN_BASIC_INFO_LIST[4];

  const navigate = useNavigate();
  const params = useLocation();
  const queryParams = new URLSearchParams(window.location.search);

  const isFromImportWallet = params.state?.isFromImportWallet ?? queryParams.get('isFromImportWallet') === 'true';

  const [checked, setChecked] = useState(false);
  const onChange = () => {
    setChecked(!checked);
  };

  const onContinue = (e, isPasscode) => {
    e.preventDefault();
    if (!checked) {
      dispatch(
        GlobalAction.showAlert(
          Strings.ERROR,
          Strings.CHECK_UNDERSTAND_MSG_2,
        )
      );
    } else {
      if (!isPasscode) {
        navigate('/password-setup', {
          state:{
            isFromImportWallet,
          }
        });
      } else {
        navigate('/passcode-setup', {
          state:{
            isFromImportWallet,
          }
        });
      }
    }
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header
          expand
          containerClassName="trans-header"
        />
        <LearnBasisInfo item={ learnBasicsInfo } />
        <div className="content-footer text-center">
          <Checkbox
            id="checkbox"
            label={ Strings.UNDERSTAND }
            value={ checked } 
            onChange={ onChange }
          />
          <Button
            label={ Strings.SETUP_PASSWORD }
            onClick={ (e) => onContinue(e, false) }
          />
          <Button
            outline
            label={ Strings.SETUP_PASSCODE }
            onClick={ (e) => onContinue(e, true) }
          />
        </div>
      </div>
    </React.Fragment>
  );
}
