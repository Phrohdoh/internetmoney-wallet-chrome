import React, { useEffect, useState } from 'react';
import { Links, Strings } from 'resources';

import { Link, useNavigate } from 'react-router-dom';

import Button from 'components/Button';
import Checkbox from 'components/Checkbox';
import Header from 'components/Header';

import startBackground from '../../assets/images/start-background.svg';

import './onboarding.scss';
import { useDispatch } from 'react-redux';
import { GlobalAction } from 'redux/slices/globalSlice';

export default function Start () {
  const dispatch = useDispatch();
  const [checked, setChecked] = useState(false);
  const onChange = () => {
    setChecked(!checked);
  };

  const navigate = useNavigate();

  const [isPopupView, setIsPopupView] = useState();

  useEffect(() => {
    if (chrome.extension) {
      const popupViews = chrome.extension.getViews({ type: 'popup' });
      const currentView = popupViews.find((view) => view === window);
      setIsPopupView(currentView !== undefined);
    }
  }, []);

  // Action and functions
  const checkIsAcknowledge = () => {    
    if (!checked) {
      dispatch(
        GlobalAction.showAlert(
          Strings.ERROR,
          Strings.CHECK_ACKNOWLEDGE_MSG,
        )
      );
    }    
    return checked;
  };
  
  return (
    <React.Fragment>
      <div className="container overflow-hidden">
        <Header 
          hideBack
          containerClassName="trans-header"
          expand
        />
        <div className="header-content">
          <div className="heading2 text-center">{Strings.WELCOME_TITLE}</div>
          <h1 className="text-center text-theme-color">{Strings.INTERNET_MONEY}</h1>
        </div>
        <div className="body-content text-center">
          <h5>{Strings.WELCOME_MSG}</h5>
          <Button 
            label={ Strings.CREATE_NEW_WALLET_UPPERCASE }
            onClick={ (e) => {
              e.preventDefault();
              if (checkIsAcknowledge()) {
                if (isPopupView) {
                  window.open('index.html?screen=/learn-step-one', '_blank');
                } else {
                  navigate('/learn-step-one');
                }
              }
            } }
          />
          <Button
            outline
            label={ Strings.IMPORT_WALLET_UPPERCASE }
            onClick={ (e) => {
              e.preventDefault();
              if (checkIsAcknowledge()) {
                if (isPopupView) {
                  window.open('index.html?screen=/learn-step-five&isFromImportWallet=true', '_blank');
                } else {
                  navigate('/learn-step-five',
                  {
                    state: {
                      isFromImportWallet: true,
                    }
                  });
                }
              }
            } }
          />
          <Checkbox
            id="checkbox"
            labelFormatString={Strings.formatString(
              Strings.ACKNOWLEDGE_MSG,
              <Link
                onClick={ (e) => window.open(Links.PRIVACY_POLICY, '_blank') }
                className="text-theme-color"
              >
                {Strings.PRIVACY_POLICY}
              </Link>,
              <Link
                onClick={ (e) => window.open(Links.TERMS_OF_SERVICE, '_blank') }
                className="text-theme-color"
              >
                {Strings.TERMS_AND_CONDITIONS}
              </Link>
            )}
            value={ checked } 
            onChange={ onChange }
            className="accept-checkbox text-left"
          />
        </div>
        <div className="footer-content">                
          <img src={ startBackground } alt="Frame" className="IM-Frame" />
        </div>
      </div>
    </React.Fragment>
  );
}
