import React, { useState } from 'react';
import { Strings } from 'resources';
import { useNavigate, useLocation } from 'react-router-dom';
// Components
import Header from 'components/Header';
import Button from 'components/Button';
import InputField from 'components/InputField';

import '../onboarding.scss';

import visibleIcon from 'assets/images/visible.svg';
import invisibleIcon from 'assets/images/invisible.svg';

export default function PasswordSetup () {

  const params = useLocation();

  const [error, setError] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [passwordType, setPasswordType] = useState('password');
  const [confirmPasswordType, setConfirmPasswordType] = useState('password');

  const onChangePassword = (e) => {
    setPassword(e.target.value);
  };
  const onChangeConfirmPassword = (e) => {
    setConfirmPassword(e.target.value);
  };
 
  const handleValidation = () => {
        
    const minLengthRegExp = /.{10,}/;
    const passwordLength = password.length;
    const minLengthPassword = minLengthRegExp.test(password);
    let errMsg = '';
    if (passwordLength === 0) {
      errMsg = Strings.ENTER_PASS_MSG;
    }
    else if (!minLengthPassword) {
      errMsg = Strings.ENTER_VALID_PASS_MSG;
    } else {
      errMsg = '';

      if (confirmPassword || (password && confirmPassword.length > 0)) {
        if (password !== confirmPassword) {
          setConfirmPasswordError(Strings.MATCH_PASS_MSG);
        } else {
          setConfirmPasswordError('');
          if (params.state.isFromImportWallet) {
            navigate('/import-wallet',
              {
                state: {
                  password: password
                }
              });
          }
          else {
            navigate('/create-wallet',
              {
                state: {
                  password: password
                }
              });
          }
        }
      }

    }    
    setError(errMsg);
    
  };

  const navigate = useNavigate();

  const createWallet = (e) => {
    e.preventDefault();
    handleValidation();
  };

  const togglePassword = () => {
    if (passwordType === 'password') {
      setPasswordType('text');
    }
    else
      setPasswordType('password');
  };

  const toggleConfirmPassword = () => {
    if (confirmPasswordType === 'password') {
      setConfirmPasswordType('text');
    }
    else
      setConfirmPasswordType('password');
  };
    

  return (
    <React.Fragment>
      <div className="container">
        <Header expand/>
        <div className="content">                
          <h2 className="text-theme-color text-center mb-0">{Strings.PASS_SETUP}</h2>
          <p className="opacity text-center">{Strings.PASSWORD_NOTES}</p>
          <br />
          <InputField 
            type={ passwordType === 'password' ? 'password' : 'text' }
            label={ Strings.PASSWORD }                    
            value={ password }
            onChange={ onChangePassword }
            error={ error }
            name="password"
            onIconClick={ togglePassword }
            icon={ passwordType === 'password' ? <img src={ visibleIcon } alt="visible" /> : <img src={ invisibleIcon } alt="invisible" /> }
          />
          <InputField 
            type={ confirmPasswordType === 'password' ? 'password' : 'text' }
            label={ Strings.CONF_PASSWORD }
            value={ confirmPassword }
            onChange={ onChangeConfirmPassword }
            error={ confirmPasswordError }
            name="confirm-password"
            onIconClick={ toggleConfirmPassword }
            icon={ confirmPasswordType === 'password' ? <img src={ visibleIcon } alt="visible" /> : <img src={ invisibleIcon } alt="invisible" /> }
          />
        </div>
        <div className="content-footer">
          <Button 
            label={ Strings.CONTINUE_UPPERCASE }
            onClick={ (e) => createWallet(e) } 
          />
        </div>
      </div>
    </React.Fragment>
  );
}
