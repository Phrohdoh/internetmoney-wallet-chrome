import React, { useState, useEffect } from 'react';

import { useDispatch } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
import { decryptMnemonicSecurely } from 'web3-layer';

import { setItem } from 'utils/storage';
import { STORAGE_KEYS } from '../../../constants';
import { Strings } from 'resources';
import { WalletAction } from 'redux/slices/walletSlice';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import Checkbox from 'components/Checkbox';

import '../onboarding.scss';
import { GlobalAction } from 'redux/slices/globalSlice';

export default function YourSecretRecoveryPhrase () {
  const dispatch = useDispatch();

  const [phraseArray, setSecratePhrase] = useState([]);
  const [greetingStatus, setGreetingStatus] = useState(false);
  const [checked, setChecked] = useState(false);    

  const onChange = () => {
    setChecked(!checked);
  };

  const params = useLocation();
  const navigate = useNavigate();
  
  useEffect(() => {
    getSecretPhrase();
  }, []);
  
  const getSecretPhrase = async () => {
    const mnemonicObject = await decryptMnemonicSecurely(
      params.state.wallet.mnemonic,
      params.state.password,
    );

    const phrases = mnemonicObject.mnemonic.split(' ');
    setSecratePhrase(phrases);
  };

  const createWallet = (e) => {
    e.preventDefault();

    if(!checked) {
      dispatch(
        GlobalAction.showAlert(
          Strings.ERROR,
          Strings.CHECK_CONFIRM_MSG,
        )
      );
    } else{
      setItem(STORAGE_KEYS.WALLET_OBJECT, JSON.stringify(params.state.wallet));
      setItem(STORAGE_KEYS.IS_LOGIN, true);
      setItem(STORAGE_KEYS.IS_PASSCODE, params.state.isPasscode ? 'true' : 'false');
      setItem(STORAGE_KEYS.IS_UPDATED_HASH, 'true');
      setItem(STORAGE_KEYS.ACCOUNT_LIST, JSON.stringify([
        {
          publicAddress: params.state.wallet.publicAddress,
          isHardware: false,
          isImported: false,
          isArchived: false,
          isPopularTokens: true,
          name: 'Account 1',
        },
      ]));

      dispatch(WalletAction.initApp(params.state.wallet, params.state.password, params.state.isPasscode));      
      setGreetingStatus(!greetingStatus);
      setTimeout(() => {
        navigate('/welcome');
      }, 10);
    }
  };

  return (
    <div className="container">
      <Header expand/>
      <div className="content">            
        <div className="content-area">
          <h2 className="text-theme-color uppercase mb-0">{Strings.YOUR_SEED_PHRASE_UPPERCASE}</h2>
          <p>{Strings.RECOVERY_24_WORD_MSG}</p>
          {phraseArray && 
            <div className="tags">
              {phraseArray.map((item, index) => (
                <div key={ index } className="tag">
                  <span>{index+ 1}</span>
                  <div className="tagStyle"><div>{item}</div></div>
                </div>
              ))}
            </div>
          }
        </div>                
      </div>
      <div className="content-footer">
        {!params.state.isSetting && (
          <React.Fragment>
            <Checkbox
              className="confirm_recovery_phrase"
              id="checkbox"
              label={ Strings.CONFIRM_MSG }
              value={ checked }
              onChange={ onChange }
            />       
            <Button
              className="btn-full"
              label={ Strings.CLICK_TO_CONFIRM }
              onClick={ (e) => createWallet(e) }
            />
          </React.Fragment>
        )}
      </div>
    </div>
  );
}
