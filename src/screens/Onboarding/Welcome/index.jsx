import React from 'react';
import { Strings } from 'resources';
import { useSpring, animated } from 'react-spring';
import {  useNavigate } from 'react-router-dom';

// Components
import Header from 'components/Header';
import Button from 'components/Button';

// Images
import website from 'assets/images/website.svg';
import twitter from 'assets/images/twitter.svg';
import telegram from 'assets/images/telegram.svg';
import reddit from 'assets/images/reddit.svg';
import imLogo from 'assets/images/im3d.png';

import '../onboarding.scss';

export default function Welcome () {

  const navigate = useNavigate();

  const getStarted = (e) => {
    e.preventDefault();
    navigate('/wallet');
  };

  const animatedStyle = useSpring({
    to: { scale: (1), opacity:1 },
    from: { scale: (1.4), opacity: 0.5},
    delay: 100,
  });

  const renderSocialButton = (icon, url, id) => {
    return (
      <span 
        data-testid={ `social-link-${id}` }
        onClick={ (_e) =>
          window.open(url)
        } 
        target="_blank" 
        className="social-icon">
        <img src={ icon } alt="" />
      </span>
    );
  };
  const renderSocialButtons = () => {
    return (
      <div className="social">
        {renderSocialButton(website, 'https://internetmoney.io/', 'website')}
        {renderSocialButton(twitter, 'https://twitter.com/internetmoneyio', 'twitter')}
        {renderSocialButton(telegram, 'https://t.me/internetmoneyio', 'telegram')}
        {renderSocialButton(reddit, 'https://www.reddit.com/r/internetmoneyio/', 'reddit')}
      </div>
    );
  };

  return (        
    <div className="container welcome-container">
      <Header containerClassName="trans-header" expand/>     
      <React.Fragment>
        <div className="padder">
          <div className="wallet-header">
            <animated.div className="wallet-icon-circle" style={ animatedStyle }><animated.img src={ imLogo } alt="wallet" style={ animatedStyle } /></animated.div>
            <div className="heading2 text-center">{Strings.WELCOME_TITLE}</div>
            <h1 className="text-center text-theme-color">{Strings.INTERNET_MONEY_WALLET}</h1>
          </div>
        </div>
        <div className="content-background ds-flex-col full-flex">
          <div className="wrapper">
            <div className="full-flex overflow-auto">            
              <p className="opacity">{Strings.GET_START_INFO_MSG}</p>
              {renderSocialButtons()}
              <p className="opacity">{Strings.GET_START_THANKYOU_MSG} </p>
            </div>
            <Button
              label={ Strings.GET_START }
              className="btn-full"
              onClick={ (e) => getStarted(e) }
            />
          </div>
        </div>
      </React.Fragment>
    </div>
  );
}
