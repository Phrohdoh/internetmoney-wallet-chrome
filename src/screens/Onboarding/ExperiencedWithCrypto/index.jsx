import React from 'react';
import { Strings } from 'resources';
import { useNavigate } from 'react-router-dom';


// Components
import Header from 'components/Header';
import Button from 'components/Button';

import '../onboarding.scss';


export default function ExperiencedWithCrypto () {

  const navigate = useNavigate();
  const onContinue = (isImportWallet) => {       
    navigate('/learn-step-five',
      {
        state: {
          isFromImportWallet: isImportWallet,
        }
      });
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header
          expand
          onClickBack={() => {
            navigate('/');
          }}
        />
        <div className="content">            
          <div className="content-area">
            <h2 className="text-theme-color">{Strings.THANK_YOU_FOR_CHOOSE}</h2>
            <p className="opacity">{Strings.EXPERIENCE_CRYPTO_MSG}</p>                     
          </div>
          <div className="content-footer">
            <Button label={ Strings.CREATE_NEW_WALLET_UPPERCASE } onClick={ (e) => { 
              e.preventDefault(); 
              onContinue(false); 
            } } />
            <Button outline label={ Strings.IMPORT_WALLET_UPPERCASE } onClick={ (e) => { 
              e.preventDefault(); 
              onContinue(true); 
            } } />
          </div>  
        </div>           
      </div>
    </React.Fragment>
  );
}
