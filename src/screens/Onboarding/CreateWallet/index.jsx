import React, { useState, useEffect } from 'react';
import { Strings } from 'resources';
import { useSpring, animated } from 'react-spring';
import {  useNavigate, useLocation } from 'react-router-dom';
import { createNewWallet } from 'web3-layer';
import { useSelector } from 'react-redux';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import Loader from 'components/Loader';
import ProgressBar from 'components/ProgressBar';
import WalletIcon from 'components/WalletIcon';

import imLogo from 'assets/images/im3d.png';

import '../onboarding.scss';
import { getNetwork } from 'redux/selectors';

export default function CreatingWallet () {

  const navigate = useNavigate();
  const params = useLocation();

  const selectedNetwork = useSelector(getNetwork);

  const [walletDetails, setWalletDetails] = useState({});
  const [isProcressComplete, setIsProcressComplete] = useState(false);
  const [createWalletSuccess, setCreateWalletSuccess] = useState(false);
    
    
  useEffect(() => {
    setTimeout(() => {
      setIsProcressComplete(true);
    }, 4200);
    createWallet(params.state.password); 
  }, []);

  // Create wallet
  const createWallet = async _password => {
    const intialDetails = {
      mnemonic: null,
      password: params.state.password,
      numberOfWords: 24,
    };

    const wallet = await createNewWallet(intialDetails, selectedNetwork);

    /* istanbul ignore if */
    if (!wallet.success) {
      return;
    } else {
      setCreateWalletSuccess(true);
    }
    setWalletDetails(wallet);

  };

  const postCreateWallet = (e) => {
    e.preventDefault();
    navigate('/secret-recovery-phrase',
      {
        state: {
          password: params.state.password,
          wallet: walletDetails,
          isPasscode: params.state.isPasscode
        }
      });
  };

  const animatedStyle = useSpring({
    to: { scale: (1), opacity:1 },
    from: { scale: (1.4), opacity: 0.5},
    delay: 4200,
  });

  return (
        
    <div className="container square-background">
      <Header expand/>
      {createWalletSuccess && isProcressComplete ? 
        <React.Fragment>
          <div className="wallet-header">
            <animated.div className="wallet-icon-circle" style={ animatedStyle }><animated.img src={ imLogo } alt="wallet" style={ animatedStyle } /></animated.div>
            <div className="h2 text-center normal-weight pl-3 pr-3"><strong>{Strings.CONGRATULATIONS}</strong>
              {Strings.WALLET_CREATE_SUCCESS_MSG}
            </div>
          </div>
          <div className="box-content text-center">
            <h5>{Strings.ALMOST_THERE}</h5>
            <p className="opacity">{Strings.CONTINUE_INFO_MSG} </p>
            <Button 
              label={ Strings.CONTINUE_UPPERCASE }
              className="btn-full" 
              onClick={ (e) => postCreateWallet(e) } 
            />
          </div>
        </React.Fragment> 
            
        : 
        <React.Fragment>
          <div className="wallet-header">
            <animated.div className="wallet-icon"><WalletIcon /></animated.div>
            <ProgressBar />
            <p className="opacity">{Strings.WALLET_CREATE_STRINGS[0]}</p>
          </div>
          <div className="box-content">
            <div className="heading1">{Strings.CREATE_WALLET} <strong>{Strings.WALLET}</strong></div>                        
            <Loader />
          </div>
        </React.Fragment>
      }
    </div>
  );
}
