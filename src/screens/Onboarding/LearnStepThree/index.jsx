import React from 'react';
import { Strings } from 'resources';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import LearnBasisInfo from 'components/LearnBasisInfo';

import '../onboarding.scss';

export default function LearnStepThree () {

  const learnBasicsInfo = Strings.LEARN_BASIC_INFO_LIST[2];

  return (
    <React.Fragment>
      <div className="container">
        <Header expand/>
        <LearnBasisInfo item={ learnBasicsInfo } />
        <div className="content-footer">
          <Button
            link={ '/learn-step-four' }
            label={ Strings.CONTINUE_UPPERCASE }            
          />
        </div>
      </div>
    </React.Fragment>
  );
}
