import React from 'react';
import { Strings } from 'resources';

import { IMAGES } from 'themes';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import LearnBasisInfo from 'components/LearnBasisInfo';

import '../onboarding.scss';

export default function LearnStepTwo () {

  const learnBasicsInfo = Strings.LEARN_BASIC_INFO_LIST[1];

  return (
    <React.Fragment>
      <div className="container">
        <Header
          containerClassName="trans-header"
          changeBackIcon={ IMAGES.BLACK_BACK_ICON }
          expandBlack
        />
        <LearnBasisInfo item={ learnBasicsInfo } />
        <div className="content-footer">
          <Button
            link={ '/learn-step-three' }
            label={ Strings.CONTINUE_UPPERCASE }
          />
        </div>
      </div>
    </React.Fragment>
  );
}
