import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { NetworksAction } from 'redux/slices/networksSlice';
import { getItem } from 'utils/storage';
import { STORAGE_KEYS } from '../../constants';

// utils
import { parseObject } from 'utils/parse';

import './splash.scss';
import { getNetwork } from 'redux/selectors';

export default function Splash () {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const selectedNetwork = useSelector(getNetwork);

  useEffect(() => {
    dispatch(NetworksAction.initNetworks());
  }, []);

  useEffect(() => {
    if (selectedNetwork.chainId) getStorageData();
  }, [selectedNetwork]);

  const getStorageData = () => {
    const isPasscode = getItem(STORAGE_KEYS.IS_PASSCODE);
    const walletObject = getItem(STORAGE_KEYS.WALLET_OBJECT);
    const loginData = getItem(STORAGE_KEYS.IS_LOGIN);

    if (!loginData.error && !walletObject.error) {
      const walletData = parseObject(walletObject.data);
      if (walletData.success) {
        navigate('/login-password-verification', {
          state: {
            walletDetail: walletData.data,
            isPasscode: isPasscode.data === 'true',
          }
        });
      }
    } else {
      navigate('/on-boarding');
    }
  };

  return <div className="splash-screen" />;
}
