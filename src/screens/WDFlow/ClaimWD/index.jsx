import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'resources';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import {
  getSingleAccount,
  claimWDTxnObj,
  getContractInstance,
  createAndSendTxn,
  updateTxnLists,
  WalletTransactionTypes,
  calculateEstimateFee,
  calculateMaxFee,
} from 'web3-layer';
import ABI from 'web3-layer/ABI';

import { TransactionsAction } from 'redux/slices/transactionsSlice';
import { STORAGE_KEYS } from '../../../constants';
import moment from 'moment';

// utils
import { useTokenIcons } from 'utils/hooks';
import { displayAddress } from 'utils/displayAddress';

// Components
import Header from 'components/Header';
import Button from 'components/Button';
import Spinner from 'components/Spinner';
import Modal from 'components/Modal';
import EstimatedGasFee from 'components/EstimatedGasFee';
import AlertModel from 'components/AlertModel';

// Images
import downArrow from 'assets/images/down-arrow.svg';
import dropIcon from 'assets/images/drop.svg';
import IMD from 'assets/images/time3d.png';

import { setItem } from 'utils/storage';

import './claim-wd.scss';
import IntegerUnits from 'utils/IntegerUnits';
import { GlobalAction } from 'redux/slices/globalSlice';
import { getUserFriendlyErrorMessage } from 'utils/errors';
import { getNetwork } from 'redux/selectors';

export default function ClaimWD () {
  const navigate = useNavigate();
  const params = useLocation();
  const dispatch = useDispatch();
  const getTokenIcon = useTokenIcons();

  const networks = useSelector((state) => state.networks);
  const wallet = useSelector((state) => state.wallet);
  const transactions = useSelector((state) => state.transactions);
  const selectedNetwork = useSelector(getNetwork);
  const [selectedRange, setSelectedRange] = useState(1);
  const [txnObject, setTxnObject] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [gasRange, setGasRange] = useState({});
  const [accountBalance, setAccountBalance] = useState(new IntegerUnits(0));

  const networkDetail = networks.wdSupportObject;
  const [dividendsDetail] = useState(params.state.dividendsDetail);
  const [wdStateDetail] = useState(params.state.wdStateDetail);

  const [isShowEstimatedGasFee, setIsShowEstimatedGasFee] = useState(false);
  const [error, setError] = useState(false);
  const [isShowAlert, setIsShowAlert] = useState(false);

  const isSupportUSD = useState(true);
  const [usdValue, setUsdValue] = useState(0);

  // hardware wallet
  const [isShowHardwareModal, setIsShowHardwareModal] = useState(false);
  const [isShowHardwareAcceptRejectModal, setIsShowHardwareAcceptRejectModal] =
    useState(false);
  const [hardwareError, setHardwareError] = useState('');
  const [isShowDeclinePopup, setIsShowDeclinePopup] = useState(false);

  useEffect(() => {
    getTxnObject();
    getAccountBalance();
  }, []);

  // Action Methods
  const getAccountBalance = async () => {
    const balance = await getSingleAccount(
      params.state.fromAccount.publicAddress,
      selectedNetwork
    );

    if(balance.success){
      setUsdValue(balance.tokenValues.usdPrice);
      setAccountBalance(balance.account.value);
    }
  };

  const getTxnObject = async () => {
    setIsLoading(true);
    const txObject = await claimWDTxnObj(
      networkDetail.wdAddress,
      params.state.fromAccount.publicAddress,
      selectedNetwork
    );
    setGasRange(txObject.range);
    setIsLoading(false);
    setTxnObject(txObject.txnObject);
  };

  const updateTxnObjectTypeOne = (gas, gasPrice) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.gasPrice = gasPrice;
    setTxnObject(tempTxObj);
  };

  const updateTxnObjectTypeTwo = (gas, maxPrioFee, maxFeePerGas) => {
    const tempTxObj = { ...txnObject };
    tempTxObj.gas = gas;
    tempTxObj.maxPriorityFeePerGas = maxPrioFee;
    tempTxObj.maxFeePerGas = maxFeePerGas;
    setTxnObject(tempTxObj);
  };

  const getEstimateFee = () => {
    if (!txnObject) return '';
    const estimateFee = calculateEstimateFee(txnObject, selectedNetwork.txnType);
    return `${estimateFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const getMaxFee = () => {
    if (!txnObject) return '';
    const maxFee = calculateMaxFee(txnObject);
    return `${maxFee.toString(6)} ${selectedNetwork.sym}`;
  };

  const clickToClaimTokens = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const isHardware = !!params.state.fromAccount.isHardware;
    if (isHardware) setIsShowHardwareAcceptRejectModal(true);

    try {
      const swapContract = await getContractInstance(
        networkDetail.swapRouterV3,
        ABI["quotes"],
        networkDetail
      );

      const wdContract = await getContractInstance(
        networkDetail.wdAddress,
        ABI["wdtoken"],
        networkDetail
      );

      let tx = null;
      if (params.state.isSweep) {
        tx = await swapContract.contractInstance.methods.distributeAll("0");
        txnObject.to = networkDetail.swapRouterV3;
      } else {
        tx = await wdContract.contractInstance.methods.claimDividend(params.state.fromAccount.publicAddress, "0");
        txnObject.to = networkDetail.wdAddress;
      }

      txnObject.data = tx.encodeABI();

      const txnResult = await createAndSendTxn(
        WalletTransactionTypes.CLAIM,
        txnObject,
        wallet,
        params.state.fromAccount,
        accountBalance,
        selectedNetwork
      );

      if (txnResult.success) {
        const { tempTxns, tempTxnsList } = updateTxnLists(
          IntegerUnits.serialize({
            type: WalletTransactionTypes.CLAIM,
            transactionDetail: {
              accountName: params.state.fromAccount.name,
              claimableDiv: dividendsDetail.claimableDiv,
              displayAmount: dividendsDetail.claimableDiv,
              displayDecimal: '18',
              fromAddress: params.state.fromAccount.publicAddress,
              fromToken: { symbol: selectedNetwork.sym },
              isSupportUSD: isSupportUSD,
              networkDetail: selectedNetwork,
              toAddress: networkDetail.wdAddress,
              transferedAmount: dividendsDetail.claimableDiv,
              usdValue: usdValue,
              utcTime: moment.utc().format(),
              wdAddress: networkDetail.wdAddress,
              wdTokenDetail: wdStateDetail,
            },
            receipt: txnResult.receipt,
            blockNumber: txnResult.blockNumber,
          }),
          transactions,
          selectedNetwork
        );

        dispatch(TransactionsAction.saveTransactions(tempTxnsList));
        setItem(STORAGE_KEYS.TRANSACTIONS, JSON.stringify(tempTxns));
        setIsLoading(false);
        if (isHardware) setIsShowHardwareAcceptRejectModal(false);
        dispatch(
          GlobalAction.showAlert(
            Strings.SUCCESS,
            Strings.SUBMIT_SUCCESS,
          ),
        );
        navigate('/wd-flow');
      } else {
        setIsLoading(false);
        if (!isHardware) {
          if (txnResult.error.toLowerCase().includes('insufficient funds')) {
            setError(
              Strings.formatString(Strings.INSUFFICIENT_FUNDS, [
                selectedNetwork.sym
              ])
            );
          } else {
            setError(txnResult.error);
          }
          setIsShowAlert(true);
        } else {
          if (txnResult.error.toLowerCase().includes('insufficient funds')) {
            setHardwareError(
              Strings.formatString(Strings.INSUFFICIENT_FUNDS, [
                selectedNetwork.sym
              ])
            );
          } else {
            setHardwareError(txnResult.error);
          }
          setIsShowAlert(false);
          setIsShowHardwareAcceptRejectModal(false);
          setIsShowHardwareModal(true);
          if (
            txnResult.error ===
            'Ledger device: Condition of use not satisfied (denied by the user?) (0x6985)' ||
            txnResult.error === 'Action cancelled by user'
          ) {
            setHardwareError('');
            setIsShowHardwareModal(false);
            setIsShowDeclinePopup(true);
          }
        }
      }
    } catch (err) {
      setIsLoading(false);
      if (isHardware) setIsShowHardwareAcceptRejectModal(false);
      console.error(err);
    }
  };

  const onClickSuccess = () => {
    setIsShowAlert(false);
    navigate('/wd-flow');
  };

  const renderContentHeader = () => {
    return (
      <div className="send-token-header text-center">
        <p>{Strings.CURRENT_NETWORK}</p>
        <h6 className="mb-0">{selectedNetwork.networkName}</h6>
      </div>
    );
  };

  const headingStyle = () => {
    return (
      <div className="heading-styles">
        <span className="line" />
        <span className="line" />
        <h6 className="text-center uppercase">
          {Strings.CONFIRM_TRANSACTION_UPPERCASE}
        </h6>
        <span className="line" />
        <span className="line" />
      </div>
    );
  };

  const SendTokenAccounts = () => {
    return (
      <div className="send-details">
        <div className="from-to-pattern">
          <img src={ downArrow } alt="" className="from-to-arrow" />
        </div>
        <div className="send-row">
          <div className="body">
            <small>{Strings.FROM_TEXT}</small>
            <h6 className="text-gray-color">{params.state.fromAccount.name}</h6>
          </div>
          <div className="right text-right">
            <h6 className="uppercase">{selectedNetwork.sym}</h6>
            <span>
              <small>{Strings.BALANCE}</small> {accountBalance.toString(6)}
            </span>
          </div>
          <div className="icon">
            <img src={ getTokenIcon('', selectedNetwork.chainId) } />
          </div>
        </div>
        <div className="send-row">
          <div className="body">
            <small>{`${Strings.TO_WD_ADDRESS} (${wdStateDetail.sym})`} </small>
            <h6 className="text-gray-color text-eclipse-style wd-add">
              {displayAddress(networkDetail.wdAddress)}              
              <img src={ IMD } alt="IMD" />
            </h6>
          </div>
        </div>
      </div>
    );
  };

  const SendTokenAmount = () => {
    return (
      <div className="send-amount">
        <small>{Strings.DIVIDENDS_AVAILABLE_TO_CLAIM}</small>
        <h2 className="uppercase">
          {`${dividendsDetail.claimableDiv.toString(6)} ${selectedNetwork.sym}`}
          <img src={ getTokenIcon('', selectedNetwork.chainId) } alt="" />
        </h2>
        {params.state.isSupportUSD && (
          <span>
            <small>{Strings.VALUE}: </small>
            {`$${params.state.transferAmount.multiply(params.state.usdValue).toString(6)}`}
          </span>
        )}
      </div>
    );
  };

  const SendTokenEstimated = () => {
    return (
      <div className="send-estimated">
        <div className="send-row">
          <div className="body">
            <h6>{Strings.ESTIMATE_GAS_FEE}</h6>
            <h6
              className="uppercase text-theme-color"
              onClick={ () => setIsShowEstimatedGasFee(true) }
            >
              {getEstimateFee()}
              <img src={ dropIcon } alt="" />
            </h6>
          </div>
        </div>
        {
          selectedNetwork.txnType == 2 &&  (
            <div className="send-row">
              <div className="body">
                <span>
                  <small className="text-gray-color3">{Strings.MAX_FEE}</small>{' '}
                </span>
                <h6>{getMaxFee()}</h6>
              </div>
            </div>
          )
        }
      </div>
    );
  };

  return (
    <React.Fragment>
      <div className="container">
        <Header headerTitle={ Strings.CLAIM_DIVIDENDS_UPPERCASE } support />
        {renderContentHeader()}
        <div className="shape-rounded"></div>
        <div className="wrapper ct-wrapper">
          <div className="ct-container">
            <div className="content">
              {headingStyle()}
              {SendTokenAccounts()}
              {SendTokenAmount()}
              {SendTokenEstimated()}
            </div>
            {isShowAlert && (
              <AlertModel
                isShowAlert={ isShowAlert }
                onPressSuccess={ () => onClickSuccess() }
                className="text-center"
              >
                <p>{getUserFriendlyErrorMessage(error)}</p>
              </AlertModel>
            )}
          </div>
        </div>
        <div className="content-footer">
          {!isLoading ? (
            <Button
              onClick={ (e) => clickToClaimTokens(e) }
              label={ Strings.SEND_TRANSACTION_CONFIRM }
            />
          ) : (
            <Spinner />
          )}
        </div>
        {isShowEstimatedGasFee ? (
          <Modal
            isOpen={ isShowEstimatedGasFee }
            closeModal={ () => setIsShowEstimatedGasFee(false) }
          >
            <EstimatedGasFee
              selectedRange={ selectedRange }
              gasRange={ gasRange }
              txnObject={ txnObject }
              network={ selectedNetwork }
              onClickSave={ (updatedObject) => {
                setIsShowEstimatedGasFee(false);
                if (selectedNetwork.txnType != 0) {
                  updateTxnObjectTypeTwo(
                    updatedObject.gas,
                    updatedObject.maxPrioFee,
                    updatedObject.maxFeePerGas
                  );
                  setSelectedRange(selectedRange);
                } else {
                  updateTxnObjectTypeOne(
                    updatedObject.gas,
                    updatedObject.gasPrice
                  );
                }
              } }
            />
          </Modal>
        ) : null}
        {isShowHardwareModal && (
          <Modal
            isOpen={ isShowHardwareModal }
            closeModal={ () => {
              setIsShowHardwareModal(!isShowHardwareModal);
            } }
          >
            <div className="modal-body">
              {hardwareError && (
                <div>
                  <label className="label-error-form">{hardwareError}</label>
                  <Button
                    onClick={(e) => {
                      e.preventDefault();
                      setIsShowHardwareModal(!isShowHardwareModal)
                      navigate('/wd-flow');
                    }}
                    label={ Strings.GO_BACK_BUTTON_UPPERCASE }
                  />
                </div>
              )}
            </div>
          </Modal>
        )}
        {isShowHardwareAcceptRejectModal && (
          <Modal isOpen={ isShowHardwareAcceptRejectModal }>
            <div className="spinner-container-hardware">
              <div className="loading-spinner-hardware" />
            </div>
            <p className="label-approve-reject-form">
              {Strings.APPROVE_OR_REJECT}
            </p>
          </Modal>
        )}
        {isShowDeclinePopup && (
          <AlertModel
            alertTitle={ Strings.DECLINED }
            isShowAlert={ isShowDeclinePopup }
            onPressSuccess={ () => {
              setIsShowDeclinePopup(!isShowDeclinePopup);
              navigate('/wd-flow');
            } }
            className="text-center"
            successBtnTitle={ Strings.OKAY }
          >
            <p>{Strings.USER_REJECTED}</p>
          </AlertModel>
        )}
      </div>
    </React.Fragment>
  );
}

ClaimWD.propTypes = {
  subtitle: PropTypes.string,
  title: PropTypes.string,
  isModalVisible: PropTypes.string,
  children: PropTypes.string,
  onItemClicked: PropTypes.func,
  isActive: PropTypes.bool,
  onClickFunc: PropTypes.func,
  message: PropTypes.string,
  label: PropTypes.string,
  rightText: PropTypes.string,
  submessage: PropTypes.string,
};
