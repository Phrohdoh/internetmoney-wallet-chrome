import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Links, Strings } from 'resources';
import { useNavigate, Link } from 'react-router-dom';

import './wd.scss';

// utils
import { useTokenIcons } from 'utils/hooks';
import { displayAddress } from 'utils/displayAddress';

// Components
import Navbar from 'components/Navbar';
import Button from 'components/Button';
import Modal from 'components/Modal';
import Spinner from 'components/Spinner';
import StyledSpinner from './StyledSpinner';

// Images
import rightArrow from 'assets/images/right-arrow.svg';
import time from 'assets/images/time.png';
import support from 'assets/images/support.svg';
import refreshIcon from 'assets/images/refresh-icon.png';

import { useDispatch, useSelector } from 'react-redux';
import { getUserDividendsInfo, getWDStats } from 'web3-layer';
import { AccountAction,  } from 'redux/slices/accountSlice';
import { getAccountList, getNetwork } from 'redux/selectors';
import IntegerUnits from 'utils/IntegerUnits';

export default function WDFlow () {

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const getTokenIcon = useTokenIcons();

  const networks = useSelector(state => state.networks);
  const accountList = useSelector(getAccountList);

  const selectedNetwork = useSelector(getNetwork);
  const [selectedAccount, setSelectedAccount] = useState(accountList?.[0]);
  const [isShowAccountList, setIsShowAccountList] = useState(false);
  const [isShowAvailableInfo, setIsShowAvailableInfo] = useState(false);
  const [isShowClaimInfo, setIsShowClaimInfo] = useState(false);
  const [isShowSweepInfo, setIsShowSweepInfo] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [dividendsDetail, setDividendsDetail] = useState({});
  const [wdStateDetail, setWdStateDetail] = useState({});
  const [error, setError] = useState('');
  const [refreshingScreen, setRefreshingScreen] = useState(false)
  const IS_SELECTED_DIVIDEND_ACCOUNT = 'isSelectedDividendAccount';

  const refreshScreen = async () => {
    setRefreshingScreen(true)
    await getWDStateDetail()
  }

  // refreshScreen helper
  useEffect(() => {
    if (refreshingScreen) {
      const timer = setTimeout(() => {
        getDividendsDetail().then(() => {});
        setRefreshingScreen(false)
      }, 3000)
      return () => {
        clearTimeout(timer);
      };
    }
  }, [refreshingScreen])


  useEffect(() => {
    if (Array.isArray(accountList)) {
      setSelectedAccount(getSelectedDividendAccount() || accountList[0]);
    } else {
      getWDStateDetail().then(() => {});
    }
  }, [accountList]);

  useEffect(() => {
    if (selectedAccount) {
      getWDStateDetail().then(() => {});
    }
  }, [selectedAccount]);

  const getSelectedDividendAccount = () => {
    let selectedDividendAccount = false;

    if (Array.isArray(accountList)) {
      accountList.forEach((account) => {
        if (IS_SELECTED_DIVIDEND_ACCOUNT in account && account[IS_SELECTED_DIVIDEND_ACCOUNT]) {
          selectedDividendAccount = account;
        }
      });
    }

    return selectedDividendAccount;
  };

  const setSelectedDividendAccount = (selectedAccount) => {
    const accountsToSave = [];

    accountList.forEach((account) => {
      accountsToSave.push({
        ...account,
        isSelectedDividendAccount: account.publicAddress === selectedAccount.publicAddress
      });
    });

    setSelectedAccount(selectedAccount);
    setIsShowAccountList(false);
    dispatch(AccountAction.saveAccountList(accountsToSave));
  };

  const getWDStateDetail = async () => {
    if (networks.wdSupportObject.success && networks.wdSupportObject.wdAddress) {
      setIsLoading(true);
      const wdDetail = await getWDStats(
        networks.wdSupportObject.wdAddress,
        selectedNetwork
      );
      if (wdDetail.success) {
        setWdStateDetail(wdDetail.stats);
      }
      getDividendsDetail().then(() => {});
    }
  };

  const getDividendsDetail = async () => {
    setIsLoading(true);
    const dividends = await getUserDividendsInfo(
      selectedAccount.publicAddress,
      networks.wdSupportObject.wdAddress,
      networks.wdSupportObject.swapRouterV3,
      selectedNetwork,
    );
    console.log('dividends', dividends)

    setIsLoading(false);
    if (dividends.success) {
      setDividendsDetail(dividends.stats);
    }
  };

  const setTokenValue = value => {
    return `${value.toString(9)} ${selectedNetwork.sym}`;
  };

  const renderOption = (title, label, onClick) => {
    return (
      <div
        className={ 'option-button' }
        onClick={ onClick }
      >
        <div className="body">
          <h6 className="uppercase">{title}</h6>
          <span className="text-theme-color text-eclipse-style">
            {displayAddress(label)}
          </span>
        </div>
        <img src={ rightArrow } />
      </div>
    );
  };

  const renderListitem = (label, value) => {
    return (
      <div className={ 'ls-item' }>
        <div className="body">
          <span>{label}</span>
        </div>
        <div className="right text-right">
          <strong>{value}</strong>
        </div>
      </div>
    );
  };

  const renderAccountListCell = (item, index) => {
    return (
      <div
        key={ index }
        className="listitem-control noselect"
        data-testid={ `wd-account-list-cell-button_${index}` }
        onClick={ () => {
          setSelectedDividendAccount(item);
        } }>
        <div className="body">
          <h6>
            {item.name}
          </h6>
        </div>
      </div>
    );
  };

  const renderSweepInfo = (item, index) => {
    return (
      <div className="listitem-control noselect">
        <div className="body">
          <h6>
            By sweeping, all dividends that have accrued are brought into the TIME smart contract and made available to claim. 
            This is an optional transaction, as when one person sweeps, dividends are distributed for everyone.
          </h6>
        </div>
      </div>
    );
  };

  const renderClaimInfo = (item, index) => {
    return (
      <div className="listitem-control noselect">
        <div className="body">
          <h6>
            By claiming, you are transferring all earned dividends to your account holding TIME.
          </h6>
        </div>
      </div>
    );
  };

  const renderAvailableInfo = () => {
    return (
      <div className="listitem-control noselect">
        <div className="body">
          <h6>
            Available = Sweepable + Claimable Dividends
          </h6>
        </div>
      </div>
    );
  };

  // render Create Account View
  const renderAccounts = (list) => {
    return (
      <React.Fragment>
        <div className="modal-header">
          <h6 className="text-center">{Strings.CHOOSE_ACCOUNTS_UPPERCASE}</h6>
        </div>
        <div className="modal-body">
          <div className="languages-list">
            {
              list.map((item, index) => (
                renderAccountListCell(item, index)
              ))
            }
          </div>
        </div>
      </React.Fragment>

    );
  };

  const showAvailableInfo = () => {
    setIsShowAvailableInfo(true);
  };

  const showSweepInfo = () => {
    setIsShowSweepInfo(true);
  };

  const showClaimInfo = () => {
    setIsShowClaimInfo(true);
  };

  return (
    <div className="main-container">
      <Navbar />
      <div className="container">
        <div className="main-content">
          <div className="dash-header">
            <div className="left">
              <img src={ time } alt="TIME" />
            </div>
            <div className="body">
              <h2 className="text-theme-color mb-0 uppercase">{Strings.WALLET_DIVIDENDS_UPPERCASE}</h2>
            </div>
            <div className="right">
            { !refreshingScreen && (
              <div>
                <p onClick={() => refreshScreen()} style={{ cursor: 'pointer' }} className="mb-0">
                  <img style={{ margRight: '20px' }} src={refreshIcon} width="20px" alt="refresh" />
                </p>
              </div>
            )}
            { refreshingScreen &&
              <StyledSpinner />
            }
              <Link to={ { pathname: '' } }
                onClick={ (e) => {
                  e.preventDefault();
                  navigate('/support', {
                    state: {
                      supportType: 'contactus',
                      title: Strings.CONTACT_US,
                    }
                  });
                } } className="header-icon">
                <img src={ support } alt="Support" />
              </Link>
            </div>
          </div>
          <div className="ds-flex-col full-flex overflow-auto">
            {!isLoading ? (
              networks.wdSupportObject.success && networks.wdSupportObject.wdAddress ? (
                <React.Fragment>
                  <div className="header-container">
                    {selectedAccount && (
                      <div className="options-item">
                        {renderOption(
                          selectedAccount.name,
                          selectedAccount.publicAddress,
                          () => setIsShowAccountList(true)
                        )}
                      </div>
                    )}
                    <div className="wd-header text-center pb-0">
                        <span className="label-style-no-display-block mb-0" style={{ marginBottom: 25 }}>
                          {Strings.AVAILABLE_CLAIM}
                        </span>
                        <span style={{ marginLeft: 10, cursor: 'pointer' }} onClick={ () => showAvailableInfo() }>
                          <img src={ support } alt="click here to learn more" style={{ marginTop: 10 }} />
                        </span>
                      <h2>
                        {
                          dividendsDetail.claimableDiv === undefined
                          ? '0'
                          : dividendsDetail.claimableDiv
                              .add(
                                dividendsDetail.pendingDistribution
                                  .multiply(dividendsDetail.balance)
                                  .divide(wdStateDetail.totalSupply)
                              )
                              .toString(9)
                        }
                        {' '}
                        {selectedNetwork.sym}
                        <img src={ getTokenIcon('', selectedNetwork.chainId) } />
                      </h2>
                    </div>
                  </div>
                  <div className="content-background ds-flex-col full-flex overflow-initial">
                    <div className="wrapper">
                      <div className="wd-header text-center top-border bottom-border">
                        <span className="label-style mb-0">{Strings.WD_BALANCE}</span>
                        <h2>
                          {
                            dividendsDetail.balance === undefined
                              ? '0'
                              : dividendsDetail.balance.toString(2)
                          }
                          {' '}
                          {wdStateDetail.sym ?? 'TIME'}
                          <img src={ time } alt="TIME"/>
                        </h2>
                      </div>
                      {renderListitem(
                        Strings.TOTAL_DIVIDENDS_CLAIMED,
                        setTokenValue(
                          dividendsDetail.totalEarnedDiv === undefined
                            ? 0
                            : dividendsDetail.totalEarnedDiv.subtract(dividendsDetail.claimableDiv).toString(9)
                        ),
                      )}
                      {renderListitem(
                        Strings.CLAIMABLE_DIVIDENDS,
                        setTokenValue(dividendsDetail.claimableDiv ?? 0),
                      )}
                      {renderListitem(
                        Strings.SWEEPABLE_AMOUNT,
                        setTokenValue(
                          dividendsDetail.pendingDistribution === undefined
                            ? 0
                            : dividendsDetail.pendingDistribution.multiply(dividendsDetail.balance).divide(wdStateDetail.totalSupply)
                        ),
                      )}
                      <div className="wd-footer text-center">
                        <span className="opacity">
                        </span>
                      </div>
                      {error ? <span className="error">{error}</span> : null}

                      <div className="text-center">
                        <span>
                            <Button
                              className="button-inline button-spacer"
                              label={ Strings.SWEEP_DIVIDENDS_UPPERCASE }
                              onClick={ (e) =>
                                {
                                  e.preventDefault();
                                  if (dividendsDetail.pendingDistribution.multiply(dividendsDetail.balance).divide(wdStateDetail.totalSupply).gt(new IntegerUnits(0))) {
                                    dividendsDetail.claimableDiv = dividendsDetail.pendingDistribution.multiply(dividendsDetail.balance).divide(wdStateDetail.totalSupply);
                                    navigate('/claim-WD', {
                                      state: {
                                        dividendsDetail: dividendsDetail,
                                        wdStateDetail: wdStateDetail,
                                        fromAccount: selectedAccount,
                                        isSweep: true,
                                      }
                                    });
                                  } else{
                                    setError(Strings.SWEEP_ZERO_BALANCE_ERROR_MSG);
                                  }
                                }
                              }
                            />
                        </span>

                        <span style={{ marginTop: 10, marginLeft: 13, cursor: 'pointer' }} onClick={ () => showSweepInfo() }>
                          <img src={ support } alt="learn more" style={{ marginTop: 10 }} />
                        </span>
                      </div>

                      <div className="text-center">
                        <span>
                          <Button
                            className="button-inline button-spacer"
                            label={ Strings.CLAIM_DIVIDENDS_UPPERCASE }
                            onClick={ (e) =>
                            {
                              e.preventDefault();
                              if (dividendsDetail.claimableDiv.gt(new IntegerUnits(0))) {
                                navigate('/claim-WD', {
                                  state: {
                                    dividendsDetail: dividendsDetail,
                                    wdStateDetail: wdStateDetail,
                                    fromAccount: selectedAccount,
                                    isSweep: false,
                                  }
                                });
                              } else{
                                setError(Strings.CLAIM_ZERO_BALANCE_ERROR_MSG);
                              }
                            }
                            }
                          />
                        </span>

                        <span style={{ marginTop: 10, marginLeft: 15, cursor: 'pointer' }} onClick={ () => showClaimInfo() }>
                          <img src={ support } alt="learn more" style={{ marginTop: 10 }} />
                        </span>
                              
                      </div>
                      <div className="text-gray-color2 text-center mt-1">
                        <strong>
                          {Strings.formatString(
                            Strings.WHAT_IS_TIME,
                            <Link
                              to="/wd-flow"
                              onClick={(_e) => window.open(Links.WHAT_IS_TIME)}
                              className="text-theme-color uppercase text-underline"
                            >
                              {" "}
                              {Strings.LEARN_MORE}
                            </Link>
                          )}
                        </strong>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              ) : (
                <div className="full-flex justify-center">
                  <span className="text-gray-color3 text-center">{Strings.TIME_NOT_SUPPORTED}</span>
                  <div className="text-gray-color2 text-center mt-1">
                    <strong>
                      {Strings.formatString(
                        Strings.WHAT_IS_TIME,
                        <Link
                          to="/wd-flow"
                          onClick={(_e) => window.open(Links.WHAT_IS_TIME)}
                          className="text-theme-color uppercase text-underline"
                        >
                          {" "}
                          {Strings.LEARN_MORE}
                        </Link>
                      )}
                    </strong>
                  </div>
                </div>
              )
            )
              : <Spinner />
            }
          </div>
        </div>
        {isShowAccountList ?
          <Modal
            isOpen={ isShowAccountList }
            closeModal={ () => setIsShowAccountList(false) }
          >
            <div className="modal-body">
              {renderAccounts(accountList)}
            </div>
          </Modal>
          : null}

        {isShowAvailableInfo?
          <Modal
            isOpen={ isShowAvailableInfo }
            closeModal={ () => setIsShowAvailableInfo(false) }
          >
            <div className="modal-body">
              {renderAvailableInfo()}
            </div>
          </Modal>
          : null}   

          {isShowClaimInfo?
            <Modal
              isOpen={ isShowClaimInfo }
              closeModal={ () => setIsShowClaimInfo(false) }
            >
              <div className="modal-body">
                {renderClaimInfo()}
              </div>
            </Modal>
            : null} 

            {isShowSweepInfo?
              <Modal
                isOpen={ isShowSweepInfo }
                closeModal={ () => setIsShowSweepInfo(false) }
              >
                <div className="modal-body">
                  {renderSweepInfo()}
                </div>
              </Modal>
              : null} 
      </div>
    </div>
  );
}

WDFlow.propTypes = {
  title: PropTypes.string,
  content: PropTypes.string,
  className: PropTypes.string,
};
