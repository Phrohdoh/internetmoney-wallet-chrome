import React from 'react';
import './styled-spinner.scss';

export default function StyledSpinner () {
  return (
    <React.Fragment>
      <div className="wallet-loading-spinner" ></div>
    </React.Fragment>
  );
}
