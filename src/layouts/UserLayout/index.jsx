import React from 'react';
import PropTypes from 'prop-types';
import './user-layout.scss';
import { Toaster } from 'react-hot-toast';
import WalletConnectModals from 'components/WalletConnectModals';

export default function UserLayout (props) {

  const {Component}= props;
    
  return(
    <React.Fragment>
      <div className='user-layout'>
        <Component />
        <WalletConnectModals />
        <Toaster
          position="top-center"
          toastOptions={ {
            className: 'toast-styles',
            duration: 4000,
            style: {
              background: '#fba81a',
              color: '#000',
            },
          } }
        />
      </div>
    </React.Fragment>
  );
}

UserLayout.propTypes = {
  Component: PropTypes.func,
};
