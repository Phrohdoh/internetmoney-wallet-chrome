import { STORAGE_KEYS } from "constants";
import { AccountAction } from "redux/slices/accountSlice";
import {
  getFullAccountList,
  getMnemonic,
  getPassword,
  getWalletObject,
} from 'redux/selectors';
import { setItem } from "utils/storage";
import { getPublicAddressOfIndex } from "web3-layer";

const ACTION_TYPES = [
  'account/saveAccountList',
  'account/updateAccountObject',
];

// {
//   [accountAddress]: true
// }
const checkedAddresses = {};

// This middleware checks for accounts which were created/imported prior to adding
// the `isImported` flag, determines whether or not the account was imported, and
// sets the flag accordingly.
export const checkForImportedAccountsMiddleware = store => next => async (action) => {  
  next(action);

  if (!ACTION_TYPES.includes(action.type)) {
    return;
  }

  const state = store.getState();
  const accountList = getFullAccountList(state);
  const password = getPassword(state);
  const mnemonic = getMnemonic(state);
  const walletObject = getWalletObject(state);

  if (accountList === undefined || password === '' || mnemonic === undefined || walletObject === undefined) {
    return;
  }

  const accountsToCheck = accountList.filter(account => {
    const publicAddress = account.publicAddress.toLowerCase();
    if (checkedAddresses[publicAddress]) {
      return;
    }
    if (account.isImported !== undefined) {
      return;
    }
    checkedAddresses[publicAddress] = true;
    return publicAddress;
  });

  if (accountsToCheck.length === 0) {
    return;
  }

  const possibleGeneratedAddressesPromises = [];
  for (let i = 0; i < accountList.length; i += 1) {
    possibleGeneratedAddressesPromises.push(getPublicAddressOfIndex(
      mnemonic,
      password,
      i,
    ));
  }

  const possibleGeneratedAddresses = await Promise.all(possibleGeneratedAddressesPromises);

  const isImportedUpdates = accountsToCheck.map(account => ({
    publicAddress: account.publicAddress.toLowerCase(),
    isImported: !account.isHardware && possibleGeneratedAddresses.every(generatedAddress => (
      generatedAddress.toLowerCase() !== account.publicAddress.toLowerCase()
    )),
  }));

  store.dispatch(AccountAction.isImportedUpdates(isImportedUpdates));
  setItem(STORAGE_KEYS.ACCOUNT_LIST, JSON.stringify(getFullAccountList(store.getState())));
};