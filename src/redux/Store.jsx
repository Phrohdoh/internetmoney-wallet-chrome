import { configureStore } from '@reduxjs/toolkit';

import { GlobalReducer } from 'redux/slices/globalSlice';
import {WalletReducer}  from 'redux/slices/walletSlice';
import {AccountReducer} from 'redux/slices/accountSlice';
import {NetworksReducer} from 'redux/slices/networksSlice';
import WalletConnectReducer from 'redux/slices/walletConnectSlice';
import { TransactionsReducer } from 'redux/slices/transactionsSlice';
import appConfig from 'config';
import { fetchTokenPricesMiddleware } from './middleware/fetchTokenPricesMiddleware';
import { fetchTokenBalancesMiddleware } from './middleware/fetchTokenBalancesMiddleware';
import { checkForDefaultTokenAccountsMiddleware } from './middleware/checkForDefaultTokenAccountsMiddleware';
import { checkForImportedAccountsMiddleware } from './middleware/checkForImportedAccountsMiddleware';

const reducer = {   
  global: GlobalReducer,
  wallet: WalletReducer,
  account: AccountReducer,
  networks: NetworksReducer,
  walletConnect: WalletConnectReducer,  
  transactions: TransactionsReducer, 
};

export const store = configureStore({
  reducer,    
  middleware: getDefaultMiddleware => ([
    ...getDefaultMiddleware(),
    fetchTokenPricesMiddleware,
    fetchTokenBalancesMiddleware,
    checkForDefaultTokenAccountsMiddleware,
    checkForImportedAccountsMiddleware,
    // Disabling the logger (for now) because it's so noisy
    // logger,
  ]),
  devTools: (appConfig.environment !== 'production') ? true : false,
});
