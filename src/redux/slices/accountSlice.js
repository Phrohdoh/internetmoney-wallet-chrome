import { createSlice } from '@reduxjs/toolkit';
import { STORAGE_KEYS } from '../../constants';
import { getItem } from 'utils/storage';
import { WalletConnectAction } from 'redux/slices/walletConnectSlice';

// utils
import { parseObject } from 'utils/parse';

const assignDefaultIsArchived = (accounts) => (
  accounts.map(account => ({
    ...account,
    isArchived: account.isArchived ?? false,
  }))
);

export const initialState= {
  accountList: [],
};

const accountSlice = createSlice({
  name: 'account',
  initialState,
  reducers: {
    resetSliceData: state => {
      state.accountList = [];
    },
    saveAccountList: (state, action) => {
      state.accountList = assignDefaultIsArchived(action.payload);
    },
    updateAccountObject: (state, action) => {
      state.accountList = assignDefaultIsArchived(action.payload);
    },
    isImportedUpdates: (state, action) => {
      action.payload.forEach(({ publicAddress, isImported }) => {
        const account = state.accountList.find(account => (
          account.publicAddress.toLowerCase() === publicAddress.toLowerCase()
        ));
        account.isImported = isImported;
      });
    },
    isPopularTokensUpdate: (state, action) => {
      action.payload.forEach(({ publicAddress, isPopularTokens }) => {
        const account = state.accountList.find(account => (
          account.publicAddress.toLowerCase() === publicAddress.toLowerCase()
        ));
        account.isPopularTokens = isPopularTokens;
      });
    },
    removeAccount: (state, action) => {
      const { isImported, isHardware } = action.payload;
      const publicAddress = action.payload.publicAddress.toLowerCase();
      if (isImported || isHardware) {
        // Completely remove imported/hardware accounts
        state.accountList = state.accountList.filter((
          account => account.publicAddress.toLowerCase() !== publicAddress
        ));
      } else {
        // Archive generated accounts
        state.accountList.find((
          account => account.publicAddress.toLowerCase() === publicAddress
        )).isArchived = true;
      }
    },
    restoreAccount: (state, action) => {
      const publicAddress = action.payload.publicAddress.toLowerCase();
      state.accountList.find((
        account => account.publicAddress.toLowerCase() === publicAddress
      )).isArchived = false;
    },
  },
});

const initAccounts= () => dispatch => {  
  // getAccounts
  dispatch(initAccountList());
};

export const initAccountList = () => dispatch => {
  const accountList = getItem(STORAGE_KEYS.ACCOUNT_LIST);
  const parseObj = parseObject(accountList.data);
  if (parseObj.success) {
    dispatch(accountSlice.actions.saveAccountList(parseObj.data));
    dispatch(WalletConnectAction.initSelectedAccounts(parseObj.data[0].publicAddress));
  }  
};

export const AccountAction = { ...accountSlice.actions, initAccounts, initAccountList };
export const AccountReducer = accountSlice.reducer;