import { createSlice } from '@reduxjs/toolkit';
import { STORAGE_KEYS, BACKEND_SERVER } from '../../constants';
import { getItem, setItem } from 'utils/storage';
import { handleNetworkTokenIcons } from 'themes';

// utils
import { parseObject } from 'utils/parse';
import { log } from 'utils/log';
import fallbackNetworks from 'constants/fallbackNetworks';

const networkSlice = createSlice({
  name: 'networks',
  initialState: {
    tokens: [],
    networks: [],    
    removedDefaultNetworks: {},
    selectedNetworkObj: {},
    wdSupportObject: {},
    swapSupportedNetworks: [],
    /*
    *  {
    *    [chainId]: {
    *      [accountAddress]: [address]
    *    }
    *  }
    */
    removedDefaultTokens: {},
    /*
    *  {
    *    [chainId]: {
    *      [tokenAddress]: {
    *        value: number,
    *        fetching: boolean,
    *        lastFetched: number (ms timestamp),
    *      }
    *    }
    *  }
    */
    usdPrices: {},
    /*
    *  {
    *    [chainId]: {
    *      [accountAddress]: {
    *        [tokenAddress]: {
    *          value: number,
    *          fetching: boolean,
    *          lastFetched: number (ms timestamp),
    *        }
    *      }
    *    }
    *  }
    */
    tokenBalances: {},
  },
  reducers: {
    resetSliceData: state => {
      state.tokens = [];
      state.usdPrices = {};
      state.tokenBalances = {};
    },
    saveTokens: (state, action) => {
      state.tokens = action.payload;
    },
    saveNetworks: (state, action) => {
      state.networks = action.payload;
    },
    initializeRemovedDefaultNetworks: (state, action) => {
      state.removedDefaultNetworks = action.payload;
    },
    removeDefaultNetwork: (state, action) => {
      const chainId = parseInt(action.payload);
      state.removedDefaultNetworks[chainId] = true;
    },
    saveSelectedNetworkObject: (state, action) => {
      state.selectedNetworkObj = action.payload;
    },
    saveWDAddressObject: (state, action) => {
      state.wdSupportObject = action.payload;
    },
    saveSwapSupportedNetworks: (state, action) => {
      state.swapSupportedNetworks = action.payload;
    },
    initializeRemovedDefaultTokens: (state, action) => {
      state.removedDefaultTokens = action.payload;
    },
    removeDefaultToken: (state, action) => {
      const address = action.payload.address.toLowerCase();
      const accountAddress = action.payload.accountAddress.toLowerCase();
      const { chainId } = state.selectedNetworkObj;
      state.removedDefaultTokens[chainId] ??= {};
      state.removedDefaultTokens[chainId][accountAddress] ??= [];
      const removedTokens = state.removedDefaultTokens[chainId][accountAddress];
      if (!removedTokens.includes(address)) {
        removedTokens.push(address);
      }
    },
    fetchingTokenPrices: (state, action) => {
      action.payload.forEach(payload => {
        const { chainId } = payload;
        const tokenAddress = payload.tokenAddress.toLowerCase();
        state.usdPrices[chainId] ??= {};
        state.usdPrices[chainId][tokenAddress] ??= {};
        state.usdPrices[chainId][tokenAddress].fetching = true;
      });
    },
    fetchedTokenPrices: (state, action) => {
      action.payload.forEach(payload => {
        const { success, chainId, usdPrice } = payload;
        const tokenAddress = payload.tokenAddress.toLowerCase();
        if (state.usdPrices[chainId][tokenAddress]) {
          if (success) {
            state.usdPrices[chainId][tokenAddress].value = usdPrice;
            state.usdPrices[chainId][tokenAddress].fetching = false;
            state.usdPrices[chainId][tokenAddress].lastFetched = Date.now();
          } else {
            state.usdPrices[chainId][tokenAddress].fetching = false;
          }
        }
      });
    },
    fetchingTokenBalances: (state, action) => {
      action.payload.forEach(payload => {
        const { chainId } = payload;
        const accountAddress = payload.accountAddress.toLowerCase();
        const tokenAddress = payload.tokenAddress.toLowerCase();
        state.tokenBalances[chainId] ??= {};
        state.tokenBalances[chainId][accountAddress] ??= {};
        state.tokenBalances[chainId][accountAddress][tokenAddress] ??= {};
        state.tokenBalances[chainId][accountAddress][tokenAddress].fetching = true;
      });
    },
    fetchedTokenBalances: (state, action) => {
      action.payload.forEach(payload => {
        const { success, chainId, balance } = payload;
        const accountAddress = payload.accountAddress.toLowerCase();
        const tokenAddress = payload.tokenAddress.toLowerCase();
        if (state.tokenBalances[chainId][accountAddress][tokenAddress]) {
          if (success) {
            state.tokenBalances[chainId][accountAddress][tokenAddress].value = balance;
            state.tokenBalances[chainId][accountAddress][tokenAddress].fetching = false;
            state.tokenBalances[chainId][accountAddress][tokenAddress].lastFetched = Date.now();
          } else {
            state.tokenBalances[chainId][accountAddress][tokenAddress].fetching = false;
          }
        }
      });
    },
    refreshBalances: () => {
      // noop (handled by fetchTokenBalancesMiddleware and fetchTokenPricesMiddleware)
      // Maybe not ideal, but it's better than the mess we had before
    },
  },
});

const initNetworks = () => async (dispatch, getState) => {
  try {
    const removedDefaultNetworksResult = getItem(STORAGE_KEYS.REMOVED_DEFAULT_NETWORKS);
    if (!removedDefaultNetworksResult.error) {
      const parsedRemovedDefaultNetworks = parseObject(removedDefaultNetworksResult.data);
      if (parsedRemovedDefaultNetworks.success) {
        dispatch(NetworksAction.initializeRemovedDefaultNetworks(parsedRemovedDefaultNetworks.data));
      }
    }

    const removedDefaultTokensResult = getItem(STORAGE_KEYS.REMOVED_DEFAULT_TOKENS);
    if (!removedDefaultTokensResult.error) {
      const parsedRemovedDefaultTokens = parseObject(removedDefaultTokensResult.data);
      if (parsedRemovedDefaultTokens.success) {
        dispatch(NetworksAction.initializeRemovedDefaultTokens(parsedRemovedDefaultTokens.data));
      }
    }

    let NetworkDetails = [];
    try {
      const networksUrl = BACKEND_SERVER.BASE_URL + BACKEND_SERVER.GET_NETWORKS;
      const networksResponse = await fetch(networksUrl);
      NetworkDetails = (await networksResponse.json())
        .filter(network => !network.disabled)
        .map(network => ({
          ...network,
          isDefault: true,
        }));
    } catch (e) {
      NetworkDetails = fallbackNetworks;
    }

    const {data, error} = getItem(STORAGE_KEYS.NETWORKS);

    if(!error){
      const parseNetworks = parseObject(data);

      console.log("NETWORKS: ", parseNetworks.data)
      if(parseNetworks.success){
        // Fix to convert any string chainId to number
        parseNetworks.data.forEach(network => {
          network.chainId = parseInt(network.chainId);
        });

        let mergedNetworks = [...NetworkDetails];
        parseNetworks.data.forEach(network => {
          if (!network.isDefault) {
            const defaultNetwork = mergedNetworks.find(mergedNetwork => mergedNetwork.chainId === network.chainId && mergedNetwork.isDefault);
            if (defaultNetwork) {
              network.icon ??= defaultNetwork.icon;
              network.tokens ??= defaultNetwork.tokens;
              network.wNativeAddress ??= defaultNetwork.wNativeAddress;
              network.tokens.forEach(token => {
                try {
                  const defaultToken = defaultNetwork.tokens.find(t => {
                    const defaultTokenAddress = t.address?.toLowerCase() ?? t.tokenId?.toLowerCase();
                    const tokenAddress = token.address?.toLowerCase() ?? token.tokenId?.toLowerCase();
                    return defaultTokenAddress === tokenAddress;
                  });
                  if (defaultToken) {
                    token.symbol = defaultToken.symbol;
                    token.decimals = defaultToken.decimals;
                  }
                } catch (e) {
                  // Failed to assign default token values
                }
              });
            }
            mergedNetworks = mergedNetworks.filter(mergedNetwork => mergedNetwork.chainId !== network.chainId);
          }
          const alreadyIncluded = mergedNetworks.some(mergedNetwork => mergedNetwork.chainId === network.chainId);
          if (!alreadyIncluded) {
            mergedNetworks.push(network);
          }
        })
        handleNetworkTokenIcons(mergedNetworks);

        console.log("MERGED:", mergedNetworks)
        dispatch(NetworksAction.saveNetworks(mergedNetworks));
      }
    } else {
      await setItem(
        STORAGE_KEYS.NETWORKS,
        JSON.stringify(NetworkDetails),
      );
      handleNetworkTokenIcons(NetworkDetails);
      dispatch(NetworksAction.saveNetworks(NetworkDetails));
    }

    const state= getState();
    const selectedNetwork = state.networks.selectedNetworkObj.chainId;
    // get networks from storage when we add option to pick networks
    
    dispatch(getNetworkTokenList(selectedNetwork));
    dispatch(setSelectedNetwork());
    dispatch(checkIsNetworkSupport(state.networks.selectedNetworkObj)); 
    dispatch(getSwapSupportNetworkList()); 
  } catch (e) {
    console.log('er >>>', e);
  }
};


const getNetworkTokenList = (chainId) => (dispatch, _getState) => {

 
  const chainData = getItem(STORAGE_KEYS.CHAIN_TOKENS);
  if (chainData.data) {
    
    let chainTokens = undefined;

    const parseObj = parseObject(chainData.data);
    if (parseObj.success) {
      chainTokens = parseObj.data;
    }
    if (chainTokens) {
      let upgraded = false;
      chainTokens.forEach(chain => {
        chain.accountTokens?.forEach(account => {
          account.tokens?.forEach(token => {
            if (token.tokenId !== undefined) {
              token.address = token.tokenId;
              delete token.tokenId;
              upgraded = true;
            }
            if (token.decimal !== undefined) {
              token.decimals = token.decimal;
              delete token.decimal;
              upgraded = true;
            }
          });
        });
      });
      if (upgraded) {
        setItem(STORAGE_KEYS.CHAIN_TOKENS, JSON.stringify(chainTokens));
      }

      const chainObj = chainTokens.find(
        chain => chain.chainId === chainId,
      );
      if (chainObj) {
        dispatch(NetworksAction.saveTokens(chainObj.accountTokens));
      } else {
        dispatch(NetworksAction.saveTokens([]));
      }
    }
  }
};

const setSelectedNetwork = () => async (dispatch, _getState) => {
  // const network = getItem(STORAGE_KEYS.NETWORKS);
  const selectedNetwork = getItem(STORAGE_KEYS.SELECTED_NETWORK);

  const parseSelectedObj = parseObject(selectedNetwork.data);

  if (selectedNetwork && parseSelectedObj.success) {
    dispatch(
      networkSlice.actions.saveSelectedNetworkObject(parseSelectedObj.data),
    );
  } else {
    const networksUrl = BACKEND_SERVER.BASE_URL + BACKEND_SERVER.GET_NETWORKS;
    let NetworkDetails;
    try {
      const networksResponse = await fetch(networksUrl);
      NetworkDetails = (await networksResponse.json())
        .filter(network => !network.disabled);
    } catch (e) {
      NetworkDetails = fallbackNetworks;
    }
    dispatch(networkSlice.actions.saveSelectedNetworkObject(NetworkDetails[0]));
    await setItem(
      STORAGE_KEYS.SELECTED_NETWORK,
      JSON.stringify(NetworkDetails[0]),
    );
  }

};

const checkIsNetworkSupport = networkDetail => async dispatch => {

  try{

    if(!networkDetail.chainId)
      return;
      
    const body = {
      chainId: networkDetail.chainId,
      networkName: networkDetail.networkName
    };
    const baseUrl = BACKEND_SERVER.BASE_URL + BACKEND_SERVER.GET_NETWORK_SUPPORT_DETAIL;
    
    const response = await fetch(baseUrl, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify(body),
    });
    const data = await response.json();
    dispatch(networkSlice.actions.saveWDAddressObject(data));
  }
  catch(e){
    log(e);
  }
};

const getSwapSupportNetworkList = () => async dispatch => {

  try{
    const baseUrl =
      BACKEND_SERVER.BASE_URL + BACKEND_SERVER.GET_SWAP_SUPPORT_NETWORK_LIST;

    const response = await fetch(baseUrl, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    });

    const data = await response.json();

    if (response.status === 200) {
      if (data.success) {
        dispatch(networkSlice.actions.saveSwapSupportedNetworks(data.data));
      }
    }
  }
  catch(e){
    log(e);
  }
};

export const NetworksAction = {
  ...networkSlice.actions,
  initNetworks,
  checkIsNetworkSupport
};
export const NetworksReducer = networkSlice.reducer;
