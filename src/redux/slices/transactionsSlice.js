import { STORAGE_KEYS } from '../../constants';
import { getItem } from 'utils/storage';
import { createSlice } from '@reduxjs/toolkit';

// utils
import { parseObject } from 'utils/parse';

const transactionsSlice = createSlice({
  name: 'transactions',
  initialState: {
    recentAddresses: [],
    transactionsList: [],
  },
  reducers: {
    resetSliceData: state => {
      state.recentAddresses = [];
      state.transactionsList = [];
    },
    saveTransactions: (state, action) => {
      state.transactionsList = action.payload;
    },
    saveRecentAddresses: (state, action) => {
      state.recentAddresses = action.payload;
    },
  },
});

const initTransactions = () => (dispatch) => {
  
  dispatch(getTransactionsList());
};

const getTransactionsList = () => async (dispatch, _getState) => {

  const state = _getState();
  const networks = state.networks;
 
  const transactionsData = await getItem(STORAGE_KEYS.TRANSACTIONS);
  const recentTransactionsData = await getItem(STORAGE_KEYS.RECENT_ADDRESSES);
  
  if (transactionsData) {
    
    let transactions = undefined;    
    const parseObj = parseObject(transactionsData.data);
    if (parseObj.success) {
      transactions = parseObj.data;
    }

    if (transactions) {
      const transactionObj = transactions.find(
        trans => trans.chainId === networks.selectedNetworkObj.chainId,
      );
      if (transactionObj) {
        dispatch(TransactionsAction.saveTransactions(transactionObj.transactions));
      } else {
        dispatch(TransactionsAction.saveTransactions([]));
      }
    }

    let recentAddresses = undefined;
    const parseObj2 = parseObject(recentTransactionsData.data);
    
    if (parseObj2.success) {
      recentAddresses = parseObj2.data;
    }

    if (recentAddresses) {
      dispatch(TransactionsAction.saveRecentAddresses(recentAddresses));
    } else {
      dispatch(TransactionsAction.saveRecentAddresses([]));
    }
  }
};

export const TransactionsAction = {
  ...transactionsSlice.actions,
  initTransactions
};
export const TransactionsReducer = transactionsSlice.reducer;
