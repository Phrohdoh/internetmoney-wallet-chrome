import { createSlice } from '@reduxjs/toolkit';
import { Strings } from 'resources';
import { getUserFriendlyErrorMessage } from 'utils/errors';

export const initialState = {
  alertData: {
    isShowAlert: false,
    alertTitle: '',
    alertMsg: '',
    successBtnTitle: Strings.OK,
  },
  isLoading: false,
};

const globalSlice = createSlice({
  name: 'global',
  initialState,
  reducers: {
    showAlertMsg: (state, action) => {
      state.alertData = {
        ...state.alertData,
        isShowAlert: true,
        ...action.payload,
      };
    },
    hideAlertMsg: (state, action) => {
      state.alertData = {
        ...state.alertData,
        ...action.payload,
        isShowAlert: false,
      };
    },
    isShowLoading: (state, action) => {
      state.isLoading = action.payload;
    },
  },
});

const showAlert = (
  title = '',
  message = '',
  successBtnTitle = Strings.OK,
) => dispatch => {
  message = getUserFriendlyErrorMessage(message);
  setTimeout(() => {
    dispatch(
      globalSlice.actions.showAlertMsg({
        alertTitle: title,
        alertMsg: message,
        successBtnTitle: successBtnTitle,
      }),
    );
  }, 200);
};

const hideAlert = () => dispatch => {
  dispatch(
    globalSlice.actions.hideAlertMsg({
      alertTitle: '',
      alertMsg: '',
      successBtnTitle: Strings.OK,
    }),
  );
};

export const GlobalAction = { ...globalSlice.actions, showAlert, hideAlert };
export const GlobalReducer = globalSlice.reducer;