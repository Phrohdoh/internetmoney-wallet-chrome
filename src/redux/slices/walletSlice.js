import { createSlice } from '@reduxjs/toolkit';
import { log } from 'utils/log';
import { AccountAction } from './accountSlice';
import { NetworksAction } from './networksSlice';
import { TransactionsAction } from './transactionsSlice';
import { getItem, setItem } from 'utils/storage';
import { STORAGE_KEYS } from '../../constants';
import { parseObject } from 'utils/parse';
import { WalletConnectAction } from './walletConnectSlice';

export const initialState = {
  walletDetail: {},
  password: '',
  isPasscode: false,
};

const walletSlice = createSlice({
  name: 'wallet',
  initialState,
  reducers: {
    savePassword: (state, action) => {
      state.password = action.payload;
    },
    setIsPasscode: (state, action) => {
      state.isPasscode = action.payload;
    },
    saveWalletDetail: (state, action) => {
      state.walletDetail = action.payload;
    },
    removeAccount: (state, action) => {
      const { isImported, isHardware } = action.payload;
      const publicAddress = action.payload.publicAddress.toLowerCase();
      if (isImported || isHardware) {
        // Completely remove imported/hardware accounts from wallet object
        state.walletDetail.walletObject = state.walletDetail.walletObject.filter((
          account => `0x${account.address.toLowerCase()}` !== publicAddress
        ));
      } else {
        // Archive generated accounts (keep in wallet object)
      }
    },
  },
});

const initApp = (wallet, password, isPasscode, networks) => async dispatch => {
  try {
    // save wallet object into redux
    dispatch(walletSlice.actions.saveWalletDetail(wallet));
    // save password object into redux
    dispatch(walletSlice.actions.savePassword(password));
    dispatch(walletSlice.actions.setIsPasscode(isPasscode));

    // saveBlockNumber(wallet);

    // dispatch(AccountAction.initAccounts(wallet));
    // // get Network & tokenList
    // dispatch(NetworksAction.initNetworks());
    // // get TransactionList
    // dispatch(TransactionsAction.initTransactions());

    // reset All Data
    dispatch(resetData(wallet, networks));

  } catch (e) {
    return log(e.message);
  }
};

const saveBlockNumber = async wallet => {
  try {
    const { error, data } = await getItem(STORAGE_KEYS.BLOCK_NO);

    const parseObj = parseObject(data);
    if (!parseObj.success) {
      const tempObj = wallet.blockNumbers.reduce(
        (r, c) => Object.assign(r, c),
        {},
      );
      await setItem(
        STORAGE_KEYS.BLOCK_NO,
        JSON.stringify(tempObj),
      );
    }
  } catch (e) {
    log('e >>', e);
  }
};

const resetData = (wallet, networks) => async dispatch => {
  try {
    // reset all slice data
    dispatch(AccountAction.resetSliceData());
    dispatch(NetworksAction.resetSliceData());
    dispatch(TransactionsAction.resetSliceData());
    dispatch(WalletConnectAction.initCompletedAuthRequests());
    dispatch(WalletConnectAction.initCompletedSessionProposals());
    dispatch(WalletConnectAction.initCompletedSessionRequests());
    dispatch(WalletConnectAction.initRequestVerifications());
    dispatch(AccountAction.initAccounts(wallet));
    // get Network & tokenList
    dispatch(NetworksAction.initNetworks(networks));
    // get TransactionList
    dispatch(TransactionsAction.initTransactions());

  } catch (e) {
    return log(e.message);
  }
};

export const WalletAction = { ...walletSlice.actions, initApp, resetData };
export const WalletReducer = walletSlice.reducer;
export default walletSlice;
