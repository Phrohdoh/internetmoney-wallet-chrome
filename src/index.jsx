/* istanbul ignore file */
import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from 'reportWebVitals';
import Navigation from 'navigation';
import {log} from 'utils/log';
import appConfig from 'config';

import { store } from 'redux/Store';
import { Provider } from 'react-redux';
import { pair, openedByDapp } from 'utils/walletConnect';
import { WalletConnectProvider } from 'contexts/WalletConnectContext';
import ErrorBoundary from 'components/ErrorBoundary';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.Fragment>
    <ErrorBoundary>
      <Provider store={ store }>
        <WalletConnectProvider>
          <Navigation />
        </WalletConnectProvider>
      </Provider>
    </ErrorBoundary>
  </React.Fragment>
);

const handleUniversalLink = (link) => {
  const url = new URL(link);
  const uriParam = url.searchParams.get("uri");
  if (typeof uriParam === 'string' && uriParam.length > 0) {
    openedByDapp();
    pair(uriParam);
  }
}

// Handle the "Universal Link" for Wallet Connect when the creating this tab
handleUniversalLink(window.location.href);

// Handle the "Universal Link" for Wallet Connect when switching to this tab
chrome.runtime?.onMessage.addListener((message,) => {
  if (message.type === 'walletConnectSessionRequest') {
    handleUniversalLink(message.extensionUrl);
  }
});

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

if(appConfig.environment !== 'production')
  reportWebVitals(log);
