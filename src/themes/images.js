import PLACEHOLDER_ICON from 'assets/images/token_placeholder_icon.png';

// Common Images

import BLACK_BACK_ICON from 'assets/images/black-back-icon.svg';

// Learn Basic Images
import GET_READY_BG from 'assets/images/graphic-image1.jpg';
import UNDERSTAND_BASIC_BG from 'assets/images/graphic-image2.jpg';
import PASS_SETUP_BG from 'assets/images/shield.png';

export const IMAGES = {
  BLACK_BACK_ICON: BLACK_BACK_ICON,
  GET_READY_BG: GET_READY_BG,
  UNDERSTAND_BASIC_BG: UNDERSTAND_BASIC_BG,
  PASS_SETUP_BG:PASS_SETUP_BG,
  TOKEN_PLACEHOLDER_ICON: PLACEHOLDER_ICON,
};

export const handleNetworkTokenIcons = (NetworkDetails) => {
  NetworkDetails.forEach((network) => {
    IMAGES[network.chainId] = {
      NATIVE_ICON: network.icon,
    };
    network.tokens?.forEach((token) => {
      IMAGES[network.chainId][token.address.toLowerCase()] = token.icon;
    });
  });
};