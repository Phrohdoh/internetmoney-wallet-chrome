/**
 * config
 * application Configuration
 */
let config = {
  appName: 'IM Crypto Wallet',
  version: '19.11.2021',
  environment: 'local',
};

const deploymentType = process.env.REACT_APP_STAGE;
  
if (deploymentType === 'production') {
  config = {
    ...config,
    version: Date.now(),
    environment: 'production',
  };
} else if (deploymentType === 'staging') {
  config = {
    ...config,
    version: Date.now(),
    environment: 'staging',
  };
} else if (deploymentType === 'development') {
  config = {
    ...config,
    version: Date.now(),
    environment: 'development',
  };
}

export default config;
  
