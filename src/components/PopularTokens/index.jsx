import PropTypes from "prop-types";
import React from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

import { Strings } from "resources";

import "./popular-tokens.scss";
import PopularTokenListCell from "components/PopularTokenListCell";
import { getNetwork } from "redux/selectors";

/**
 * Popular Tokens Component
 */
export default function PopularTokens(props) {
  const { setIsShowAddAlert, setSelectedToken, popularTokens } = props;

  const params = useLocation();
  const data = params.state;

  const selectedNetwork = useSelector(getNetwork);

  const renderTokensList = (tokenList) => {
    if (tokenList.length === 0) {
      return (
        <div className="text-center">{Strings.ADDED_ALL_POPULAR_TOKENS_MSG}</div>
      );
    }
    return (
      <div className="overflow-auto">
        <div className="list-style">
          {tokenList.map((token, index) => (
            <PopularTokenListCell
              key={index}
              index={index}
              item={token}
              networkObj={selectedNetwork}
              onSelectToken={(token) => {
                setIsShowAddAlert(true);
                setSelectedToken(token);
              }}
            />
          ))}
        </div>
      </div>
    );
  };

  return <React.Fragment>{renderTokensList(popularTokens)}</React.Fragment>;
}

PopularTokens.propTypes = {
  setIsShowAddAlert: PropTypes.func,
  setSelectedToken: PropTypes.func,
  popularTokens: PropTypes.arrayOf(PropTypes.object),
};
