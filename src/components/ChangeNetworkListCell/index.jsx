import React from 'react';
import PropTypes from 'prop-types';

// utils
import { useTokenIcons } from 'utils/hooks';

import './round-listitem.scss';
import rightArrow from 'assets/images/right-arrow.svg';
import checkBox from 'assets/images/check-box.svg';

const ChangeNetworkListCell = ({
  className = '',  
  dataTestid = '',
  ...props
}) => {

  const { item, isShowSwapSupported, swapSupportedList = [] } = props;
  const isSwapSupport = swapSupportedList.includes(item.chainId.toString());
  const getTokenIcon = useTokenIcons();
  return (
    <React.Fragment>
      <div data-testid={ dataTestid } 
        onClick={ () => {
          if (props.isShowSwapSupported) {
            if (isSwapSupport) {
              props.onSelectNetwork(item);
            }
          } else {
            props.onSelectNetwork(item);
          }
        } } 
        className={ `list-item select-network-item noselect ${!isSwapSupport && isShowSwapSupported ? 'disable' : 'enable'} ${className}` }
      >     
        <div className={ 'left' }>
          <img src={ getTokenIcon('', item.chainId) } alt="" />
        </div>
        <div className="body">
          <h6>
            {item.networkName}
          </h6>
        </div>      
        <div className="right">
          {item.rpc === props.selectedNetwork.rpc && (
            <i className="icon">
              <img src={ checkBox } alt="checkBox" />
            </i>
          )}
          <i className="ls-icon">
            <img src={ rightArrow } alt="Arrow" />
          </i>
        </div>       
      </div>
    </React.Fragment>
  );
};

ChangeNetworkListCell.propTypes = {
  item: PropTypes.object.isRequired,
  onSelectNetwork: PropTypes.func.isRequired,
  className: PropTypes.string,
  dataTestid: PropTypes.string.isRequired,
  selectedNetwork: PropTypes.object.isRequired,
  isShowSwapSupported: PropTypes.bool,
  swapSupportedList: PropTypes.array,
};

export default ChangeNetworkListCell;
