import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'resources';

const ErrorBoundaryDisplay = ({ errorMessage }) => {
  const [displayError, setDisplayError] = useState(false);
  const toggle = useCallback(() => {
    setDisplayError(!displayError);
  }, [displayError]);
  return (
    <div className="main-container">
      <div className="container">
        <div className="content-background ds-flex-col full-flex">
          <div className="main-content">
            <div className="error-boundary">
              <p>{Strings.UNEXPECTED_ERROR}</p>
              <input
                type="button"
                className='button'
                onClick={ toggle }
                value={displayError ? Strings.HIDE_ERROR : Strings.SHOW_ERROR}
              />
              <br />
              {
                displayError
                  ? <p className="error-boundary-message">{errorMessage}</p>
                  : null
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

ErrorBoundaryDisplay.propTypes = {
  errorMessage: PropTypes.string,
}

export default ErrorBoundaryDisplay;