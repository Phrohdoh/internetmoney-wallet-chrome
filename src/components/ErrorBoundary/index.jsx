import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ErrorBoundaryDisplay from './ErrorBoundaryDisplay';
import './errorBoundary.scss';
import Logger from 'utils/logger';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { errorMessage: undefined };
  }

  componentDidCatch(error, errorInfo) {
    Logger.fatal('Caught an unexpected error which would cause the wallet to crash.');
    const errorMessage = `${error.message}
${errorInfo.componentStack}`;
    this.setState({
      errorMessage,
    });
  }

  render() {
    return this.state.errorMessage !== undefined
      ? <ErrorBoundaryDisplay errorMessage={this.state.errorMessage} />
      : this.props.children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.element,
};

export default ErrorBoundary;
