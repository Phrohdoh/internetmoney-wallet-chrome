import React from 'react';
import PropTypes from 'prop-types';
import './progressbar.scss';

const ProgressBar = ({
  className = '',
}) => {

  return (
    <React.Fragment>
      <div className={ `progress ${className}` }>
        <div className="progress-done"></div>
      </div>
    </React.Fragment>
  );
};

ProgressBar.propTypes = {
  className: PropTypes.string,  
};

export default ProgressBar;
