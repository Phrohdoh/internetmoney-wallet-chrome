import React from 'react';
import PropTypes from 'prop-types';

import './alert-model.scss';

import Button from 'components/Button';
import { Strings } from 'resources';

const AlertModel = props => {
  return (
    <React.Fragment>
      <div className={ `modal modal-flex center ${props.isShowAlert} ${props.className}` }>
        <div className="bodyclose modal-flex" data-testid="modal"></div>
        <div className="modal-container">
          <div className="modal-content alert-model-content">          
            {props.alertTitle && (
              <h6 className="titleStyle">
                {props.alertTitle}
              </h6>
            )}
            {!props.children ? (
              <p className="descriptionStyle">{props.alertMsg}</p>
            ) : (
              props.children
            )}
            <div className="actionBtnContainerStyle">
              {props.onPressSuccess && (
                <Button
                  testId={ 'alert_success_btn' }
                  label={ props.successBtnTitle }
                  className="btn-full"
                  onClick={ (e) => {
                    e.preventDefault();
                    props.onPressSuccess(e);
                  } }
                />
              )}
              {props.onPressCancel && (
                <Button
                  outline
                  testId={ 'alert_cancel_btn' }
                  label={ props.cancelBtnTitle }
                  className="btn-full"
                  onClick={ (e) => {
                    e.preventDefault();
                    props.onPressCancel(e);
                  } }
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment> 
  );
};

AlertModel.defaultProps = {
  isShowAlert: false,
  alertTitle: undefined,
  alertMsg: undefined,
  successBtnTitle: Strings.OK,
  cancelBtnTitle: Strings.CANCEL,
};

AlertModel.propTypes = {
  isShowAlert: PropTypes.bool,
  alertTitle: PropTypes.string,
  alertMsg: PropTypes.string,
  children: PropTypes.element,
  successBtnTitle: PropTypes.string,
  cancelBtnTitle: PropTypes.string,
  onPressSuccess: PropTypes.func,
  onPressCancel: PropTypes.func,
  className: PropTypes.string,
  closeModal: PropTypes.func,
};

export default AlertModel;
