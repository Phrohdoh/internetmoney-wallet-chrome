import React from 'react';
import PropTypes from 'prop-types';

import './modal.scss';

const Modal = ({
  isOpen=false,
  closeModal,
  className = '',
  ...props
}) => {

  if (!isOpen) {
    return null;
  }

  return (
    <React.Fragment>
      <div className={ `modal modal-flex bottom ${isOpen} ${className}` }>
        <div className="bodyclose modal-flex" data-testid="modal" onClick={ closeModal }></div>
        <div className="modal-container">
          <div className="modal-content">          
            {props.children}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.object,
};

export default Modal;
