import React from 'react';
import PropTypes from 'prop-types';

import './segments.scss';


const SegmentController = (props) => {
  const { segments } = props;


  // render custom component
  const rendrSegmentTab = (segment, index) => {
    return (
      <button
        key={ index + segment.title }
        data-testid={ index + segment.title + '-tab' }
        onClick={ () => {          
          props.onChangeIndex(index);
        } }
        className={ `${props.currentIndex === index}` }
      >
        <span>{segment.title}</span>
      </button>
    );
  };
  
  return (    
    <React.Fragment>
      <div className={ 'segment-style noselect' }>
        {segments.map((item, index) => {
          return rendrSegmentTab(item, index);
        })}
        <div className="segment-highlight"></div>
      </div>
    </React.Fragment>
  );
};

SegmentController.defaultProps = {
  currentIndex: 0,
};

SegmentController.propTypes = {
  segments: PropTypes.array.isRequired,  
  onChangeIndex: PropTypes.func.isRequired,
  currentIndex: PropTypes.number.isRequired,
};

export default SegmentController;
