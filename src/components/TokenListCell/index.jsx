import React from 'react';
import PropTypes from 'prop-types';
import './token-list-cell.scss';

//  Utils
import { useTokenIcons } from 'utils/hooks';

// Images
import remove from 'assets/images/remove.svg';

const TokenListCell = props => {

  const { item, index, networkObj, onClickDelete, onSelectToken, isRemoveEnable } = props;
  const getTokenIcon = useTokenIcons();

  const usdPrice = item.usdPrice?.value;
  const balance = item.balance?.value;
  const usdValue = (usdPrice === undefined || balance === undefined)
    ? undefined
    : balance.multiply(usdPrice);

  const valueDisplay = usdValue === undefined
    ? null
    : `$${usdValue.toString(2)}`;

  const priceDisplay = usdPrice === undefined
    ? null
    : `$${usdPrice.toString(2, false, false, 2)}`;

  const handleTokenClick = () => {
    if (onSelectToken) onSelectToken(item);
  };

  return (
    <React.Fragment>
      <div key={ index } className="list-item">
        <div className={`left ${onSelectToken ? 'pointer' : ''}`} onClick={ handleTokenClick }>
          <img src={ getTokenIcon(item.address, networkObj.chainId) } alt={ item.title } />
        </div>
        <div className={`body ${onSelectToken ? 'pointer' : ''}`} onClick={ handleTokenClick }>
          <h6>{item.symbol}</h6>
          <div className="token-price text-theme-color">
            {priceDisplay}
          </div>
        </div>
        <div className="right pointer text-right pl-sm">
          <span className="flex-row">
            <span className="text-overflow">{balance?.toString(6, true) ?? ''}</span>
            <span className="left-sm-space">{item.symbol}</span>
          </span>
          <div className="price-value text-theme-color">
            {valueDisplay}
          </div>
        </div>
        {isRemoveEnable && !item.isNative && (
          <div className="icon ml-sm">
            <button className="remove-btn" onClick={ () => onClickDelete(item) }><img src={ remove } alt="Remove" /></button>
          </div>
        )}
      </div>
    </React.Fragment>
  );
};

TokenListCell.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  networkObj: PropTypes.object,
  getBalance: PropTypes.func,
  onSelectToken: PropTypes.func,
  onClickDelete: PropTypes.func,
  isRemoveEnable: PropTypes.bool,
};

export default TokenListCell;
