import React from 'react';
import PropTypes from 'prop-types';

import './radio.scss';

const Radio = ({
  testId,
  label,
  value,
  onChange,
  className = '',
  name
}) => {

  return (
    <React.Fragment>
      <label className={ `single-radio ${className}` }>
        <input data-testid={ testId } type="radio" checked={ value } onChange={ onChange } name={ name }/>
        <i className="radio-icon"></i>
        <span>{label}</span>
      </label>
    </React.Fragment>
  );
};

Radio.propTypes = {
  testId: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.bool,
  className: PropTypes.string,
  onChange: PropTypes.func,
  name: PropTypes.string,
};

export default Radio;
