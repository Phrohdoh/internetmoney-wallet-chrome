import React from 'react';
import PropTypes from 'prop-types';
import { Strings } from 'resources';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { displayAddress } from 'utils/displayAddress';

// Images
import sendIcon from 'assets/images/send-icon.svg';
import receiveIcon from 'assets/images/receive-icon.svg';
import swapIcon from 'assets/images/swap-icon.svg';
import claimIcon from 'assets/images/time3d.png';
import dappIcon from 'assets/images/dapp-transections.svg';
import referralEarningsIcon from 'assets/images/referral-earnings.svg';

import './transactions-list-cell.scss';
import IntegerUnits from 'utils/IntegerUnits';
import { useWalletConnect } from 'contexts/WalletConnectContext';

export default function TransactionListCell (props) {

  const { item, index } = props;
  const { transactionDetail } = item;
  const navigate = useNavigate();
  const { sessions } = useWalletConnect();

  let peerMeta;
  if (item.type === 'dapp') {
    const isV2 = typeof transactionDetail.dappTransactionDetail.topic === 'string';
    peerMeta = isV2
      ? sessions.find(s => s.topic === transactionDetail.dappTransactionDetail.topic)?.peer?.metadata
      : transactionDetail.dappTransactionDetail.connection.peerMeta;
    peerMeta = peerMeta ?? {};
  }

  const getTransImage = () => {
    if (item.type === 'send') {
      return sendIcon;
    } else if (item.type === 'swap') {
      return swapIcon;
    } else if (item.type === 'claim') {
      return claimIcon;
    } else if (item.type === 'dapp') {
      return dappIcon;
    } else if (item.type === 'referral_claim') {
      return referralEarningsIcon;
    } else {
      return receiveIcon;
    }
  };

  const getTransType = () => {
    if (item.type === 'send') {
      return Strings.SEND;
    } else if (item.type === 'swap') {
      return Strings.SWAP;
    } else if (item.type === 'claim') {
      return Strings.CLAIM_DIV;
    } else if (item.type === 'dapp') {
      return Strings.DAPP;
    } else if (item.type === 'referral_claim') {
      return Strings.REFERRAL_EARNINGS;
    } else {
      return Strings.RECEIVE;
    }
  };

  return (
    
    <React.Fragment >
      <li
        onClick={ () => {
          navigate('/transactions-details',
            {
              state: {
                item,
                accountIndex: index,
              }
            });
        } }
        key={ `transaction-${index + 1}` } data-testid={ `transaction-${index + 1}` } className="listitem">
        <div className="left icon">
          <img src={ getTransImage() } alt={ item.type } />
        </div>
        <div className="body">
          <h6>
            {item.type === 'dapp' ? (                    
              getTransType() +
                ' ' +
                peerMeta.name                  
            ) : item.type === 'referral_claim' ? (
              getTransType() + ' IM'
            ): item.type !== 'claim' ? (                    
              getTransType() + ' ' + transactionDetail.fromToken.symbol                    
            ) : (                        
              getTransType()
            )}
          </h6>
          
          {transactionDetail.accountName ? (    
            <span className="small-text text-gray-color2">            
              {transactionDetail.accountName}
            </span>           
          ) : (                        
            <span className="small-text text-eclipse-style text-theme-color">
              {displayAddress(transactionDetail.fromAddress)}             
            </span>        
          )}             
          
        </div>
        <div className="right text-right">
          <h6 className="text-theme-color mb-0">
            {transactionDetail.displayAmount.toString(6)}
          </h6>
          {transactionDetail.isSupportUSD && (
            <small className="small-text text-gray-color2 text-right">   
              ${transactionDetail.displayAmount.multiply(transactionDetail.usdValue).toString(2)}
            </small>
          )}
        </div>
      </li>
    </React.Fragment>
  );
}

TransactionListCell.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
};
