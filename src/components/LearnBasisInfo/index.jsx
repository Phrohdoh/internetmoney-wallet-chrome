import React from 'react';
import PropTypes from 'prop-types';

import { IMAGES } from 'themes';
import './learn-basis-Info.scss';

const LearnBasisInfo = props => {
    
  const { item } = props;

  return (
    <React.Fragment>
      {
        item.image && (
          <div className="content-image">
            <img src={ IMAGES[item.image] } alt="" />
          </div>
        )
      }
      <div className="content">     
        <div className="content-area">
          <h2 className="text-theme-color mb-0">{item.title}</h2>
          <p className="opacity">{item.subtitle}</p>
          <p>{item.description}</p>
        </div>
      </div>
    </React.Fragment>
  );
};

LearnBasisInfo.propTypes = {
  item: PropTypes.object,
};

export default LearnBasisInfo;
