import React, { Fragment, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { GlobalAction } from 'redux/slices/globalSlice';
import AlertModel from 'components/AlertModel';

import './global-alert-modal.scss';

const GlobalAlertModal = () => {
  const dispatch = useDispatch();
  const { isShowAlert, alertTitle, alertMsg } = useSelector(state => state.global.alertData);
  const onPressSuccess = useCallback(() => {
    dispatch(GlobalAction.hideAlert());
  }, []);
  if (!isShowAlert) {
    return null;
  }
  return (
    <React.Fragment>
        <AlertModel
          isShowAlert={ isShowAlert }
          className="text-center"
          onPressSuccess={ onPressSuccess }
        >
          <Fragment>
            <h3 className="global-alert-title">
              {alertTitle}
            </h3>
            <p>
              {alertMsg}
            </p>
          </Fragment>
        </AlertModel>
    </React.Fragment> 
  );
};

export default GlobalAlertModal;
