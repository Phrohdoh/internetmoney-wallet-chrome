import React from 'react';
import PropTypes from 'prop-types';

import './range-selection.scss';

import { Strings } from 'resources';

export default function RangeSelection (props) {

  const range = Strings.RANGE_LIST;
  
  const renderRangeTab = (title, index) => {
    return (    
      <li
        key={ index + title }
        data-testid={ `range-selection-${title}-tab-button` }
        className={ props.selectedRange === index ? 'active' : 'inactive' }
        onClick={ () => props.onSelectRange(index) }>
        <span>{title}</span>
      </li>    
    );
  };

  return (
    <React.Fragment>
      <ul className="estimated-gas-range-tabs">
        {range.map((item, index) => {
          return renderRangeTab(item, index);
        })}
      </ul>
    </React.Fragment>
  );
}

RangeSelection.propTypes = {
  title: PropTypes.string,
  selectedRange: PropTypes.number,
  onSelectRange: PropTypes.func,
};
