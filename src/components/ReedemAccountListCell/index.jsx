import React from 'react';
import PropTypes from 'prop-types';

import './reedem-account-list-cell.scss';
import { Strings } from 'resources';

const ReedemAccountListCell = ({
  className = '',
  dataTestid = '',
  ...props
}) => {
  
  const { item, index } = props;

  return (
    <React.Fragment>
      <div data-testid={ dataTestid } onClick={ () => props.onSelectAccount(item, index) } className={ `listitem-control round-listitem noselect ${className}` }>
        <div className="body">
          <h6>
            {`${Strings.ACCOUNT} ${index + 1}`}
          </h6>
        </div>      
        <div className="right text-right">
          {item.name}
        </div>
      </div>
    </React.Fragment>
  );
};

ReedemAccountListCell.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  className: PropTypes.string,  
  onSelectAccount: PropTypes.func.isRequired,
  dataTestid: PropTypes.string.isRequired,
};

export default ReedemAccountListCell;
