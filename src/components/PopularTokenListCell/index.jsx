import React from 'react';
import PropTypes from 'prop-types';
import './popular-token-list-cell.scss';

//  Utils
import { useTokenIcons } from 'utils/hooks';

const PopularTokenListCell = props => {

  const { item, index, networkObj, onSelectToken } = props;
  const getTokenIcon = useTokenIcons();

  const handleTokenClick = () => {
    if (onSelectToken) onSelectToken(item);
  };

  return (
    <React.Fragment>
      <div key={ index } className="list-item">
        <div className={`left ${onSelectToken ? 'pointer' : ''}`} onClick={ handleTokenClick }>
          <img src={ getTokenIcon(item.address, networkObj.chainId) } alt={ item.title } />
        </div>
        <div className={`body ${onSelectToken ? 'pointer' : ''}`} onClick={ handleTokenClick }>
          <h6>{item.symbol}</h6>
        </div>
      </div>
    </React.Fragment>
  );
};

PopularTokenListCell.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  networkObj: PropTypes.object,
  onSelectToken: PropTypes.func,
};

export default PopularTokenListCell;
