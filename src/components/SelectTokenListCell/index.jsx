import React from 'react';
import PropTypes from 'prop-types';

// utils
import { useTokenIcons } from 'utils/hooks';

import './round-listitem.scss';

const SelectTokenListCell = ({
  className = '',
  subtext = '',
  dataTestid = '',
  ...props
}) => {

  const { item, index, networkObj } = props;
  const getTokenIcon = useTokenIcons();
  
  const displayedBalance = item.balance?.value?.toString(6, true) ?? '0';

  return (
    <React.Fragment>
      <div data-testid={ dataTestid } onClick={ () => props.onSelectToken(item, index) } className={ `listitem-control noselect ${className}` }>     
        <div className={ `left ${index}` }>
          <img src={ getTokenIcon(item.address, networkObj.chainId) } alt="" />
        </div>
        <div className="body">
          <h6>
            {item.symbol}
          </h6>
        </div>      
        <div className="right text-right">
          <small>{subtext}</small>
          <div className="large-text text-theme-color">{displayedBalance}</div>
        </div>
      </div>
    </React.Fragment>
  );
};

SelectTokenListCell.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  onSelectToken: PropTypes.func.isRequired,
  className: PropTypes.string,
  subtext: PropTypes.string.isRequired,
  networkObj: PropTypes.object,
  dataTestid: PropTypes.string.isRequired,
};

export default SelectTokenListCell;
