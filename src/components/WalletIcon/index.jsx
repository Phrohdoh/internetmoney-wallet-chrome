import React from 'react';
import { useSpring, animated } from 'react-spring';

function Line1 () {
  const animatedStyle = useSpring({
    to: { stroke: '#FBA81A' },
    from: { stroke: '#3A3A3A' },
    delay: 1000,
  });

  return (
    <g>
      <animated.path
        stroke="#3A3A3A"
        strokeWidth="7.415"
        style={ animatedStyle }
        d="M117.698 78.15c0-9.555 7.747-17.3 17.302-17.3h42.019v34.603H135c-9.555 0-17.302-7.746-17.302-17.302v0z"
      >
      </animated.path>
    </g>
  );
}

function Line2 () {
  const animatedStyle = useSpring({
    to: { stroke: '#FBA81A' },
    from: { stroke: '#3A3A3A' },
    delay: 3000,
  });
  return (
    <g>
      <animated.path
        stroke="#3A3A3A"
        strokeWidth="7.415"
        style={ animatedStyle }
        d="M4 16.358C4 9.534 9.533 4 16.358 4H164.66c6.826 0 12.359 5.533 12.359 12.358v106.284c0 6.825-5.533 12.358-12.359 12.358H16.358C9.533 135 4 129.467 4 122.642V16.358z"
      >
      </animated.path>
    </g>
  );
}
function Line3 () {
  const animatedStyle = useSpring({
    to: { stroke: '#FBA81A' },
    from: { stroke: '#3A3A3A' },
    delay: 2000,
  });
  return (
    <g>
      <animated.path
        stroke="#3A3A3A"
        strokeWidth="7.415"
        style={ animatedStyle }
        d="M4 31.189C4 24.363 9.533 18.83 16.358 18.83H164.66c6.826 0 12.359 5.533 12.359 12.359v91.453c0 6.825-5.533 12.358-12.359 12.358H16.358C9.533 135 4 129.467 4 122.642V31.189z"
      >
      </animated.path>
    </g>
  );
}
function WalletIcon () {
  const WalletLine = [
    <Line1 key="Line1"/>,
    <Line2 key="Line2"/>,
    <Line3 key="Line3"/>,
  ];
  return (
    <React.Fragment>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="181"
        height="139"
        fill="none"
        viewBox="0 0 181 139"
      >

        {WalletLine}

      </svg>
    </React.Fragment>
  );
}

export default WalletIcon;
