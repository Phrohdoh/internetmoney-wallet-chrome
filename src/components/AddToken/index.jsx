import PropTypes from 'prop-types';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import ImportToken from 'components/ImportToken';
import PopularTokens from 'components/PopularTokens';
import { getTokensByAddress } from 'redux/selectors';

import { Strings } from 'resources';

import './add-token.scss';

// Tab name enums
export const Tabs = {
  POPULAR_TOKENS: 'popular-tokens-tab',
  IMPORT_TOKEN: 'import-token-tab',
};

/**
 * Add Token Component
 */
export default function AddToken(props) {
  const { onImportTokenSuccess, setIsShowAddAlert, setIsShowAddToken, setSelectedToken, popularTokens } = props;

  const params = useLocation();
  const account = params.state.item;

  const [activeTab, setActiveTab] = useState(Tabs.POPULAR_TOKENS);

  const tokens = useSelector(getTokensByAddress)[account.publicAddress.toLowerCase()];

  return (
    <div className="add-token-modal">
      <div className="modal-header">
        <span className="h6">{Strings.ADD_TOKEN_UPPERCASE}</span>
        <span data-testid="close-add-token-modal" className="close" onClick={() => setIsShowAddToken(false)}>
          <span className="close-icon">&times;</span>
        </span>
      </div>
      <div className="modal-body">
        <div className="tabs">
          <div className="tab-nav">
            <div
              onClick={() => setActiveTab(Tabs.POPULAR_TOKENS)}
              data-testid={Tabs.POPULAR_TOKENS}
              className={`tabStyle tab-first ${activeTab === Tabs.POPULAR_TOKENS ? 'active' : ''}`}
            >
              <span className="noselect">{Strings.POPULAR_TOKENS_TAB_UPPERCASE}</span>
            </div>
            <div
              onClick={() => setActiveTab(Tabs.IMPORT_TOKEN)}
              data-testid={Tabs.IMPORT_TOKEN}
              className={`tabStyle tab-last ${activeTab === Tabs.IMPORT_TOKEN ? 'active' : ''}`}
            >
              <span className="noselect">{Strings.IMPORT_TOKEN_TAB_UPPERCASE}</span>
            </div>
            {activeTab && <div className="tab-highlight"></div>}
          </div>
          <div className="tab-content">
            {activeTab === Tabs.POPULAR_TOKENS && (
              <PopularTokens
                setIsShowAddAlert={setIsShowAddAlert}
                setSelectedToken={setSelectedToken}
                popularTokens={popularTokens}
              />
            )}
            {activeTab === Tabs.IMPORT_TOKEN && (
              <ImportToken account={account} tokens={tokens} onImportTokenSuccess={onImportTokenSuccess} />
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

AddToken.propTypes = {
  onImportTokenSuccess: PropTypes.func,
  setIsShowAddAlert: PropTypes.func,
  setIsShowAddToken: PropTypes.func,
  setSelectedToken: PropTypes.func,
  popularTokens: PropTypes.arrayOf(PropTypes.object),
};
