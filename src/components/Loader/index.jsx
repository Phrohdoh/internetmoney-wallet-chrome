import React from 'react';
import PropTypes from 'prop-types';
import './loader.scss';

const Loader = ({
  className = '',
}) => {

  return (
    <React.Fragment>
      <div className={ `dot-loader-container ${className}` }>
        <div className="dot-loader"></div>
        <div className="dot-loader dot-loader--2"></div>
        <div className="dot-loader dot-loader--3"></div>
      </div>
    </React.Fragment>
  );
};

Loader.propTypes = {
  className: PropTypes.string,
};

export default Loader;
