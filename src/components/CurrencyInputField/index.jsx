import React from 'react';
import PropTypes from 'prop-types';
import CurrencyInput from 'react-currency-input-field';

const CurrencyInputField = ({
  type='text',
  label,
  // eslint-disable-next-line
  onChange = () => {},
  value,
  error,  
  className = '',
  icon,
  onIconClick,
  name,
  rightButtonLabel,
  rightButtonClick,
  dataTestid = '',
  isUSD,
  decimalsLimit = 6,
  ...props
}) => {

  if (rightButtonClick || rightButtonLabel) className += ' inline';
  return (
    <React.Fragment>
      <div className={ `form-group ${className}` }>
        {label ? <label>{label}</label> : null }
        <div className="form-icon"> 
          <CurrencyInput
            allowNegativeValue={ false }
            decimalsLimit={ decimalsLimit }
            prefix={ isUSD ? '$': '' }
            decimalSeparator="."
            groupSeparator=","
            data-testid={ dataTestid } type={ type } value={ value } onValueChange={ onChange } name={ name } className="form-control" { ...props } />
          {icon ?
            <i className="input-icon" data-testid={ `input-icon-${name}` } onClick={ onIconClick }>
              {icon}
            </i> : 
            null }
          {rightButtonClick || rightButtonLabel ? 
            <button className="add-btn" onClick={ rightButtonClick }>{rightButtonLabel}</button> : null
          }
        </div>
        {error && <span className="error">{error}</span> }      
      </div>

    </React.Fragment>
  );
};

CurrencyInputField.propTypes = {
  type: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string.isRequired,
  error: PropTypes.string,
  className: PropTypes.string,
  icon: PropTypes.object, 
  onIconClick: PropTypes.func,
  name: PropTypes.string,
  rightButtonLabel: PropTypes.string,
  rightButtonClick: PropTypes.func,
  dataTestid: PropTypes.string,
  isError: PropTypes.bool,
  errorText: PropTypes.string,
  isPassword: PropTypes.bool,
  isUSD: PropTypes.bool,
  decimalsLimit: PropTypes.number,
};

export default CurrencyInputField;
