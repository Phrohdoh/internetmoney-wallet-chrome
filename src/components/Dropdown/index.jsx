import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './dropdown.scss';
import downArrow from 'assets/images/dropdown-icon.svg';

const Dropdown = ({  
  label,
  onClick,
  value,
  error,
  className = '',
  subtext,
  subvalue = '',
  icon,
  dataTestid = '',
  smallValue = false,
  themeColorSubtext = false,
}) => {
  return (
    <React.Fragment>
      <div data-testid={ dataTestid } onClick={ onClick } className={ `form-group ${className}` }>
        {label ? <label>{label}</label> : null }
        <div className="dropdown-control noselect">
          {icon ?
            <div className='dropdown-img'><img src={ icon } alt="" /></div>: null
          }
          <div className="dropdown-value">
            <span className={smallValue ? 'small-value' : ''}>{value}</span>
            {subtext ? <small className={themeColorSubtext ? 'theme-color' : ''}>{subtext} <em>{subvalue}</em></small> : null }
          </div>
          <span className="dropdown-icon">
            <img src={ downArrow } alt="Arrow" />
          </span>
        </div>
        {error && <span className="error">{error}</span>}
      </div>
    </React.Fragment>
  );
};

Dropdown.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func,
  value: PropTypes.string.isRequired,
  error: PropTypes.string,
  className: PropTypes.string,
  subtext: PropTypes.string,
  subvalue: PropTypes.any,
  icon: PropTypes.string,
  dataTestid: PropTypes.string,
  smallValue: PropTypes.bool,
  themeColorSubtext: PropTypes.bool,
};

export default Dropdown;
