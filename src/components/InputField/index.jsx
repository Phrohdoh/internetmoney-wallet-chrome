import React from 'react';
import PropTypes from 'prop-types';

import './input-field.scss';

const InputField = ({
  type='text',
  label,
  onChange = () => {},
  onPaste = () => {},
  value,
  error,  
  className = '',
  icon,
  onIconClick,
  name,
  rightButtonLabel,
  rightButtonClick,
  dataTestid = '',
  ...props
}) => {

  if (rightButtonClick || rightButtonLabel) className += ' inline';
  return (
    <React.Fragment>
      <div className={ `form-group ${className}` }>
        {label ? <label>{label}</label> : null }
        <div className="form-icon"> 
          <input
            data-testid={dataTestid}
            type={type}
            value={value}
            onChange={onChange}
            onPaste={onPaste}
            name={name}
            className="form-control"
            {...props}
          />
          {icon ?
            <i className="input-icon" data-testid={ `input-icon-${name}` } onClick={ onIconClick }>
              {icon}
            </i> : 
            null }
          {rightButtonClick || rightButtonLabel ? 
            <button className="add-btn" onClick={ rightButtonClick }>{rightButtonLabel}</button> : null
          }
        </div>
        {error && <span className="error">{error}</span> }      
      </div>

    </React.Fragment>
  );
};

InputField.propTypes = {
  type: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
  onPaste: PropTypes.func,
  value: PropTypes.string.isRequired,
  error: PropTypes.string,
  className: PropTypes.string,
  icon: PropTypes.object, 
  onIconClick: PropTypes.func,
  name: PropTypes.string,
  rightButtonLabel: PropTypes.string,
  rightButtonClick: PropTypes.func,
  dataTestid: PropTypes.string,
  isError: PropTypes.bool,
  errorText: PropTypes.string,
  isPassword: PropTypes.bool,
};

export default InputField;
