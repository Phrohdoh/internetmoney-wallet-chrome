import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';

import './navbar.scss';
import { Strings } from 'resources';

// utils
import { useTokenIcons } from 'utils/hooks';

import walletIcon from 'assets/images/wallet-nav.svg';
import timeIcon from 'assets/images/time.png';
import swapIcon from 'assets/images/swap-nav-icon.svg';
import settingsIcon from 'assets/images/settings-nav.svg';
import expandIcon from 'assets/images/expand.svg';
import { getNetwork } from 'redux/selectors';

const Navbar = () => {
  const getTokenIcon = useTokenIcons();

  const networks = useSelector(state => state.networks);
  const selectedNetwork = useSelector(getNetwork);
  const [isPopupView, setIsPopupView] = useState();

  const swapSupported = networks?.wdSupportObject?.success && networks?.wdSupportObject?.swapRouterV3;

  useEffect(() => {
    if (chrome.extension) {
      const popupViews = chrome.extension.getViews({ type: 'popup' });
      const currentView = popupViews.find((view) => view === window);
      setIsPopupView(currentView !== undefined);
    }
  }, []);

  const renderNavItem = (label, icon, link) => {
    return (
      <li className={ `nav-item ${label}` }>
        <NavLink
          to={ { pathname: link } }
          className={isActive => (
            "selected" + ((!isActive.isActive || link === undefined) ? " unselected" : "")
          )}
        >
          <div className="nav-icon">
            {icon &&(
              <img src={ icon } alt={ label } className="nav-ic"/>
            )}
          </div>
          <span>{label}</span>
        </NavLink>
      </li>
    );
  };
  
  const renderNavLink = (label, icon, link) => {
    return (
      <li className={ `nav-item ${label}` }>
        <a
          href={link}
          target="_blank"
          rel="noreferrer"
        >
          <div className="nav-icon">
            {icon &&(
              <img src={ icon } alt={ label } className="nav-ic"/>
            )}
          </div>
          <span>{label}</span>
        </a>
      </li>
    );
  };

  return (
    <React.Fragment>
      <div className="navbar">
        <div className="nav-list">        
          <nav className="nav">
            <ul>
              {renderNavItem(Strings.NETWORK_DASHBOARD_TAB, getTokenIcon('', selectedNetwork.chainId), '/networks')}
              {renderNavItem(Strings.CLAIM_DASHBOARD_TAB, timeIcon, '/wd-flow')}
              {renderNavItem(Strings.WALLET_DASHBOARD_TAB, walletIcon, '/wallet')}
              {renderNavItem(Strings.SWAP_DASHBOARD_TAB, swapIcon, swapSupported ? '/swap-token' : undefined)}
              {renderNavItem(Strings.SETTING_DASHBOARD_TAB, settingsIcon, '/settings')}
              {isPopupView && renderNavLink(Strings.EXPAND_DASHBOARD_TAB, expandIcon, 'index.html')}
            </ul>
          </nav>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Navbar;
