import React from 'react';
import PropTypes from 'prop-types';

import './passcode.scss';

import backspace from 'assets/images/backspace.svg';


const Passcode = props => {

  const numberList = [
    { title: '1', value: '1' },
    { title: '2', value: '2' },
    { title: '3', value: '3' },
    { title: '4', value: '4' },
    { title: '5', value: '5' },
    { title: '6', value: '6' },
    { title: '7', value: '7' },
    { title: '8', value: '8' },
    { title: '9', value: '9' },
    { title: '', value: '' },
    { title: '0', value: '0' },
    { title: 'remove', value: 'remove', image: backspace },
  ];

  const passcodeCount = 6;

  const renderKey = (item, index) => {
    return item.title ? (
      <div key={ index } className="key-style">
        {item.image ? (
          <button className='key-button' data-testid={ `passcode-${item.title}` } onClick={ () => props.onClickKey(item) }>
            <img src={ backspace } alt=""/>
          </button>
        ) : (
          <button className='key-button' data-testid={ `passcode-${item.title}` } onClick={ () => props.onClickKey(item) }>{item.title}</button>
        )}
      </div>
    ) : (
      <div key={ index }  className="empty-style"></div>
    );
  };

  const renderKeypad = () => {
    return (
      <div className="keypad-style">
        {numberList.map((item, index) => renderKey(item, index))}
      </div>
    );
  };
  
  const renderPasscodeDot = index => {
    return <div key={ index } className={ `dot-style ${props.passcode.length > index ? 'active-dot-style' : null}` }></div>;
  };

  return (
    <React.Fragment>
      <label className="passcode-label">{props.title}</label>
      <div className="dot-container">
        {[...Array(passcodeCount)].map((item, index) =>
          renderPasscodeDot(index),
        )}
      </div>
      {renderKeypad()}
    </React.Fragment>
  );
};

Passcode.defaultProps = {
  passcode: '',
};

Passcode.propTypes = {
  title: PropTypes.string,
  passcode: PropTypes.string,
  onClickKey: PropTypes.func,
};

export default Passcode;
