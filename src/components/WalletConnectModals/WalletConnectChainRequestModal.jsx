import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { rejectConnection, rejectTransaction } from 'utils/walletConnect';
import { Strings } from 'resources';
import Button from 'components/Button';
import { NetworksAction } from 'redux/slices/networksSlice';
import { WalletAction } from 'redux/slices/walletSlice';
import { setItem } from 'utils/storage';
import { STORAGE_KEYS } from '../../constants';

import './wallet-connect-modal.scss';
import { useWalletConnect } from 'contexts/WalletConnectContext';
import useDebouncedBoolean from 'utils/useDebouncedBoolean';
import classNames from 'classnames';
import { getNetwork } from 'redux/selectors';

const getMeta = (request, sessions) => {
  if (request?.cacaoPayload) {
    return request.requester.metadata;
  }
  const isSwitchEthereumChain = request.method === 'wallet_switchEthereumChain';
  if (isSwitchEthereumChain) {
    return request.connection.peerMeta;
  }
  const isTransaction = typeof request?.params?.request?.method === 'string';
  if (isTransaction) {
    return sessions.find(s => s.topic === request.topic)?.peer.metadata;
  }
  return request.proposer.metadata;
};

const getChainsFromRequest = (request) => {
  try {
    let chains = [];
    if (request.cacaoPayload !== undefined) {
      chains = [request.cacaoPayload.chainId]
    } else if (request.requiredNamespaces !== undefined) {
      if (request.requiredNamespaces.eip155?.chains !== undefined) {
        chains = [...request.requiredNamespaces.eip155.chains];
      }
      if (request.optionalNamespaces?.eip155?.chains !== undefined) {
        chains = [
          ...chains,
          ...request.optionalNamespaces.eip155.chains,
        ];
      }
    } else if (request.params?.request?.params?.[0]?.chainId !== undefined) {
      chains = [request.params?.request?.params?.[0]?.chainId];
    } else if (request.params?.chainId !== undefined) {
      chains = [request.params.chainId];
    } else if (request.params[0].chainId !== undefined) {
      chains = [request.params[0].chainId];
    }
    const chainIds = chains
      .filter(chain => chain !== null)
      .map(chain => (
        (typeof chain === 'string' && chain.startsWith('0x'))
          ? parseInt(chain, 16)
          : chain
      ))
      .map(chain => (
        (typeof chain === 'string' && chain.startsWith('eip155:'))
          ? chain.split('eip155:')[1]
          : chain
      ))
      .map(chainId => parseInt(chainId, 10));
    return chainIds;
  } catch (e) {
    return [null];
  }
};

const WalletConnectChainRequestModal = () => {
  const dispatch = useDispatch();
  const wallet = useSelector(state => state.wallet);
  const loggedIn = useSelector(state => state.wallet.password !== '');
  const networks = useSelector(state => state.networks.networks);
  const selectedNetworkObj = useSelector(getNetwork);
  const selectedChainId = selectedNetworkObj.chainId;
  const { sessions, sessionProposals, sessionRequests, authRequests } = useWalletConnect();
  const request = sessionProposals[0] ?? authRequests[0] ?? sessionRequests[0];
  const requestedChainIds = getChainsFromRequest(request);
  const chainSelected = requestedChainIds.length === 0 || requestedChainIds.includes(selectedChainId);
  const supportedChainIds = networks
    .filter(network => !network.disabled)
    .map(network => network.chainId);

  const canBeVisible = loggedIn && (request !== undefined) && !chainSelected;

  // Wait 750ms before displaying, to ensure that the request to be displayed hasn't
  // already been accepted and just hasn't been removed from the list quite yet.
  // This fixes the issue where we accept a request, and then the request modal pops up again
  // very briefly on the page until the request gets removed from Wallet Connect's state.
  const isVisible = useDebouncedBoolean(canBeVisible, 750);

  const id = request?.id;

  const onAccept = useCallback(async() => {
    const requestedNetwork = networks.find(network => network.chainId === requestedChainIds[0]);
    await setItem(
      STORAGE_KEYS.SELECTED_NETWORK,
      JSON.stringify(requestedNetwork),
    );
    dispatch(NetworksAction.saveSelectedNetworkObject(requestedNetwork));
    dispatch(WalletAction.resetData(wallet.walletDetail, requestedNetwork));
  }, [id]);

  const onReject = useCallback(async() => {
    if (typeof request?.params?.request?.method === 'string') {
      await rejectTransaction(sessionRequests[0]);
    } else {
      rejectConnection(id);
    }
  }, [id]);

  if (!isVisible) {
    return null;
  }

  let errorMessage = undefined;

  if (requestedChainIds === null) {
    errorMessage = Strings.WC_CHAIN_REQUEST_UNABLE_TO_PARSE;
  } else if (!supportedChainIds.includes(requestedChainIds[0])) {
    errorMessage = Strings.WC_CHAIN_REQUEST_UNSUPPORTED_CHAIN;
  }

  const meta = getMeta(request, sessions);
  const dappName = meta?.name;
  const dappUrl = meta?.url;
  const dappIcon = meta?.icons?.[0];

  const networkName = errorMessage === undefined
    ? networks.find(network => network.chainId === requestedChainIds[0]).networkName
    : undefined;
  const networkIcon = errorMessage === undefined
    ? networks.find(network => network.chainId === requestedChainIds[0]).icon
    : undefined;

  const verificationMessage = {
    'UNKNOWN': 'Unknown domain.',
    'VALID': 'Successfully verified domain.',
    'INVALID': 'Domain verification failed!',
  }[request.verification];

  const verificationColor = {
    'UNKNOWN': 'gray',
    'VALID': 'green',
    'INVALID': 'red',
  }[request.verification];

  const buttons = errorMessage === undefined
    ? (
      <div className="buttons">
        <Button
          onClick={onAccept}
          label={ Strings.WC_CHAIN_REQUEST_ACCEPT }
          className="btn-full"
        />
        <Button
          onClick={onReject}
          outline
          label={ Strings.WC_CHAIN_REQUEST_REJECT }
          className="btn-full"
        />
      </div>
    )
    : (
      <div className="buttons">
        <Button
          onClick={onReject}
          label={ Strings.WC_CHAIN_REQUEST_DISMISS }
          className="btn-full"
        />
      </div>
    );

    const chainRequest = errorMessage === undefined
    ? (
      <div className="chain">
        <img src={networkIcon} className="icon" />
        <div className="info">
          <div className="name">
            {networkName}
          </div>
          <div className="chainId">
            ({Strings.WC_CHAIN_REQUEST_CHAIN_ID}: {requestedChainIds[0]})
          </div>
        </div>
      </div>
    )
    : (
      <div className="error">
        {errorMessage}
      </div>
    );

  return (
    <div className="wallet-connect-modal-overlay">
      <div className="wallet-connect-modal">
        <div className="title">
          {Strings.WC_CHAIN_REQUEST_TITLE}
        </div>
        <div className="dapp">
          <img src={dappIcon} className="icon" />
          <div className="info">
            <div className="name">
              {dappName}
            </div>
            <div className="url">
              {dappUrl}
            </div>
            <div className={classNames('verification', verificationColor)}>
              {verificationMessage}
            </div>
          </div>
        </div>
        {chainRequest}
        {buttons}
      </div>
    </div>
  );
};

export default WalletConnectChainRequestModal;