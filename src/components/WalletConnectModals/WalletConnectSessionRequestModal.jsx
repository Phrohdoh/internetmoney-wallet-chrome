import React, { useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { acceptConnection, approveAuthRequest, getAuthRequest, getWeb3Wallet, rejectAuthRequest, rejectConnection } from 'utils/walletConnect';
import { Strings } from 'resources';
import Button from 'components/Button';
import Spinner from 'components/Spinner';

import './wallet-connect-modal.scss';
import Dropdown from 'components/Dropdown';
import Modal from 'components/Modal';
import AccountListitem from 'components/AccountListitem';
import { STORAGE_KEYS } from 'constants';
import { setItem } from 'utils/storage';
import { WalletConnectAction } from 'redux/slices/walletConnectSlice';
import {
  getAccountList,
  getNetwork,
  getSelectedWalletConnectAccount,
} from 'redux/selectors';
import { useWalletConnect } from 'contexts/WalletConnectContext';
import useDebouncedBoolean from 'utils/useDebouncedBoolean';
import { ethSignAuthRequest } from 'web3-layer';
import classNames from 'classnames';

const getChainsFromRequest = (request) => {
  try {
    let chains = [];
    if (request.cacaoPayload !== undefined) {
      chains = [request.cacaoPayload.chainId]
    } else if (request.requiredNamespaces !== undefined) {
      if (request.requiredNamespaces.eip155?.chains !== undefined) {
        chains = [...request.requiredNamespaces.eip155.chains];
      }
      if (request.optionalNamespaces?.eip155?.chains !== undefined) {
        chains = [
          ...chains,
          ...request.optionalNamespaces.eip155.chains,
        ];
      }
    } else if (request.params?.request?.params?.[0]?.chainId !== undefined) {
      chains = [request.params?.request?.params?.[0]?.chainId];
    } else if (request.params?.chainId !== undefined) {
      chains = [request.params.chainId];
    } else if (request.params[0].chainId !== undefined) {
      chains = [request.params[0].chainId];
    }
    const chainIds = chains
      .filter(chain => chain !== null)
      .map(chain => (
        (typeof chain === 'string' && chain.startsWith('0x'))
          ? parseInt(chain, 16)
          : chain
      ))
      .map(chain => (
        (typeof chain === 'string' && chain.startsWith('eip155:'))
          ? chain.split('eip155:')[1]
          : chain
      ))
      .map(chainId => parseInt(chainId, 10));
    return chainIds;
  } catch (e) {
    return [null];
  }
};

const STATUSES = {
  DEFAULT: 'DEFAULT',
  CONNECTING: 'CONNECTING',
  REJECTING: 'REJECTING',
  ERROR: 'ERROR',
};

const WalletConnectSessionRequestModal = () => {
  const dispatch = useDispatch();
  const loggedIn = useSelector(state => state.wallet.password !== '');
  const wallet = useSelector((state) => state.wallet);
  const selectedNetworkObj = useSelector(getNetwork);
  const selectedChainId = selectedNetworkObj.chainId;
  const { sessionProposals, authRequests } = useWalletConnect();
  const request = sessionProposals[0] ?? authRequests[0];
  const isAuthRequest = request?.cacaoPayload !== undefined;
  const requestedChainIds = getChainsFromRequest(request);
  const chainSelected = requestedChainIds.length === 0 || requestedChainIds.includes(selectedChainId);
  const canBeVisible = loggedIn && (request !== undefined) && chainSelected;
  const [status, setStatus] = useState(STATUSES.DEFAULT);
  const submitting = [STATUSES.CONNECTING, STATUSES.REJECTING].includes(status);
  const errorMessage = status === STATUSES.ERROR
    ? Strings.WC_SESSION_REQUEST_ERROR
    : undefined;

  // Wait 750ms before displaying, to ensure that the request to be displayed hasn't
  // already been accepted and just hasn't been removed from the list quite yet.
  // This fixes the issue where we accept a request, and then the request modal pops up again
  // very briefly on the page until the request gets removed from Wallet Connect's state.
  const isVisible = useDebouncedBoolean(canBeVisible, 750);

  const id = request?.id;
  const accountList = useSelector(getAccountList);
  const selectedAccount = useSelector(getSelectedWalletConnectAccount);
  const [showAccountList, setShowAccountList] = useState(false);

  const iss = `did:pkh:eip155:${selectedChainId}:${selectedAccount?.publicAddress}`;

  const onSelectAccount = useCallback(async (account) => {
    setShowAccountList(false);
    await setItem(
      STORAGE_KEYS.WALLET_CONNECTED_ACCOUNT,
      JSON.stringify(account.publicAddress),
    );
    dispatch(WalletConnectAction.updateSelectedAccount(account.publicAddress));
    setStatus(STATUSES.DEFAULT);
  }, []);

  const onAcceptAuthRequest = useCallback(async () => {
    const web3Wallet = await getWeb3Wallet();
    const authRequest = await getAuthRequest(id);
    const message = web3Wallet.formatMessage(authRequest.cacaoPayload, iss);
    const payload = {
      address: selectedAccount.publicAddress,
      message,
    };
    const resultObj = await ethSignAuthRequest(
      payload,
      wallet.walletDetail.walletObject,
      wallet.password,
      selectedNetworkObj,
    );

    const signature = resultObj.signResult;

    approveAuthRequest(id, iss, signature);
  }, [id, iss, selectedAccount, wallet, selectedNetworkObj]);

  const onAccept = useCallback(async() => {
    setStatus(STATUSES.CONNECTING);
    try {
      if (isAuthRequest) {
        await onAcceptAuthRequest();
      } else {
        await acceptConnection(id);
      }
      setStatus(STATUSES.DEFAULT);
    } catch (e) {
      setStatus(STATUSES.ERROR);
    }
  }, [id, isAuthRequest, onAcceptAuthRequest]);

  const onReject = useCallback(async() => {
    setStatus(STATUSES.REJECTING);
    try {
      if (isAuthRequest) {
        await rejectAuthRequest(id, iss);
      } else {
        await rejectConnection(id);
      }
      setStatus(STATUSES.DEFAULT);
    } catch (e) {
      setStatus(STATUSES.ERROR);
    }
  }, [id, iss]);

  if (!isVisible) {
    return null;
  }

  const meta = isAuthRequest
    ? request.requester.metadata
    : request.proposer.metadata;
  const dappName = meta.name;
  const dappUrl = meta.url;
  const dappIcon = meta.icons?.[0];

  const verificationMessage = {
    'UNKNOWN': 'Unknown domain.',
    'VALID': 'Successfully verified domain.',
    'INVALID': 'Domain verification failed!',
  }[request.verification];

  const verificationColor = {
    'UNKNOWN': 'gray',
    'VALID': 'green',
    'INVALID': 'red',
  }[request.verification];

  return (
    <div className="wallet-connect-modal-overlay">
      <div className="wallet-connect-modal">
        <div className="title">
          {Strings.WC_SESSION_REQUEST_TITLE}
        </div>
        <div className="dapp">
          <img src={dappIcon} className="icon" />
          <div className="info">
            <div className="name">
              {dappName}
            </div>
            <div className="url">
              {dappUrl}
            </div>
            <div className={classNames('verification', verificationColor)}>
              {verificationMessage}
            </div>
          </div>
        </div>
        <div className="modal-body hide-right-details">
          <Dropdown
            dataTestid="gift-reedem-choose-account-button"
            label={ Strings.WC_SESSION_REQUEST_SELECT_ACCOUNT }
            value={ selectedAccount?.name }
            onClick={ (e) => {
              e.preventDefault();
              setShowAccountList(true);
            } }
            error={ errorMessage }
          />
          {
            showAccountList && (
              <Modal
                isOpen={ showAccountList }
                dataTestid="accounts-modal"
                closeModal={ () => setShowAccountList(false) }
              >
                <div className="modal-body">
                  <div className="modal-header">
                    <h6 className="text-center">{Strings.WC_SESSION_REQUEST_SELECT_ACCOUNT}</h6>
                  </div>
                  <div className="modal-body hide-right-details">
                    {
                      accountList.map((item, index) => (
                        <AccountListitem
                          dataTestid={ `select-wc-account-${index}` }
                          key={ index }
                          item={ item }
                          index={ index }
                          onSelectAccount={onSelectAccount}
                        />
                      ))
                    }
                  </div>
                </div>
              </Modal>
            )
          }
        </div>
        <div className="buttons">
          {!submitting && (
            <Button
              onClick={selectedAccount && onAccept}
              label={ Strings.WC_SESSION_REQUEST_ACCEPT }
              className="btn-full"
            />
          )}
          {!submitting && (
            <Button
              onClick={onReject}
              outline
              label={ Strings.WC_SESSION_REQUEST_REJECT }
              className="btn-full"
            />
          )}
          {submitting && <Spinner />}
        </div>
      </div>
    </div>
  );
};

export default WalletConnectSessionRequestModal;