import React, { useCallback } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { rejectTransaction } from 'utils/walletConnect';
import { Strings } from 'resources';
import Button from 'components/Button';

import './wallet-connect-modal.scss';
import { useWalletConnect } from 'contexts/WalletConnectContext';
import useDebouncedBoolean from 'utils/useDebouncedBoolean';
import classNames from 'classnames';

const getMeta = (transaction, sessions) => {
  return sessions.find(s => s.topic === transaction.topic)?.peer.metadata;
};

const WalletConnectTransactionRequestModal = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const viewingRequest = [
    '/signature-request',
    '/dapp-transactions',
  ].includes(location.pathname);
  const loggedIn = useSelector(state => state.wallet.password !== '');
  const { sessions, sessionRequests } = useWalletConnect();
  const transaction = sessionRequests[0];
  const isChainSwitchRequest = transaction?.params?.request?.method === 'wallet_switchEthereumChain';

  const canBeVisible = loggedIn && (transaction !== undefined) && !viewingRequest && !isChainSwitchRequest;

  // Wait 750ms before displaying, to ensure that the request to be displayed hasn't
  // already been accepted and just hasn't been removed from the list quite yet.
  // This fixes the issue where we accept a request, and then the request modal pops up again
  // very briefly on the page until the request gets removed from Wallet Connect's state.
  const isVisible = useDebouncedBoolean(canBeVisible, 750);

  const viewTransactionScreen = useCallback((transaction) => {
    const isSendTransaction = transaction.params.request.method === 'eth_sendTransaction';
    if (isSendTransaction) {
      navigate('/dapp-transactions', {
        state: {
          transaction: transaction,
        }
      });
    } else {
      navigate('/signature-request', {
        state:{
          transaction: transaction,
        }
      });
    }
  }, [navigate]);

  const onView = useCallback((e) => {
    e.preventDefault();
    viewTransactionScreen(transaction);
  }, [transaction, viewTransactionScreen]);

  const onReject = useCallback(async(e) => {
    e.preventDefault();
    await rejectTransaction(transaction);
  }, [transaction]);

  if (!isVisible) {
    return null;
  }

  const meta = getMeta(transaction, sessions);
  const dappName = meta?.name;
  const dappUrl = meta?.url;
  const dappIcon = meta?.icons?.[0];

  const verificationMessage = {
    'UNKNOWN': 'Unknown domain.',
    'VALID': 'Successfully verified domain.',
    'INVALID': 'Domain verification failed!',
  }[transaction.verification];

  const verificationColor = {
    'UNKNOWN': 'gray',
    'VALID': 'green',
    'INVALID': 'red',
  }[transaction.verification];

  return (
    <div className="wallet-connect-modal-overlay">
      <div className="wallet-connect-modal">
        <div className="title">
          {Strings.WC_TRANSACTION_REQUEST_TITLE}
        </div>
        <div className="dapp">
          <img src={dappIcon} className="icon" />
          <div className="info">
            <div className="name">
              {dappName}
            </div>
            <div className="url">
              {dappUrl}
            </div>
            <div className={classNames('verification', verificationColor)}>
              {verificationMessage}
            </div>
          </div>
        </div>
        <div className="buttons">
          <Button
            onClick={onView}
            label={ Strings.WC_TRANSACTION_REQUEST_VIEW }
            className="btn-full"
          />
          <Button
            onClick={onReject}
            outline
            label={ Strings.WC_TRANSACTION_REQUEST_REJECT }
            className="btn-full"
          />
        </div>
      </div>
    </div>
  );
};

export default WalletConnectTransactionRequestModal;