import { Fragment } from 'react';
import WalletConnectSessionRequestModal from './WalletConnectSessionRequestModal';
import WalletConnectTransactionRequestModal from './WalletConnectTransactionRequestModal';
import WalletConnectChainRequestModal from './WalletConnectChainRequestModal';

const WalletConnectModals = () => {
  return (
    <Fragment>
      <WalletConnectSessionRequestModal />
      <WalletConnectTransactionRequestModal />
      <WalletConnectChainRequestModal />
    </Fragment>
  ); 
}

export default WalletConnectModals;