import React, { useState } from 'react';
import { Strings } from 'resources';
import PropTypes from 'prop-types';
import './percentageInput.scss';

const PercentageInput = ({
  onChange = () => {},
}) => {
  return (
    <div className='form-group'>
      <div className="slippage-input">
        <label className='slippage-input-label'>{Strings.SLIPPAGE}</label>
        <input
          className='slippage-input-field'
          // name="slippage"
          defaultValue={ 1 }
          onChange={ onChange }
          placeholder={ Strings.PERCENT }
        />
      </div>
    </div>
  );
};

PercentageInput.propTypes = {
  onChange: PropTypes.func,
}

export default PercentageInput;
