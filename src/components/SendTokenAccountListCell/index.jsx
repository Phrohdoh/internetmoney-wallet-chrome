import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import './round-listitem.scss';

import rightArrow from 'assets/images/right-arrow.svg';
import { getTokensByAddress } from 'redux/selectors';

const SendTokenAccountListCell = ({
  className = '',
  subtext = '',
  dataTestid = '',
  ...props
}) => {
  const { item, index } = props;
  const tokens = useSelector(getTokensByAddress)[item.publicAddress.toLowerCase()];
  const nativeToken = tokens[0];

  return (
    <React.Fragment>
      <div data-testid={ dataTestid } onClick={ () => props.onSelectAccount(item, index) } className={ `listitem-control noselect ${className}` }>
        <div className="body">
          <h6>
            {item.name}
          </h6>
        </div>
        <div className="right text-right">
          <small>{subtext}</small>
          <div className="large-text text-theme-color">
            {nativeToken?.balance?.value?.toString(6, true)}
            {' '}
            {nativeToken.symbol}
          </div>
        </div>
        <i className="ls-icon">
          <img src={ rightArrow } alt="Arrow" />
        </i>
      </div>
    </React.Fragment>
  );
};

SendTokenAccountListCell.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  onSelectAccount: PropTypes.func.isRequired,
  className: PropTypes.string,
  subtext: PropTypes.string.isRequired,
  dataTestid: PropTypes.string.isRequired,
};

export default SendTokenAccountListCell;
