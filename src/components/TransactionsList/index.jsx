import React from 'react';
import PropTypes from 'prop-types';

import TransactionListCell from 'components/TransactionListCell';

import './transactions-list.scss';

export default function TransactionsList (props) {

  const { transactionsList } = props;

  return (
    <ul className="list-container transactionslist-container">
      {
        transactionsList.map((item, index) => (
          <React.Fragment key={ index }>
            <TransactionListCell
              item = { item } 
              index={ index }
              { ...props }
            />
          </React.Fragment>
        )
        )}
    </ul>
  );
}

TransactionsList.propTypes = {
  transactionsList: PropTypes.array,
};
