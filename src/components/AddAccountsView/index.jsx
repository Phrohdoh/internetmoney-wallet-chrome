import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  createNewAccount,
  importAccount,
  checkImportAccount,
} from 'web3-layer';
import { useSelector, useDispatch, useStore } from 'react-redux';
import { STORAGE_KEYS } from '../../constants';

import './add-accounts.scss';

import { Strings } from 'resources';
import { setItem } from 'utils/storage';
// Components
import Button from 'components/Button';
import InputField from 'components/InputField';
import Spinner from 'components/Spinner';
import Dropdown from 'components/Dropdown';
import { WalletAction } from 'redux/slices/walletSlice';
import { AccountAction } from 'redux/slices/accountSlice';
import {
  getHardwareDeviceAccountObject,
  getListOfAccounts,
  getListOfTrezorAccounts,
  getTrezorHardwareDeviceAccountObject,
} from 'web3-layer/hardwareWeb3Layer';
import HardwareAccountListItem from './HardwareAccountListItem';
import AccountListCell from 'components/AccountListCell';
import {
  getArchivedAccountList,
  getFullAccountList,
  getNetwork,
} from 'redux/selectors';

export default function AddAccounts (props) {
  const store = useStore();
  const dispatch = useDispatch();
  const wallet = useSelector((state) => state.wallet);
  const accounts = useSelector((state) => state.account);
  const networks = useSelector((state) => state.networks);
  const selectedNetwork = useSelector(getNetwork);
  const initialValues = {
    accountName: '',
    privateKey: '',
  };

  const [values, setValues] = useState(initialValues);
  const [accountNameError, setAccountNameError] = useState('');
  const [privateKeyError, setPrivateKeyError] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isShowCreateAccount, setIsShowCreateAccount] = useState(false);
  const [isShowRestoreAccount, setIsShowRestoreAccount] = useState(false);
  const [isShowImportAccount, setIsShowImportAccount] = useState(false);

  // state for connect hardware device
  const [isShowConnectWallet, setIsShowConnectWallet] = useState(false);
  const [isShowImportConnectWallet, setIsShowImportConnectWallet] =
    useState(false);
  const [
    isShowImportHardwareListConnectWallet,
    setIsShowImportHardwareListConnectWallet,
  ] = useState(false);
  const [hardwareName, setHardwareName] = useState('');
  const [showDerivationPathList, setShowDerivationPathList] = useState(false);
  const [
    selectedDerivationPathPattern,
    setSelectedDerivationPathPattern
  ] = useState(Strings.DERIVATION_PATH_LIST[0]);
  const [hardwareAccountList, setHardwareAccountList] = useState([]);
  const [selectedHardwareAccount, setSelectedHardwareAccount] = useState([]);
  const [hardwareError, setHardwareError] = useState('');
  const [data, setData] = useState([0, 4]);
  const archivedAccountList = useSelector(getArchivedAccountList);

  const reset = () => {
    setValues(initialValues);
    setAccountNameError('');
    setPrivateKeyError('');
    setIsShowCreateAccount(false);
    setIsShowRestoreAccount(false);
    setIsShowImportAccount(false);
    setIsShowConnectWallet(false);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleHardwareInputChange = (e) => {
    const { name, value } = e.target;
    setSelectedHardwareAccount({
      ...selectedHardwareAccount,
      [name]: value,
    });
  };

  const handleCreateAccount = async (e) => {
    e.preventDefault();
    const accountNameLength = values.accountName.length;
    const isExistAccounts = accounts.accountList.find(
      (accObj) => accObj.name === values.accountName.trim()
    );
    if (accountNameLength === 0) {
      setAccountNameError(Strings.ENTER_VALID_ACCOUNT_NAME_MSG);
    } else if (isExistAccounts) {
      setAccountNameError(Strings.ACCOUNT_NAME_EXIST_MSG);
    } else {
      setIsLoading(true);
      const newAccountObj = await createNewAccount(
        wallet.walletDetail.mnemonic,
        wallet.walletDetail.walletObject,
        wallet.password,
        selectedNetwork,
      );
      if (newAccountObj.success) {
        const updatedWallet = { ...wallet.walletDetail };
        updatedWallet.walletObject = newAccountObj.object;
        const accountList = [...accounts.accountList];
        accountList.push({
          publicAddress: newAccountObj.account.publicAddress,
          isHardware: false,
          isImported: false,
          isArchived: false,
          isPopularTokens: true,
          name: values.accountName.trim(),
        });

        dispatch(WalletAction.saveWalletDetail(updatedWallet));
        dispatch(AccountAction.updateAccountObject(accountList));
        handleStates(updatedWallet, accountList);

        setIsLoading(false);
        reset();
        props.createAccountSuccess();
      }
    }
  };

  // Import account
  const handleImportAccount = async (e) => {
    e.preventDefault();
    setAccountNameError();

    const accountNameLength = values.accountName.length;
    const privateKeyErrorLength = values.privateKey.length;
    const isExistAccounts = accounts.accountList.find(
      (accObj) => accObj.name === values.accountName.trim()
    );
    if (accountNameLength === 0) {
      setAccountNameError(Strings.ENTER_VALID_ACCOUNT_NAME_MSG);
    } else if (privateKeyErrorLength === 0) {
      setPrivateKeyError(Strings.ENTER_VALID_PRIVATE_KEY);
    } else if (isExistAccounts) {
      setAccountNameError(Strings.ACCOUNT_NAME_EXIST_MSG);
    } else {
      setIsLoading(true);
      const checkAccount = await checkImportAccount(
        wallet.walletDetail.walletObject,
        values.privateKey.trim(),
        selectedNetwork,
      );
      if (checkAccount.success) {
        const importAccountObj = await importAccount(
          wallet.walletDetail.walletObject,
          values.privateKey.trim(),
          wallet.password,
          selectedNetwork,
        );
        setTimeout(() => {
          setIsLoading(false);
        }, 200);
        if (importAccountObj.success) {
          const updatedWallet = { ...wallet.walletDetail };
          updatedWallet.walletObject = importAccountObj.object;
          const accountList = [...accounts.accountList];
          accountList.push({
            publicAddress: importAccountObj.account.publicAddress,
            isHardware: false,
            isImported: true,
            isArchived: false,
            isPopularTokens: true,
            name: values.accountName.trim(),
          });

          dispatch(WalletAction.saveWalletDetail(updatedWallet));
          dispatch(AccountAction.updateAccountObject(accountList));

          handleStates(updatedWallet, accountList);
          reset();
          props.importAccountSuccess();
        } else {
          setPrivateKeyError(importAccountObj.error);
        }
      } else {
        setPrivateKeyError(checkAccount.error);
      }
      setIsLoading(false);
    }
  };

  const getOwnTabs = () => {
    return Promise.all(
      chrome.extension.getViews({ type: 'tab' })
        .map(view =>
          new Promise(resolve =>
            view.chrome.tabs.getCurrent(tab =>
              resolve(Object.assign(tab, { url: view.location.href }))))));
  };

  const openOptions = async (url) => {
    const ownTabs = await getOwnTabs();
    const tab = ownTabs.find(tab => tab.url.includes(url));
    if (tab) {
      chrome.tabs.update(tab.id, { active: true });
    } else {
      chrome.tabs.create({ url });
    }
  };

  // get hardware token list
  const handleHardwareConnectionAccountList = async (
    e,
    fromIndex,
    toIndex,
    hardwareName,
    derivationPathPattern = selectedDerivationPathPattern.key,
    existingList = hardwareAccountList,
  ) => {
    e.preventDefault();
    setIsLoading(true);
    let etherConnection = null;
    if (hardwareName === 'ledger') {
      etherConnection = await getListOfAccounts(fromIndex, toIndex + 20, derivationPathPattern);
    } else {
      etherConnection = await getListOfTrezorAccounts(fromIndex, toIndex + 20, derivationPathPattern);
    }

    if (etherConnection.success) {
      setData([fromIndex, toIndex]);
      setHardwareError('');
      setHardwareAccountList([
        ...existingList,
        ...etherConnection.hardwareAccountList,
      ]);
      setIsShowConnectWallet(false);
      setIsLoading(false);
      setIsShowImportHardwareListConnectWallet(true);

    } else {
      setIsLoading(false);
      setHardwareError(etherConnection.error);
    }
  };

  // Import Hardware account
  const handleImportHardwareAccount = async (e) => {
    try {
      e.preventDefault();
      setAccountNameError('');

      setIsLoading(true);
      const accountList = [...accounts.accountList];
      let firstAccount = null;
      if (hardwareName === 'trezor') {
        firstAccount = await getListOfTrezorAccounts(0, 0, selectedDerivationPathPattern.key);

        if (!firstAccount.success) {
          setIsLoading(false);
          throw {
            success: false,
            error: firstAccount.error,
          };
        }
      }

      for (let i = 0; i < selectedHardwareAccount.length; i++) {
        let accountObject = null;
        if (hardwareName === 'ledger') {
          accountObject = await getHardwareDeviceAccountObject(
            'Ledger ' + `${accountList.length}`,
            hardwareName,
            selectedHardwareAccount[i].index,
            selectedDerivationPathPattern.key,
          );
        } else {
          accountObject = await getTrezorHardwareDeviceAccountObject(
            `Trezor Account ${accountList.length + 1}`,
            hardwareName,
            firstAccount.hardwareAccountList[0],
            selectedHardwareAccount[i].index,
            selectedHardwareAccount[i].publicAddress,
            selectedDerivationPathPattern.key,
          );
        }

        if (accountObject.success) {
          accountList.push({
            ...accountObject.accountObject,
            isPopularTokens: true,
          });
        } else {
          setIsLoading(false);
          setAccountNameError(accountObject.error);
          return;
        }
      }

      dispatch(AccountAction.saveAccountList(accountList));
      setItem(STORAGE_KEYS.ACCOUNT_LIST, JSON.stringify(accountList));
      reset();
      props.connectHardwareWalletSuccess();
      setHardwareName('');
      setIsLoading(false);
      setIsShowImportHardwareListConnectWallet(false);
      setIsShowImportConnectWallet(true);
    } catch (e) {
      setIsLoading(false);
      console.log('error while hardware wallet', e);
    }
  };

  const handleStates = async (updatedWallet, accountList) => {
    setItem(STORAGE_KEYS.WALLET_OBJECT, JSON.stringify(updatedWallet));
    setItem(STORAGE_KEYS.ACCOUNT_LIST, JSON.stringify(accountList));
  };

  // Add account view
  const renderAddAccountView = () => {
    return (
      !isShowCreateAccount &&
      !isShowRestoreAccount &&
      !isShowImportAccount &&
      !isShowConnectWallet &&
      !isShowImportConnectWallet &&
      !isShowImportHardwareListConnectWallet &&
      !isLoading && (
        <React.Fragment>
          <div className="modal-header">
            <h6 className="text-center">{Strings.ADD_ACCOUNT}</h6>
          </div>

          <Button
            onClick={ (e) => {
              e.preventDefault();
              setIsShowCreateAccount(true);
              setIsShowRestoreAccount(false);
              setIsShowImportAccount(false);
              setIsShowConnectWallet(false);
            } }
            label={ Strings.CREATE_NEW_ACCOUNT }
          />
          {
            archivedAccountList.length > 0
              ? (
                <Button
                  onClick={ (e) => {
                    e.preventDefault();
                    setIsShowCreateAccount(false);
                    setIsShowRestoreAccount(true);
                    setIsShowImportAccount(false);
                    setIsShowConnectWallet(false);
                  } }
                  outline
                  label={ Strings.RESTORE_ACCOUNT }
                />
              )
              : null 
          }
          <Button
            onClick={ (e) => {
              e.preventDefault();
              setIsShowCreateAccount(false);
              setIsShowRestoreAccount(false);
              setIsShowImportAccount(true);
              setIsShowConnectWallet(false);
            } }
            outline
            label={ Strings.IMPORT_ACCOUNT }
          />
          <Button
            onClick={ (e) => {
              e.preventDefault();
              setIsShowCreateAccount(false);
              setIsShowRestoreAccount(false);
              setIsShowImportAccount(false);
              openOptions('index.html');
              setIsShowConnectWallet(true);
            } }
            outline
            label={ Strings.CONNECT_HARDWARE_WALLET }
          />
        </React.Fragment>
      )
    );
  };

  // render Create Account View
  const renderCreateAccountView = () => {
    return (
      isShowCreateAccount && (
        <React.Fragment>
          <div className="modal-header">
            <h6 className="text-center">{Strings.CREATE_ACCOUNT}</h6>
          </div>
          {isLoading && <Spinner />}
          <React.Fragment>
            <InputField
              label={ Strings.ACCOUNT_NAME }
              value={ values.accountName }
              onChange={ handleInputChange }
              error={ accountNameError }
              name="accountName"
            />
            <Button
              onClick={ (e) => handleCreateAccount(e) }
              label={ Strings.CREATE_NEW_ACCOUNT }
              className="btn-full"
            />
          </React.Fragment>
        </React.Fragment>
      )
    );
  };

  const renderRestoreAccountView = () => {
    return (
      isShowRestoreAccount && (
        <React.Fragment>
          <div className="modal-header">
            <h6 className="text-center">{Strings.RESTORE_ACCOUNT}</h6>
          </div>
          {isLoading && <Spinner />}
          <ul className="list-container is-add-button">
            {archivedAccountList.map((item, index) => (
              <AccountListCell
                key={ index + item + 'accList' }
                item={ item }
                index={ index }
                isShowAddress={ true }
                isShowBalance={ false }
                isRemoving={false}
                onClick={() => {
                  dispatch(AccountAction.restoreAccount(item));
                  setItem(STORAGE_KEYS.ACCOUNT_LIST, JSON.stringify(getFullAccountList(store.getState())));
                  reset();
                  props.restoreAccountSuccess();
                }}
                { ...props }
              />
            ))}
          </ul>
        </React.Fragment>
      )
    );
  };

  // render Import Account View
  const renderImportAccountView = () => {
    return (
      isShowImportAccount && (
        <React.Fragment>
          <div className="modal-header">
            <h6 className="text-center">{Strings.IMPORT_ACCOUNT}</h6>
            <p className="italic opacity text-center">
              {Strings.IMPORT_ACCOUNT_MSG}
            </p>
          </div>
          {isLoading && <Spinner />}
          <React.Fragment>
            <InputField
              label={ Strings.ACCOUNT_NAME }
              value={ values.accountName }
              onChange={ handleInputChange }
              error={ accountNameError }
              name="accountName"
            />
            <InputField
              label={ Strings.PRIVATE_KEY }
              value={ values.privateKey }
              onChange={ handleInputChange }
              error={ privateKeyError }
              name="privateKey"
            />
            <Button
              onClick={ (e) => handleImportAccount(e) }
              label={ Strings.IMPORT_ACCOUNT }
              className="btn-full"
            />
          </React.Fragment>
        </React.Fragment>
      )
    );
  };

  // render Connect Hardware Wallet View
  const renderConnectHardwareView = () => {
    return (
      isShowConnectWallet && (
        <React.Fragment>
          <div className="modal-header">
            <h6 className="text-center">{Strings.CONNECT_HARDWARE_WALLET}</h6>
          </div>
          {isLoading && <Spinner />}
          {hardwareError && (
            <label className="label-error-form">{hardwareError}</label>
          )}
          <React.Fragment>
            <div className="ledger-button">
              <span
                onClick={ (e) => {
                  setHardwareName('ledger');
                  setSelectedDerivationPathPattern(Strings.DERIVATION_PATH_LIST[1]);
                  handleHardwareConnectionAccountList(
                    e,
                    data[0],
                    data[1],
                    'ledger',
                    Strings.DERIVATION_PATH_LIST[1].key,
                  );

                } }
                className="nano-button noselect">
                {Strings.LEDGER}
              </span>
              <span className="nano-button noselect"
                onClick={(e) => {
                  setHardwareName('trezor');
                  handleHardwareConnectionAccountList(
                    e,
                    data[0],
                    data[1],
                    'trezor',
                  );
                }}
              >
                {Strings.TREZOR}
              </span>
            </div>
          </React.Fragment>
        </React.Fragment>
      )
    );
  };

  // render Import Connected Hardware Wallet View
  const renderImportAccountListConnectedHardwareView = () => {
    return (
      (isShowImportHardwareListConnectWallet && !showDerivationPathList) && (
        <React.Fragment>
          <div className="modal-header">
            <h5 className="text-center-hardware">
              {Strings.SELECT_AN_ACCOUNT}
            </h5>
          </div>
          <Dropdown
              dataTestid="hardware-derivation-path"
              value={ selectedDerivationPathPattern.title }
              className="text-gray-theme"
              onClick={ (e) => { 
                e.preventDefault(); 
                setShowDerivationPathList(true); 
              } }
            />   
          {isLoading && <Spinner />}
          {hardwareAccountList.slice(data[0], data[1] + 1).map((item, index) => (
            <HardwareAccountListItem
              key={ index }
              item={ item }
              index={ index }
              data={ data }
              selectedNetwork={ selectedNetwork }
              selectedHardwareAccount={ selectedHardwareAccount }
              setSelectedHardwareAccount={ setSelectedHardwareAccount }
            />
          ))}
          <div className="prev-next-button">
            {data[0] === 0 ? (
              <span>
                <label className="prev-button empty">{Strings.PREVIOUS}</label>
              </span>
            ) : (
              <span>
                <label
                  className="prev-button"
                  onClick={ (_e) => {
                    setIsLoading(true);
                    setData([data[0] - 5, data[1] - 5]);
                    setIsLoading(false);
                  } }
                >
                  {Strings.PREVIOUS}
                </label>
              </span>
            )}
            <span>
              <label
                className="next-button"
                onClick={ (e) => {
                  if (hardwareAccountList.length > data[1] + 5) {
                    setData([data[0] + 5, data[1] + 5]);
                  } else {
                    handleHardwareConnectionAccountList(
                      e,
                      data[0] + 5,
                      data[1] + 5,
                      hardwareName
                    );
                  }
                } }
              >
                {Strings.NEXT}
              </label>
            </span>
          </div>
          {accountNameError && (
            <label className="label-error-form">
              {accountNameError ?? hardwareError}
            </label>
          )}
          {hardwareError && (
            <label className="label-error-form">{hardwareError}</label>
          )}
          {selectedHardwareAccount.length === 0 ? (
            <Button label={ Strings.CONNECT_ACCOUNTS } className="btn-full empty" />
          ) : (
            <Button
              onClick={ (e) => {
                handleImportHardwareAccount(e);
              } }
              label={ Strings.CONNECT_ACCOUNTS }
              className="btn-full"
            />
          )}
        </React.Fragment>
      )
    );
  };

  // render Import Connected Hardware Wallet View
  const renderImportConnectedHardwareView = () => {
    return (
      isShowImportConnectWallet && (
        <React.Fragment>
          <div className="modal-header">
            <h6 className="text-center">
              {Strings.IMPORT_CONNECTED_HARDWARE_WALLET}
            </h6>
          </div>
          {isLoading && <Spinner />}
          <React.Fragment>
            <InputField
              label={ Strings.ACCOUNT_NAME }
              value={ selectedHardwareAccount.accountName }
              onChange={ handleHardwareInputChange }
              error={ accountNameError }
              name="accountName"
            />
            <label className="label-form">{Strings.ADDRESS} : </label>
            <p className="address-data">
              {selectedHardwareAccount?.publicAddress}
            </p>
            <div className="margin-border" />
            <Button
              onClick={ (e) => handleImportHardwareAccount(e) }
              label={ Strings.IMPORT_CONNECTED_HARDWARE_WALLET }
              className="btn-full"
            />
          </React.Fragment>
        </React.Fragment>
      )
    );
  };

  const renderShowDerivationPathOptions = () => (
    showDerivationPathList && (
      <React.Fragment>
        <div className="modal-header">
          <h6 className="text-center">{Strings.DERIVATION_PATH_TITLE}</h6>
        </div>
        <div className="modal-body">
          <div className="options-list">
            {
              Strings.DERIVATION_PATH_LIST.map((item, index) => (
                <div 
                  key={ item.key }
                  data-testid={ `derivation_path_option_${index}` } 
                  onClick={ (e) => {
                    setShowDerivationPathList(false);
                    setSelectedDerivationPathPattern(item);
                    setHardwareAccountList([]);
                    setData([0, 4]);
                    handleHardwareConnectionAccountList(
                      e,
                      0,
                      4,
                      hardwareName,
                      item.key,
                      [],
                    );
                   } }
                  className={ 'listitem-control noselect' }
                >
                  <div className="body">
                    <h6>{item.title}</h6>
                  </div>
                </div>
              ))
            }
          </div>
        </div>
      </React.Fragment>
    )
  );

  return (
    <React.Fragment>
      {renderAddAccountView()}
      {renderCreateAccountView()}
      {renderRestoreAccountView()}
      {renderImportAccountView()}
      {renderConnectHardwareView()}
      {renderImportAccountListConnectedHardwareView()}
      {renderImportConnectedHardwareView()}
      {renderShowDerivationPathOptions()}
    </React.Fragment>
  );
}

AddAccounts.propTypes = {
  createAccountSuccess: PropTypes.func,
  importAccountSuccess: PropTypes.func,
  connectHardwareWalletSuccess: PropTypes.func,
  restoreAccountSuccess: PropTypes.func,
};
