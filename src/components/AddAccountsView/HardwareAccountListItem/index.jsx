import { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Checkbox from 'components/Checkbox';
import { getSingleAccount } from 'web3-layer';
import { useSelector } from 'react-redux';

const HardwareAccountListItem = (props) => {
  const {
    item,
    selectedNetwork,
    index,
    selectedHardwareAccount,
    setSelectedHardwareAccount,
    data,
  } = props;
  const accounts = useSelector((state) => state.account);
  const isAccountAlreadyImported = accounts.accountList.find(
    (value) => value.publicAddress === item
  );

  const isAccountArraySelectedImported = selectedHardwareAccount.find(
    (value) => value.publicAddress === item
  );
  const [tokenBalance, setTokenBalance] = useState(0);

  const setDefaultTokenBalance = useCallback(async () => {
    const balance = await getSingleAccount(item, selectedNetwork);
    console.log('[setDefaultTokenBalance] Balance for account "%s": %O', item, balance);
    setTokenBalance(balance.account.value.toString(6));
  }, [item, selectedNetwork]);

  useEffect(() => {
    setDefaultTokenBalance();
  }, [setDefaultTokenBalance]);

  return (
    <div className="hardware-account-list">
      {isAccountAlreadyImported ? (
        <Checkbox
          className="check-box-hardware empty"
          id="checkbox"
          value={ isAccountAlreadyImported }
        />
      ) : (
        <Checkbox
          className="check-box-hardware"
          id="checkbox"
          value={ !!isAccountArraySelectedImported }
          onChange={ () => {
            if (!isAccountAlreadyImported) {
              const isExisted = selectedHardwareAccount?.findIndex(
                (value) => value?.publicAddress === item
              );

              if (isExisted >= 0) {
                selectedHardwareAccount.splice(isExisted, 1);
                setSelectedHardwareAccount([...selectedHardwareAccount]);
                return;
              }
              setSelectedHardwareAccount([
                ...selectedHardwareAccount,
                {
                  publicAddress: item,
                  index: data[0] + index,
                },
              ]);
            }
          } }
        />
      )}
      <div className="address">
        <label className="address-data-list">{data[0] + index + 1} . </label>
        <span className="address-data-list">
          {item.slice(0, 6)} ... {item.slice(-6)}
        </span>
      </div>
      <div>
        <span className="address-data-list value">
          {tokenBalance} {selectedNetwork.sym}
        </span>
      </div>
    </div>
  );
};

export default HardwareAccountListItem;

HardwareAccountListItem.propTypes = {
  item: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  selectedNetwork: PropTypes.object.isRequired,
  selectedHardwareAccount: PropTypes.array.isRequired,
  setSelectedHardwareAccount: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
};
