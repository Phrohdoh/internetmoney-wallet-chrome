import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './button.scss';

const Button = ({
  outline=false,
  link,
  label,
  onClick,
  className = '',
  icon,
  testId,
  ...props
}) => {


  if (outline) className += ' button-outline';
  return (
    <React.Fragment>
      <Link data-testid={ testId } to={ { pathname: link} } onClick={ onClick } className={ `button noselect ${className}` } { ...props }>
        {icon ? <img src={ icon } alt={ label } /> : null}
        {label}
      </Link>
    </React.Fragment>
  );
};

Button.propTypes = {
  outline: PropTypes.bool,
  link: PropTypes.string,
  label: PropTypes.string.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func,
  icon: PropTypes.string,
  testId: PropTypes.string,
};

export default Button;
