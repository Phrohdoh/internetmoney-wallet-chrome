import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NetworksAction } from 'redux/slices/networksSlice';
import { isTokenAddressValid } from 'web3-layer';

import Button from 'components/Button';
import InputField from 'components/InputField';

import { parseObject } from 'utils/parse';
import { getItem, setItem } from 'utils/storage';
import { Validation } from 'utils/validations';

import { Strings } from 'resources';
import { STORAGE_KEYS } from '../../constants';

import './import-token.scss';
import { getNetwork } from 'redux/selectors';

/**
 * Import Token Component 
 */
export default function ImportToken (props) {
  const { account, tokens } = props;

  const dispatch = useDispatch();

  const networks = useSelector(state => state.networks);
  const selectedNetwork = useSelector(getNetwork);

  const [errorTokenAddress, setErrorTokenAddress] = useState('');
  const [tokenSymbolError, setTokenSymbolError] = useState('');
  const [tokenPrecisionError, setTokenPrecisionError] = useState('');
  const [tokenSuccess, setTokenSuccess] = useState(false);
  const [tokenDetail, setTokenDetail] = useState(undefined);

  const initialValues = {
    tokenAddress: '',
    tokenSymbol: '',
    tokenPrecision: '',
  };

  const [values, setValues] = useState(initialValues);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const reset = () => {
    setValues(initialValues);
  };

  const checkImportTokenValidation = () => {
    const isEmptyToken = Validation.isEmpty(values.tokenAddress);
    const isInvalidTokenSymbol= (values.tokenSymbol+''.trim()).length > 11;
    const isInvalidTokenPrecision= parseInt(values.tokenPrecision) < 0 || parseInt(values.tokenPrecision) > 36;
    const isDuplicateToken = checkIsTokenImported(values.tokenAddress);

    if (isDuplicateToken) {
      setErrorTokenAddress(Strings.TOKEN_ALREADY_EXIST_MSG);
    }

    if (isEmptyToken) {
      setErrorTokenAddress('Token Address should not be empty!');
    }

    if(isInvalidTokenSymbol){
      setTokenSymbolError(Strings.TOKEN_SYMBOL_ERROR);
    }

    if(isInvalidTokenPrecision){
      setTokenPrecisionError(Strings.TOKEN_PRECISION_ERROR);
    }
    
    if(isEmptyToken || isInvalidTokenSymbol || isInvalidTokenPrecision || isDuplicateToken){
      return false;
    }
    else {
      return tokenSuccess;
    }
  };

  const handleIsTokenAddressValid = async () => {
    if (!checkIsTokenImported(values.tokenAddress)) {
      const tokenAddressLength = values.tokenAddress.length;
      if (tokenAddressLength === 0) {
        setErrorTokenAddress('Token Address should not be empty!');
      }
      else {
        const newTokenObj = await isTokenAddressValid(
          values.tokenAddress.trim(),
          selectedNetwork
        );
        if (newTokenObj.success) {
          setTokenSuccess(newTokenObj.success);
          setTokenDetail(newTokenObj.tokenDetails);
          setValues({
            tokenAddress: values.tokenAddress,
            tokenSymbol: newTokenObj.tokenDetails.symbol,
            tokenPrecision: newTokenObj.tokenDetails.decimals
          });
          setErrorTokenAddress('');
        }
        else {
          setTokenDetail(undefined);
          setErrorTokenAddress(newTokenObj.error);
        }
      }
    }
    else{
      setErrorTokenAddress('Token Address has already been imported!');
    }
  };

  const checkIsTokenImported = (address) => {
    return tokens.some(token => token.address.toLowerCase() === address.toLowerCase());
  };

  const handleTokenImported = async (e) => {
    e.preventDefault();
    if (checkImportTokenValidation()) {
      const networkTokens = networks.tokens ? [...networks.tokens] : [];

      const accountTokens = networkTokens
        .find(({ id }) => id.toLowerCase() === account.publicAddress.toLowerCase());
      
      if (accountTokens) {
        const tokenIndex = networkTokens.indexOf(accountTokens);
        const tokenList = [...accountTokens.tokens];
        tokenList.push({ ...tokenDetail, address: values.tokenAddress.trim() });
        const newAccountTokens = { ...accountTokens };
        newAccountTokens.tokens = tokenList;
        networkTokens[tokenIndex] = newAccountTokens;
      } else {
        const newAccountTokens = {
          id: account.publicAddress,
          tokens: [{ ...tokenDetail, address: values.tokenAddress.trim() }],
        };
        networkTokens.push(newAccountTokens);
      }

      const { data } = getItem(STORAGE_KEYS.CHAIN_TOKENS);
      const parsedData = parseObject(data);

      const chainTokens = parsedData.success ? parsedData.data : [];
      const chain = chainTokens.find((chain) => chain.chainId === selectedNetwork.chainId);

      if (chain) {
        const chainIndex = chainTokens.indexOf(chain);        
        chain.accountTokens = networkTokens;
        chainTokens[chainIndex] = chain;
      } else {
        chainTokens.push({
          chainId: selectedNetwork.chainId,
          accountTokens: networkTokens,
        });
      }

      dispatch(NetworksAction.saveTokens(networkTokens));
      setItem(STORAGE_KEYS.CHAIN_TOKENS, JSON.stringify(chainTokens));

      reset();

      if (props.onImportTokenSuccess) props.onImportTokenSuccess();
    }
  };

  const renderImportTokenView = () => {
    return (
      <React.Fragment>
        <p className="italic opacity text-center">{Strings.IMPORT_TOKEN_MSG}</p>
        <InputField
          label={ Strings.TOKEN_ADDRESS }
          value={ values.tokenAddress }
          onChange={ handleInputChange }
          error={ errorTokenAddress }
          onBlur={ (e) => handleIsTokenAddressValid(e) }
          name="tokenAddress"

        />
        <InputField
          label={ Strings.TOKEN_SYMBOL }
          value={ values.tokenSymbol }
          onChange={ handleInputChange }
          error={ tokenSymbolError }
          name="tokenSymbol"
        />
        <InputField
          label={ Strings.TOKEN_PRECISION }
          value={ values.tokenPrecision }
          onChange={ handleInputChange }
          error={ tokenPrecisionError }
          name="tokenPrecision"
        />

        <Button
          onClick={ (e) => handleTokenImported(e) }
          label={ Strings.IMPORT_TOKEN_UPPERCASE }
          className="btn-full"
        />
      </React.Fragment>
    );
  };

  return renderImportTokenView();
}

ImportToken.propTypes = {
  account: PropTypes.object,
  tokens: PropTypes.arrayOf(PropTypes.object),
  onImportTokenSuccess: PropTypes.func,
};
