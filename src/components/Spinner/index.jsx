import React from 'react';
import './spinner.scss';

export default function Spinner () {
  return (
    <React.Fragment>
      <div className="spinner-container">
        <div className="loading-spinner"></div>
      </div>
    </React.Fragment>
  );
}
