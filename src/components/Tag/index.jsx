import React from 'react';
import PropTypes from 'prop-types';

import close from 'assets/images/close.svg';

import './tags.scss';
import classNames from 'classnames';

export default function Tag (props) {
   
  const { tag, index, error } = props;

  return (
    <li key={ index } className="tag" >
      <span>{index + 1}.</span>
      <div className={classNames('tagStyle', {
        errorTagStyle: error,
      })} >
        <React.Fragment>
          <div className="hide">{tag}</div>
          <input 
            data-testid={ 'tag-' + index }
            value={ tag } 
            onChange={
              text => {
                props.onEndEditing(text.target.value, index);
              }
            }
          />
                  
          {
            props.onDelete ?
              <span data-testid={ 'tag-delete-' + index } className="remove" 
                onClick={ () => props.onDelete(index) }
              >
                <img src={ close } alt="remove" />
              </span>
              : null
          }
        </React.Fragment>
      </div>
    </li>
  );
}

Tag.propTypes = {
  tag: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  onEndEditing: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  error: PropTypes.bool.isRequired,
};
