import React from 'react';
import PropTypes from 'prop-types';

// utils
import { useTokenIcons } from 'utils/hooks';
import remove from 'assets/images/remove.svg';

import './round-listitem.scss';
import rightArrow from 'assets/images/right-arrow.svg';
import checkBox from 'assets/images/check-box.svg';
import { useSelector } from 'react-redux';

const NetworkListCell = ({
  className = '',  
  dataTestid = '',
  ...props
}) => {

  const { item, index, isRemoveEnable, onClickDelete } = props;
  const getTokenIcon = useTokenIcons();
  const swapSupportedNetworks = useSelector(state => state.networks.swapSupportedNetworks);
  const swapSupported = swapSupportedNetworks.includes(item.chainId);
  
  return (
    <React.Fragment>
      <div data-testid={ dataTestid } className={ `select-network-item ds-flex noselect ${className}` }>     
        <div onClick={ () => props.onSelectNetwork(item, index) } className={ 'list-item full-flex' }>
          <div className={ `left ${index}` }>
            <img src={ getTokenIcon('', item.chainId) } alt="" />
          </div>
          <div className="body">
            <h6>
              {item.networkName}
              {swapSupported && (
                <span className="swap-badge">Swap</span>
              )}
            </h6>
          </div>
          <div className="right">
            {item.rpc === props.selectedNetwork.rpc && (
              <i className="icon">
                <img src={ checkBox } alt="checkBox" />
              </i>
            )}
            {!isRemoveEnable && (
              <i className="ls-icon">
                <img src={ rightArrow } alt="Arrow" />
              </i>
            )}
          </div>
        </div>        
        {isRemoveEnable && (
          <div className="icon is-remove">
            <button className="remove-btn" onClick={ () => onClickDelete(item) }><img src={ remove } alt="Remove" /></button>
          </div>
        )}
      </div>
    </React.Fragment>
  );
};

NetworkListCell.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  onSelectNetwork: PropTypes.func.isRequired,
  className: PropTypes.string,
  dataTestid: PropTypes.string.isRequired,
  selectedNetwork: PropTypes.object.isRequired,
  isRemoveEnable: PropTypes.bool,
  onClickDelete: PropTypes.func,
};

export default NetworkListCell;
