import React from 'react';
import PropTypes from 'prop-types';

import ChangeNetworkListCell from 'components/ChangeNetworkListCell';

import './network-selection-modal.scss';

import { Strings } from 'resources';
import { useSelector } from 'react-redux';
import { getNetwork } from 'redux/selectors';

const NetworkSelectionModal = ({
  isOpen = false,
  closeModal,
  className = '',
  ...props
}) => {

  const { onSelectNetwork, isShowSwapSupported } = props;

  const networks = useSelector(state => state.networks);
  const selectedNetwork = useSelector(getNetwork);

  return (
    <React.Fragment>
      <div className={ `modal modal-flex bottom ${isOpen} ${className}` }>
        <div className="bodyclose modal-flex" data-testid="modal" onClick={ closeModal }></div>
        <div className="modal-container">
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="text-center text-theme-color">{Strings.NETWORK_SWITCH}</h4>
            </div>
            <div className="list-style network-list-style">
              {
                networks.networks.map((item, index) => (
                  <ChangeNetworkListCell
                    key={ index }
                    item={ item }
                    index={ index }
                    dataTestid={ `network-list-cell-${index}` }
                    selectedNetwork={ selectedNetwork }
                    swapSupportedList={ networks.swapSupportedNetworks }
                    isShowSwapSupported={ isShowSwapSupported }
                    onSelectNetwork={ item => onSelectNetwork(item) }
                  />
                ))
              }
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

NetworkSelectionModal.propTypes = {
  isOpen: PropTypes.bool,
  closeModal: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.object,
  onSelectNetwork: PropTypes.func,
  isShowSwapSupported: PropTypes.bool,
};

export default NetworkSelectionModal;
