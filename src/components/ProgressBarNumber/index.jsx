import React from 'react';
import PropTypes from 'prop-types';

import './Progressbar-number.scss';

const ProgressBarNumber = (props) => {

  return (
    <React.Fragment>
      <ul className="progressbar-number">
        {props.steps.map((item, index) => (
          <li key={ index } className={ `${item === props.selectedSteps || item < props.selectedSteps ? 'active' : null }` }><span>{item}</span></li>
        ))}
      </ul>
    </React.Fragment>
  );
};

ProgressBarNumber.propTypes = {
  steps: PropTypes.array.isRequired,  
  selectedSteps: PropTypes.number.isRequired,
};

export default ProgressBarNumber;
