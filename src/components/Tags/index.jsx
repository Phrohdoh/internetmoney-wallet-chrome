import React from 'react';
import PropTypes from 'prop-types';

import Tag from 'components/Tag';

import './tags.scss';

const Tags = ({
  items,
  onDelete,
  className = '',
  onEndEditing,
  hasError = () => false,
}) => {
   
  return (
    <React.Fragment>
      <ul className={ `tags ${className}` }>
        {items.map((element, index) => {
          return (
            <Tag
              key={ index }
              tag={ element }
              index={ index }
              onEndEditing={ onEndEditing }
              onDelete={ onDelete }
              error={ hasError(element) }
            />
          );
        })}
      </ul>
    </React.Fragment>
  );
};

Tags.propTypes = {
  items: PropTypes.array.isRequired,
  className: PropTypes.string,
  onEndEditing: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  hasError: PropTypes.func.isRequired,
};

export default Tags;
