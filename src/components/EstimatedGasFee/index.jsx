import React, { useCallback, useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import './estimated-gas-fee.scss';

import { Strings } from 'resources';

// Components
import Button from 'components/Button';
import RangeSelection from 'components/RangeSelection';
import RangeNumber from 'components/RangeNumber';

// Images
import dropIcon from 'assets/images/drop.svg';
import IntegerUnits, {
  sanitizeFloatingPointString,
  sanitizeIntegerString,
} from 'utils/IntegerUnits';
import { calculateEstimateFee, calculateMaxFee } from 'web3-layer';

const GWEI = new IntegerUnits(1000000000, 18);
const GAS_INCREMENT = new IntegerUnits(1, 0);
const FEE_INCREMENT = GWEI.multiply(0.1);

export default function EstimatedGasFee (props) {

  const { gasRange, txnObject, network } = props;
  const [selectedRange, setSelectedRange] = useState(props.selectedRange);
  const [isShowAdvance, setIsShowAdvance] = useState(false);
  const [gas, setGas] = useState(new IntegerUnits(0));
  const [gasText, setGasText] = useState('');
  const [gasPrice, setGasPrice] = useState(new IntegerUnits(0));
  const [gasPriceText, setGasPriceText] = useState('');
  const [maxPrioFee, setMaxPrioFee] = useState(new IntegerUnits(0));
  const [maxPrioFeeText, setMaxPrioFeeText] = useState('');
  const [maxFeePerGas, setMaxFeePerGas] = useState(new IntegerUnits(0));
  const [maxFeePerGasText, setMaxFeePerGasText] = useState('');
  const [minGas, setMinGas] = useState(new IntegerUnits(0));
  const [minMaxPrioFee, setMinMaxPrioFee] = useState(new IntegerUnits(0));
  const [minGasPrice, setMinGasPrice] = useState(new IntegerUnits(0));

  useEffect(() => {
    setGas(txnObject.gas);
    setGasText(txnObject.gas.value.toString());
    setGasPrice(txnObject.gasPrice);
    setGasPriceText(txnObject.gasPrice.divide(GWEI).toPrecision(18).toString(6, true, true));
    if (txnObject.maxPriorityFeePerGas) {
      setMaxPrioFee(txnObject.maxPriorityFeePerGas);
      setMaxPrioFeeText(txnObject.maxPriorityFeePerGas.divide(GWEI).toPrecision(18).toString(6, true, true));
    }

    if (txnObject.maxFeePerGas) {
      setMaxFeePerGas(txnObject.maxFeePerGas);
      setMaxFeePerGasText(txnObject.maxFeePerGas.divide(GWEI).toPrecision(18).toString(6, true, true));
    }

    if (network.txnType != 0) {
      setMinGas(gasRange.gas.minCap);
      setMinMaxPrioFee(gasRange.maxPriorityFeePerGas.minCap);
    }

    if (network.txnType == 0) {
      setMinGas(gasRange.gas.minCap);
      setMinGasPrice(gasRange.gasPrice.minCap);
    }

  }, [gasRange, txnObject]);

  useEffect(() => {
    getGasFeeValue();
  }, [selectedRange]);

  const getGasFeeValue = () => {
    if (network.txnType != 0) {
      const defaultGasRange = gasRange.gas.default;
      const defaultMaxPrioRange = gasRange.maxPriorityFeePerGas.default;
      const defaultMaxFeeRange = gasRange.maxFee.default;

      if (selectedRange === 0) {
        setGas(defaultGasRange.low);
        setGasText(defaultGasRange.low.value.toString());
        setMaxPrioFee(defaultMaxPrioRange.low);
        setMaxPrioFeeText(defaultMaxPrioRange.low.divide(GWEI).toPrecision(18).toString(6, true, true));
        setMaxFeePerGas(defaultMaxFeeRange.low);
        setMaxFeePerGasText(defaultMaxFeeRange.low.divide(GWEI).toPrecision(18).toString(6, true, true));
      } else if (selectedRange === 1) {
        setGas(defaultGasRange.medium);
        setGasText(defaultGasRange.medium.value.toString());
        setMaxPrioFee(defaultMaxPrioRange.medium);
        setMaxPrioFeeText(defaultMaxPrioRange.medium.divide(GWEI).toPrecision(18).toString(6, true, true));
        setMaxFeePerGas(defaultMaxFeeRange.medium);
        setMaxFeePerGasText(defaultMaxFeeRange.medium.divide(GWEI).toPrecision(18).toString(6, true, true));
      } else if (selectedRange === 2) {
        setGas(defaultGasRange.high);
        setGasText(defaultGasRange.high.value.toString());
        setMaxPrioFee(defaultMaxPrioRange.high);
        setMaxPrioFeeText(defaultMaxPrioRange.high.divide(GWEI).toPrecision(18).toString(6, true, true));
        setMaxFeePerGas(defaultMaxFeeRange.high);
        setMaxFeePerGasText(defaultMaxFeeRange.high.divide(GWEI).toPrecision(18).toString(6, true, true));
      }
    }
  };

  const estimateFee = calculateEstimateFee({
      gas,
      gasPrice,
      maxPriorityFeePerGas: maxPrioFee
  }, network.txnType).toString(6);

  const maxFee = calculateMaxFee({
    gas,
    maxFeePerGas,
    maxPriorityFeePerGas: maxPrioFee,
  }).toString(6);

  const getTimeToSend = () => {
    if (selectedRange === 0) {
      return Strings.LOW_SEND_TIME;
    } else if (selectedRange === 1) {
      return Strings.MEDIUM_SEND_TIME;
    } else if (selectedRange === 2) {
      return Strings.HIGH_SEND_TIME;
    }
  };

  const validationTwo = () => {
    return maxPrioFee.gte(minMaxPrioFee) && gas.gte(minGas);
  };

  const validationZero = () => {
    return gasPrice.gte(minGasPrice) && gas.gte(minGas);
  };

  const onChangeGasText = useCallback(text => {
    text = sanitizeIntegerString(text);
    setGas(IntegerUnits.fromFloatingPoint(text));
    setGasText(text);
  }, [setGas, setGasText]);

  const onIncrementGas = useCallback(() => {
    const value = gas.add(GAS_INCREMENT);
    setGas(value);
    setGasText(value.value.toString());
  }, [gas, setGas, setGasText]);

  const onDecrementGas = useCallback(() => {
    if (gas.lte(minGas)) {
      return;
    }
    const value = gas.subtract(GAS_INCREMENT);
    setGas(value);
    setGasText(value.value.toString());
  }, [gas, minGas, setGas, setGasText]);

  const onChangeGasPriceText = useCallback(text => {
    text = sanitizeFloatingPointString(text);
    setGasPrice(IntegerUnits.fromFloatingPoint(text).multiply(GWEI));
    setGasPriceText(text);
  }, [setGasPrice, setGasPriceText]);

  const onIncrementGasPrice = useCallback(() => {
    const value = gasPrice.add(FEE_INCREMENT);
    setGasPrice(value);
    setGasPriceText(value.divide(GWEI).toPrecision(18).toString(6, true, true));
  }, [gasPrice, setGasPrice, setGasPriceText]);

  const onDecrementGasPrice = useCallback(() => {
    if (gasPrice.lte(minGasPrice)) {
      return;
    }
    const value = gas.gt(FEE_INCREMENT)
      ? gas.subtract(FEE_INCREMENT)
      : new IntegerUnits(0);
    setGasPrice(value);
    setGasPriceText(value.divide(GWEI).toPrecision(18).toString(6, true, true));
  }, [gasPrice, minGasPrice, setGasPrice, setGasPriceText]);

  const onChangeMaxPrioFeeText = useCallback(text => {
    text = sanitizeFloatingPointString(text);
    setMaxPrioFee(IntegerUnits.fromFloatingPoint(text).multiply(GWEI));
    setMaxPrioFeeText(text);
  }, [setMaxPrioFee, setMaxPrioFeeText]);

  const onIncrementMaxPrioFee = useCallback(() => {
    const value = maxPrioFee.add(FEE_INCREMENT);
    setMaxPrioFee(value);
    setMaxPrioFeeText(value.divide(GWEI).toPrecision(18).toString(6, true, true));
  }, [maxPrioFee, setMaxPrioFee, setMaxPrioFeeText]);

  const onDecrementMaxPrioFee = useCallback(() => {
    if (maxPrioFee.lte(minMaxPrioFee)) {
      return;
    }
    const value = maxPrioFee.gt(FEE_INCREMENT)
      ? maxPrioFee.subtract(FEE_INCREMENT)
      : new IntegerUnits(0)
    setMaxPrioFee(value);
    setMaxPrioFeeText(value.divide(GWEI).toPrecision(18).toString(6, true, true));
  }, [maxPrioFee, minMaxPrioFee, setMaxPrioFee, setMaxPrioFeeText]);

  const onChangeMaxFeePerGasText = useCallback(text => {
    text = sanitizeFloatingPointString(text);
    setMaxFeePerGas(IntegerUnits.fromFloatingPoint(text).multiply(GWEI));
    setMaxFeePerGasText(text);
  }, [setMaxFeePerGas, setMaxFeePerGasText]);

  const onIncrementMaxFeePerGas = useCallback(() => {
    const value = maxFeePerGas.add(FEE_INCREMENT);
    setMaxFeePerGas(value);
    setMaxFeePerGasText(value.divide(GWEI).toPrecision(18).toString(6, true, true));
  }, [maxFeePerGas, setMaxFeePerGas, setMaxFeePerGasText]);

  const onDecrementMaxFeePerGas = useCallback(() => {
    if (maxFeePerGas.lte(maxPrioFee)) {
      return;
    }
    const value = maxFeePerGas.gt(FEE_INCREMENT)
      ? maxFeePerGas.subtract(FEE_INCREMENT)
      : new IntegerUnits(0)
    setMaxFeePerGas(value);
    setMaxFeePerGasText(value.divide(GWEI).toPrecision(18).toString(6, true, true));
  }, [maxFeePerGas, maxPrioFee, setMaxFeePerGas, setMaxFeePerGasText]);

  const renderTypeOneOptions = () => {
    return (
      network.txnType == 0 && (
        <React.Fragment>
          <RangeNumber
            title={Strings.GAS_LIMIT}
            valueText={gasText}
            isShowError={gas.lt(minGas)}
            error={Strings.formatString(Strings.MIN_GAS_LIMIT_MSG, gas.value.toString(), minGas)}
            onClickIncrement={onIncrementGas}
            onClickDecrement={onDecrementGas}
            onChange={onChangeGasText}
          />
          <RangeNumber
            title={Strings.GAS_PRICE}
            value={gasPrice}
            valueText={gasPriceText}
            isShowError={gasPrice.lt(minGasPrice)}
            error={Strings.MIN_GAS_PRICE_MSG}
            onClickIncrement={onIncrementGasPrice}
            onClickDecrement={onDecrementGasPrice}
            onChange={onChangeGasPriceText}
          />
        </React.Fragment>
      )
    );
  };

  const renderTypeTwoOptions = () => {
    return (
      network.txnType != 0 && (
        <React.Fragment>
          <div className="link-dropdown" onClick={() => setIsShowAdvance(!isShowAdvance)}>
            {Strings.ADVANCE_OPTION}
            <img src={dropIcon} alt={Strings.ADVANCE_OPTION} />
          </div>
          {isShowAdvance && (
            <div className="tab-content">
              <RangeNumber
                title={Strings.GAS_LIMIT}
                valueText={gasText}
                isShowError={gas.lt(minGas)}
                error={Strings.formatString(Strings.MIN_GAS_LIMIT_MSG, [gas.value.toString(), minGas.value.toString()]) }
                onClickIncrement={onIncrementGas}
                onClickDecrement={onDecrementGas}
                onChange={onChangeGasText}
              />
              <RangeNumber
                title={Strings.MAX_PRIO_FEE}
                value={maxPrioFee}
                valueText={maxPrioFeeText}
                isShowError={maxPrioFee.lt(minMaxPrioFee)}
                error={Strings.MAX_PRIO_FEE_NOT_ALLOW_MSG}
                onClickIncrement={onIncrementMaxPrioFee}
                onClickDecrement={onDecrementMaxPrioFee}
                onChange={onChangeMaxPrioFeeText}
              />
              <RangeNumber
                title={Strings.MAX_FEE_PER_GAS}
                value={maxFeePerGas}
                valueText={maxFeePerGasText}
                isShowError={maxFeePerGas.lt(maxPrioFee)}
                error={Strings.MAX_FEE_NOT_ALLOW_MSG}
                onClickIncrement={onIncrementMaxFeePerGas}
                onClickDecrement={onDecrementMaxFeePerGas}
                onChange={onChangeMaxFeePerGasText}
              />
            </div>
          )}
        </React.Fragment>
      )
    );
  };

  return (

    <div className="estimated-gas-fee-content">
      <div className="modal-header">
        <h6 className="text-center">{Strings.ESTIMATE_GAS_FEE}</h6>
      </div>
      <div className="modal-body text-center">
        <h2>{estimateFee} {network.sym}</h2>
        {network.txnType != 0 && (
          <h6>
            <small className="text-gray-color3">{Strings.MAX_FEE}</small>
            {' '}
            {maxFee}
            {' '}
            {network.sym}
          </h6>
        )}
        <div>
          <small className="text-green-color opacity">{getTimeToSend()}</small>
        </div>
        <div className="tabs-wrapper">
          {network.txnType != 0 && (
            <React.Fragment>
              <RangeSelection
                selectedRange={selectedRange}
                onSelectRange={index => setSelectedRange(index)}
              />
              <div className="line-style"></div>
            </React.Fragment>
          )}

          {renderTypeTwoOptions()}
          {renderTypeOneOptions()}

          <Button
            label={Strings.SAVE_UPPERCASE}
            onClick={(e) => {
              e.preventDefault();
              if (network.txnType != 0) {
                if (validationTwo) {
                  props.onClickSave({
                    gas,
                    maxPrioFee,
                    maxFeePerGas,
                  });
                }
              } else {
                if (validationZero) {
                  props.onClickSave({
                    gas,
                    gasPrice,
                  });
                }
              }
            }}
          />

        </div>
      </div>
    </div>
  );
}

EstimatedGasFee.propTypes = {
  key: PropTypes.string,
  index: PropTypes.string,
  title: PropTypes.string,
  selectedRange: PropTypes.number,
  onSelectRange: PropTypes.func,
  isActive: PropTypes.bool,
  gasRange: PropTypes.object,
  txnObject: PropTypes.object,
  onClickSave: PropTypes.func,
  network: PropTypes.object,
};
