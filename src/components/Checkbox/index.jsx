import React from 'react';
import PropTypes from 'prop-types';

import './checkbox.scss';

const Checkbox = ({
  id,
  label,
  labelFormatString,
  value,
  onChange,
  className = '',
}) => {

  return (
    <React.Fragment>
      <label className={ `single-checkbox ${className}` }>
        <input data-testid="checkbox-input" id={ id } type="checkbox" checked={ value } onChange={ onChange }/>
        <i className="check-icon"></i>
        {label ? <span className="opacity">{ label  }</span> : null }
        {labelFormatString ? <span  className="opacity">{labelFormatString}</span> : null}
      </label>
    </React.Fragment>
  );
};

Checkbox.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  labelFormatString: PropTypes.array,
  value: PropTypes.bool,
  className: PropTypes.string,
  onChange: PropTypes.func,
};

export default Checkbox;
