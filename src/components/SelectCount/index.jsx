import React from 'react';
import PropTypes from 'prop-types';
import './select-count.scss';

const SelectCount = ({
  label,
  items,
  selectedCount,
  onPressCount,
  className = '',
}) => {

    
  const renderCountCell = (count, index) => {
    return (
      <li className={ 'radio-number' }
        key={ `${count}-${index}` }
        data-testid={ 'select-count-'+index }
        onClick={ () => onPressCount(count) }
      >
        <span className={ `noselect ${selectedCount === count ? 'selected' : ''}` }>{count}</span>                
      </li>
    );
  };
    
  return (
    <React.Fragment>
      <div className="form-group">
        {label ? 
          <label>{label}</label> 
          : null}
        <ul className={ `radio-numbers ${className}` }>
          {items.map((item, index) => {
            return renderCountCell(item, index);
          })}
        </ul>
      </div>
    </React.Fragment>
  );
};

SelectCount.propTypes = {
  items: PropTypes.array.isRequired,
  label: PropTypes.string,
  selectedCount: PropTypes.number.isRequired,
  onPressCount: PropTypes.func.isRequired,
  className: PropTypes.string,
};

export default SelectCount;
