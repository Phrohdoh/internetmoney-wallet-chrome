import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import classnames from 'classnames';

import { displayAddress } from 'utils/displayAddress';

import './account-list-cell.scss';
import { Strings } from 'resources';
import { useDispatch, useSelector, useStore } from 'react-redux';

import copyIcon from 'assets/images/copy-yellow.svg';
import remove from 'assets/images/remove.svg';
import toast from 'react-hot-toast';
import copy from 'copy-to-clipboard'; 

import Button from 'components/Button';
import { AccountAction } from 'redux/slices/accountSlice';
import { STORAGE_KEYS } from 'constants';
import { setItem } from 'utils/storage';
import { WalletAction  } from 'redux/slices/walletSlice';
import AlertModel from 'components/AlertModel';
import {
  getTotalValueByAddress,
  getWalletDetail,
  getFullAccountList,
} from 'redux/selectors';

const AccountListCell = (props) => {
  const { item, index, isShowAddress, isShowBalance, isRemoving, onClick } = props;
  const navigate = useNavigate();
  const store = useStore();
  const dispatch = useDispatch();
  const [showRemoveModal, setShowRemoveModal] = useState(false);
  let clipboard = false;

  const publicAddress = item.publicAddress.toLowerCase();
  const accountBalance = useSelector(getTotalValueByAddress)[publicAddress];

  const copyToClipboard = (e) => {
    clipboard = true;
    copy(item.publicAddress);
    toast(Strings.ADDRESS_COPIED_SUCCESS);
  };

  const onRemove = useCallback(async () => {
    setShowRemoveModal(false);
    dispatch(AccountAction.removeAccount(item));
    dispatch(WalletAction.removeAccount(item));
    setItem(STORAGE_KEYS.ACCOUNT_LIST, JSON.stringify(getFullAccountList(store.getState())));
    setItem(STORAGE_KEYS.WALLET_OBJECT, JSON.stringify(getWalletDetail(store.getState())));
  }, [dispatch, item]);

  let handleClick = onClick;
  if (handleClick === undefined && !isRemoving) {
    handleClick = () => {
      setTimeout(() => {
        if (clipboard === false) {
          navigate('/accounts-details', {
            state: {
              item,
              accountIndex: index,
            },
          });
        }
      }, 10);
    };
  }

  return (
    <li
      key={ `account-${index + 1}` }
      data-testid={ `account-${index + 1}` }
      className={classnames('listitem', { disabled: isRemoving })}
      onClick={handleClick}
    >
      <div className="body">
        <React.Fragment>
          <h6 className="uppercase">{item.name}</h6>
          <span className="small-text text-eclipse-style text-theme-color">
            {isShowAddress ? 
              displayAddress(item.publicAddress)             
              : '--'}
              <Button
                className='copy-button'
                icon={ copyIcon }
                onClick={ (e) => copyToClipboard(e) }
                label=""
              />
          </span>
        </React.Fragment>
      </div>
      <div className="right text-right">
        <small className="text-gray-color">{Strings.VALUE}</small>
        <h3 className="text-theme-color mb-0 wrap-text">
          ${isShowBalance ? accountBalance?.toString() : '--'}
        </h3>
      </div>
      {
        isRemoving
          ? (
            <div className="icon ml-sm">
              <button className="remove-btn" onClick={ () => setShowRemoveModal(true) }>
                <img src={ remove } alt="Remove" />
              </button>
            </div>
          )
          : <i className="ls-icon" />
      }
      {showRemoveModal && (
        <AlertModel
          alertTitle={ Strings.REMOVE_ACCOUNT }
          isShowAlert={ showRemoveModal }
          onPressSuccess={ onRemove }
          successBtnTitle={ Strings.YES }
          onPressCancel={ () => {
            setShowRemoveModal(false);
          } }
          className="text-center"
        >
          <p>{Strings.formatString(Strings.REMOVE_ACCOUNT_MESSAGE, [item.name])}</p>
        </AlertModel>
      )}
    </li>
  );
};

AccountListCell.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  isShowAddress: PropTypes.bool,
  isShowBalance: PropTypes.bool,
  isRemoving: PropTypes.bool,
  onClick: PropTypes.func,
};

export default AccountListCell;
