import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { STORAGE_KEYS } from 'constants';
import { removeStorageItem, setItem } from 'utils/storage';
import './lockWallet.scss';

export const LockWallet = () => {
  const navigate = useNavigate();

  useEffect(() => {
    setItem(STORAGE_KEYS.IS_VERIFIED_LOGIN, false);
    setItem(STORAGE_KEYS.LAST_LOGIN_TIME, undefined);
    removeStorageItem(STORAGE_KEYS.WALLET_PASS);
    navigate('/');
  }, []);

  return <div className='container' />
};

export default LockWallet;
