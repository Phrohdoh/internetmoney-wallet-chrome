import React from 'react';
import PropTypes from 'prop-types';
import { displayAddress } from 'utils/displayAddress';

import './recent-sent-cell.scss';
import rightArrow from 'assets/images/right-arrow.svg';

const RecentSentCell = ({
  className = '',
  dataTestid = '',
  ...props
}) => {
  
  const { item, index, accounts } = props;

  const getAccountName = () => {
    const nameObj = accounts.find(
      account => account.publicAddress.toLowerCase() === item.toLowerCase(),
    );
    if (!nameObj) {
      return '';
    } else {
      return nameObj.name;
    }
  };

  return (
    <React.Fragment>
      <div data-testid={ dataTestid } onClick={ () => props.onSelectRecentAccount(item, index) } className={ `recent-listitem noselect ${className}` }>
        <div className="body">
          <h6>
            {getAccountName()}
          </h6>
        </div>
        <div className="right">
          <strong className="text-theme-color">{displayAddress(item)}</strong>
        </div>
        <i className="ls-icon">
          <img src={ rightArrow } alt="Arrow" />
        </i>
      </div>
    </React.Fragment>
  );
};

RecentSentCell.propTypes = {
  className: PropTypes.string,
  dataTestid: PropTypes.string,
  item: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  onSelectRecentAccount: PropTypes.func.isRequired,
  accounts: PropTypes.array,
};

export default RecentSentCell;
