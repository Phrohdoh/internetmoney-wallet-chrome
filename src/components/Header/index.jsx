import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import { Strings } from 'resources';
import { useSelector, useDispatch } from 'react-redux';

import NetworkSelectionModal from 'components/NetworkSelectionModal';

import { setItem } from 'utils/storage';

// Images
import backIcon from '../../assets/images/back-icon.svg';
import supportIcon from 'assets/images/support.svg';
import notificationIcon from 'assets/images/notification.svg';
import settings from 'assets/images/settings.svg';
import expandIcon from 'assets/images/expand.svg';
import expandBlackIcon from 'assets/images/expand-black.svg';

import { NetworksAction } from 'redux/slices/networksSlice';
import { WalletAction } from 'redux/slices/walletSlice';

import { STORAGE_KEYS } from '../../constants';

import './header.scss';
import { GlobalAction } from 'redux/slices/globalSlice';
import { getNetwork } from 'redux/selectors';

const Header = ({
  changeBackIcon,
  outline,
  containerClassName = '',
  className = '',
  headerTitle = '',
  rightButtonLink = '',
  rightIcon = '',
  rightIconLink = '',
  support,
  notification,
  onClickBack,
  networkHeader,
  onSettingClick,
  expand,
  hideBack,
  expandBlack
}) => {

  const navigate = useNavigate();
  const location = useLocation();
  const dispatch = useDispatch();

  const networks = useSelector(state => state.networks);
  const wallet = useSelector(state => state.wallet);
  const selectedNetwork = useSelector(getNetwork);
  const [isShowNetworkList, setIsShowNetworkList] = useState(false);

  if (outline) className += ' button-outline';

  const changeNetwork = async choosedNetwork => {

    await setItem(
      STORAGE_KEYS.SELECTED_NETWORK,
      JSON.stringify(choosedNetwork),
    );
    dispatch(NetworksAction.saveSelectedNetworkObject(choosedNetwork));
    dispatch(WalletAction.resetData(wallet.walletDetail, choosedNetwork));

    setTimeout(() => {
      dispatch(
        GlobalAction.showAlert(
          Strings.NETWORK_SWITCH_TITLE,
          `${Strings.NETWORK_CHANGE_SUCCESS_MSG}${choosedNetwork.networkName}`,
        ),
      );
    }, 400);
  };

  const expandView= () => {
    window.open('index.html?screen='+location.pathname, '_blank');
  };

  return (
    <React.Fragment>
      <div className={ `header ${containerClassName}` }>
        {!hideBack ?
          <button
            onClick={ () => {
              navigate(-1);
              if (onClickBack) {
                onClickBack();
              }
            } }
            className={ `back-icon ${className}` }
          >
            <img src={ changeBackIcon ? changeBackIcon : backIcon } alt="Back" />
          </button>
          :
          null
        }
        {
          networkHeader ?
            <div className="header-network" onClick={ () => setIsShowNetworkList(true) }>
              <p>{Strings.CURRENT_NETWORK}</p>
              <h6>{selectedNetwork.networkName}</h6>
            </div>
            :
            <div className="headerTitleStyle">
              {headerTitle}
            </div>
        }
        

        <div>
          {expand || expandBlack ?
            <a onClick={ (e) => {
              e.preventDefault();
              expandView();
            } } target="_blank" rel="noreferrer"  data-testid="header-right-button" className="header-icon expand-icon">
              <img src={ !expandBlack ? expandIcon: expandBlackIcon } alt="settings" />
            </a> : null
          }
          {onSettingClick &&
            <Link to={ { pathname: rightButtonLink } } onClick={ onSettingClick } data-testid="header-right-button" className="header-icon">
              <img src={ settings } alt="settings" />
            </Link>
          }
          {notification &&
            <Link to={ { pathname: '' } } className={ 'header-icon disable' }>
              <img src={ notificationIcon } alt="" />
              <span className="badge"></span>
            </Link>
          }
          {support &&
            <Link to={ { pathname: '' } }
              onClick={ (e) => {
                e.preventDefault();
                navigate('/support', {
                  state: {
                    supportType: 'contactus',
                    title: Strings.CONTACT_US,
                  }
                });
              } }
              className={ 'header-icon' }>
              <img src={ supportIcon } alt="" />
              {/* <span className="badge"></span> */}
            </Link>
          }
          {rightIcon &&
            <Link to={ rightIconLink } className={ 'header-icon' }>
              <img src={ rightIcon } alt="" />
            </Link>
          }
        </div>
      </div>
      {isShowNetworkList && (
        <NetworkSelectionModal
          isOpen={ isShowNetworkList }
          closeModal={ (_e) => setIsShowNetworkList(false) }
          onSelectNetwork={ item => {
            setIsShowNetworkList(false);
            if (item.chainId !== selectedNetwork.chainId) {
              changeNetwork(item);
            }
          } }
        />
      )}
    </React.Fragment>
  );
};

Header.propTypes = {
  outline: PropTypes.string,
  containerClassName: PropTypes.string,
  className: PropTypes.string,
  headerTitle: PropTypes.string,
  rightButtonLink: PropTypes.string,
  rightIcon: PropTypes.string,
  rightIconLink: PropTypes.string,
  support: PropTypes.bool,
  notification: PropTypes.bool,
  onClickBack: PropTypes.func,
  changeBackIcon: PropTypes.string,
  networkHeader: PropTypes.bool,
  onSettingClick: PropTypes.func,
  expand: PropTypes.bool,
  hideBack: PropTypes.bool,
  expandBlack: PropTypes.bool,
};

export default Header;
