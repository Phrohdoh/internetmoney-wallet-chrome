import React from 'react';
import PropTypes from 'prop-types';

import { displayAddress } from 'utils/displayAddress';

import './account-listitem.scss';

const ReedemAccountListCell = ({
  className = '',
  dataTestid = '',
  ...props
}) => {
  
  const { item, index } = props;
  
  return (
    <React.Fragment>
      <div data-testid={ dataTestid } onClick={ () => props.onSelectAccount(item, index) } className={ `listitem round-listitem noselect ${className}` }>
        <div className="body">
          <h6>
            {item.name}
          </h6>
          <span className="small-text text-eclipse-style text-theme-color">
            {displayAddress(item.publicAddress)}
          </span>
        </div>        
      </div>
    </React.Fragment>
  );
};

ReedemAccountListCell.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  className: PropTypes.string,  
  onSelectAccount: PropTypes.func.isRequired,
  dataTestid: PropTypes.string.isRequired,
};

export default ReedemAccountListCell;
