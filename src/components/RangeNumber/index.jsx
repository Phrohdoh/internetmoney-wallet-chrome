import React from 'react';
import PropTypes from 'prop-types';

import './range-number.scss';

import { Strings } from 'resources';

// Images
import minusIcon from 'assets/images/minus.svg';
import plusIcon from 'assets/images/plus.svg';

export default function RangeNumber (props) {

  const onChange = (e) => {
    props.onChange(e.target.value);
  }

  return (
    <React.Fragment>
      <div className="counter-frm">
        <div className="form-header">
          <div className="body">
            <label className="label">{props.title}</label>
          </div>
          {props.value && (
            <div className="right text-right">
              <label className="label">{Strings.ESTIMATE_TEXT} {props.value.toString(6)}</label>
            </div>
          )}
        </div>
        <div className="counter-control">
          <button data-testid={ `gas-fee-minus-button-${props.id}` } className="circle-btn" onClick={ () => props.onClickDecrement() }>
            <img src={ minusIcon } alt="Decrement" />
          </button>
          <div className="display-content">
            <h6>
              <input 
                type="number" value={props.valueText} onChange={onChange} name={ props.name }
              />
            </h6>
          </div>
          <button data-testid={ `gas-fee-add-button-${props.id}` } className="circle-btn" onClick={ () => props.onClickIncrement() }>
            <img src={ plusIcon } alt="Increment" />
          </button>
        </div>
        {props.isShowError && (
          <span className="error">
            {props.error}
          </span>
        )}
      </div>
    </React.Fragment>
  );
}

RangeNumber.propTypes = {
  title: PropTypes.string,
  value: PropTypes.any,
  valueText: PropTypes.string,
  onClickDecrement: PropTypes.func,
  onClickIncrement: PropTypes.func,
  onChange: PropTypes.func,
  name: PropTypes.string,
  id: PropTypes.string,
  isShowError: PropTypes.bool,
  error: PropTypes.string,
};
