import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import './transactions-filter.scss';

import { Strings } from 'resources';

// Components
import Button from 'components/Button';
import Dropdown from 'components/Dropdown';

// Images
import sendIcon from 'assets/images/send-icon.svg';
import receiveIcon from 'assets/images/receive-icon.svg';
import swapIcon from 'assets/images/swap-icon.svg';
import claimIcon from 'assets/images/time3d.png';
import dappIcon from 'assets/images/dapp-transections.svg';
import checkIcon from 'assets/images/check.svg';

export default function TransactionsFilter (props) {
  const [isShowAccounts, setIsShowAccounts] = useState(false);
  
  const { selectedFilterAccount, accountName, selectedFiltersArray } = props;
  const [arrSelectedType, setArrSelectedType] = useState([selectedFiltersArray]);

  const [selectedAccount, setSelectedAccount] = useState(selectedFilterAccount);
  const [selectedAccountName, setSelectedAccountName] = useState(accountName);
  const [uniqueTransAddress, setUniqueTransAddress] = useState([]);

  const accounts = useSelector(state => state.account); 

  const filterOptions = [
    { title: Strings.SEND_TRANSACTION, type: 'send' },
    { title: Strings.SWAP_TRANSACTION, type: 'swap' },
    { title: Strings.CLAIM_TRANSACTION, type: 'claim' },
    { title: Strings.DAPP_TRANSACTION, type: 'dapp' }
  ];

  useEffect(() => {
    filterUniqueAccountList();
  }, []);

  const filterUniqueAccountList = () => {
    const uniqueFromAddress = [
      ...new Set(
        props.transactionsList.map(item => item.transactionDetail.fromAddress),
      ),
    ];
    setUniqueTransAddress(uniqueFromAddress);
  };

  const getTransImage = item => {
    if (item.type === 'send') {
      return sendIcon;
    } else if (item.type === 'swap') {
      return swapIcon;
    } else if (item.type === 'claim') {
      return claimIcon;
    } else if (item.type === 'dapp') {
      return dappIcon;
    } else {
      return receiveIcon;
    }
  };

  const getAccountName = (item, index) => {
    const nameObj = accounts.accountList.find(
      account => account.publicAddress.toLowerCase() === item.toLowerCase(),
    );
    if (!nameObj) {
      return ` ${Strings.ACCOUNT} ${index + 1}`;
    } else {
      return nameObj.name;
    }
  };

  const updateSelectedTypes = (item) => {
    if (arrSelectedType.includes(item.type)) {
      setArrSelectedType(arrSelectedType.filter((x) => x !== item.type));
    } else {
      setArrSelectedType([...arrSelectedType, item.type]);
    }
  };

  const FilterListItem = (props) => {
    const { item } = props;
    return (
      <div className={ 'filter-item' } data-testid={ `filter-by-${item.type}` } onClick={ () => {
        updateSelectedTypes(item);
      } }>
        <div className="left">
          <span className={ `checkbox-style ${arrSelectedType.includes(item.type) ? 'checkbox-selected' : null}` }>
            {arrSelectedType.includes(item.type) ? <img src={ checkIcon } alt="" />: null }
          </span>
        </div>
        <div className="body">
          <div className="icon">
            <img src={ getTransImage(item) } alt={ item.title } />
          </div>
          <h6>{item.title}</h6>
        </div>        
      </div>
    );
  };

  const filterList = (props) => {
    return (
      !!props.lists &&
      <div className="filterlist-style">
        {
          props.lists.map((item, index) => (
            <FilterListItem
              key={ index }
              item={ item }
              index={ index }
            />
          ))
        }
      </div>
    );
  };

  const renderFilterView = () => {
    const accName = selectedAccountName ? selectedAccountName : Strings.ALL;
    return (
      !isShowAccounts && (
        <React.Fragment>
          <div className="modal-header">
            <h6 className="text-center">{Strings.FILTER_UPPERCASE}</h6>
          </div>         
          <React.Fragment>
            <Dropdown
              dataTestid="send-token-choose-account-button"
              label={ Strings.CHOOSE_ACCOUNTS_UPPERCASE }
              value={ accName }
              onClick={ (e) => { e.preventDefault(); setIsShowAccounts(true); } }
            />
            {filterList({ lists: filterOptions})}
            <Button
              onClick={ (e) => 
              {
                e.preventDefault();
                props.onSelectFilter(
                  arrSelectedType,
                  selectedAccount ? selectedAccount.address : selectedAccount,
                  selectedAccountName,
                );
              }
              }
              label={ Strings.APPLY_UPPERCASE }
              className="btn-full"
            />
          </React.Fragment>
        </React.Fragment>
      )
    );
  };

  const renderAccountListCell = (item, index) => {
    const accName = getAccountName(item, index);
    return (
      <div key={ index } data-testid={ `filter-option-${item}-btn` } onClick={ () => 
      {
        setSelectedAccount({ accountName: accName, address: item });
        setSelectedAccountName(accName);
        setIsShowAccounts(false);
      } } 
      className={ 'listitem-control round-listitem noselect' }
      >
        <div className="body">
          <h6>
            {accName}
          </h6>
        </div>
      </div>
    );
  };

  const renderAllCell = (item, index) => {
    return (
      <div key={ index } data-testid={ 'filter-select-all-account-btn' } 
        onClick={ () => {
          setIsShowAccounts(false);
          setSelectedAccountName(undefined);
          setSelectedAccount(undefined);
        } }
        className={ 'listitem-control round-listitem noselect' }
      >
        <div className="body">
          <h6>{Strings.ALL}</h6>
        </div>
      </div>
    );
  };

  const renderFilterChooseAccount = () => {
    return (
      isShowAccounts &&  (
        <React.Fragment>
          <div className="modal-header">
            <h6 className="text-center">{Strings.CHOOSE_ACCOUNTS_UPPERCASE}</h6>
          </div>         
          <div className="modal-body">
            {renderAllCell()}
            {              
              uniqueTransAddress.map((item, index) => (
                renderAccountListCell(item, index)
              ))
            }
          </div>
        </React.Fragment>
      )
    );
  };

  return (
    <React.Fragment>
      { renderFilterView() }
      { renderFilterChooseAccount() }
    </React.Fragment>
  );
}

TransactionsFilter.propTypes = {
  applyFilterSuccess: PropTypes.func,
  title: PropTypes.string,
  item: PropTypes.string,
  index: PropTypes.number,
  lists: PropTypes.array,
  selectedFilterType: PropTypes.array,
  selectedFilterAccount: PropTypes.string,
  accountName: PropTypes.string,
  transactionsList: PropTypes.array.isRequired,
  onSelectFilter: PropTypes.func,
  selectedFiltersArray: PropTypes.array,
};
