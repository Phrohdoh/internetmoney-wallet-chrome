import React from 'react';
import PropTypes from 'prop-types';

import './listitem.scss';
import rightArrow from 'assets/images/right-arrow.svg';

const Listitem = (props) => {

  return (    
    <React.Fragment>
      <div className="list-item">
        {
          props.icon ?
            <div className="left">
              <img src={ props.icon } alt={ props.title } />
            </div> 
            : null 
        }
        <div className="body">
          <h6>{props.title}</h6>
        </div>
        {
          props.value ?
            <div className="right text-right">
              <div className="price-value text-theme-color">{props.value}</div>
            </div>
            : null 
        }
        <i className="ls-icon">
          <img src={ rightArrow } alt="Arrow" />
        </i>
      </div>  
    </React.Fragment> 
  );
};

Listitem.propTypes = {
  icon: PropTypes.string,
  title: PropTypes.string,
  value: PropTypes.string,  
};

export default Listitem;
