import Eth, { ledgerService } from "@ledgerhq/hw-app-eth";
import TransportWebHID from "@ledgerhq/hw-transport-webhid";
import TrezorConnect from "@trezor/connect-web";
import { ethers } from "ethers";
import { Strings } from "resources";
import { transformTypedData } from "@trezor/connect-plugin-ethereum";
import { Web3 } from "web3";
const ethUtil = require("ethereumjs-util");

// import { Chain, Common, Hardfork } from "@ethereumjs/common";
// import {
//   TransactionFactory,
//   FeeMarketEIP1559Transaction,
// } from "@ethereumjs/tx";

// const {
//   TransactionFactory,
//   FeeMarketEIP1559Transaction,
// } = require("@ethereumjs/tx");
// const { default: Common, Chain, Hardfork } = require("@ethereumjs/common");

// const commonEIP1559 = new Common({
//   chain: Chain.Mainnet,
//   hardfork: Hardfork.London,
// });

import { createValidRawTx } from "./web3Layer";
import IntegerUnits from "utils/IntegerUnits";
import Logger from "utils/logger";
import { getErrorResponse } from "utils/errors";

const getDerivationPath = (index, derivationPathPattern) => {
  derivationPathPattern = derivationPathPattern.toLowerCase();
  if (derivationPathPattern === 'ledgerlive') {
    return `m/44'/60'/${index}'/0/0`;
  }
  if (derivationPathPattern === 'legacy') {
    return `m/44'/60'/0'/${index}`;
  }
  if (derivationPathPattern === 'bip44') {
    return `m/44'/60'/0'/0/${index}`;
  }
};

const replace = (value, callback) => {
  if (value === undefined) {
    return undefined;
  }
  return callback(value);
}

const replaceWithInt = (value) => replace(
  value,
  (value) => parseInt(value, 10),
);

const replaceWithIntegerUnits = (value, decimals) => replace(
  value,
  (value) => new IntegerUnits(value, decimals),
);

const serializeReceipt = (receipt) => ({
  ...receipt,
  blockNumber: replaceWithInt(receipt.blockNumber),
  cumulativeGasUsed: replaceWithIntegerUnits(receipt.cumulativeGasUsed, 0),
  effectiveGasPrice: replaceWithIntegerUnits(receipt.effectiveGasPrice, 18),
  gasUsed: replaceWithIntegerUnits(receipt.gasUsed, 0),
  status: replaceWithInt(receipt.status),
  transactionIndex: replaceWithInt(receipt.transactionIndex),
  type: replaceWithInt(receipt.type),
  logs: receipt.logs.map(log => ({
    ...log,
    blockNumber: replaceWithInt(log.blockNumber),
    logIndex: replaceWithInt(log.logIndex),
    transactionIndex: replaceWithInt(log.transactionIndex),
  })),
});

export const handleEtherConnectionErrors = (error) => {
  const message = error.message ?? error.error;
  if (message == Strings.LEDGER_ERROR_06D00) {
    return { success: false, error: Strings.PLEASE_OPEN_DESIRED_APP };
  } else if (
    message == Strings.LEDGER_ERROR_06B0C ||
    message == Strings.DEVICE_IS_ALREADY_OPEN
  ) {
    return { success: false, error: Strings.DEVICE_CONNECTED_BUT_LOCKED };
  } else if (message == Strings.LEDGER_ERROR_06A15) {
    return { success: false, error: Strings.IN_WRONG_APP_OPEN_ETHEREUM };
  } else if (message == Strings.LEDGER_ERROR_06511) {
    return { success: false, error: Strings.USER_NOT_IN_APP_IN_HOME_SCREEN };
  }
  return { success: false, error: message };
};

export const getEtherConnection = async () => {
  try {
    const transport = await TransportWebHID.create();
    const etherConnection = new Eth(transport);
    return { success: true, etherConnection: etherConnection, transport };
  } catch (err) {
    Logger.error('Error getting ether connection');
    console.error(err);
    return handleEtherConnectionErrors(err);
  }
};

export const getListOfAccounts = async (fromIndex, toIndex, derivationPathPattern) => {
  try {
    const etherConnection = await getEtherConnection();
    if (!etherConnection.success) throw new Error(etherConnection.error);

    try {
      const result = [];
      for (let i = fromIndex; i <= toIndex; i++) {
        result.push(
          await etherConnection.etherConnection.getAddress(
            getDerivationPath(i, derivationPathPattern),
            false
          )
        );
      }
      const ledgerAccountList = [];
      for (let i = 0; i < result.length; i++) {
        ledgerAccountList.push(result[i].address);
      }
      return { success: true, hardwareAccountList: ledgerAccountList };
    } catch (err) {
      throw err;
    } finally {
      etherConnection.transport.close();
    }
  } catch (err) {
    Logger.error('Error in getListOfAccounts', {
      fromIndex,
      toIndex,
      derivationPathPattern
    });
    console.error(err);
    return handleEtherConnectionErrors(err);
  }
};

// I will be writing logic for this, till then use this as skeleton
export const testIfPublicAddressMatched = async (isHardwareCode) => {
  try {
    const { firstAccount, accountIndex, derivationPathPattern } = getHardwareDeviceDetails(isHardwareCode);

    const deviceFirstAccount = await getListOfAccounts(0, 0, derivationPathPattern);
    if (!deviceFirstAccount.success) throw new Error(deviceFirstAccount.error);

    if (
      firstAccount.toLowerCase() ==
      deviceFirstAccount.hardwareAccountList[0].toLowerCase()
    ) {
      return { success: true, accountIndex: accountIndex };
    }

    throw new Error(Strings.HARDWARE_PUBLIC_ADDRESS_NOT_MATCH);
  } catch (err) {
    Logger.error('Error in testIfPublicAddressMatched', {
      isHardwareCode
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const getHardwareDeviceAccountObject = async (
  accountName,
  whichDevice,
  accountIndex,
  derivationPathPattern,
) => {
  try {
    const firstAccount = await getListOfAccounts(0, 0, derivationPathPattern);
    const importAccount = await getListOfAccounts(accountIndex, accountIndex, derivationPathPattern);
    if (!firstAccount.success || !importAccount.success) {
      throw {
        success: false,
        error: firstAccount.error ?? importAccount.error,
      };
    }
    return {
      success: true,
      accountObject: {
        publicAddress: importAccount.hardwareAccountList[0],
        isHardware: (
          whichDevice +
          "/" +
          accountIndex +
          "/" +
          firstAccount.hardwareAccountList[0] +
          "/" +
          derivationPathPattern
        ).toLowerCase(),
        name: accountName,
      },
    };
  } catch (err) {
    Logger.error('Error in getHardwareDeviceAccountObject', {
      whichDevice,
      derivationPathPattern
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const testForValidateConnectionInLedger = async (isHardwareCode) => {
  try {
    const isPublicAddressValid = await testIfPublicAddressMatched(
      isHardwareCode
    );

    if (!isPublicAddressValid.success) {
      throw new Error(isPublicAddressValid.error);
    }

    return {
      success: true,
      accountIndex: isPublicAddressValid.accountIndex,
    };
  } catch (err) {
    Logger.error('Error in testForValidateConnectionInLedger', {
      isHardwareCode,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const signTxnInLedger = async (
  networkDetails,
  txnObject,
  isHardwareCode
) => {
  try {
    const isValid = await testForValidateConnectionInLedger(isHardwareCode);
    if (isValid.success) {
      const etherConnection = (await getEtherConnection()).etherConnection;
      try {
        const { derivationPathPattern } = getHardwareDeviceDetails(isHardwareCode);
        const signPath = getDerivationPath(isValid.accountIndex, derivationPathPattern);
        const web3 = new Web3(networkDetails.rpc);
        const validateTx = createValidRawTx(networkDetails.txnType, txnObject);
        const nonce = await web3.eth.getTransactionCount(
          validateTx?.txnObject.from,
          "latest"
        );
        const transaction = parseInt(networkDetails.txnType) == 0
          ? {
            to: validateTx?.txnObject.to,
            gasPrice: validateTx.txnObject.gasPrice,
            gasLimit: validateTx?.txnObject.gas,
            chainId: networkDetails.chainId,
            nonce: nonce,
            data: validateTx?.txnObject.data,
            value: validateTx?.txnObject.value ?? "0x00",
          }
          : {
            type: 2,
            to: validateTx?.txnObject.to,
            nonce: nonce,
            maxFeePerGas: validateTx.txnObject.maxFeePerGas,
            maxPriorityFeePerGas: validateTx.txnObject.maxPriorityFeePerGas,
            gasLimit: validateTx?.txnObject.gas,
            chainId: networkDetails.chainId,
            data: validateTx?.txnObject.data,
            value: validateTx?.txnObject.value ?? "0x00",
          };
        const unsignedTx = ethers.utils.serializeTransaction(transaction).substring(2);
        let signature;
        try {
          const resolution = await ledgerService.resolveTransaction(unsignedTx);
          signature = await etherConnection.signTransaction(
            signPath,
            unsignedTx,
            resolution
          );
        } catch (err) {
          Logger.error('Error in signTxnInLedger', {
            isHardwareCode,
          });
          console.error(err);
          if (
            err.message ===
            "Ledger device: Condition of use not satisfied (denied by the user?) (0x6985)"
          ) {
            return { status: false, error: err.message };
          }
          try {
            signature = await etherConnection.signTransaction(
              signPath,
              unsignedTx
            );
          } catch (err) {
            Logger.error('Error in signTxnInLedger', {
              isHardwareCode,
            });
            console.error(err);
            return { status: false, error: err.message };
          }
        }
        signature.r = "0x" + signature.r;
        signature.s = "0x" + signature.s;
        signature.v = parseInt(signature.v, 16);
        signature.from = validateTx?.txnObject.from;
        const signedTx = ethers.utils.serializeTransaction(
          transaction,
          signature
        );
        let receipt = await web3.eth.sendSignedTransaction(signedTx);
        receipt = serializeReceipt(receipt);
        return {
          success: true,
          receipt,
        };
      } catch (err) {
        Logger.error('Error in signTxnInLedger', {
          isHardwareCode,
        });
        console.error(err);
        return getErrorResponse(err);
      } finally {
        etherConnection.transport.close();
      }
    } else {
      return isValid;
    }
  } catch (err) {
    Logger.error('Error in signTxnInLedger', {
      isHardwareCode,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const formateToSignature = (result) => {
  var v = result["v"] - 27;
  v = v.toString(16);
  if (v.length < 2) {
    v = "0" + v;
  }
  return "0x" + result["r"] + result["s"] + v;
};

export const personalSignPayloadObjectInLedger = async (
  payload,
  isHardwareCode
) => {
  try {
    const isValid = await testForValidateConnectionInLedger(isHardwareCode);
    if (isValid.success) {
      const etherConnection = (await getEtherConnection()).etherConnection;
      try {
        const web3 = new Web3();
        const { derivationPathPattern } = getHardwareDeviceDetails(isHardwareCode);
        const signPath = getDerivationPath(isValid.accountIndex, derivationPathPattern);
        const signMsg = web3.utils.hexToUtf8(payload.params[0]);

        const signatureResult = await etherConnection.signPersonalMessage(
          signPath,
          Buffer.from(signMsg).toString("hex")
        );
        const signatureFromLedger = formateToSignature(signatureResult);
        return {
          success: true,
          signMsg: signMsg,
          signResult: signatureFromLedger,
          method: "sign",
        };
      } catch (err) {
        Logger.error('Error in personalSignPayloadObjectInLedger', {
          isHardwareCode,
        });
        console.error(err);
        return getErrorResponse(err);
      } finally {
        etherConnection.transport.close();
      }
    } else {
      return isValid;
    }
  } catch (err) {
    Logger.error('Error in personalSignPayloadObjectInLedger', {
      isHardwareCode,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const ethSignPayloadObjectInLedger = async (payload, isHardwareCode) => {
  try {
    const isValid = await testForValidateConnectionInLedger(isHardwareCode);
    if (isValid.success) {
      const etherConnection = (await getEtherConnection()).etherConnection;
      try {
        const web3 = new Web3();
        const { derivationPathPattern } = getHardwareDeviceDetails(isHardwareCode);
        const signPath = getDerivationPath(isValid.accountIndex, derivationPathPattern);
        let signMsg = payload.params[1];
        let signResult;

        try {
          signMsg = web3.utils.hexToUtf8(payload.params[1]);

          try {
            const signatureResult = await etherConnection.signPersonalMessage(
              signPath,
              Buffer.from(signMsg).toString("hex")
            );
            const signatureFromLedger = formateToSignature(signatureResult);
            signResult = signatureFromLedger;
          } catch (err) {
            Logger.error('Error in ethSignPayloadObjectInLedger', {
              isHardwareCode,
            });
            console.error(err);
            return getErrorResponse(err);
          }
        } catch {
          try {
            const signatureResult = await etherConnection.signPersonalMessage(
              signPath,
              Buffer.from(signMsg).toString("hex")
            );

            const signatureFromLedger = formateToSignature(signatureResult);
            signResult = signatureFromLedger;
          } catch (err) {
            Logger.error('Error in ethSignPayloadObjectInLedger', {
              isHardwareCode,
            });
            console.error(err);
            return getErrorResponse(err);
          }
          // const signingKey = new ethers.utils.SigningKey(privateKey.privateKey);
          // const sigParams = await signingKey.signDigest(
          //   ethers.utils.arrayify(payload.params[1])
          // );
          // signResult = await ethers.utils.joinSignature(sigParams);
        }

        return {
          success: true,
          signMsg: signMsg,
          signResult: signResult,
          method: "sign",
        };
      } catch (err) {
        Logger.error('Error in ethSignPayloadObjectInLedger', {
          isHardwareCode,
        });
        console.error(err);
        return getErrorResponse(err);
      } finally {
        etherConnection.transport.close();
      }
    } else {
      return isValid;
    }
  } catch (err) {
    Logger.error('Error in ethSignPayloadObjectInLedger', {
      isHardwareCode,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const ethSignTransactionPayloadObjectInLedger = async (
  payload,
  networkDetails,
  isHardwareCode
) => {
  try {
    const isValid = await testForValidateConnectionInLedger(isHardwareCode);
    if (isValid.success) {
      const etherConnection = (await getEtherConnection()).etherConnection;
      try {
        const txnObject = payload["params"][0];
        const { derivationPathPattern } = getHardwareDeviceDetails(isHardwareCode);
        const signPath = getDerivationPath(isValid.accountIndex, derivationPathPattern);
        const web3 = new Web3(networkDetails.rpc);
        const whichNetwork = txnObject.gasPrice ? 0 : 2;
        const validateTx = createValidRawTx(whichNetwork, txnObject);
        const nonce =
          txnObject.nonce ??
          (await web3.eth.getTransactionCount(
            validateTx?.txnObject.from,
            "latest"
          ));
        const gasPrice =
          parseInt(whichNetwork) == 0
            ? validateTx.txnObject.gasPrice
            : validateTx.txnObject.maxFeePerGas;
        const transaction = {
          to: validateTx?.txnObject.to,
          gasPrice: gasPrice,
          gasLimit: validateTx?.txnObject.gas,
          chainId: networkDetails.chainId,
          nonce: nonce,
          data: validateTx?.txnObject.data ?? "0x00",
          value: validateTx?.txnObject.value ?? "0x00",
        };
        const unsignedTx = ethers.utils
          .serializeTransaction(transaction)
          .substring(2);
        let signature;
        try {
          const resolution = await ledgerService.resolveTransaction(unsignedTx);
          signature = await etherConnection.signTransaction(
            signPath,
            unsignedTx,
            resolution
          );
        } catch (err) {
          if (
            err.message ===
            "Ledger device: Condition of use not satisfied (denied by the user?) (0x6985)"
          ) {
            Logger.error('Error in ethSignTransactionPayloadObjectInLedger', {
              isHardwareCode,
            });
            console.error(err);
            return { status: false, error: err.message };
          }
          try {
            signature = await etherConnection.signTransaction(
              signPath,
              unsignedTx
            );
          } catch (err) {
            Logger.error('Error in ethSignTransactionPayloadObjectInLedger', {
              isHardwareCode,
            });
            console.error(err);
            return { status: false, error: err.message };
          }
        }
        signature.r = "0x" + signature.r;
        signature.s = "0x" + signature.s;
        signature.v = parseInt(signature.v, 16);
        signature.from = validateTx?.txnObject.from;
        const signedTx = ethers.utils.serializeTransaction(
          transaction,
          signature
        );

        return {
          success: true,
          signResult: signedTx,
          method: "sign",
        };
      } catch (err) {
        Logger.error('Error in ethSignTransactionPayloadObjectInLedger', {
          isHardwareCode,
        });
        console.error(err);
        return getErrorResponse(err);
      } finally {
        etherConnection.transport.close();
      }
    } else {
      return isValid;
    }
  } catch (err) {
    Logger.error('Error in ethSignTransactionPayloadObjectInLedger', {
      isHardwareCode,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const ethSignTypedDataPayloadObjectInLedger = async (
  payload,
  isHardwareCode
) => {
  try {
    const isValid = await testForValidateConnectionInLedger(isHardwareCode);
    if (isValid.success) {
      const etherConnection = (await getEtherConnection()).etherConnection;
      const signMsg = JSON.parse(payload.params[1]);
      const { derivationPathPattern } = getHardwareDeviceDetails(isHardwareCode);
      const signPath = getDerivationPath(isValid.accountIndex, derivationPathPattern);
      try {
        const signatureResult = await etherConnection.signEIP712Message(
          signPath,
          signMsg
        );
        const signatureFromLedger = formateToSignature(signatureResult);

        return {
          success: true,
          signMsg: signMsg,
          signResult: signatureFromLedger,
          method: "sign",
        };
      } catch (err) {
        if (err.message.includes("INS_NOT_SUPPORTED")) {
          const { domain_separator_hash, message_hash } = transformTypedData(
            signMsg,
            true
          );

          const signatureResult = await etherConnection.signEIP712HashedMessage(
            signPath,
            domain_separator_hash,
            message_hash
          );
          const signatureFromLedger = formateToSignature(signatureResult);

          return {
            success: true,
            signMsg: signMsg,
            signResult: signatureFromLedger,
            method: "sign",
          };
        }
        throw err;
      } finally {
        etherConnection.transport.close();
      }
    } else {
      return isValid;
    }
  } catch (err) {
    Logger.error('Error in ethSignTypedDataPayloadObjectInLedger', {
      isHardwareCode,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// ----------------------------------
// TREZOR hardware wallet connect
// ----------------------------------

export const trezorManifest = () => {
  TrezorConnect.manifest({
    email: "dev@internetmoney.io",
    appUrl: "chrome-extension://ckklhkaabbmdjkahiaaplikpdddkenic/index.html`",
  });
};

export const getListOfTrezorAccounts = async (fromIndex, toIndex, derivationPathPattern) => {
  try {
    trezorManifest();
    let bundleArray = [];

    for (let i = fromIndex; i <= toIndex; i++) {
      bundleArray.push({
        path: getDerivationPath(i, derivationPathPattern),
        showOnTrezor: false,
      });
    }

    const result = await TrezorConnect.ethereumGetAddress({
      bundle: bundleArray,
    });
    if (!result.success) throw new Error(result.payload.error);

    return {
      success: true,
      hardwareAccountList: result.payload.map((item) => item.address),
    };
  } catch (err) {
    Logger.error('Error in getListOfTrezorAccounts', {
      derivationPathPattern,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const getTrezorHardwareDeviceAccountObject = async (
  accountName,
  whichDevice,
  trezorFirstAddress,
  accountIndex,
  accountAddress,
  derivationPathPattern,
) => {
  try {
    return {
      success: true,
      accountObject: {
        publicAddress: accountAddress,
        isHardware: (
          whichDevice +
          "/" +
          accountIndex +
          "/" +
          trezorFirstAddress +
          "/" +
          derivationPathPattern
        ).toLowerCase(),
        name: accountName,
      },
    };
  } catch (err) {
    Logger.error('Error in getTrezorHardwareDeviceAccountObject', {
      whichDevice,
      derivationPathPattern
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const signTxnInTrezor = async (
  networkDetails,
  txnObject,
  isHardwareCode
) => {
  try {
    trezorManifest();

    const { accountIndex, derivationPathPattern } = getHardwareDeviceDetails(isHardwareCode);

    const signPath = getDerivationPath(accountIndex, derivationPathPattern);
    const web3 = new Web3(networkDetails.rpc);
    const validateTx = createValidRawTx(networkDetails.txnType, txnObject);

    const nonce = await web3.eth.getTransactionCount(
      validateTx?.txnObject.from,
      "latest"
    );

    const _normalize = (buf) => {
      return ethUtil.bufferToHex(buf).toString();
    };

    const transaction = parseInt(networkDetails.txnType) == 0
      ? {
        to: validateTx?.txnObject.to,
        gasPrice: validateTx.txnObject.gasPrice,
        gasLimit: validateTx?.txnObject.gas,
        chainId: networkDetails.chainId,
        nonce: _normalize(parseInt(nonce)),
        data: validateTx?.txnObject.data ?? "0x00",
        value: validateTx?.txnObject.value ?? "0x00",
      }
      : {
        to: validateTx?.txnObject.to,
        nonce: _normalize(parseInt(nonce)),
        maxFeePerGas: validateTx.txnObject.maxFeePerGas,
        maxPriorityFeePerGas: validateTx.txnObject.maxPriorityFeePerGas,
        gasLimit: validateTx?.txnObject.gas,
        chainId: networkDetails.chainId,
        data: validateTx?.txnObject.data,
        value: validateTx?.txnObject.value ?? "0x00",
      };

    const signature = await TrezorConnect.ethereumSignTransaction({
      path: signPath,
      transaction: transaction,
    });

    if (parseInt(networkDetails.txnType) == 2) {
      transaction.type = 2;
    }

    if (!signature.success) throw new Error(signature.payload.error);

    signature.payload.v = parseInt(signature.payload.v, 16);
    signature.payload.from = validateTx?.txnObject.from;
    const signedTx = ethers.utils.serializeTransaction(
      transaction,
      signature.payload
    );
    let receipt = await web3.eth.sendSignedTransaction(
      signedTx.toString("hex")
    );
    receipt = serializeReceipt(receipt);
    return {
      success: true,
      receipt,
    };
  } catch (err) {
    Logger.error('Error in signTxnInTrezor', {
      isHardwareCode,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const personalSignPayloadObjectInTrezor = async (
  payload,
  isHardwareCode
) => {
  try {
    trezorManifest();

    const { accountIndex, derivationPathPattern } = getHardwareDeviceDetails(isHardwareCode);

    const signPath = getDerivationPath(accountIndex, derivationPathPattern);
    const web3 = new Web3();
    const signMsg = web3.utils.hexToUtf8(payload.params[0]);

    const signatureResult = await TrezorConnect.ethereumSignMessage({
      path: signPath,
      message: signMsg,
    });
    if (!signatureResult.success) throw new Error(signatureResult.payload.error);

    return {
      success: true,
      signMsg: signMsg,
      signResult: "0x" + signatureResult.payload.signature,
      method: "sign",
    };
  } catch (err) {
    Logger.error('Error in personalSignPayloadObjectInTrezor', {
      isHardwareCode,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const ethSignPayloadObjectInTrezor = async (payload, isHardwareCode) => {
  try {
    trezorManifest();

    const web3 = new Web3();
    const { accountIndex, derivationPathPattern } = getHardwareDeviceDetails(isHardwareCode);

    const signPath = getDerivationPath(accountIndex, derivationPathPattern);
    let signMsg = payload.params[1];

    let signResult;

    try {
      signMsg = web3.utils.hexToUtf8(payload.params[1]);

      const signatureResult = await TrezorConnect.ethereumSignMessage({
        path: signPath,
        message: signMsg,
      });
      if (!signatureResult.success) throw new Error(signatureResult.payload.error);

      signResult = signatureResult.payload.signature;
    } catch (err) {
      Logger.error('Error in ethSignPayloadObjectInTrezor', {
        isHardwareCode,
      });
      console.error(err);
      const signatureResult = await TrezorConnect.ethereumSignMessage({
        path: signPath,
        message: signMsg,
      });
      if (!signatureResult.success) throw new Error(signatureResult.payload.error);

      signResult = signatureResult.payload.signature;
    }

    return {
      success: true,
      signMsg,
      signResult: "0x" + signResult,
      method: "sign",
    };
  } catch (err) {
    Logger.error('Error in ethSignPayloadObjectInTrezor', {
      isHardwareCode,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const ethSignTransactionPayloadObjectInTrezor = async (
  payload,
  networkDetails,
  isHardwareCode
) => {
  try {
    trezorManifest();

    const { accountIndex, derivationPathPattern } = getHardwareDeviceDetails(isHardwareCode);
    const signPath = getDerivationPath(accountIndex, derivationPathPattern);
    const txnObject = payload["params"][0];
    const web3 = new Web3(networkDetails.rpc);
    const whichNetwork = txnObject.gasPrice ? 0 : 2;
    const validateTx = createValidRawTx(whichNetwork, txnObject);

    const nonce =
      txnObject.nonce ??
      (await web3.eth.getTransactionCount(
        validateTx?.txnObject.from,
        "latest"
      ));

    const _normalize = (buf) => ethUtil.bufferToHex(buf).toString();

    const gasPrice =
      parseInt(whichNetwork) == 0
        ? validateTx.txnObject.gasPrice
        : validateTx.txnObject.maxFeePerGas;
    const transaction = {
      to: validateTx?.txnObject.to,
      gasPrice: gasPrice,
      gasLimit: validateTx?.txnObject.gas,
      chainId: networkDetails.chainId,
      nonce: _normalize(nonce),
      data: validateTx?.txnObject.data ?? "0x00",
      value: validateTx?.txnObject.value ?? "0x00",
    };
    const signature = await TrezorConnect.ethereumSignTransaction({
      path: signPath,
      transaction: transaction,
    });

    if (!signature.success) throw new Error(signature.payload.error);

    signature.payload.v = parseInt(signature.payload.v, 16);
    signature.payload.from = validateTx?.txnObject.from;
    const signedTx = ethers.utils.serializeTransaction(
      transaction,
      signature.payload
    );
    return {
      success: true,
      signResult: signedTx,
      method: "sign",
    };
  } catch (err) {
    Logger.error('Error in ethSignTransactionPayloadObjectInTrezor', {
      isHardwareCode,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const ethSignTypedDataPayloadObjectInTrezor = async (
  payload,
  isHardwareCode
) => {
  try {
    trezorManifest();

    const { accountIndex, derivationPathPattern } = getHardwareDeviceDetails(isHardwareCode);
    const signPath = getDerivationPath(accountIndex, derivationPathPattern);
    const signMsg = JSON.parse(payload.params[1]);
    const { domain_separator_hash, message_hash } = transformTypedData(
      signMsg,
      true
    );
    const signatureResult = await TrezorConnect.ethereumSignTypedData({
      path: signPath,
      data: signMsg,
      metamask_v4_compat: true,
      domain_separator_hash,
      message_hash,
    });
    if (!signatureResult.success) throw new Error(signatureResult.payload.error);

    return {
      success: true,
      signMsg: signMsg,
      signResult: signatureResult.payload.signature,
      method: "sign",
    };
  } catch (err) {
    Logger.error('Error in ethSignTypedDataPayloadObjectInTrezor', {
      isHardwareCode,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// ----------------------------
// (General Way of handling all type of wallet)
// ----------------------------

export const getHardwareDeviceDetails = (isHardware) => {
  try {
    if (!isHardware) throw new Error("Failed - Isn't Hardware Wallet");

    const splitData = isHardware.split("/");
    return {
      success: true,
      whichDevice: splitData[0],
      accountIndex: splitData[1],
      firstAccount: splitData[2],
      // default to bip44 for accounts imported before the derivation path was included
      derivationPathPattern: splitData[3] ?? 'bip44',
    };
  } catch (err) {
    Logger.error('Error in getHardwareDeviceDetails', {
      isHardware,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

const whichDevice = (deviceName) => {
  if (deviceName.toLowerCase() == "ledger") {
    return 1;
  } else if (deviceName.toLowerCase() == "trezor") {
    return 2;
  }
  return -1;
};

export const personalSignPayloadObjectInHDW = async (
  payload,
  isHardwareCode,
  deviceNum
) => {
  try {
    if (deviceNum == 1) {
      return await personalSignPayloadObjectInLedger(payload, isHardwareCode);
    } else if (deviceNum == 2) {
      return await personalSignPayloadObjectInTrezor(payload, isHardwareCode);
    }
  } catch (err) {
    Logger.error('Error in personalSignPayloadObjectInHDW', {
      isHardwareCode,
      deviceNum,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const ethSignPayloadObjectInHDW = async (
  payload,
  isHardwareCode,
  deviceNum
) => {
  try {
    if (deviceNum == 1) {
      return await ethSignPayloadObjectInLedger(payload, isHardwareCode);
    } else if (deviceNum == 2) {
      return await ethSignPayloadObjectInTrezor(payload, isHardwareCode);
    }
  } catch (err) {
    Logger.error('Error in ethSignPayloadObjectInHDW', {
      isHardwareCode,
      deviceNum,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const ethSignTransactionPayloadObjectInHDW = async (
  payload,
  networkDetails,
  isHardwareCode,
  deviceNum
) => {
  try {
    if (deviceNum == 1) {
      return await ethSignTransactionPayloadObjectInLedger(
        payload,
        networkDetails,
        isHardwareCode
      );
    } else if (deviceNum == 2) {
      return await ethSignTransactionPayloadObjectInTrezor(
        payload,
        networkDetails,
        isHardwareCode
      );
    }
  } catch (err) {
    Logger.error('Error in ethSignPayloadObjectInHDW', {
      isHardwareCode,
      deviceNum,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const ethSignTypedDataPayloadObjectInHDW = async (
  payload,
  isHardwareCode,
  deviceNum
) => {
  try {
    if (deviceNum == 1) {
      return await ethSignTypedDataPayloadObjectInLedger(
        payload,
        isHardwareCode
      );
    } else if (deviceNum == 2) {
      return await ethSignTypedDataPayloadObjectInTrezor(
        payload,
        isHardwareCode
      );
    }
  } catch (err) {
    Logger.error('Error in ethSignTypedDataPayloadObjectInHDW', {
      isHardwareCode,
      deviceNum,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const ethSignAuthRequestInHDW = async (
  payload,
  isHardwareCode,
  deviceNum
) => {
  try {
    // Create payload structure expected for ethSignPayloadObjectInHDW
    payload = {
      params: [
        undefined,
        payload.message,
      ],
    };
    return await ethSignPayloadObjectInHDW(payload, isHardwareCode, deviceNum);
  } catch (err) {
    Logger.error('Error in ethSignAuthRequestInHDW', {
      isHardwareCode,
      deviceNum,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const handleDappTxnObjectInHDW = async (
  payload,
  isHardwareCode,
  networkDetails
) => {
  try {
    let resultObj;

    if (typeof payload.topic === 'string' && typeof payload.iss !== 'string') {
      payload = payload.params.request;
    }

    const method = payload.method;
    const deviceName = getHardwareDeviceDetails(isHardwareCode).whichDevice;
    const deviceNum = whichDevice(deviceName);
    if (deviceNum == -1) throw new Error('No Decive Name matched');

    switch (method) {
      case "personal_sign":
        resultObj = await personalSignPayloadObjectInHDW(
          payload,
          isHardwareCode,
          deviceNum
        );
        break;
      case "eth_sign":
        resultObj = await ethSignPayloadObjectInHDW(
          payload,
          isHardwareCode,
          deviceNum
        );
        break;
      case "eth_signTypedData":
        resultObj = await ethSignTypedDataPayloadObjectInHDW(
          payload,
          isHardwareCode,
          deviceNum
        );
        break;
      case "eth_signTypedData_v4":
        resultObj = await ethSignTypedDataPayloadObjectInHDW(
          payload,
          isHardwareCode,
          deviceNum
        );
        break;
      case "eth_signTransaction":
        resultObj = await ethSignTransactionPayloadObjectInHDW(
          payload,
          networkDetails,
          isHardwareCode,
          deviceNum
        );
        break;
      // case "eth_sendRawTransaction":
      //   resultObj = await ethSendRawTransactionObject(
      //     payload,
      //     fromAddress,
      //     walletEncObject,
      //     password,
      //     networkDetails
      //   );
      // break;
      default:
        throw new Error('No transaction methods matched!');
    }
    if (!resultObj.success) throw new Error(resultObj.error);

    return resultObj;
  } catch (err) {
    Logger.error('Error in handleDappTxnObjectInHDW', {
      isHardwareCode,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// export const verifySign = (tx, payload, address) => {
//   try {
//     const txData = tx;
//     // The fromTxData utility expects a type to support transactions with a type other than 0
//     txData.type = tx.type;
//     // The fromTxData utility expects v,r and s to be hex prefixed
//     txData.v = ethUtil.addHexPrefix(payload.v);
//     txData.chainId = ethUtil.addHexPrefix(tx.chainId);
//     // txData.v = ethUtil.addHexPrefix(1);
//     txData.r = ethUtil.addHexPrefix(payload.r);
//     txData.s = ethUtil.addHexPrefix(payload.s);
//     // Adopt the 'common' option from the original transaction and set the
//     // returned object to be frozen if the original is frozen.

//     const newOrMutatedTx = TransactionFactory.fromTxData(txData, {
//       common: tx.common,
//       freeze: Object.isFrozen(tx),
//     });

//     const addressSignedWith = ethUtil.toChecksumAddress(
//       ethUtil.addHexPrefix(newOrMutatedTx.getSenderAddress().toString("hex"))
//     );
//     console.log("Signed Address >>>", addressSignedWith);

//     const correctAddress = ethUtil.toChecksumAddress(address);
//     console.log("correctAddress>>>", address);

//     if (addressSignedWith !== correctAddress) {
//       throw new Error("signature doesn't match the right address");
//     }

//     return newOrMutatedTx;
//   } catch (er) {
//     // console.log(er.message);
//   }
// };

// // The transaction of hardware with pulsechain is remaining
// // Resume from here
// export const signTxnInTrezor = async (
//   networkDetails,
//   txnObject,
//   isHardwareCode
// ) => {
//   try {
//     trezorManifest();
//     try {
//       const isValid = getHardwareDeviceDetails(isHardwareCode);

//       const signPath = "m/44'/60'/" + isValid.accountIndex + "'/0/0";
//       // console.log(signPath);
//       // const signPath =
//       //   "m/44'/60'/" + isValid.accountIndex.toString() + "'/0/0";
//       // const result = await TrezorConnect.ethereumGetAddress({
//       //   bundle: [
//       //     { path: signPath, showOnTrezor: false }, // account 1
//       //     { path: "m/44'/60'/0'", showOnTrezor: false }, // account 1
//       //     { path: "m/44'/60'/0'/0/0", showOnTrezor: false }, // account 1
//       //   ],
//       // });

//       // console.log("result >>", result);
//       const web3 = new Web3(networkDetails.rpc);
//       const validateTx = createValidRawTx(networkDetails.txnType, txnObject);
//       // const validateTx = createValidRawTx(0, txnObject);

//       const _normalize = (buf) => {
//         return ethUtil.bufferToHex(buf).toString();
//       };

//       const checkIfOld = (tx) => {
//         // console.log("chainID >>", tx.getChainId());
//         return typeof tx.getChainId === "function";
//       };
//       const nonce = await web3.eth.getTransactionCount(
//         validateTx?.txnObject.from,
//         "latest"
//       );
//       let transaction = {};
//       let transactionEIP = {};

//       const gasPrice =
//         parseInt(networkDetails.txnType) == 0
//           ? validateTx.txnObject.gasPrice
//           : validateTx.txnObject.maxFeePerGas;

//       //     {
//       //   nonce: '0x00',
//       //   maxFeePerGas: '0x19184e72a000',
//       //   maxPriorityFeePerGas: '0x09184e72a000',
//       //   gasLimit: '0x2710',
//       //   to: '0x0000000000000000000000000000000000000000',
//       //   value: '0x00',
//       //   data: '0x7f7465737432000000000000000000000000000000000000000000000000000000600057',
//       //   type: 2,
//       //   v: '0x01',
//       // },
//       // { common: commonEIP1559, freeze: false },
//       let txJSON = "";
//       if (networkDetails.txnType != 0) {
//         let tx = {
//           to: _normalize(validateTx?.txnObject.to),
//           value: _normalize(validateTx?.txnObject.value ?? "0x00"),
//           gasLimit: _normalize(validateTx?.txnObject.gas),
//           nonce: _normalize(nonce),
//           data: _normalize(validateTx?.txnObject.data ?? "0x00"),
//           maxFeePerGas: _normalize(validateTx?.txnObject.maxFeePerGas),
//           maxPriorityFeePerGas: _normalize(
//             validateTx?.txnObject.maxPriorityFeePerGas
//           ),
//           // chainId: networkDetails.chainId,
//           gasPrice: gasPrice,
//           type: networkDetails.txnType,
//         };

//         tx = FeeMarketEIP1559Transaction.fromTxData(tx, {
//           common: commonEIP1559,
//           freeze: false,
//         });

//         // console.log("OLD >>", checkIfOld(tx));

//         // console.log("transaction  >>>", tx);
//         // console.log("transaction  JSON >>>", tx.toJSON());

//         txJSON = tx.toJSON();

//         const t = tx.toJSON();
//         t.chainId = networkDetails.chainId;
//         t.to = _normalize(tx.to);
//         t.type = networkDetails.txnType;
//         // console.log("t>> ", t);
//         transactionEIP = t;

//         // transaction = {
//         //   ...tx.toJSON(),
//         //   chainId: networkDetails.chainId,
//         //   to: _normalize(tx.to),
//         //   type: networkDetails.txnType,
//         // };

//         // console.log("transaction  >>>", transactionEIP);

//         // delete transactionEIP.accessList;
//         delete transactionEIP.v;
//         delete transactionEIP.s;
//         delete transactionEIP.r;

//         transaction = {
//           // to: _normalize(validateTx?.txnObject.to),
//           // value: _normalize(validateTx?.txnObject.value ?? "0x00"),
//           // data: _normalize(validateTx?.txnObject.data ?? "0x00"),
//           // maxFeePerGas: _normalize(validateTx?.txnObject.maxFeePerGas),
//           // maxPriorityFeePerGas: _normalize(
//           //   validateTx?.txnObject.maxPriorityFeePerGas
//           // ),
//           // gasLimit: _normalize(validateTx?.txnObject.gas),
//           // nonce: _normalize(nonce),
//           // // ...JSON.stringify(validateTx.txnObject),
//           // chainId: networkDetails.chainId,
//           // // to: _normalize(validateTx?.txnObject.to),
//           // type: networkDetails.txnType,
//           to: _normalize(validateTx?.txnObject.to),
//           value: _normalize(validateTx?.txnObject.value ?? "0x00"),
//           data: _normalize(validateTx?.txnObject.data ?? "0x00"),
//           gasPrice: _normalize(gasPrice),
//           gasLimit: _normalize(validateTx?.txnObject.gas),
//           chainId: networkDetails.chainId,
//           nonce: _normalize(nonce),
//           type: 0,
//         };
//       } else {
//         transaction = {
//           to: _normalize(validateTx?.txnObject.to),
//           value: _normalize(validateTx?.txnObject.value ?? "0x00"),
//           data: _normalize(validateTx?.txnObject.data ?? "0x00"),
//           gasPrice: _normalize(validateTx?.txnObject.gasPrice),
//           gasLimit: _normalize(validateTx?.txnObject.gas),
//           chainId: networkDetails.chainId,
//           nonce: _normalize(nonce),
//           type: 0,
//         };
//       }

//       console.log("before transaction >>", transaction);
//       const signature = await TrezorConnect.ethereumSignTransaction({
//         path: signPath,
//         transaction: transaction,
//       });
//       console.log("signature >>>", signature);

//       // console.log(
//       //   "Verify Sign >>>",
//       //   verifySign(txJSON, signature.payload, validateTx?.txnObject.from)
//       // );
//       if (!signature.success) {
//         return {
//           success: false,
//           error: signature.payload.error,
//         };
//       }

//       // signature.payload.v = parseInt(signature.payload.v, 16);
//       signature.payload.from = validateTx?.txnObject.from;

//       // const payload = {};

//       transactionEIP.v = ethUtil.addHexPrefix(signature.payload.v);
//       transactionEIP.r = ethUtil.addHexPrefix(signature.payload.r);
//       transactionEIP.s = ethUtil.addHexPrefix(signature.payload.s);

//       console.log("after transaction >>", transaction);

//       const signedTx = ethers.utils.serializeTransaction(
//         transaction,
//         signature.payload
//       );
//       console.log("signedTx >>", signedTx);
//       console.log("hash >>", ethers.utils.keccak256(signedTx));

//       const provider = new ethers.providers.JsonRpcProvider(networkDetails.rpc);
//       console.log("Ethers getNetwork", provider.connection);
//       console.log("Ethers getNetwork", ethers.providers.getNetwork(941));
//       console.log("Ethers getNetwork", ethers.providers.getNetwork(1));
//       // const res = await provider.sendTransaction(signedTx);

//       // console.log("receipt >>", res);

//       // console.log(
//       //   "Balance >>",
//       //   await web3.eth.getBalance(validateTx?.txnObject.from)
//       // );

//       const receipt = await web3.eth.sendSignedTransaction(
//         signedTx.toString("hex")
//       );
//       console.log("receipt >>", receipt);
//       return {
//         success: true,
//         receipt,
//       };
//     } catch (error) {
//       return { success: false, error: error.message };
//     }
//   } catch (error) {
//     return { success: false, error: error };
//   }
// };
