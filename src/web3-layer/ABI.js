import erc20 from '@openzeppelin/contracts/build/contracts/ERC20.json';

export default {
  wdtoken: [
    {
      "inputs": [
        {
          "internalType": "string",
          "name": "_tokenName",
          "type": "string"
        },
        {
          "internalType": "string",
          "name": "_tokenSymbol",
          "type": "string"
        },
        {
          "internalType": "uint256",
          "name": "_cap",
          "type": "uint256"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "owner",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "spender",
          "type": "address"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "value",
          "type": "uint256"
        }
      ],
      "name": "Approval",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "account",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "recipient",
          "type": "address"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "ClaimDividend",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "from",
          "type": "address"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "weiAmount",
          "type": "uint256"
        }
      ],
      "name": "DistributeDividend",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "previousOwner",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "newOwner",
          "type": "address"
        }
      ],
      "name": "OwnershipTransferred",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "from",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "value",
          "type": "uint256"
        }
      ],
      "name": "Transfer",
      "type": "event"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "account",
          "type": "address"
        }
      ],
      "name": "accumulativeDividendOf",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "owner",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "spender",
          "type": "address"
        }
      ],
      "name": "allowance",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "spender",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "approve",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "account",
          "type": "address"
        }
      ],
      "name": "balanceOf",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "burn",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "account",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "burnFrom",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "cap",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address payable",
          "name": "recipient",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "claimDividend",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "account",
          "type": "address"
        }
      ],
      "name": "claimableDividendOf",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "amount",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "max",
          "type": "uint256"
        }
      ],
      "name": "clamp",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "pure",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "name": "cumulativeDividendClaimed",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "decimals",
      "outputs": [
        {
          "internalType": "uint8",
          "name": "",
          "type": "uint8"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "spender",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "subtractedValue",
          "type": "uint256"
        }
      ],
      "name": "decreaseAllowance",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "walletSwapAddress",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "distributeAll",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "magDividendPerShare",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "balance",
          "type": "uint256"
        },
        {
          "internalType": "int256",
          "name": "correction",
          "type": "int256"
        }
      ],
      "name": "dividendFrom",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "claimableDividend",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "productRemainder",
          "type": "uint256"
        }
      ],
      "stateMutability": "pure",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "spender",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "addedValue",
          "type": "uint256"
        }
      ],
      "name": "increaseAllowance",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "name": "magnifiedDividendCorrections",
      "outputs": [
        {
          "internalType": "int256",
          "name": "",
          "type": "int256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "magnifiedDividendPerShare",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "magnitude",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "account",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "mint",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256[]",
          "name": "mints",
          "type": "uint256[]"
        }
      ],
      "name": "mintMany",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes[]",
          "name": "data",
          "type": "bytes[]"
        }
      ],
      "name": "multicall",
      "outputs": [
        {
          "internalType": "bytes[]",
          "name": "results",
          "type": "bytes[]"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "name",
      "outputs": [
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "owner",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "tokenID",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "recipient",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "recoverERC20",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "renounceOwnership",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "symbol",
      "outputs": [
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "totalSupply",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "transfer",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "from",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "amount",
          "type": "uint256"
        }
      ],
      "name": "transferFrom",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "newOwner",
          "type": "address"
        }
      ],
      "name": "transferOwnership",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "stateMutability": "payable",
      "type": "receive"
    }
  ],
  airdrop: [
    {
      inputs: [
        {
          internalType: "address",
          name: "_internetMoney",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "_internetMoneyDecimal",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "_maxLimitReferralCount",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "_extraBonusPercentage",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "_IM_USDValue",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "_cryptoUSDValue",
          type: "uint256",
        },
      ],
      stateMutability: "nonpayable",
      type: "constructor",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "address",
          name: "to",
          type: "address",
        },
        {
          indexed: true,
          internalType: "bool",
          name: "referred",
          type: "bool",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "airdropAmount",
          type: "uint256",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "ethValue",
          type: "uint256",
        },
      ],
      name: "Airdrop",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "address",
          name: "from",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "rewardAmount",
          type: "uint256",
        },
      ],
      name: "ClaimRewards",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "address",
          name: "previousOwner",
          type: "address",
        },
        {
          indexed: true,
          internalType: "address",
          name: "newOwner",
          type: "address",
        },
      ],
      name: "OwnershipTransferred",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: "uint256",
          name: "newAirDropAmount",
          type: "uint256",
        },
      ],
      name: "SetAirdropAmount",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: "uint256",
          name: "newCryptoUSDValue",
          type: "uint256",
        },
      ],
      name: "SetCryptoUSDValue",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: "uint256",
          name: "newIMUSDValue",
          type: "uint256",
        },
      ],
      name: "SetIMUSDValue",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: "uint256",
          name: "newInternetMoneyDecimal",
          type: "uint256",
        },
      ],
      name: "SetInternetMoneyDecimal",
      type: "event",
    },
    {
      inputs: [],
      name: "active",
      outputs: [
        {
          internalType: "bool",
          name: "",
          type: "bool",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "",
          type: "address",
        },
      ],
      name: "addressVerified",
      outputs: [
        {
          internalType: "bool",
          name: "",
          type: "bool",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "airdropAmount",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "airdropCount",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "bytes32",
          name: "_hash",
          type: "bytes32",
        },
      ],
      name: "checkMobileNumber",
      outputs: [
        {
          internalType: "bool",
          name: "",
          type: "bool",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "_referrerAddress",
          type: "address",
        },
      ],
      name: "claimEarnings",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [],
      name: "cryptoBalance",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "cryptoUSDValue",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "cryptoUSDValueDecimals",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "extraBonusPercentage",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "getAmountDetails",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
        {
          internalType: "address",
          name: "",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "bytes32",
          name: "mobileHash",
          type: "bytes32",
        },
        {
          internalType: "uint256",
          name: "_v",
          type: "uint256",
        },
        {
          internalType: "bytes32",
          name: "_r",
          type: "bytes32",
        },
        {
          internalType: "bytes32",
          name: "_s",
          type: "bytes32",
        },
      ],
      name: "getCheckMobileHashWithSignature",
      outputs: [
        {
          internalType: "address",
          name: "",
          type: "address",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "bytes32",
          name: "_hash",
          type: "bytes32",
        },
      ],
      name: "getMessageHash",
      outputs: [
        {
          internalType: "bytes32",
          name: "",
          type: "bytes32",
        },
      ],
      stateMutability: "pure",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "_referrerAddress",
          type: "address",
        },
      ],
      name: "getReferralEarningsOf",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "_referrerAddress",
          type: "address",
        },
      ],
      name: "getReferrerDetails",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
        {
          internalType: "uint256[]",
          name: "",
          type: "uint256[]",
        },
        {
          internalType: "uint256[]",
          name: "",
          type: "uint256[]",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "getTotalYetToBeClaimedTokenBalance",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "imUSDValue",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "imUSDValueDecimals",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "internetMoney",
      outputs: [
        {
          internalType: "contract IERC20",
          name: "",
          type: "address",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "internetMoneyDecimal",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "isLimitReached",
      outputs: [
        {
          internalType: "bool",
          name: "",
          type: "bool",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "maxLimitAirdropCount",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "owner",
      outputs: [
        {
          internalType: "address",
          name: "",
          type: "address",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "bool",
          name: "_status",
          type: "bool",
        },
      ],
      name: "pauseAirdrop",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "_to",
          type: "address",
        },
        {
          internalType: "address",
          name: "_referrerAddress",
          type: "address",
        },
        {
          internalType: "bytes32",
          name: "_hash",
          type: "bytes32",
        },
        {
          internalType: "uint256",
          name: "calAirdropAmount",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "calCryptoAmount",
          type: "uint256",
        },
      ],
      name: "performAirdrop",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [],
      name: "referrerAmount",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "",
          type: "address",
        },
      ],
      name: "referrerDetails",
      outputs: [
        {
          internalType: "uint256",
          name: "claimedIndex",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "referrerClaimedEarnings",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "referrerTotalEarnings",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "renounceOwnership",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "_performer",
          type: "address",
        },
        {
          internalType: "bool",
          name: "_status",
          type: "bool",
        },
      ],
      name: "setAidropPerformer",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "_amount",
          type: "uint256",
        },
      ],
      name: "setAirdropAmount",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "_percentage",
          type: "uint256",
        },
      ],
      name: "setExtraBonusPercentage",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "_decimal",
          type: "uint256",
        },
      ],
      name: "setInternetMoneyDecimal",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "_tokenAddress",
          type: "address",
        },
      ],
      name: "setInternetMoneyToken",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "_maxLimitAirdropCount",
          type: "uint256",
        },
      ],
      name: "setMaxLimitAirdropCount",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "_signer",
          type: "address",
        },
        {
          internalType: "bool",
          name: "_status",
          type: "bool",
        },
      ],
      name: "setMobileLinkedDataAccess",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "_referrerAmount",
          type: "uint256",
        },
      ],
      name: "setReferrerAmount",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "_usdValueInCents",
          type: "uint256",
        },
      ],
      name: "set_CryptoUSDValue",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "uint256",
          name: "_usdValueInCents",
          type: "uint256",
        },
      ],
      name: "set_IM_USDValue",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [],
      name: "totalAirdropAmountInCrypto",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "totalAirdropAmountInTokens",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "totalClaimedAirdropAmountInTokens",
      outputs: [
        {
          internalType: "uint256",
          name: "",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "newOwner",
          type: "address",
        },
      ],
      name: "transferOwnership",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "_toAddress",
          type: "address",
        },
      ],
      name: "withdrawCrypto",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        {
          internalType: "address",
          name: "_tokenAddress",
          type: "address",
        },
        {
          internalType: "address",
          name: "_toAddress",
          type: "address",
        },
      ],
      name: "withdrawToken",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      stateMutability: "payable",
      type: "receive",
    },
  ],
  erc20: erc20.abi,
  factory: [
    {
      type: "constructor",
      stateMutability: "nonpayable",
      payable: false,
      inputs: [
        { type: "address", name: "_feeToSetter", internalType: "address" },
      ],
    },
    {
      type: "event",
      name: "PairCreated",
      inputs: [
        {
          type: "address",
          name: "token0",
          internalType: "address",
          indexed: true,
        },
        {
          type: "address",
          name: "token1",
          internalType: "address",
          indexed: true,
        },
        {
          type: "address",
          name: "pair",
          internalType: "address",
          indexed: false,
        },
        {
          type: "uint256",
          name: "",
          internalType: "uint256",
          indexed: false,
        },
      ],
      anonymous: false,
    },
    {
      type: "function",
      stateMutability: "view",
      payable: false,
      outputs: [{ type: "bytes32", name: "", internalType: "bytes32" }],
      name: "INIT_CODE_PAIR_HASH",
      inputs: [],
      constant: true,
    },
    {
      type: "function",
      stateMutability: "view",
      payable: false,
      outputs: [{ type: "address", name: "", internalType: "address" }],
      name: "allPairs",
      inputs: [{ type: "uint256", name: "", internalType: "uint256" }],
      constant: true,
    },
    {
      type: "function",
      stateMutability: "view",
      payable: false,
      outputs: [{ type: "uint256", name: "", internalType: "uint256" }],
      name: "allPairsLength",
      inputs: [],
      constant: true,
    },
    {
      type: "function",
      stateMutability: "nonpayable",
      payable: false,
      outputs: [{ type: "address", name: "pair", internalType: "address" }],
      name: "createPair",
      inputs: [
        { type: "address", name: "tokenA", internalType: "address" },
        { type: "address", name: "tokenB", internalType: "address" },
      ],
      constant: false,
    },
    {
      type: "function",
      stateMutability: "view",
      payable: false,
      outputs: [{ type: "address", name: "", internalType: "address" }],
      name: "feeTo",
      inputs: [],
      constant: true,
    },
    {
      type: "function",
      stateMutability: "view",
      payable: false,
      outputs: [{ type: "address", name: "", internalType: "address" }],
      name: "feeToSetter",
      inputs: [],
      constant: true,
    },
    {
      type: "function",
      stateMutability: "view",
      payable: false,
      outputs: [{ type: "address", name: "", internalType: "address" }],
      name: "getPair",
      inputs: [
        { type: "address", name: "", internalType: "address" },
        { type: "address", name: "", internalType: "address" },
      ],
      constant: true,
    },
    {
      type: "function",
      stateMutability: "nonpayable",
      payable: false,
      outputs: [],
      name: "setFeeTo",
      inputs: [{ type: "address", name: "_feeTo", internalType: "address" }],
      constant: false,
    },
    {
      type: "function",
      stateMutability: "nonpayable",
      payable: false,
      outputs: [],
      name: "setFeeToSetter",
      inputs: [
        { type: "address", name: "_feeToSetter", internalType: "address" },
      ],
      constant: false,
    },
  ],
  pair: [
    {
      inputs: [],
      payable: false,
      stateMutability: "nonpayable",
      type: "constructor",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "address",
          name: "owner",
          type: "address",
        },
        {
          indexed: true,
          internalType: "address",
          name: "spender",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "value",
          type: "uint256",
        },
      ],
      name: "Approval",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "address",
          name: "sender",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "amount0",
          type: "uint256",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "amount1",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "to",
          type: "address",
        },
      ],
      name: "Burn",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "address",
          name: "sender",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "amount0",
          type: "uint256",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "amount1",
          type: "uint256",
        },
      ],
      name: "Mint",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "address",
          name: "sender",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "amount0In",
          type: "uint256",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "amount1In",
          type: "uint256",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "amount0Out",
          type: "uint256",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "amount1Out",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "to",
          type: "address",
        },
      ],
      name: "Swap",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: false,
          internalType: "uint112",
          name: "reserve0",
          type: "uint112",
        },
        {
          indexed: false,
          internalType: "uint112",
          name: "reserve1",
          type: "uint112",
        },
      ],
      name: "Sync",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "address",
          name: "from",
          type: "address",
        },
        {
          indexed: true,
          internalType: "address",
          name: "to",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "value",
          type: "uint256",
        },
      ],
      name: "Transfer",
      type: "event",
    },
    {
      constant: true,
      inputs: [],
      name: "DOMAIN_SEPARATOR",
      outputs: [{ internalType: "bytes32", name: "", type: "bytes32" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "MINIMUM_LIQUIDITY",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "PERMIT_TYPEHASH",
      outputs: [{ internalType: "bytes32", name: "", type: "bytes32" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: true,
      inputs: [
        { internalType: "address", name: "", type: "address" },
        { internalType: "address", name: "", type: "address" },
      ],
      name: "allowance",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: false,
      inputs: [
        { internalType: "address", name: "spender", type: "address" },
        { internalType: "uint256", name: "value", type: "uint256" },
      ],
      name: "approve",
      outputs: [{ internalType: "bool", name: "", type: "bool" }],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: true,
      inputs: [{ internalType: "address", name: "", type: "address" }],
      name: "balanceOf",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: false,
      inputs: [{ internalType: "address", name: "to", type: "address" }],
      name: "burn",
      outputs: [
        { internalType: "uint256", name: "amount0", type: "uint256" },
        { internalType: "uint256", name: "amount1", type: "uint256" },
      ],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "decimals",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "factory",
      outputs: [{ internalType: "address", name: "", type: "address" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "getReserves",
      outputs: [
        { internalType: "uint112", name: "_reserve0", type: "uint112" },
        { internalType: "uint112", name: "_reserve1", type: "uint112" },
        {
          internalType: "uint32",
          name: "_blockTimestampLast",
          type: "uint32",
        },
      ],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: false,
      inputs: [
        { internalType: "address", name: "_token0", type: "address" },
        { internalType: "address", name: "_token1", type: "address" },
      ],
      name: "initialize",
      outputs: [],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "kLast",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: false,
      inputs: [{ internalType: "address", name: "to", type: "address" }],
      name: "mint",
      outputs: [
        { internalType: "uint256", name: "liquidity", type: "uint256" },
      ],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "name",
      outputs: [{ internalType: "string", name: "", type: "string" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: true,
      inputs: [{ internalType: "address", name: "", type: "address" }],
      name: "nonces",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: false,
      inputs: [
        { internalType: "address", name: "owner", type: "address" },
        { internalType: "address", name: "spender", type: "address" },
        { internalType: "uint256", name: "value", type: "uint256" },
        { internalType: "uint256", name: "deadline", type: "uint256" },
        { internalType: "uint256", name: "v", type: "uint256" },
        { internalType: "bytes32", name: "r", type: "bytes32" },
        { internalType: "bytes32", name: "s", type: "bytes32" },
      ],
      name: "permit",
      outputs: [],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "price0CumulativeLast",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "price1CumulativeLast",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: false,
      inputs: [{ internalType: "address", name: "to", type: "address" }],
      name: "skim",
      outputs: [],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: false,
      inputs: [
        { internalType: "uint256", name: "amount0Out", type: "uint256" },
        { internalType: "uint256", name: "amount1Out", type: "uint256" },
        { internalType: "address", name: "to", type: "address" },
        { internalType: "bytes", name: "data", type: "bytes" },
      ],
      name: "swap",
      outputs: [],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "symbol",
      outputs: [{ internalType: "string", name: "", type: "string" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: false,
      inputs: [],
      name: "sync",
      outputs: [],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "token0",
      outputs: [{ internalType: "address", name: "", type: "address" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "token1",
      outputs: [{ internalType: "address", name: "", type: "address" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "totalSupply",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: false,
      inputs: [
        { internalType: "address", name: "to", type: "address" },
        { internalType: "uint256", name: "value", type: "uint256" },
      ],
      name: "transfer",
      outputs: [{ internalType: "bool", name: "", type: "bool" }],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: false,
      inputs: [
        { internalType: "address", name: "from", type: "address" },
        { internalType: "address", name: "to", type: "address" },
        { internalType: "uint256", name: "value", type: "uint256" },
      ],
      name: "transferFrom",
      outputs: [{ internalType: "bool", name: "", type: "bool" }],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
  ],
  router: [{ "type": "constructor", "stateMutability": "nonpayable", "inputs": [{ "type": "address", "name": "_factory", "internalType": "address" }, { "type": "address", "name": "_WPLS", "internalType": "address" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "address", "name": "", "internalType": "address" }], "name": "WPLS", "inputs": [] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [{ "type": "uint256", "name": "amountA", "internalType": "uint256" }, { "type": "uint256", "name": "amountB", "internalType": "uint256" }, { "type": "uint256", "name": "liquidity", "internalType": "uint256" }], "name": "addLiquidity", "inputs": [{ "type": "address", "name": "tokenA", "internalType": "address" }, { "type": "address", "name": "tokenB", "internalType": "address" }, { "type": "uint256", "name": "amountADesired", "internalType": "uint256" }, { "type": "uint256", "name": "amountBDesired", "internalType": "uint256" }, { "type": "uint256", "name": "amountAMin", "internalType": "uint256" }, { "type": "uint256", "name": "amountBMin", "internalType": "uint256" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "payable", "outputs": [{ "type": "uint256", "name": "amountToken", "internalType": "uint256" }, { "type": "uint256", "name": "amountETH", "internalType": "uint256" }, { "type": "uint256", "name": "liquidity", "internalType": "uint256" }], "name": "addLiquidityETH", "inputs": [{ "type": "address", "name": "token", "internalType": "address" }, { "type": "uint256", "name": "amountTokenDesired", "internalType": "uint256" }, { "type": "uint256", "name": "amountTokenMin", "internalType": "uint256" }, { "type": "uint256", "name": "amountETHMin", "internalType": "uint256" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "address", "name": "", "internalType": "address" }], "name": "factory", "inputs": [] }, { "type": "function", "stateMutability": "pure", "outputs": [{ "type": "uint256", "name": "amountIn", "internalType": "uint256" }], "name": "getAmountIn", "inputs": [{ "type": "uint256", "name": "amountOut", "internalType": "uint256" }, { "type": "uint256", "name": "reserveIn", "internalType": "uint256" }, { "type": "uint256", "name": "reserveOut", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "pure", "outputs": [{ "type": "uint256", "name": "amountOut", "internalType": "uint256" }], "name": "getAmountOut", "inputs": [{ "type": "uint256", "name": "amountIn", "internalType": "uint256" }, { "type": "uint256", "name": "reserveIn", "internalType": "uint256" }, { "type": "uint256", "name": "reserveOut", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint256[]", "name": "amounts", "internalType": "uint256[]" }], "name": "getAmountsIn", "inputs": [{ "type": "uint256", "name": "amountOut", "internalType": "uint256" }, { "type": "address[]", "name": "path", "internalType": "address[]" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint256[]", "name": "amounts", "internalType": "uint256[]" }], "name": "getAmountsOut", "inputs": [{ "type": "uint256", "name": "amountIn", "internalType": "uint256" }, { "type": "address[]", "name": "path", "internalType": "address[]" }] }, { "type": "function", "stateMutability": "pure", "outputs": [{ "type": "uint256", "name": "amountB", "internalType": "uint256" }], "name": "quote", "inputs": [{ "type": "uint256", "name": "amountA", "internalType": "uint256" }, { "type": "uint256", "name": "reserveA", "internalType": "uint256" }, { "type": "uint256", "name": "reserveB", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [{ "type": "uint256", "name": "amountA", "internalType": "uint256" }, { "type": "uint256", "name": "amountB", "internalType": "uint256" }], "name": "removeLiquidity", "inputs": [{ "type": "address", "name": "tokenA", "internalType": "address" }, { "type": "address", "name": "tokenB", "internalType": "address" }, { "type": "uint256", "name": "liquidity", "internalType": "uint256" }, { "type": "uint256", "name": "amountAMin", "internalType": "uint256" }, { "type": "uint256", "name": "amountBMin", "internalType": "uint256" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [{ "type": "uint256", "name": "amountToken", "internalType": "uint256" }, { "type": "uint256", "name": "amountETH", "internalType": "uint256" }], "name": "removeLiquidityETH", "inputs": [{ "type": "address", "name": "token", "internalType": "address" }, { "type": "uint256", "name": "liquidity", "internalType": "uint256" }, { "type": "uint256", "name": "amountTokenMin", "internalType": "uint256" }, { "type": "uint256", "name": "amountETHMin", "internalType": "uint256" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [{ "type": "uint256", "name": "amountETH", "internalType": "uint256" }], "name": "removeLiquidityETHSupportingFeeOnTransferTokens", "inputs": [{ "type": "address", "name": "token", "internalType": "address" }, { "type": "uint256", "name": "liquidity", "internalType": "uint256" }, { "type": "uint256", "name": "amountTokenMin", "internalType": "uint256" }, { "type": "uint256", "name": "amountETHMin", "internalType": "uint256" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [{ "type": "uint256", "name": "amountToken", "internalType": "uint256" }, { "type": "uint256", "name": "amountETH", "internalType": "uint256" }], "name": "removeLiquidityETHWithPermit", "inputs": [{ "type": "address", "name": "token", "internalType": "address" }, { "type": "uint256", "name": "liquidity", "internalType": "uint256" }, { "type": "uint256", "name": "amountTokenMin", "internalType": "uint256" }, { "type": "uint256", "name": "amountETHMin", "internalType": "uint256" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }, { "type": "bool", "name": "approveMax", "internalType": "bool" }, { "type": "uint8", "name": "v", "internalType": "uint8" }, { "type": "bytes32", "name": "r", "internalType": "bytes32" }, { "type": "bytes32", "name": "s", "internalType": "bytes32" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [{ "type": "uint256", "name": "amountETH", "internalType": "uint256" }], "name": "removeLiquidityETHWithPermitSupportingFeeOnTransferTokens", "inputs": [{ "type": "address", "name": "token", "internalType": "address" }, { "type": "uint256", "name": "liquidity", "internalType": "uint256" }, { "type": "uint256", "name": "amountTokenMin", "internalType": "uint256" }, { "type": "uint256", "name": "amountETHMin", "internalType": "uint256" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }, { "type": "bool", "name": "approveMax", "internalType": "bool" }, { "type": "uint8", "name": "v", "internalType": "uint8" }, { "type": "bytes32", "name": "r", "internalType": "bytes32" }, { "type": "bytes32", "name": "s", "internalType": "bytes32" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [{ "type": "uint256", "name": "amountA", "internalType": "uint256" }, { "type": "uint256", "name": "amountB", "internalType": "uint256" }], "name": "removeLiquidityWithPermit", "inputs": [{ "type": "address", "name": "tokenA", "internalType": "address" }, { "type": "address", "name": "tokenB", "internalType": "address" }, { "type": "uint256", "name": "liquidity", "internalType": "uint256" }, { "type": "uint256", "name": "amountAMin", "internalType": "uint256" }, { "type": "uint256", "name": "amountBMin", "internalType": "uint256" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }, { "type": "bool", "name": "approveMax", "internalType": "bool" }, { "type": "uint8", "name": "v", "internalType": "uint8" }, { "type": "bytes32", "name": "r", "internalType": "bytes32" }, { "type": "bytes32", "name": "s", "internalType": "bytes32" }] }, { "type": "function", "stateMutability": "payable", "outputs": [{ "type": "uint256[]", "name": "amounts", "internalType": "uint256[]" }], "name": "swapETHForExactTokens", "inputs": [{ "type": "uint256", "name": "amountOut", "internalType": "uint256" }, { "type": "address[]", "name": "path", "internalType": "address[]" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "payable", "outputs": [{ "type": "uint256[]", "name": "amounts", "internalType": "uint256[]" }], "name": "swapExactETHForTokens", "inputs": [{ "type": "uint256", "name": "amountOutMin", "internalType": "uint256" }, { "type": "address[]", "name": "path", "internalType": "address[]" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "payable", "outputs": [], "name": "swapExactETHForTokensSupportingFeeOnTransferTokens", "inputs": [{ "type": "uint256", "name": "amountOutMin", "internalType": "uint256" }, { "type": "address[]", "name": "path", "internalType": "address[]" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [{ "type": "uint256[]", "name": "amounts", "internalType": "uint256[]" }], "name": "swapExactTokensForETH", "inputs": [{ "type": "uint256", "name": "amountIn", "internalType": "uint256" }, { "type": "uint256", "name": "amountOutMin", "internalType": "uint256" }, { "type": "address[]", "name": "path", "internalType": "address[]" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [], "name": "swapExactTokensForETHSupportingFeeOnTransferTokens", "inputs": [{ "type": "uint256", "name": "amountIn", "internalType": "uint256" }, { "type": "uint256", "name": "amountOutMin", "internalType": "uint256" }, { "type": "address[]", "name": "path", "internalType": "address[]" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [{ "type": "uint256[]", "name": "amounts", "internalType": "uint256[]" }], "name": "swapExactTokensForTokens", "inputs": [{ "type": "uint256", "name": "amountIn", "internalType": "uint256" }, { "type": "uint256", "name": "amountOutMin", "internalType": "uint256" }, { "type": "address[]", "name": "path", "internalType": "address[]" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [], "name": "swapExactTokensForTokensSupportingFeeOnTransferTokens", "inputs": [{ "type": "uint256", "name": "amountIn", "internalType": "uint256" }, { "type": "uint256", "name": "amountOutMin", "internalType": "uint256" }, { "type": "address[]", "name": "path", "internalType": "address[]" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [{ "type": "uint256[]", "name": "amounts", "internalType": "uint256[]" }], "name": "swapTokensForExactETH", "inputs": [{ "type": "uint256", "name": "amountOut", "internalType": "uint256" }, { "type": "uint256", "name": "amountInMax", "internalType": "uint256" }, { "type": "address[]", "name": "path", "internalType": "address[]" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [{ "type": "uint256[]", "name": "amounts", "internalType": "uint256[]" }], "name": "swapTokensForExactTokens", "inputs": [{ "type": "uint256", "name": "amountOut", "internalType": "uint256" }, { "type": "uint256", "name": "amountInMax", "internalType": "uint256" }, { "type": "address[]", "name": "path", "internalType": "address[]" }, { "type": "address", "name": "to", "internalType": "address" }, { "type": "uint256", "name": "deadline", "internalType": "uint256" }] }, { "type": "receive", "stateMutability": "payable" }],
  quotes: [{ "type": "constructor", "stateMutability": "nonpayable", "inputs": [{ "type": "address", "name": "_destination", "internalType": "address payable" }, { "type": "address", "name": "_wNative", "internalType": "address payable" }, { "type": "uint96", "name": "_fee", "internalType": "uint96" }] }, { "type": "error", "name": "DestinationMissing", "inputs": [] }, { "type": "error", "name": "DexConflict", "inputs": [{ "type": "uint256", "name": "dexIndex", "internalType": "uint256" }] }, { "type": "error", "name": "FeeMissing", "inputs": [{ "type": "uint256", "name": "expected", "internalType": "uint256" }, { "type": "uint256", "name": "provided", "internalType": "uint256" }, { "type": "string", "name": "message", "internalType": "string" }] }, { "type": "error", "name": "NativeMissing", "inputs": [{ "type": "uint256", "name": "pathIndex", "internalType": "uint256" }] }, { "type": "event", "name": "AddDex", "inputs": [{ "type": "address", "name": "executor", "internalType": "address", "indexed": true }, { "type": "uint256", "name": "dexId", "internalType": "uint256", "indexed": true }], "anonymous": false }, { "type": "event", "name": "OwnershipTransferred", "inputs": [{ "type": "address", "name": "previousOwner", "internalType": "address", "indexed": true }, { "type": "address", "name": "newOwner", "internalType": "address", "indexed": true }], "anonymous": false }, { "type": "event", "name": "UpdateDex", "inputs": [{ "type": "address", "name": "executor", "internalType": "address", "indexed": true }, { "type": "uint256", "name": "dexId", "internalType": "uint256", "indexed": true }], "anonymous": false }, { "type": "event", "name": "UpdateMigrationAddresses", "inputs": [{ "type": "address", "name": "executor", "internalType": "address", "indexed": true }, { "type": "address[]", "name": "addresses", "internalType": "address[]", "indexed": false }], "anonymous": false }, { "type": "event", "name": "UpdateTokenFee", "inputs": [{ "type": "address", "name": "token", "internalType": "address", "indexed": true }, { "type": "uint256", "name": "feeNumerator", "internalType": "uint256", "indexed": true }], "anonymous": false }, { "type": "function", "stateMutability": "payable", "outputs": [], "name": "addDex", "inputs": [{ "type": "string", "name": "_dexName", "internalType": "string" }, { "type": "address", "name": "_router", "internalType": "address" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "tuple[]", "name": "_dexInfo", "internalType": "struct InternetMoneySwapRouter.Dex[]", "components": [{ "type": "uint96", "name": "id", "internalType": "uint96" }, { "type": "address", "name": "router", "internalType": "address" }, { "type": "bool", "name": "disabled", "internalType": "bool" }, { "type": "string", "name": "name", "internalType": "string" }] }, { "type": "address", "name": "_wNative", "internalType": "address" }, { "type": "address", "name": "_destination", "internalType": "address" }, { "type": "uint256", "name": "_fee", "internalType": "uint256" }, { "type": "uint256", "name": "_feeDenominator", "internalType": "uint256" }], "name": "allDexInfo", "inputs": [] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint256", "name": "", "internalType": "uint256" }], "name": "amountOutFrom", "inputs": [{ "type": "address", "name": "factory", "internalType": "address" }, { "type": "address", "name": "tokenIn", "internalType": "address" }, { "type": "address", "name": "tokenOut", "internalType": "address" }, { "type": "uint256", "name": "amountIn", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint256", "name": "tokenA", "internalType": "uint256" }, { "type": "uint256", "name": "tokenB", "internalType": "uint256" }], "name": "checkPairForValidPrice", "inputs": [{ "type": "address", "name": "factory", "internalType": "address" }, { "type": "address", "name": "token", "internalType": "address" }] }, { "type": "function", "stateMutability": "pure", "outputs": [{ "type": "uint256", "name": "", "internalType": "uint256" }], "name": "clamp", "inputs": [{ "type": "uint256", "name": "amount", "internalType": "uint256" }, { "type": "uint256", "name": "max", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "address", "name": "", "internalType": "address payable" }], "name": "destination", "inputs": [] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint96", "name": "id", "internalType": "uint96" }, { "type": "address", "name": "router", "internalType": "address" }, { "type": "bool", "name": "disabled", "internalType": "bool" }, { "type": "string", "name": "name", "internalType": "string" }], "name": "dexInfo", "inputs": [{ "type": "uint256", "name": "", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "payable", "outputs": [], "name": "disableDex", "inputs": [{ "type": "uint256", "name": "id", "internalType": "uint256" }, { "type": "bool", "name": "disabled", "internalType": "bool" }] }, { "type": "function", "stateMutability": "payable", "outputs": [], "name": "distribute", "inputs": [{ "type": "uint256", "name": "amount", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "payable", "outputs": [], "name": "distributeAll", "inputs": [{ "type": "uint256", "name": "amount", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint256", "name": "", "internalType": "uint256" }], "name": "fee", "inputs": [] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint256", "name": "", "internalType": "uint256" }], "name": "feeDenominator", "inputs": [] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint256", "name": "", "internalType": "uint256" }], "name": "feeNumerator", "inputs": [{ "type": "address", "name": "", "internalType": "address" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "address", "name": "", "internalType": "address" }, { "type": "uint256", "name": "", "internalType": "uint256" }], "name": "getFeeMinimum", "inputs": [{ "type": "address", "name": "factory", "internalType": "address" }, { "type": "uint256", "name": "_amountIn", "internalType": "uint256" }, { "type": "address[]", "name": "path", "internalType": "address[]" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint256", "name": "reserveA", "internalType": "uint256" }, { "type": "uint256", "name": "reserveB", "internalType": "uint256" }], "name": "getPairReserves", "inputs": [{ "type": "address", "name": "pair", "internalType": "address" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint256", "name": "minimum", "internalType": "uint256" }], "name": "getSingleFeeMinimum", "inputs": [{ "type": "address", "name": "factory", "internalType": "address" }, { "type": "uint256", "name": "_amountIn", "internalType": "uint256" }, { "type": "address", "name": "source", "internalType": "address" }] }, { "type": "function", "stateMutability": "pure", "outputs": [{ "type": "bool", "name": "", "internalType": "bool" }], "name": "hasEnoughReserves", "inputs": [{ "type": "uint256", "name": "tokenAmount", "internalType": "uint256" }, { "type": "uint256", "name": "wethAmount", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "address", "name": "", "internalType": "address" }], "name": "migrationAddresses", "inputs": [{ "type": "uint256", "name": "", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "address", "name": "", "internalType": "address" }], "name": "owner", "inputs": [] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint256", "name": "", "internalType": "uint256" }], "name": "pendingDistribution", "inputs": [] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint256", "name": "", "internalType": "uint256" }, { "type": "uint256", "name": "", "internalType": "uint256" }], "name": "pendingDistributionSegmented", "inputs": [] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [], "name": "renounceOwnership", "inputs": [] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "uint256", "name": "", "internalType": "uint256" }], "name": "routerToDex", "inputs": [{ "type": "address", "name": "", "internalType": "address" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [], "name": "setMigrationAddress", "inputs": [{ "type": "address[]", "name": "_migrationAddresses", "internalType": "address[]" }] }, { "type": "function", "stateMutability": "payable", "outputs": [], "name": "swapNativeToV2", "inputs": [{ "type": "uint256", "name": "_dexId", "internalType": "uint256" }, { "type": "address", "name": "recipient", "internalType": "address" }, { "type": "address[]", "name": "_path", "internalType": "address[]" }, { "type": "uint256", "name": "_amountIn", "internalType": "uint256" }, { "type": "uint256", "name": "_minAmountOut", "internalType": "uint256" }, { "type": "uint256", "name": "_deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "payable", "outputs": [], "name": "swapToNativeV2", "inputs": [{ "type": "uint256", "name": "_dexId", "internalType": "uint256" }, { "type": "address", "name": "recipient", "internalType": "address payable" }, { "type": "address[]", "name": "_path", "internalType": "address[]" }, { "type": "uint256", "name": "_amountIn", "internalType": "uint256" }, { "type": "uint256", "name": "_minAmountOut", "internalType": "uint256" }, { "type": "uint256", "name": "_deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "payable", "outputs": [], "name": "swapTokenV2", "inputs": [{ "type": "uint256", "name": "_dexId", "internalType": "uint256" }, { "type": "address", "name": "recipient", "internalType": "address" }, { "type": "address[]", "name": "_path", "internalType": "address[]" }, { "type": "uint256", "name": "_amountIn", "internalType": "uint256" }, { "type": "uint256", "name": "_minAmountOut", "internalType": "uint256" }, { "type": "uint256", "name": "_deadline", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "nonpayable", "outputs": [], "name": "transferOwnership", "inputs": [{ "type": "address", "name": "newOwner", "internalType": "address" }] }, { "type": "function", "stateMutability": "payable", "outputs": [], "name": "updateDex", "inputs": [{ "type": "uint256", "name": "id", "internalType": "uint256" }, { "type": "string", "name": "_name", "internalType": "string" }] }, { "type": "function", "stateMutability": "payable", "outputs": [], "name": "updateTokenFee", "inputs": [{ "type": "address", "name": "token", "internalType": "address" }, { "type": "uint256", "name": "_feeNumerator", "internalType": "uint256" }] }, { "type": "function", "stateMutability": "view", "outputs": [{ "type": "address", "name": "", "internalType": "address payable" }], "name": "wNative", "inputs": [] }, { "type": "receive", "stateMutability": "payable" }],
  quotesV2: [{"type":"constructor","stateMutability":"nonpayable","inputs":[{"type":"address","name":"_destination","internalType":"address payable"},{"type":"address","name":"_wNative","internalType":"address payable"},{"type":"uint96","name":"_fee","internalType":"uint96"}]},{"type":"error","name":"Deadline","inputs":[]},{"type":"error","name":"DestinationMissing","inputs":[]},{"type":"error","name":"DexConflict","inputs":[{"type":"uint256","name":"dexIndex","internalType":"uint256"}]},{"type":"error","name":"DexDisabled","inputs":[]},{"type":"error","name":"DexTypeInvalid","inputs":[]},{"type":"error","name":"FeeMissing","inputs":[{"type":"uint256","name":"expected","internalType":"uint256"},{"type":"uint256","name":"provided","internalType":"uint256"},{"type":"string","name":"message","internalType":"string"}]},{"type":"error","name":"FunderMismatch","inputs":[{"type":"address","name":"expected","internalType":"address"},{"type":"address","name":"provided","internalType":"address"}]},{"type":"error","name":"NativeMissing","inputs":[{"type":"uint256","name":"pathIndex","internalType":"uint256"}]},{"type":"error","name":"ValueMismatch","inputs":[{"type":"uint256","name":"consumed","internalType":"uint256"},{"type":"uint256","name":"provided","internalType":"uint256"}]},{"type":"event","name":"AddDex","inputs":[{"type":"address","name":"executor","internalType":"address","indexed":true},{"type":"uint256","name":"dexId","internalType":"uint256","indexed":true}],"anonymous":false},{"type":"event","name":"FeesCollected","inputs":[{"type":"uint256","name":"amount","internalType":"uint256","indexed":true},{"type":"bool","name":"isNative","internalType":"bool","indexed":true}],"anonymous":false},{"type":"event","name":"FeesDistributed","inputs":[{"type":"uint256","name":"amount","internalType":"uint256","indexed":false}],"anonymous":false},{"type":"event","name":"IMSwap","inputs":[{"type":"bytes32","name":"inAndOutTokens","internalType":"bytes32","indexed":true},{"type":"uint256","name":"amountIn","internalType":"uint256","indexed":false},{"type":"address","name":"sender","internalType":"address","indexed":true},{"type":"uint256","name":"dexId","internalType":"uint256","indexed":true}],"anonymous":false},{"type":"event","name":"OwnershipTransferred","inputs":[{"type":"address","name":"previousOwner","internalType":"address","indexed":true},{"type":"address","name":"newOwner","internalType":"address","indexed":true}],"anonymous":false},{"type":"event","name":"UpdateDex","inputs":[{"type":"address","name":"executor","internalType":"address","indexed":true},{"type":"uint256","name":"dexId","internalType":"uint256","indexed":true}],"anonymous":false},{"type":"event","name":"UpdateMigrationAddresses","inputs":[{"type":"address","name":"executor","internalType":"address","indexed":true},{"type":"address[]","name":"addresses","internalType":"address[]","indexed":false}],"anonymous":false},{"type":"fallback","stateMutability":"payable"},{"type":"function","stateMutability":"payable","outputs":[],"name":"addDex","inputs":[{"type":"string","name":"_dexName","internalType":"string"},{"type":"address","name":"_router","internalType":"address"},{"type":"address","name":"_wNative","internalType":"address payable"},{"type":"uint8","name":"dexType","internalType":"enum DexTracker.DexType"}]},{"type":"function","stateMutability":"view","outputs":[{"type":"uint256","name":"","internalType":"uint256"}],"name":"amountOutFrom","inputs":[{"type":"address","name":"factory","internalType":"address"},{"type":"address","name":"tokenIn","internalType":"address"},{"type":"address","name":"tokenOut","internalType":"address"},{"type":"uint256","name":"amountIn","internalType":"uint256"}]},{"type":"function","stateMutability":"view","outputs":[{"type":"uint256","name":"tokenReserve","internalType":"uint256"},{"type":"uint256","name":"wethReserve","internalType":"uint256"}],"name":"checkPairForValidPrice","inputs":[{"type":"address","name":"factory","internalType":"address"},{"type":"address","name":"token","internalType":"address"}]},{"type":"function","stateMutability":"pure","outputs":[{"type":"uint256","name":"","internalType":"uint256"}],"name":"clamp","inputs":[{"type":"uint256","name":"amount","internalType":"uint256"},{"type":"uint256","name":"max","internalType":"uint256"}]},{"type":"function","stateMutability":"view","outputs":[{"type":"address","name":"","internalType":"address payable"}],"name":"destination","inputs":[]},{"type":"function","stateMutability":"view","outputs":[{"type":"uint64","name":"id","internalType":"uint64"},{"type":"address","name":"router","internalType":"address"},{"type":"bool","name":"disabled","internalType":"bool"},{"type":"address","name":"wNative","internalType":"address payable"},{"type":"uint8","name":"dexType","internalType":"enum DexTracker.DexType"},{"type":"string","name":"name","internalType":"string"}],"name":"dexInfo","inputs":[{"type":"uint256","name":"","internalType":"uint256"}]},{"type":"function","stateMutability":"view","outputs":[{"type":"uint256","name":"","internalType":"uint256"}],"name":"dexInfoSize","inputs":[]},{"type":"function","stateMutability":"payable","outputs":[],"name":"disableDex","inputs":[{"type":"uint256","name":"id","internalType":"uint256"},{"type":"bool","name":"disabled","internalType":"bool"}]},{"type":"function","stateMutability":"payable","outputs":[],"name":"distribute","inputs":[{"type":"uint256","name":"amount","internalType":"uint256"}]},{"type":"function","stateMutability":"payable","outputs":[],"name":"distributeAll","inputs":[{"type":"uint256","name":"amount","internalType":"uint256"}]},{"type":"function","stateMutability":"view","outputs":[{"type":"uint256","name":"","internalType":"uint256"}],"name":"fee","inputs":[]},{"type":"function","stateMutability":"view","outputs":[{"type":"uint256","name":"","internalType":"uint256"}],"name":"feeDenominator","inputs":[]},{"type":"function","stateMutability":"view","outputs":[{"type":"address","name":"","internalType":"address"},{"type":"uint256","name":"","internalType":"uint256"}],"name":"getFeeMinimum","inputs":[{"type":"address","name":"factory","internalType":"address"},{"type":"uint256","name":"_amountIn","internalType":"uint256"},{"type":"address[]","name":"path","internalType":"address[]"}]},{"type":"function","stateMutability":"view","outputs":[{"type":"uint256","name":"minimum","internalType":"uint256"}],"name":"getSingleFeeMinimum","inputs":[{"type":"address","name":"factory","internalType":"address"},{"type":"uint256","name":"_amountIn","internalType":"uint256"},{"type":"address","name":"source","internalType":"address"}]},{"type":"function","stateMutability":"view","outputs":[{"type":"address","name":"","internalType":"address"}],"name":"migrationAddresses","inputs":[{"type":"uint256","name":"","internalType":"uint256"}]},{"type":"function","stateMutability":"view","outputs":[{"type":"address","name":"","internalType":"address"}],"name":"owner","inputs":[]},{"type":"function","stateMutability":"view","outputs":[{"type":"uint256","name":"","internalType":"uint256"}],"name":"pendingDistribution","inputs":[]},{"type":"function","stateMutability":"view","outputs":[{"type":"uint256","name":"","internalType":"uint256"},{"type":"uint256","name":"","internalType":"uint256"}],"name":"pendingDistributionSegmented","inputs":[]},{"type":"function","stateMutability":"nonpayable","outputs":[],"name":"renounceOwnership","inputs":[]},{"type":"function","stateMutability":"view","outputs":[{"type":"uint256","name":"","internalType":"uint256"}],"name":"routerToDex","inputs":[{"type":"address","name":"","internalType":"address"}]},{"type":"function","stateMutability":"nonpayable","outputs":[],"name":"setMigrationAddress","inputs":[{"type":"address[]","name":"_migrationAddresses","internalType":"address[]"}]},{"type":"function","stateMutability":"payable","outputs":[],"name":"swapBalancerV2","inputs":[{"type":"uint256","name":"dexId","internalType":"uint256"},{"type":"tuple[]","name":"swaps","internalType":"struct IVault.BatchSwapStep[]","components":[{"type":"bytes32","name":"poolId","internalType":"bytes32"},{"type":"uint256","name":"assetInIndex","internalType":"uint256"},{"type":"uint256","name":"assetOutIndex","internalType":"uint256"},{"type":"uint256","name":"amount","internalType":"uint256"},{"type":"bytes","name":"userData","internalType":"bytes"}]},{"type":"address[]","name":"assets","internalType":"contract IAsset[]"},{"type":"tuple","name":"funds","internalType":"struct IVault.FundManagement","components":[{"type":"address","name":"sender","internalType":"address"},{"type":"bool","name":"fromInternalBalance","internalType":"bool"},{"type":"address","name":"recipient","internalType":"address payable"},{"type":"bool","name":"toInternalBalance","internalType":"bool"}]},{"type":"int256[]","name":"limits","internalType":"int256[]"},{"type":"uint256","name":"deadline","internalType":"uint256"},{"type":"uint256","name":"inTokenIndex","internalType":"uint256"},{"type":"uint256","name":"outTokenIndex","internalType":"uint256"},{"type":"uint256","name":"amountIn","internalType":"uint256"}]},{"type":"function","stateMutability":"payable","outputs":[],"name":"swapNativeToV2","inputs":[{"type":"uint256","name":"_dexId","internalType":"uint256"},{"type":"address","name":"recipient","internalType":"address"},{"type":"address[]","name":"_path","internalType":"address[]"},{"type":"uint256","name":"_amountIn","internalType":"uint256"},{"type":"uint256","name":"_minAmountOut","internalType":"uint256"},{"type":"uint256","name":"_deadline","internalType":"uint256"}]},{"type":"function","stateMutability":"payable","outputs":[],"name":"swapPiteas","inputs":[{"type":"uint256","name":"dexId","internalType":"uint256"},{"type":"bytes","name":"piteasCalldata","internalType":"bytes"}]},{"type":"function","stateMutability":"payable","outputs":[],"name":"swapToNativeV2","inputs":[{"type":"uint256","name":"_dexId","internalType":"uint256"},{"type":"address","name":"recipient","internalType":"address payable"},{"type":"address[]","name":"_path","internalType":"address[]"},{"type":"uint256","name":"_amountIn","internalType":"uint256"},{"type":"uint256","name":"_minAmountOut","internalType":"uint256"},{"type":"uint256","name":"_deadline","internalType":"uint256"}]},{"type":"function","stateMutability":"payable","outputs":[],"name":"swapTokenV2","inputs":[{"type":"uint256","name":"_dexId","internalType":"uint256"},{"type":"address","name":"recipient","internalType":"address"},{"type":"address[]","name":"_path","internalType":"address[]"},{"type":"uint256","name":"_amountIn","internalType":"uint256"},{"type":"uint256","name":"_minAmountOut","internalType":"uint256"},{"type":"uint256","name":"_deadline","internalType":"uint256"}]},{"type":"function","stateMutability":"payable","outputs":[],"name":"swapUniswapV3","inputs":[{"type":"uint256","name":"dexId","internalType":"uint256"},{"type":"tuple","name":"params","internalType":"struct ISwapRouter.ExactInputParams","components":[{"type":"bytes","name":"path","internalType":"bytes"},{"type":"address","name":"recipient","internalType":"address"},{"type":"uint256","name":"deadline","internalType":"uint256"},{"type":"uint256","name":"amountIn","internalType":"uint256"},{"type":"uint256","name":"amountOutMinimum","internalType":"uint256"}]}]},{"type":"function","stateMutability":"nonpayable","outputs":[],"name":"transferOwnership","inputs":[{"type":"address","name":"newOwner","internalType":"address"}]},{"type":"function","stateMutability":"payable","outputs":[],"name":"updateDex","inputs":[{"type":"uint256","name":"index","internalType":"uint256"},{"type":"string","name":"_name","internalType":"string"},{"type":"address","name":"_wNative","internalType":"address payable"},{"type":"uint8","name":"dexType","internalType":"enum DexTracker.DexType"}]},{"type":"function","stateMutability":"view","outputs":[{"type":"address","name":"","internalType":"address payable"}],"name":"wNative","inputs":[]},{"type":"function","stateMutability":"payable","outputs":[],"name":"wrap","inputs":[{"type":"address","name":"_wNative","internalType":"address payable"}]},{"type":"receive","stateMutability":"payable"}],
  quotesV3: [{"inputs":[{"internalType":"address payable","name":"_destination","type":"address"},{"internalType":"address payable","name":"_wNative","type":"address"},{"internalType":"uint96","name":"_fee","type":"uint96"}],"stateMutability":"nonpayable","type":"constructor"},{"inputs":[],"name":"Deadline","type":"error"},{"inputs":[],"name":"DestinationMissing","type":"error"},{"inputs":[{"internalType":"uint256","name":"dexIndex","type":"uint256"}],"name":"DexConflict","type":"error"},{"inputs":[],"name":"DexDisabled","type":"error"},{"inputs":[],"name":"DexTypeInvalid","type":"error"},{"inputs":[],"name":"FallbackNotAllowed","type":"error"},{"inputs":[{"internalType":"uint256","name":"expected","type":"uint256"},{"internalType":"uint256","name":"provided","type":"uint256"},{"internalType":"string","name":"message","type":"string"}],"name":"FeeMissing","type":"error"},{"inputs":[{"internalType":"address","name":"expected","type":"address"},{"internalType":"address","name":"provided","type":"address"}],"name":"FunderMismatch","type":"error"},{"inputs":[{"internalType":"uint256","name":"pathIndex","type":"uint256"}],"name":"NativeMissing","type":"error"},{"inputs":[{"internalType":"uint256","name":"consumed","type":"uint256"},{"internalType":"uint256","name":"provided","type":"uint256"}],"name":"ValueMismatch","type":"error"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"executor","type":"address"},{"indexed":true,"internalType":"uint256","name":"dexId","type":"uint256"}],"name":"AddDex","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"amount","type":"uint256"},{"indexed":true,"internalType":"bool","name":"isNative","type":"bool"}],"name":"FeesCollected","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"FeesDistributed","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"inAndOutTokens","type":"bytes32"},{"indexed":false,"internalType":"uint256","name":"amountIn","type":"uint256"},{"indexed":true,"internalType":"address","name":"sender","type":"address"},{"indexed":true,"internalType":"uint256","name":"dexId","type":"uint256"}],"name":"IMSwap","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"executor","type":"address"},{"indexed":true,"internalType":"uint256","name":"dexId","type":"uint256"}],"name":"UpdateDex","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"executor","type":"address"},{"indexed":false,"internalType":"address[]","name":"addresses","type":"address[]"}],"name":"UpdateMigrationAddresses","type":"event"},{"inputs":[{"internalType":"string","name":"_dexName","type":"string"},{"internalType":"address","name":"_router","type":"address"},{"internalType":"address payable","name":"_wNative","type":"address"},{"internalType":"enum DexTracker.DexType","name":"dexType","type":"uint8"}],"name":"addDex","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"address","name":"factory","type":"address"},{"internalType":"address","name":"tokenIn","type":"address"},{"internalType":"address","name":"tokenOut","type":"address"},{"internalType":"uint256","name":"amountIn","type":"uint256"}],"name":"amountOutFrom","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"factory","type":"address"},{"internalType":"address","name":"token","type":"address"}],"name":"checkPairForValidPrice","outputs":[{"internalType":"uint256","name":"tokenReserve","type":"uint256"},{"internalType":"uint256","name":"wethReserve","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"uint256","name":"max","type":"uint256"}],"name":"clamp","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"pure","type":"function"},{"inputs":[],"name":"destination","outputs":[{"internalType":"address payable","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"dexInfo","outputs":[{"internalType":"uint64","name":"id","type":"uint64"},{"internalType":"address","name":"router","type":"address"},{"internalType":"bool","name":"disabled","type":"bool"},{"internalType":"address payable","name":"wNative","type":"address"},{"internalType":"enum DexTracker.DexType","name":"dexType","type":"uint8"},{"internalType":"string","name":"name","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"dexInfoSize","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"id","type":"uint256"},{"internalType":"bool","name":"disabled","type":"bool"}],"name":"disableDex","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"distribute","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"distributeAll","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[],"name":"feeDenominator","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"feeNumerator","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"factory","type":"address"},{"internalType":"uint256","name":"_amountIn","type":"uint256"},{"internalType":"address[]","name":"path","type":"address[]"}],"name":"getFeeMinimum","outputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"factory","type":"address"},{"internalType":"uint256","name":"_amountIn","type":"uint256"},{"internalType":"address","name":"source","type":"address"}],"name":"getSingleFeeMinimum","outputs":[{"internalType":"uint256","name":"minimum","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"migrationAddresses","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"pendingDistribution","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"pendingDistributionSegmented","outputs":[{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"routerToDex","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address[]","name":"_migrationAddresses","type":"address[]"}],"name":"setMigrationAddress","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"dexId","type":"uint256"},{"components":[{"internalType":"bytes32","name":"poolId","type":"bytes32"},{"internalType":"uint256","name":"assetInIndex","type":"uint256"},{"internalType":"uint256","name":"assetOutIndex","type":"uint256"},{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"bytes","name":"userData","type":"bytes"}],"internalType":"struct IVault.BatchSwapStep[]","name":"swaps","type":"tuple[]"},{"internalType":"contract IAsset[]","name":"assets","type":"address[]"},{"components":[{"internalType":"address","name":"sender","type":"address"},{"internalType":"bool","name":"fromInternalBalance","type":"bool"},{"internalType":"address payable","name":"recipient","type":"address"},{"internalType":"bool","name":"toInternalBalance","type":"bool"}],"internalType":"struct IVault.FundManagement","name":"funds","type":"tuple"},{"internalType":"int256[]","name":"limits","type":"int256[]"},{"internalType":"uint256","name":"deadline","type":"uint256"},{"internalType":"uint256","name":"inTokenIndex","type":"uint256"},{"internalType":"uint256","name":"outTokenIndex","type":"uint256"},{"internalType":"uint256","name":"amountIn","type":"uint256"}],"name":"swapBalancerV2","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_dexId","type":"uint256"},{"internalType":"address","name":"recipient","type":"address"},{"internalType":"address[]","name":"_path","type":"address[]"},{"internalType":"uint256","name":"_amountIn","type":"uint256"},{"internalType":"uint256","name":"_minAmountOut","type":"uint256"},{"internalType":"uint256","name":"_deadline","type":"uint256"}],"name":"swapNativeToV2","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"dexId","type":"uint256"},{"internalType":"bytes","name":"piteasCalldata","type":"bytes"}],"name":"swapPiteas","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_dexId","type":"uint256"},{"internalType":"address payable","name":"recipient","type":"address"},{"internalType":"address[]","name":"_path","type":"address[]"},{"internalType":"uint256","name":"_amountIn","type":"uint256"},{"internalType":"uint256","name":"_minAmountOut","type":"uint256"},{"internalType":"uint256","name":"_deadline","type":"uint256"}],"name":"swapToNativeV2","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_dexId","type":"uint256"},{"internalType":"address","name":"recipient","type":"address"},{"internalType":"address[]","name":"_path","type":"address[]"},{"internalType":"uint256","name":"_amountIn","type":"uint256"},{"internalType":"uint256","name":"_minAmountOut","type":"uint256"},{"internalType":"uint256","name":"_deadline","type":"uint256"}],"name":"swapTokenV2","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"dexId","type":"uint256"},{"components":[{"internalType":"bytes","name":"path","type":"bytes"},{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"uint256","name":"amountOutMinimum","type":"uint256"}],"internalType":"struct IV3SwapRouter.ExactInputParams","name":"params","type":"tuple"}],"name":"swapUniswapV3","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"index","type":"uint256"},{"internalType":"string","name":"_name","type":"string"},{"internalType":"address payable","name":"_wNative","type":"address"},{"internalType":"enum DexTracker.DexType","name":"dexType","type":"uint8"}],"name":"updateDex","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[],"name":"wNative","outputs":[{"internalType":"address payable","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address payable","name":"_wNative","type":"address"}],"name":"wrap","outputs":[],"stateMutability":"payable","type":"function"},{"stateMutability":"payable","type":"receive"}],
  wrapper: [
    {
      constant: true,
      inputs: [],
      name: "name",
      outputs: [{ name: "", type: "string" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: false,
      inputs: [
        { name: "guy", type: "address" },
        { name: "wad", type: "uint256" },
      ],
      name: "approve",
      outputs: [{ name: "", type: "bool" }],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "totalSupply",
      outputs: [{ name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: false,
      inputs: [
        { name: "src", type: "address" },
        { name: "dst", type: "address" },
        { name: "wad", type: "uint256" },
      ],
      name: "transferFrom",
      outputs: [{ name: "", type: "bool" }],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: false,
      inputs: [{ name: "wad", type: "uint256" }],
      name: "withdraw",
      outputs: [],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "decimals",
      outputs: [{ name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: true,
      inputs: [{ name: "", type: "address" }],
      name: "balanceOf",
      outputs: [{ name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: true,
      inputs: [],
      name: "symbol",
      outputs: [{ name: "", type: "string" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    {
      constant: false,
      inputs: [
        { name: "dst", type: "address" },
        { name: "wad", type: "uint256" },
      ],
      name: "transfer",
      outputs: [{ name: "", type: "bool" }],
      payable: false,
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      constant: false,
      inputs: [],
      name: "deposit",
      outputs: [],
      payable: true,
      stateMutability: "payable",
      type: "function",
    },
    {
      constant: true,
      inputs: [
        { name: "", type: "address" },
        { name: "", type: "address" },
      ],
      name: "allowance",
      outputs: [{ name: "", type: "uint256" }],
      payable: false,
      stateMutability: "view",
      type: "function",
    },
    { payable: true, stateMutability: "payable", type: "fallback" },
    {
      anonymous: false,
      inputs: [
        { indexed: true, name: "src", type: "address" },
        { indexed: true, name: "guy", type: "address" },
        { indexed: false, name: "wad", type: "uint256" },
      ],
      name: "Approval",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        { indexed: true, name: "src", type: "address" },
        { indexed: true, name: "dst", type: "address" },
        { indexed: false, name: "wad", type: "uint256" },
      ],
      name: "Transfer",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        { indexed: true, name: "dst", type: "address" },
        { indexed: false, name: "wad", type: "uint256" },
      ],
      name: "Deposit",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        { indexed: true, name: "src", type: "address" },
        { indexed: false, name: "wad", type: "uint256" },
      ],
      name: "Withdrawal",
      type: "event",
    },
  ],
};
