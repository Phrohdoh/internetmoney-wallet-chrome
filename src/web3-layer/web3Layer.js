import axios from 'axios';
import BigNumber from 'bignumber.js';
import CryptoJS from 'crypto-js';
import { signTypedData, recoverTypedSignature } from '@metamask/eth-sig-util';
import { hdkey } from 'ethereumjs-wallet';
import fetch from 'node-fetch';
import { Web3 } from 'web3';
const bip39 = require('bip39');
const ethers = require('ethers');

import IntegerUnits from "utils/IntegerUnits";
import Logger from "utils/logger";
import missingTokenIconIcon from "../assets/images/missing_token_icon_icon.png";
import ABI from "./ABI.js";
import config from './config.json';
import pricingSupportedChainIDs from "./dexScreenerNetworkChainIds.js";
import { getHash, verifyPassword } from "./hashing";
import nativeAddress from "./nativeAddress.json";
import {
  dynamicSort,
  filterDexScrNetworkSpecificQueries,
  getErrorCorrection,
  parseIntCustom,
  getDirectTransfers,
  getFeeMultiplier,
  getGasPriceMultiplier,
  getGasMultiplier,
  retry,
} from "./utils";
import {
  FEE_RANGE_LOW_PERCENT_OFFSET,
  FEE_RANGE_HIGH_PERCENT_OFFSET,
  GAS_PRICE_RANGE_LOW_PERCENT_OFFSET,
  GAS_PRICE_RANGE_HIGH_PERCENT_OFFSET,
  GAS_RANGE_LOW_PERCENT_OFFSET,
  GAS_RANGE_HIGH_PERCENT_OFFSET,
} from 'constants';
import { CHAIN_IDS } from 'constants/common';
import { getErrorResponse } from 'utils/errors';
import { Strings } from 'resources';

BigNumber.config({ ROUNDING_MODE: BigNumber.ROUND_FLOOR, EXPONENTIAL_AT: 1e+9 })

const WALLET_FEE = 0.00729;

const DEFAULT_GAS = 21_000;

// Updated networkDetails Object:
// [added New Parameter 'txnType' to handle different types of transactions with
//  respective to different Newtorks - will come to use when having multiple networks]

// const networkDetails = {
//     txnType: 2,
//     networkName: 'PulseChain Testnet',
//     rpc: 'https://rpc.v2b.testnet.pulsechain.com',
//     chainId: 941,
//     sym: 'PLS',
//     explore: 'https://scan.v2b.testnet.pulsechain.com'
// }

const rpcList = {
  [CHAIN_IDS.ETHEREUM]: "https://rpc.ankr.com/eth",
  [CHAIN_IDS.BINANCE]: "https://bsc-dataseed1.binance.org",
  [CHAIN_IDS.POLYGON]: "https://polygon-rpc.com",
  [CHAIN_IDS.AVAX]: "https://api.avax.network/ext/bc/C/rpc",
  [CHAIN_IDS.FANTOM]: "https://rpc.ankr.com/fantom",
};
const supportedChainIds = [
  [CHAIN_IDS.ETHEREUM],
  [CHAIN_IDS.BINANCE],
  [CHAIN_IDS.POLYGON],
  [CHAIN_IDS.AVAX],
  [CHAIN_IDS.FANTOM]
];

const web3Connections = {};

const getWeb3 = (rpcUrl) => {
  if (web3Connections[rpcUrl]) return web3Connections[rpcUrl];
  web3Connections[rpcUrl] = new Web3(rpcUrl);
  return web3Connections[rpcUrl];
};

export const abi = ABI;

// Utility function to include the `data` and `to` parameters automatically
// because web3.js v4 is not currently doing so, resulting in an error with
// certain RPC nodes: https://github.com/web3/web3.js/issues/6379
const callContract = async (contractInstance, method, ...parameters) => {
  try {
    const callData = {
      data: contractInstance.methods[method](...parameters).encodeABI(),
      to: contractInstance.options.address,
    }
    const result = await contractInstance.methods[method](...parameters).call(callData);
    return result;
  } catch (err) {
    Logger.error('Error in callContract', {
      contractAddress: contractInstance.options.address,
      method,
    });
    console.error(err);
    throw err;
  }
}

// *************************************************************
// Wallet/Account Manage Functions
// *************************************************************

// #1
export const generateAccountFromMnemonicOf = async (accountIndex, mnemonic) => {
  try {
    let publicAddress = '';
    let privateKey = '';
  
    const hdPathString = "m/44'/60'/0'/0";
  
    const real = mnemonic
      .trim()
      .split(' ')
      .map(function (word) {
        return word.trim();
      })
      .filter((word) => {
        return word !== '';
      })
      .join(' ');
  
    const seed = await bip39.mnemonicToSeed(real);
    const hdWallet = hdkey.fromMasterSeed(seed);
    const root = hdWallet.derivePath(hdPathString);
  
    const child = root.deriveChild(accountIndex);
    const account = child.getWallet();
  
    publicAddress = account.getAddress().toString('hex');
    privateKey = account.getPrivateKeyString();

    return {
      address: '0x' + publicAddress,
      privateKey: privateKey,
    };
  } catch (err) {
    Logger.error('Error in generateAccountFromMnemonicOf');
    console.error(err);
    throw err;
  }
};

// A record of (address -> private key), only kept in memory for the session
let privateKeysCache = {};

// #2
export const retrievePrivateKeyOf = async (address, walletEncObject, password) => {
  if (privateKeysCache[address]) {
    return {
      success: true,
      privateKey: privateKeysCache[address],
    };
  }
  try {
    const targetWallet = walletEncObject.find(account => `0x${account.address.toLowerCase()}` === address.toLowerCase());
    const wallet = ethers.Wallet.fromEncryptedJsonSync(JSON.stringify(targetWallet), password);
    const privateKey = wallet.privateKey;
    privateKeysCache[address] = privateKey;
    return {
      success: true,
      privateKey,
    };
  } catch (err) {
    Logger.error('Error in retrievePrivateKeyOf');
    console.error(err);
    return getErrorResponse(err);
  }
};

export const createAccountFromPrivateKey = async (privateKey, password) => {
  try {
    const account = new ethers.Wallet(privateKey);
    const encrypted = await account.encrypt(password, {
      scrypt: {
        N: 8192,
        p: 1,
        r: 8,
      },
    })
    return JSON.parse(encrypted);
  } catch (err) {
    Logger.error('Error in createAccountFromPrivateKey');
    console.error(err);
    return getErrorResponse(err);
  }
}

// #3
export const isMnemonicValid = (mnemonic) => {
  try {
    return bip39.validateMnemonic(mnemonic);
  } catch (err) {
    Logger.error('Error in isMnemonicValid');
    console.error(err);
    return getErrorResponse(err);
  }
};

// Proposed replacement of the above function - `encryptMnemonic`
/**
 * This function is used to encrypt the Mnemonic phrase
 *
 * @param {string} sMnemonic - Mnemonic phrase to encrypt
 * @param {string} nPassword - User's password to encrypt with
 * @returns Encrypted Mnemonic phrase
 */
export const encryptMnemonicSecurely = async (sMnemonic, nPassword) => {
  try {
    if (!sMnemonic?.trim()) throw new Error('Mnemonic is required');
    if (!nPassword) throw new Error('Password is required');
  
    const sPasswordHash = await getPasswordHash(nPassword.toString());
  
    const mnemonic = CryptoJS.AES.encrypt(sMnemonic, sPasswordHash).toString();
  
    return { success: true, mnemonic };
  } catch (err) {
    Logger.error('Error in encryptMnemonicSecurely');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #5
export const decryptMnemonic = (cipherMnemonic, password, passwordHash) => {
  try {
    let bytes, originalMnemonic;

    // for users who had faced some error in hashin
    if (!originalMnemonic) {
      try {
        bytes = CryptoJS.AES.decrypt(cipherMnemonic, null);
      } catch (err) {
        Logger.error('Error in decryptMnemonic');
        console.error(err);
        originalMnemonic = '';
      }
    }

    // for previous users with old version of the app
    if (!originalMnemonic) {
      try {
        bytes = CryptoJS.AES.decrypt(cipherMnemonic, password);
        originalMnemonic = bytes.toString(CryptoJS.enc.Utf8);
      } catch (err) {
        Logger.error('Error in decryptMnemonic');
        console.error(err);
        originalMnemonic = '';
      }
    }

    // for current users with new version of the app & working hash
    if (!originalMnemonic) {
      bytes = CryptoJS.AES.decrypt(cipherMnemonic, passwordHash);
      originalMnemonic = bytes.toString(CryptoJS.enc.Utf8);
    }
    if (!originalMnemonic) throw new Error('Failed to update hashes');

    return { success: true, mnemonic: originalMnemonic };
  } catch (err) {
    Logger.error('Error in decryptMnemonic');
    console.error(err);
    return getErrorResponse(err);
  }
};

// Proposed replacement of the above function - `decryptMnemonic`
/**
 * This function is used to decrypt the Mnemonic phrase
 *
 * @param {string} sMnemonicCipher - Encrypted Mnemonic to decrypt
 * @param {string} nPassword - User's password to decrypt with
 * @returns Decrypted Mnemonic phrase
 */
export const decryptMnemonicSecurely = async (sMnemonicCipher, nPassword) => {
  try {
    if (!sMnemonicCipher?.trim()) throw new Error('Mnemonic Cipher is required');
    if (!nPassword) throw new Error('Password is required');

    const sPasswordHash = await getPasswordHash(nPassword.toString());

    const mnemonic = CryptoJS.AES.decrypt(
      sMnemonicCipher,
      sPasswordHash
    ).toString(CryptoJS.enc.Utf8);

    if (!mnemonic) throw new Error('Invalid password or Mnemonic Cipher');

    return { success: true, mnemonic };
  } catch (err) {
    Logger.error('Error in decryptMnemonicSecurely');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #6
export const generateRandomMnemonic = (numberOfWords) => {
  try {
    if (typeof numberOfWords === 'number') {
      const diff = numberOfWords - 12;
  
      if (numberOfWords >= 12 && numberOfWords <= 24 && diff % 3 === 0) {
        const mnemonic = bip39.generateMnemonic(128 + 32 * (diff / 3));
        return { success: true, mnemonic };
      } else throw new Error('Invalid mnemonic length');
    } else throw new Error("Expected 'number' as data type for 'numberOfWords'");
  } catch (err) {
    Logger.error('Error in generateRandomMnemonic', {
      numberOfWords,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

/* 'intialDetails' = {mnemonic: '', password: '', numberOfWords: ''}
* Case 1:
* If user is Compeletly New, then can leave mnemonic = undefined
* (It auto generates random mnemonic & creates an account)

* Case 2:
* If user has his mnemonic & looking to import, then all parameters required
*/

// Called Only ONCE!!
// Ex:
// const intialDetails = {mnemonic: null, password: 'password', numberOfWords: 12}

const getBlockNumberOfAllChains = async () => {
  try {
    const getBlockNumber = async (chainId) => {
      const web3 = new Web3(rpcList[chainId]);
      const obj = {};
      obj[chainId] = Number(await web3.eth.getBlockNumber());
      return obj;
    };
    const promises = [];

    supportedChainIds.forEach((chainId) => {
      promises.push(getBlockNumber(chainId));
    });

    const blockNumbers = await Promise.all(promises);

    return { success: true, blockNumbers };
  } catch (err) {
    Logger.error('Error in getBlockNumberOfAllChains');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #7
export const createNewWallet = async (intialDetails, networkDetails) => {
  let mnemonic = '';
  try {
    if (
      typeof intialDetails === 'undefined' ||
      Object.keys(intialDetails).length === 0
    ) {
      throw new Error('Invalid parameter');
    }

    if (!intialDetails.mnemonic) {
      if (typeof intialDetails.numberOfWords !== 'undefined') {
        const mnemonicResponse = generateRandomMnemonic(intialDetails.numberOfWords);
        if (mnemonicResponse.error) throw new Error(mnemonicResponse.error);
        mnemonic = mnemonicResponse.mnemonic;
      } else {
        throw new Error('Invalid number of words');
      }
    } else {
      mnemonic = intialDetails.mnemonic.toLowerCase();
    }

    const words = mnemonic
      .split(/\s+/)
      .filter(word => word.length > 0);
    words.forEach(word => {
      if (!bip39.wordlists.english.includes(word)) {
        throw new Error(`The word "${word}" was not found in the BIP39 English word list.`);
      }
    });

    if (!isMnemonicValid(mnemonic)) {
      throw new Error('Invalid Seed Phrase');
    }

    const accountDetails = await generateAccountFromMnemonicOf(0, mnemonic);

    const importedAccount = await importAccount(
      [],
      accountDetails.privateKey,
      intialDetails.password,
      networkDetails
    );
    if (importedAccount.error) throw new Error(importedAccount.error);

    const cipherMnemonic = await encryptMnemonicSecurely(
      mnemonic,
      intialDetails.password
    );
    if (cipherMnemonic.error) throw new Error(cipherMnemonic.error);

    const blockNumbersResponse = await getBlockNumberOfAllChains();
    if (blockNumbersResponse.error) throw new Error(blockNumbersResponse.error);

    const sPasswordHash = await getPasswordHash(
      intialDetails.password.toString()
    );

    return {
      success: true,
      mnemonic: cipherMnemonic.mnemonic,
      walletObject: importedAccount.object,
      publicAddress: accountDetails.address,
      passwordHash: sPasswordHash,
      blockNumbers: blockNumbersResponse.blockNumbers,
    };
  } catch (err) {
    Logger.error('Error in createNewWallet');
    console.error(err);
    return getErrorResponse(err);
  }
};

// # Helper Function
export const getAllAccounts = async (accountCount, mnemonic) => {
  const hdPathString = "m/44'/60'/0'/0";

  const real = mnemonic
    .trim()
    .split(' ')
    .map(function (word) {
      return word.trim();
    })
    .filter((word) => {
      return word !== '';
    })
    .join(' ');

  const seed = await bip39.mnemonicToSeed(real);
  const hdWallet = hdkey.fromMasterSeed(seed);
  const root = hdWallet.derivePath(hdPathString);

  const accounts = [];
  for (let i = 0; i < accountCount; i++) {
    const child = root.deriveChild(i);
    const account = child.getWallet();

    accounts.push('0x' + account.getAddress().toString('hex'));
  }
  return accounts;
};

export const accountsList = (walletEncObject) =>
  walletEncObject.map(({ address }) => `0x${address}`);


// #8
export const createNewAccount = async (
  cipherMnemonic,
  walletEncObject,
  password,
  networkDetails
) => {
  const accounts = accountsList(walletEncObject);

  try {
    const mnemonic = await decryptMnemonicSecurely(cipherMnemonic, password);
    if (mnemonic.error) throw new Error(mnemonic.error);

    let index = 1;

    const mnAccounts = await getAllAccounts(
      walletEncObject.length + 1,
      mnemonic.mnemonic
    );

    for (index = 0; index < mnAccounts.length; index++) {
      const account = mnAccounts[index];
      if (!accounts.includes(account)) {
        break;
      }
    }

    const accountDetails = await generateAccountFromMnemonicOf(
      index,
      mnemonic.mnemonic
    );

    const importedAccount = await importAccount(
      walletEncObject,
      accountDetails.privateKey,
      password,
      networkDetails
    );
    if (importedAccount.error) throw new Error(importedAccount.error);

    const account = importedAccount.account;

    return {
      success: true,
      object: importedAccount.object,
      account: { publicAddress: account.publicAddress, value: account.value },
    };
  } catch (err) {
    Logger.error('Error in createNewAccount');
    console.error(err);
    return getErrorResponse(err);
  }
};

export const getPublicAddressOfIndex = async (
  cipherMnemonic,
  password,
  index
) => {
  try {
    const mnemonic = await decryptMnemonicSecurely(cipherMnemonic, password);
    if (mnemonic.error) throw new Error(mnemonic.error);
  
    const accountDetails = await generateAccountFromMnemonicOf(
      index,
      mnemonic.mnemonic
    );
    if (accountDetails.error) throw new Error(accountDetails.error);
  
    return accountDetails.address.toLowerCase();
  } catch (err) {
    Logger.error('Error in getPublicAddressOfIndex');
    console.error(err);
    throw err;
  }
}

// #9
export const importAccount = async (
  walletEncObject,
  privateKey,
  password,
  networkDetails,
) => {
  const web3 = getWeb3(networkDetails.rpc);

  try {
    if (privateKey.slice(0, 2) !== '0x') {
      privateKey = `0x${privateKey}`;
    }

    if (privateKey.length != 66) throw new Error('Invalid Private Key');

    const imported = await createAccountFromPrivateKey(privateKey, password);

    const address = `0x${imported.address}`;

    const balance = await web3.eth.getBalance(address);

    return {
      success: true,
      object: [...walletEncObject, imported],
      account: {
        publicAddress: address,
        value: parseFloat(parseIntCustom(balance, 10)).toString(),
      },
    };
  } catch (err) {
    Logger.error('Error in importAccount');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #10
export const checkImportAccount = async (walletEncObject, privateKey, networkDetails) => {
  try {
    const web3 = getWeb3(networkDetails.rpc);

    if (privateKey.slice(0, 2) !== '0x') {
      privateKey = `0x${privateKey}`;
    }

    if (privateKey.length != 66) {
      throw { message: 'Invalid Private Key' };
    }

    const walletObject = await web3.eth.accounts.privateKeyToAccount(
      privateKey
    );
    const accounts = accountsList(walletEncObject);
    if (accounts.includes(walletObject.address.toLowerCase())) {
      throw { success: false, message: 'Account is already Imported' };
    }
    return { success: true };
  } catch (err) {
    Logger.error('Error in checkImportAccount');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #11
/**
 * UNUSED 
 */
export const getAccounts = async (walletEncObject, networkDetails) => {
  try {
    const web3 = getWeb3(networkDetails.rpc);
  
    const addresses = accountsList(walletEncObject);
  
    const result = await Promise.all(
      addresses.map((publicAddress) => (
        web3.eth.getBalance(publicAddress)
      ))
    );
  
    return addresses.map((publicAddress, index) => ({
      publicAddress,
      value: new IntegerUnits(result[index]),
    }));
  } catch (err) {
    console.error(err);
  }
};

// #12
export const getSingleAccount = async (accountAddress, networkDetails) => {
  try {
    const web3 = getWeb3(networkDetails.rpc);

    const promises = [];

    promises.push(web3.eth.getBalance(accountAddress));
    promises.push(getUSDPrice('', networkDetails));
    const result = await Promise.all(promises);

    const balance = new IntegerUnits(result[0]);
    const usdPrice = result[1];

    const price = usdPrice.success
      ? usdPrice.usdPrice
      : new IntegerUnits(0n, 2n);

    const accountDetails = {
      publicAddress: accountAddress.toString(),
      value: balance,
      price: price,
    };

    return {
      success: true,
      account: accountDetails,
      tokenValues: {
        balance,
        usdPrice: price,
      },
    };
  } catch (err) {
    Logger.error('Error in getSingleAccount');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #13
export const checkPasswordSecurely = async (password, passwordHash) => {
  try {
    return await verifyPassword(password, passwordHash);
  } catch (err) {
    Logger.error('Error in checkPasswordSecurely');
    console.error(err);
    return false;
  }
};

export const checkPassword = (password, passwordHash) => {
  try {
    const calPasswordHash = Web3.utils.keccak256(
      Web3.utils.keccak256(password.toString())
    );

    return calPasswordHash == passwordHash;
  } catch (err) {
    Logger.error('Error in checkPassword');
    console.error(err);
    return false;
  }
};

export const getPasswordHash = async (password) => {
  try {
    return getHash(password.toString());
  } catch (err) {
    Logger.error('Error in getPasswordHash');
    console.error(err);
    return null;
  }
};

export const getUpdatedHashes = async (
  password,
  passwordHash,
  cipherMnemonic
) => {
  try {
    const sPasswordHash = await getPasswordHash(password.toString());

    const mnemonic = decryptMnemonic(cipherMnemonic, password, passwordHash);
    if (!mnemonic.success) {
      throw { message: mnemonic.error };
    }
    const mnemonicHash = await encryptMnemonicSecurely(
      mnemonic.mnemonic,
      password
    );
    if (!mnemonicHash.success) {
      throw { message: mnemonicHash.error };
    }

    return {
      success: true,
      passwordHash: sPasswordHash,
      mnemonic: mnemonicHash.mnemonic,
    };
  } catch (err) {
    Logger.error('Error in getUpdatedHashes');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #14
export const changePassword = async (
  walletEncObject,
  cipherMnemonic,
  oldPassword,
  newPassword
) => {
  try {
    const decrypted = await Promise.all(
      walletEncObject.map(account => (
        ethers.Wallet.fromEncryptedJson(JSON.stringify(account), oldPassword)
      ))
    );

    const mnemonic = await decryptMnemonicSecurely(cipherMnemonic, oldPassword);
    if (!mnemonic.success) throw new Error('Failed to Change Password');

    const cipherMnemonicNew = await encryptMnemonicSecurely(
      mnemonic.mnemonic,
      newPassword
    );
    if (!cipherMnemonicNew.success) throw new Error('Failed to Change Password');

    walletEncObject = await Promise.all(
      decrypted.map(account => (
        account.encrypt(newPassword, {
          scrypt: {
            N: 8192,
            p: 1,
            r: 8,
          },
        })
      ))
    );

    const sPasswordHash = await getPasswordHash(newPassword.toString());

    return {
      success: true,
      mnemonic: cipherMnemonicNew.mnemonic,
      walletObject: walletEncObject,
      passwordHash: sPasswordHash,
    };
  } catch (err) {
    Logger.error('Error in changePassword');
    console.error(err);
    return getErrorResponse(err);
  }
};

// *************************************************************
// Send Token/Crypto,
// TO/FROM Address/ENS Validation &
// OR Code Validation Functions
// *************************************************************


const getMedianFee = (fees) => {
  const sorted = fees.sort((a, b) => {
    if (a < b) {
      return -1;
    }
    if (a > b) {
      return 1;
    }
    return 0;
  });
  const middle = sorted.length / 2;
  const median = sorted.length % 2 === 0
    ? (sorted[middle - 1] + sorted[middle]) / 2n
    : sorted[Math.floor(middle)];
  return median;
}

// Gets the median for each of the last 5 blocks, then returns the median of those medians
export const getMedianMaxPriorityFeePerGas = async (networkDetails) => {
  try {
    // Attempt 3 times, because occassionally the RPC will return null for the block
    return retry(async () => {
      const web3 = getWeb3(networkDetails.rpc);
      const blockNumber = Number(await web3.eth.getBlockNumber());
      const medianFees = [];
      for (let i = 0; i < 3; i += 1) {
        const block = await web3.eth.getBlock(blockNumber - i, true);
        const transactions = block.transactions;
        const priorityFees = transactions
          .map(transaction => transaction.maxPriorityFeePerGas)
          .filter(maxPriorityFeePerGas => maxPriorityFeePerGas !== undefined)
          .filter(maxPriorityFeePerGas => maxPriorityFeePerGas !== 0n);
        if (priorityFees.length > 0) {
          medianFees.push(getMedianFee(priorityFees));
        }
      }
      if (medianFees.length === 0) {
        return new IntegerUnits(0);
      }
      const median = getMedianFee(medianFees);
      return new IntegerUnits(median);
    }, 3);
  } catch (e) {
    Logger.error('Error in getMedianMaxPriorityFeePerGas');
    console.error(err);
    return new IntegerUnits(0);
  }
};

// #16
export const validateAddressFormat = (publicAddress) => {
  return Web3.utils.isAddress(publicAddress);
};

// #17
export const getContractInstance = async (
  contractAddress,
  contractABI,
  networkDetails,
) => {
  try {
    const web3 = getWeb3(networkDetails.rpc);

    web3.eth.Contract.handleRevert = true;

    const contractInstance = new web3.eth.Contract(
      contractABI,
      contractAddress
    );

    return { success: true, contractInstance: contractInstance };
  } catch (err) {
    Logger.error('Error in getContractInstance', {
      contractAddress,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// #18
export const getENSAddress = async (toAddressOrENS) => {
  try {
    const ethereum = rpcList[1];
    const web3 = getWeb3(ethereum);
    const address = await web3.eth.ens.getAddress(toAddressOrENS);
    return { success: true, publicAddress: address };
  } catch (err) {
    Logger.error('Error in getENSAddress');
    console.error(err);
    return getErrorResponse(err);
  }
};

// **********

// #19
export const getGasEstimate = async (
  value,
  fromAddress,
  toAddress,
  data,
  networkDetails
) => {
  try {
    const web3 = getWeb3(networkDetails.rpc);
    const txnObject = {
      from: fromAddress,
      to: toAddress,
      value: value.value,
      data,
    };
    const gas = data
      ? new IntegerUnits(await web3.eth.estimateGas(txnObject), 0)
      : new IntegerUnits(DEFAULT_GAS, 0);
    return {
      success: true,
      gas,
    };
  } catch (err) {   
    Logger.error('Error in getGasEstimate');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #20
export const getFeeDetails = async (networkDetails) => {
  let range,
    gasPrice,
    feeMultiplier,
    maxFeePerGas,
    maxPriorityFeePerGas;
  try {
    const web3 = getWeb3(networkDetails.rpc);

    let [gasPriceResult, medianMaxPriorityFee] = await Promise.all([
      web3.eth.getGasPrice(),
      getMedianMaxPriorityFeePerGas(networkDetails)
    ]);

    gasPrice = new IntegerUnits(gasPriceResult);
    feeMultiplier = getFeeMultiplier(networkDetails);
    
    // Max Fee Per Gas
    maxFeePerGas = gasPrice.multiply(feeMultiplier);

    const maxFeeRange = {
      low: maxFeePerGas.multiply(1 + FEE_RANGE_LOW_PERCENT_OFFSET),
      medium: maxFeePerGas,
      high: maxFeePerGas.multiply(1 + FEE_RANGE_HIGH_PERCENT_OFFSET),
    };

    // Max Priority Fee Per Gas
    if (medianMaxPriorityFee.gt(gasPrice)) {
      medianMaxPriorityFee = gasPrice;
    }
    maxPriorityFeePerGas = medianMaxPriorityFee.multiply(feeMultiplier);

    const maxPriorityFeeRange = {
      low: maxPriorityFeePerGas.multiply(1 + FEE_RANGE_LOW_PERCENT_OFFSET),
      medium: maxPriorityFeePerGas,
      high: maxPriorityFeePerGas.multiply(1 + FEE_RANGE_HIGH_PERCENT_OFFSET),
    };

    range = {
      gas: {},
      maxFee: { default: maxFeeRange },
      maxPriorityFeePerGas: {
        default: maxPriorityFeeRange,
        minCap: maxPriorityFeeRange.medium.divide(100)
      }
    };

    return {
      success: true,
      range,
      baseFee: gasPrice,
      maxPriorityFeePerGas,
      maxFeePerGas,
    };
  } catch (err) {
    Logger.error('Error in getFeeDetails');
    console.error(err);
    return getErrorResponse(err);
  }
};

// ************

// Sub_Function
export const getSendTxnObjectType0 = async (
  web3,
  fromAddress,
  toAddress,
  value,
  data,
  networkDetails
) => {
  let range, txnObject;
  try {
    const promises = [];

    promises.push(web3.eth.getGasPrice());
    promises.push(getGasEstimate(value, fromAddress, toAddress, data, networkDetails));

    const result = await Promise.all(promises);

    if (result[1].error) throw new Error(result[1].error);

    const totalPrice = new IntegerUnits(result[0]);

    const { gas } = result[1];

    const gasMultiplier = getGasMultiplier(networkDetails);
    const increasedGasAmount = gas.multiply(gasMultiplier);

    const gasObj = {
      default: {
        low: GAS_RANGE_LOW_PERCENT_OFFSET
          ? increasedGasAmount.multiply(1 + GAS_RANGE_LOW_PERCENT_OFFSET)
          : increasedGasAmount,
        medium: increasedGasAmount,
        high: GAS_RANGE_HIGH_PERCENT_OFFSET
          ? increasedGasAmount.multiply(1 + GAS_RANGE_HIGH_PERCENT_OFFSET)
          : increasedGasAmount,
      },
      minCap: data ? gas : new IntegerUnits(DEFAULT_GAS, 0),
    };

    const gasPriceObj = {
      default: {
        low: GAS_PRICE_RANGE_LOW_PERCENT_OFFSET
          ? totalPrice.multiply(1 + GAS_PRICE_RANGE_LOW_PERCENT_OFFSET)
          : totalPrice,
        medium: totalPrice,
        high: GAS_PRICE_RANGE_HIGH_PERCENT_OFFSET
          ? totalPrice.multiply(1 + GAS_PRICE_RANGE_HIGH_PERCENT_OFFSET)
          : totalPrice,
      },
      minCap: totalPrice,
    };

    range = {
      gas: gasObj,
      gasPrice: gasPriceObj,
    };

    const nonce = await web3.eth.getTransactionCount(fromAddress, 'latest');

    txnObject = {
      from: fromAddress,
      to: toAddress,
      data: data,
      value: value,
      gas: increasedGasAmount,
      gasPrice: totalPrice,
      chain: networkDetails.chainId,
      nonce: parseInt(nonce),
    };

    return {
      success: true,
      txnObject,
      range
    };
  } catch (err) {
    Logger.error('Error in getSendTxnObjectType0');
    console.error(err);
    return getErrorResponse(err);
  }
};

// Sub_Function
export const getSendTxnObjectType2 = async (
  web3,
  fromAddress,
  toAddress,
  value,
  data,
  networkDetails
) => {
  let range, txnObject;
  try {
    const promises = [];

    promises.push(getFeeDetails(networkDetails));
    promises.push(getGasEstimate(value, fromAddress, toAddress, data, networkDetails));

    const result = await Promise.all(promises);

    if (result[0].error) throw new Error(result[0].error);
    if (result[1].error) throw new Error(result[1].error);

    const {
      maxPriorityFeePerGas,
      baseFee,
      maxFeePerGas
    } = result[0];
    range = result[0].range;

    const { gas } = result[1];

    const gasMultiplier = getGasMultiplier(networkDetails);
    const increasedGasAmount = gas.multiply(gasMultiplier);

    const gasObj = {
      default: {
        low: GAS_RANGE_LOW_PERCENT_OFFSET
          ? increasedGasAmount.multiply(1 + GAS_RANGE_LOW_PERCENT_OFFSET)
          : increasedGasAmount,
        medium: increasedGasAmount,
        high: GAS_RANGE_HIGH_PERCENT_OFFSET
          ? increasedGasAmount.multiply(1 + GAS_RANGE_HIGH_PERCENT_OFFSET)
          : increasedGasAmount,
      },
      minCap: data ? gas : new IntegerUnits(DEFAULT_GAS, 0),
    };

    range = { ...range, gas: gasObj };

    const nonce = await web3.eth.getTransactionCount(fromAddress, 'latest');

    txnObject = {
      from: fromAddress,
      to: toAddress,
      data: data,
      value: value,
      gas: increasedGasAmount,
      gasPrice: baseFee,
      maxPriorityFeePerGas: maxPriorityFeePerGas,
      maxFeePerGas: maxFeePerGas,
      chain: networkDetails.chainId,
      nonce: parseInt(nonce),
    };

    return {
      success: true,
      txnObject,
      range
    };
  } catch (err) {
    Logger.error('Error in getSendTxnObjectType2');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #21
export const getSendTxnObject = async (
  isToken,
  tokenAddress,
  fromAddress,
  toAddress,
  value,
  networkDetails
) => {
  let txnObjectResult;
  try {
    const web3 = getWeb3(networkDetails.rpc);
    let data;

    const txnType = networkDetails.txnType;

    if (isToken) {
      const tokenContract = await getContractInstance(
        tokenAddress,
        ABI.erc20,
        networkDetails
      );
      if (tokenContract.error) throw new Error(tokenContract.error);
      const tokenInstance = tokenContract.contractInstance;
      data = tokenInstance.methods.transfer(toAddress, value.value).encodeABI();
      value = 0n;
      toAddress = tokenAddress;
    }

    if (txnType == 0) {
      txnObjectResult = await getSendTxnObjectType0(
        web3,
        fromAddress,
        toAddress,
        value,
        data,
        networkDetails
      );
    } else if (txnType == 2) {
      txnObjectResult = await getSendTxnObjectType2(
        web3,
        fromAddress,
        toAddress,
        value,
        data,
        networkDetails
      );
    } else throw new Error('Invalid - txnType');

    if (txnObjectResult.error) throw new Error(txnObjectResult.error);

    return txnObjectResult;
  } catch (err) {
    Logger.error('Error in getSendTxnObject');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #22
export const createValidRawTx = (txnType, txObject) => {
  let txnObject;
  try {
    if (!txObject.to) throw new Error('Invalid txnObject: Address TO is missing');
    if (txnType == 2) {
      txnObject = {
        ...(txObject.from && { from: Web3.utils.toHex(txObject.from) }),
        ...(txObject.to && { to: Web3.utils.toHex(txObject.to) }),
        ...(txObject.gas && { gas: Web3.utils.toHex(txObject.gas.value) }),
        ...(txObject.maxPriorityFeePerGas && {
          maxPriorityFeePerGas: Web3.utils.toHex(
            txObject.maxPriorityFeePerGas.toPrecision(18).value
          ),
        }),
        ...(txObject.maxFeePerGas && {
          maxFeePerGas: Web3.utils.toHex(txObject.maxFeePerGas.toPrecision(18).value),
        }),
        ...(txObject.nonce && { nonce: Web3.utils.toHex(txObject.nonce) }),
        ...(txObject.value && {
          value: Web3.utils.toHex(txObject.value.value)
        }),
        ...(txObject.data && { data: Web3.utils.toHex(txObject.data) }),
        //   ...txObject.chain && { chain: txObject.chain}
        ...(txnType && { type: Web3.utils.toHex(txnType) }),
      };
      return {
        success: true,
        txnObject,
      };
    } else if (txnType == 0) {
      txnObject = {
        ...(txObject.from && { from: Web3.utils.toHex(txObject.from) }),
        ...(txObject.to && { to: Web3.utils.toHex(txObject.to) }),
        ...(txObject.gasPrice && {
          gasPrice: Web3.utils.toHex(txObject.gasPrice.toPrecision(18).value),
        }),
        ...(txObject.gasLimit && {
          gas: Web3.utils.toHex(txObject.gasLimit.value),
        }),
        ...(txObject.gas && { gas: Web3.utils.toHex(txObject.gas.value) }),
        ...(txObject.nonce && { nonce: Web3.utils.toHex(txObject.nonce) }),
        ...(txObject.value && { value: Web3.utils.toHex(txObject.value.value) }),
        ...(txObject.data && { data: Web3.utils.toHex(txObject.data) }),
        // ...txObject.chain && { chain: txObject.chain}
        ...(txnType && { type: Web3.utils.toHex(txnType) }),
      };
      return {
        success: true,
        txnObject,
      };
    } else throw new Error('Invalid - transaction Type');
  } catch (err) {
    Logger.error('Error in createValidRawTx');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #23
export const signTx = async (
  txObject,
  walletEncObject,
  password,
  signingAddress,
  networkDetails,
  isApprove = false
) => {
  try {
    const web3 = getWeb3(networkDetails.rpc);

    if (
      networkDetails.txnType != 0
      && txObject.maxFeePerGas
      && txObject.maxPriorityFeePerGas
      && BigInt(txObject.maxFeePerGas) < BigInt(txObject.maxPriorityFeePerGas)
    ) {
      throw new Error('Max fee cannot be less than max priority fee. Please try your transaction again. This error usually resolves itself.');
    }

    const privateKey = await retrievePrivateKeyOf(
      signingAddress,
      walletEncObject,
      password
    );

    if (!privateKey.success) throw new Error('Error: Failed to sign. Please close the app and try again.');

    const txn = await web3.eth.accounts.signTransaction(
      txObject,
      privateKey.privateKey
    );

    // TODO: re-enable this check when web3's recover function is fixed
    // const recoverAddress = web3.eth.accounts.recoverTransaction(
    //   txn.rawTransaction
    // );
    // if (recoverAddress.toLowerCase() != signingAddress.toLowerCase()) {
    //   throw { message: "Error: Failed to Verify Signature" };
    // }

    if (isApprove) {
      await web3.eth.sendSignedTransaction(txn.rawTransaction);
    } else {
      web3.eth
        .sendSignedTransaction(txn.rawTransaction)
        .on('error', (err) => {
          console.error(err);
        });
    }

    const blockNumber = Number(await web3.eth.getBlockNumber());
    // TODO: understand why `getTimestamp(web3, blockNumber)` sometimes fails
    const timestamp = await getTimestamp(web3, 'latest');
    if (!timestamp.success) throw new Error('Unable to fetch TimeStamp');

    return {
      success: true,
      date: timestamp.time * 1000,
      receipt: {
        transactionHash: txn.transactionHash,
        status: true,
      },
      blockNumber: blockNumber,
    };
  } catch (err) {
    Logger.error('Error in signTx');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #24
export const isTokenAddressValid = async (
  tokenAddress,
  networkDetails
) => {
  try {
    if (
      tokenAddress.slice(0, 2) != '0x' &&
      validateAddressFormat(tokenAddress)
    ) {
      throw new Error('TokenAddress should have Prefix - 0x');
    }

    if (tokenAddress.length != 42) throw new Error('Invalid Token Address Key');

    const contract = await getContractInstance(
      tokenAddress,
      ABI.erc20,
      networkDetails
    );

    if (!contract.success) throw new Error(contract.error);

    const contractInstance = contract.contractInstance;

    const promises = [
      callContract(contractInstance, 'symbol'),
      callContract(contractInstance, 'decimals'),
    ];

    const result = await Promise.all(promises);

    const tokenSym = result[0];
    const tokenDecimal = Number(result[1]);

    if (typeof tokenSym !== 'string' || isNaN(tokenDecimal)) {
      throw new Error('Unable to fetch token details.');
    }

    return {
      success: true,
      tokenDetails: {
        symbol: tokenSym,
        decimals: tokenDecimal,
      },
    };
  } catch (err) {
    Logger.error('Error in isTokenAddressValid', {
      tokenAddress,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// #25
export const checkValidENSOrToAddress = async (toAddressOrENS) => {
  try {
    toAddressOrENS = toAddressOrENS.replace(/^\s+|\s+$/gm, '');
    toAddressOrENS = toAddressOrENS.toString().toLowerCase();
    if (toAddressOrENS.includes('.eth')) {
      const ENS = await getENSAddress(toAddressOrENS);
      if (ENS.success && validateAddressFormat(ENS.publicAddress)) {
        return { success: true, address: ENS.publicAddress.toLowerCase() };
      } else {
        throw {
          message: "ENS names are not supported.",
        };
      }
    } else {
      if (validateAddressFormat(toAddressOrENS)) {
        return { success: true, address: toAddressOrENS.toLowerCase() };
      } else if (toAddressOrENS.length > 42) {
        const startIndex = toAddressOrENS.indexOf('0x');
        if (startIndex != -1) {
          toAddressOrENS = toAddressOrENS.slice(startIndex, startIndex + 42);
          if (!validateAddressFormat(toAddressOrENS)) {
            throw { message: 'Invalid QR Code - Invalid TO Address' };
          }
        } else {
          throw { message: 'Invalid QR Code - TO Address NOT Found!' };
        }
        return { success: true, address: toAddressOrENS.toLowerCase() };
      }
    }
  } catch (err) {
    Logger.error('Error in checkValidENSOrToAddress');
    console.error(err);
    return getErrorResponse(err);
  }
};

// *************************************************************
// Get Account Balance & USD Price Functions
// *************************************************************

let dexPriceFetchCache = {}; // each value is an obejct with an `expires` and `promise` attribute
const DEX_CACHE_DURATION = 10 * 1000; // expire each cache entry after 10 seconds

// #26
export const getPriceDexSrcAPI = async (
  tokenAddress,
  networkDetails,
) => {
  try {
    const cacheKey = `${networkDetails.chainId}/${tokenAddress}`;
    let pairs;
    const cacheEntry = dexPriceFetchCache[cacheKey];
    if (cacheEntry && cacheEntry.expires > Date.now()) {
      // cache hit
      pairs = await cacheEntry.promise;
    } else {
      // cache miss
      const wNativeAddress = networkDetails.wNativeAddress ?? nativeAddress[networkDetails.chainId];
      const expires = Date.now() + DEX_CACHE_DURATION;
      const tokensPromise = fetch(`${config.DEX_API_TOKENS_URL}/${tokenAddress}`)
        .then(response => response.json())
        .then(data => data.pairs);
      const searchPromise = fetch(`${config.DEX_API_SEARCH_URL}?q=${wNativeAddress}+${tokenAddress}`)
        .then(response => response.json())
        .then(data => data.pairs);
      const promise = Promise.all([tokensPromise, searchPromise])
        .then(([tokensPairs, searchPairs]) => ([...tokensPairs, ...searchPairs]));
      dexPriceFetchCache[cacheKey] = { expires, promise };
      pairs = await promise;
    }
    if (pairs.length === 0) throw new Error('Unable to fetch price');

    const filteredPairs = filterDexScrNetworkSpecificQueries(
      networkDetails.chainId,
      pairs,
      tokenAddress
    );

    if (filteredPairs.length === 0) throw new Error('Unable to fetch price');

    const price = filteredPairs[0].priceUSD;
    return { success: true, price: price };
  } catch (err) {
    Logger.error('Error in getPriceDexSrcAPI');
    console.error(err);
    return getErrorResponse(err);
  }
};

// Helper
export const getUSDPrice = async (
  tokenAddress,
  networkDetails
) => {
  try {
    if (pricingSupportedChainIDs[networkDetails.chainId] === undefined) {
      throw new Error('Pricing not supported on this network!');
    }
    const wNativeAddress = networkDetails.wNativeAddress ?? nativeAddress[networkDetails.chainId];
    if (!wNativeAddress) throw new Error('No Native Address Found');

    if (!tokenAddress || tokenAddress == '') {
      tokenAddress = wNativeAddress;
    }

    const result = await getPriceDexSrcAPI(tokenAddress, networkDetails);

    if (!result.success) throw new Error(result.error);

    return {
      success: true,
      usdPrice: result.price,
    };
  } catch (err) {
    Logger.error('Error in getUSDPrice', {
      tokenAddress,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// #28
export const canGetUSDPrice = async (tokenAddress, networkDetails) => {
  try {
    const status = await getUSDPrice(tokenAddress, networkDetails);
    if (!status.success) throw new Error(status.error);
    return { success: true, price: status.usdPrice };
  } catch (err) {
    Logger.error('Error in canGetUSDPrice', {
      tokenAddress,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// #29
export const getBalanceOf = async (
  accountAddress,
  tokenAddress,
  tokenDecimal,
  networkDetails
) => {
  try {
    if (tokenAddress === '') {
      const web3 = getWeb3(networkDetails.rpc);
      const balance = await web3.eth.getBalance(accountAddress);
      return {
        success: true,
        balance: new IntegerUnits(balance),
      };
    }
    const contract = await getContractInstance(
      tokenAddress,
      ABI.erc20,
      networkDetails
    );
    const contractInstance = contract.contractInstance;

    const balance = await callContract(contractInstance, 'balanceOf', accountAddress);

    const tokenBalance = new IntegerUnits(balance, tokenDecimal);

    return {
      success: true,
      balance: tokenBalance,
    };
  } catch (err) {
    Logger.error('Error in getBalanceOf', {
      tokenAddress,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// *************************************************************
// Multiple Networks Managing/Related Functions
// *************************************************************

// Helper Function
export const getNetworkTxnType = async (rpcUrl) => {
  try {
    const web3 = getWeb3(rpcUrl);
    const blockNumber = Number(await web3.eth.getBlockNumber());

    const blockDetails = await web3.eth.getBlock(blockNumber);

    // checking if network supports EIP1559 or not
    if (blockDetails.baseFeePerGas) {
      return { success: true, txnType: 2 };
    } else {
      return { success: true, txnType: 0 };
    }
  } catch (err) {
    Logger.error('Error in getNetworkTxnType', {
      rpcUrl,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// Helper Function
export const isValidNetworkRPCUrl = async (networkRPC) => {
  try {
    const web3 = new Web3(networkRPC);
    const chainId = await web3.eth.getChainId();
    return { success: true, chainId };
  } catch (err) {
    Logger.error('Error in isValidNetworkRPCUrl', {
      networkRPC,
    });
    console.error(err);
    return getErrorResponse('Invalid RPC Url, please recheck!');
  }
};

// Helper Function
export const runNetworkValidation = async (networkRPC, chainId) => {
  try {
    const originalChainId = await isValidNetworkRPCUrl(networkRPC);

    if (!originalChainId.success) {
      throw { message: originalChainId.error };
    }

    if (originalChainId.chainId != chainId) {
      throw {
        message:
          "The RPC URL you have provided returned a different chain ID (" +
          originalChainId.chainId.toString() +
          "). Please update the Chain ID to match the RPC URL of the network you are trying to add.",
      };
    }

    return { success: true };
  } catch (err) {
    Logger.error('Error in runNetworkValidation', {
      networkRPC,
      chainId,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// #30
export const addNewBlockchainNetwork = async (
  networkName,
  networkRPC,
  chainId,
  symbol,
  networkExplorer
) => {
  let type;
  try {
    const promises = [];

    promises.push(runNetworkValidation(networkRPC, chainId));
    promises.push(getNetworkTxnType(networkRPC));

    const result = await Promise.all(promises);

    const check = result[0];
    type = result[1];

    if (!check.success) throw new Error(check.error);

    if (networkExplorer[networkExplorer.length - 1] != "/") {
      networkExplorer += "/";
    }

    type = type.txnType;

    const networkDetails = {
      txnType: type,
      networkName: networkName,
      rpc: networkRPC,
      chainId: parseInt(chainId),
      sym: symbol.toUpperCase(),
      explore: networkExplorer,
    };

    return { success: true, networkDetails: networkDetails };
  } catch (err) {
    Logger.error('Error in addNewBlockchainNetwork', {
      networkName,
      networkRPC,
      chainId,
      symbol,
      networkExplorer
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// *************************************************************
// Swap Functions
// *************************************************************

// Each Quotes Object: (example)
// {
//   id: 2,
//   dexName: 'Dex 2',
//   tokenAddress: tokenAddress,
//   path: [],
//   walletFee: 2 * 10 ** 17,
//   amountIn: tokenAmount,
//   amountOut: parseIntCustom(tokenAmount * 0.8),
// }

// -------------------------
// *** Approve Feature ***

// #31
export const checkAllowance = async (
  amount,
  fromDecimal,
  fromAddress,
  spenderAddress,
  tokenAddress,
  networkDetails
) => {
  try {
    if (!tokenAddress) {
      return { success: true, status: true };
    }
    const tokenContract = await getContractInstance(
      tokenAddress,
      ABI.erc20,
      networkDetails
    );
    const tokenInstance = tokenContract.contractInstance;
    const allowance = new IntegerUnits(
      await callContract(tokenInstance, 'allowance', fromAddress, spenderAddress),
      fromDecimal,
    );

    const status = amount.toPrecision(fromDecimal).lte(allowance);

    return { success: true, status: status };
  } catch (err) {
    Logger.error('Error in checkAllowance', {
      tokenAddress,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// #32
export const getTokenApproveTxnObj = async (
  spenderAddress,
  signingAddress,
  tokenAddress,
  networkDetails,
  approveAmount,
) => {
  try {
    const tokenContract = await getContractInstance(
      tokenAddress,
      ABI.erc20,
      networkDetails
    );
    const tokenInstance = tokenContract.contractInstance;

    const approveObjectData = tokenInstance.methods
      .approve(spenderAddress, approveAmount.value)
      .encodeABI();

    const approveObject = await getSmartContractFunctionTxnObject(
      signingAddress,
      tokenAddress,
      approveObjectData,
      0,
      networkDetails
    );

    if (!approveObject.success) throw new Error(approveObject.error);

    return { ...approveObject, approveAmount };
  } catch (err) {
    Logger.error('Error in getTokenApproveTxnObj', {
      tokenAddress,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// -------------------------

const getRandomCode = (len) => {
  try {
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    const generateString = (length) => {
      let result = ' ';
      const charactersLength = characters.length;
      for (let i = 0; i < length; i++) {
        result += characters.charAt(
          Math.floor(Math.random() * charactersLength)
        );
      }

      return result;
    };

    return (generateString(len) + '').trim();
  } catch (err) {
    Logger.error('Error in getRandomCode', {
      len,
    });
    console.error(err);
    return '';
  }
};

export const getBestPathsUniswapV3 = async (
  chainId,
  amountIn,
  fromToken,
  decimalFromToken,
  toToken,
  networkDetails,
  routerAddress,
  publicAddress,
) => {
  const wNativeAddress = networkDetails.wNativeAddress ?? nativeAddress[networkDetails.chainId];
  if (!wNativeAddress) throw new Error('No Native Address Found');
  const inTokenNative = fromToken.toLowerCase() === wNativeAddress.toLowerCase();
  const outTokenNative = toToken.toLowerCase() === wNativeAddress.toLowerCase();
  const useNative = inTokenNative || outTokenNative;
  const body = {
    chainId: chainId,
    inToken: fromToken,
    outToken: toToken,
    userWallet: publicAddress,
    useNative,
    inTokenNative,
    outTokenNative,
    amountIn: amountIn.toPrecision(decimalFromToken).value.toString(),
    dexRouterAddress: routerAddress,
  };
  const response = await axios.post(config.FETCH_BEST_PATH_URL_UNISWAP_V3, body);
  if (!response.data.success) throw new Error('Unable to fetch quotes');

  const { abiEncodedPath, tokenPath, quoteAmountOutToken } = response.data.data;
  const amountOut = BigInt(quoteAmountOutToken);
  const paths = [
    [
      amountOut,
      tokenPath,
      abiEncodedPath,
    ],
  ];
  return {
    success: true,
    result: paths,
  };
};

export const getBestPathsBalancerV2 = async (
  chainId,
  amountIn,
  fromToken,
  decimalFromToken,
  toToken,
  networkDetails,
  routerAddress,
  publicAddress,
) => {
  const wNativeAddress = networkDetails.wNativeAddress ?? nativeAddress[networkDetails.chainId];
  if (!wNativeAddress) throw new Error('No Native Address Found');
  const inTokenNative = fromToken.toLowerCase() === wNativeAddress.toLowerCase();
  const outTokenNative = toToken.toLowerCase() === wNativeAddress.toLowerCase();
  const useNative = inTokenNative || outTokenNative;
  const body = {
    chainId: chainId,
    inToken: fromToken,
    outToken: toToken,
    userWallet: publicAddress,
    useNative,
    inTokenNative,
    outTokenNative,
    amountIn: amountIn.toPrecision(decimalFromToken).value.toString(),
    vaultAddress: routerAddress,
  };
  const response = await axios.post(config.FETCH_BEST_PATH_URL_BALANCER_V2, body);
  if (!response.data.success) throw new Error('Unable to fetch quotes');

  const { tokenPath, quoteAmountOutToken } = response.data.data;
  const amountOut = BigInt(quoteAmountOutToken);
  const paths = [
    [
      amountOut,
      tokenPath,
      response.data.data,
    ],
  ];
  return {
    success: true,
    result: paths,
  };
};

export const getBestPathsPiteas = async (
  chainId,
  amountIn,
  fromToken,
  decimalFromToken,
  toToken,
  networkDetails,
  routerAddress,
  publicAddress,
) => {
  const wNativeAddress = networkDetails.wNativeAddress ?? nativeAddress[networkDetails.chainId];
  if (!wNativeAddress) throw new Error('No Native Address Found');
  const inTokenNative = fromToken.toLowerCase() === wNativeAddress.toLowerCase();
  const outTokenNative = toToken.toLowerCase() === wNativeAddress.toLowerCase();
  const useNative = inTokenNative || outTokenNative;
  const body = {
    chainId: chainId,
    inToken: fromToken,
    outToken: toToken,
    userWallet: publicAddress,
    useNative,
    inTokenNative,
    outTokenNative,
    amountIn: amountIn.toPrecision(decimalFromToken).value.toString(),
    dexId: routerAddress,
  };
  const response = await axios.post(config.FETCH_BEST_PATH_URL_PITEAS, body);
  if (!response.data.success) throw new Error('Unable to fetch quotes');

  const { tokenPath, quoteAmountOutToken, piteasCalldataPayload } = response.data.data;
  const amountOut = BigInt(quoteAmountOutToken);
  const paths = [
    [
      amountOut,
      tokenPath,
      piteasCalldataPayload,
    ],
  ];
  return {
    success: true,
    result: paths,
  };
};

export const getBestPaths = async (
  chainId,
  amountIn,
  fromToken,
  toToken,
  networkDetails,
  routerAddress,
  dexType,
  publicAddress,
) => {
  try {
    const tokenContract = await getContractInstance(
      fromToken,
      ABI.erc20,
      networkDetails
    );
    const tokenInstance = tokenContract.contractInstance;
    const decimalFromToken = Number(await callContract(tokenInstance, 'decimals'));

    if (dexType === DEX_TYPE.UNISWAP_V3) {
      return getBestPathsUniswapV3(
        chainId,
        amountIn,
        fromToken,
        decimalFromToken,
        toToken,
        networkDetails,
        routerAddress,
        publicAddress,
      );
    }

    if (dexType === DEX_TYPE.BALANCER_V2) {
      return getBestPathsBalancerV2(
        chainId,
        amountIn,
        fromToken,
        decimalFromToken,
        toToken,
        networkDetails,
        routerAddress,
        publicAddress,
      );
    }

    if (dexType === DEX_TYPE.PITEAS) {
      return getBestPathsPiteas(
        chainId,
        amountIn,
        fromToken,
        decimalFromToken,
        toToken,
        networkDetails,
        routerAddress,
        publicAddress,
      );
    }

    const randomCode = getRandomCode(36);
    const sHash = Web3.utils.keccak256(
      Web3.utils.keccak256(config.APP_CODE + randomCode)
    );

    const body = {
      chainId: chainId,
      dexRouterAddress: routerAddress,
      inToken: fromToken,
      outToken: toToken,
      hash: sHash,
      randomCode: randomCode,
      userWallet: publicAddress,
      amountIn: amountIn.toPrecision(decimalFromToken).value.toString(),
    };
    const response = await axios.post(config.FETCH_BEST_PATH_URL_UNISWAP_V2, body);
    if (!response.data.success) throw new Error('Unable to fetch quotes');

    return {
      success: true,
      result: [
        [
          BigInt(response.data.data.quoteAmountOutToken),
          response.data.data.tokenPath
        ]
      ],
    };
  } catch (err) {
    Logger.error('Error in getBestPaths', {
      chainId,
      fromToken,
      toToken,
      routerAddress,
      dexType,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

const WRAPPER_SWAP_TYPE = {
  NATIVE_TO_WRAPPED: 1,
  WRAPPED_TO_NATIVE: 2,
};

export const checkIfWrapperSwap = (
  tokenIN,
  tokenOUT,
  amountIn,
  networkDetails
) => {
  const wNativeAddress = networkDetails.wNativeAddress ?? nativeAddress[networkDetails.chainId];
  try {
    // type - 1 for native to wrapper, 2 for wrapper to native
    let type, wrapperAddress;
    if (
      !tokenIN &&
      wNativeAddress.toLowerCase() ==
        tokenOUT.toLowerCase()
    ) {
      type = WRAPPER_SWAP_TYPE.NATIVE_TO_WRAPPED;
      wrapperAddress = tokenOUT;
    } else if (
      !tokenOUT &&
      wNativeAddress.toLowerCase() == tokenIN.toLowerCase()
    ) {
      type = WRAPPER_SWAP_TYPE.WRAPPED_TO_NATIVE;
      wrapperAddress = tokenIN;
    } else {
      return { success: true, isWrapperSwap: false };
    }
    const quoteObj = {
      isWrapperSwap: true,
      swapType: type,
      tokenIN,
      tokenOUT,
      walletFee: 0,
      amountIn,
      amountOut: amountIn,
      wrapperAddress,
    };

    return {
      success: true,
      isWrapperSwap: true,
      quoteObj,
    };
  } catch (err) {
    Logger.error('Error in checkIfWrapperSwap', {
      tokenIN,
      tokenOUT,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const getUserDividendsInfo = async (
  accountAddress,
  wdAddress,
  swapAddress,
  networkDetails,
) => {
  try {
    const wdContract = await getContractInstance(
      wdAddress,
      ABI.wdtoken,
      networkDetails
    );
    const wdInstance = wdContract.contractInstance;

    const swapContract = await getContractInstance(
      swapAddress,
      ABI.quotesV3,
      networkDetails
    );
    const swapInstance = swapContract.contractInstance;

    const promises = [];
    promises.push(callContract(wdInstance, 'totalSupply'));
    promises.push(callContract(wdInstance, 'balanceOf', accountAddress));
    promises.push(callContract(wdInstance, 'claimableDividendOf', accountAddress));
    promises.push(callContract(wdInstance, 'accumulativeDividendOf', accountAddress));
    promises.push(callContract(swapInstance, 'pendingDistribution'));
    promises.push(getLastClaimed(networkDetails, wdAddress, accountAddress));
    const result = await Promise.all(promises);
    
    const totalSupply = new IntegerUnits(result[0]);
    const balance = new IntegerUnits(result[1]);
    const claimableDiv = new IntegerUnits(result[2]);
    const totalEarnedDiv = new IntegerUnits(result[3][0]);
    const pendingDistribution = new IntegerUnits(result[4]);

    let lastClaimed = null;
    if (result[5] !== null) {
      const dateObj = new Date(Number(result[5]) * 1000);
      lastClaimed = getCorrectDateFormat(dateObj);
    }
    const neverClaimed = lastClaimed === null;

    return {
      success: true,
      stats: {
        accountAddress: accountAddress,
        totalSupply,
        balance,
        claimableDiv,
        totalEarnedDiv,
        lastClaimed,
        neverClaimed,
        pendingDistribution,
      },
    };
  } catch (err) {
    Logger.error('Error in getUserDividendsInfo', {
      wdAddress,
      swapAddress
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const getLastClaimed = async (network, contractAddress, userAddress) => {
  try {
    const web3 = getWeb3(network.rpc);
    const address = web3.utils.toChecksumAddress(userAddress);

    const response = await fetch('https://api.internetmoney.io/api/v1/events/' + network.chainId + '/' + contractAddress + '/latest', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      eventName: 'ClaimDividend',
      args: [
        {
          index: 0,
          value: address,
        },
      ],
    }),
  });

  const data = await response.json();
  if (data.blockNumber) {
    const block = await web3.eth.getBlock(data.blockNumber)
    return block.timestamp;
  }
  return null;
  } catch (err) {
    Logger.error('Error in getLastClaimed');
    console.error(err);
    return getErrorResponse(err);
  }
};

export const DEX_TYPE = {
  UNISWAP_V2: 1,
  UNISWAP_V3: 2,
  BALANCER_V2: 3,
  PITEAS: 4,
};

// #34
export const getQuotes = async (
  swapAddress,
  tokenIN,
  tokenINDecimal,
  amountIN,
  tokenOUT,
  tokenOUTDecimal,
  networkDetails,
  slippage,
  publicAddress,
) => {
  try {
    const response = await axios.post(config.GET_SWAP_SUPPORTED_URL);
    const swapSupportedChainIDs = response.data.data;
    if (!swapSupportedChainIDs.includes(networkDetails.chainId)) {
      throw new Error('SWAP feature NOT Available in this Network');
    }

    const wNativeAddress = networkDetails.wNativeAddress ?? nativeAddress[networkDetails.chainId];

    const quoteContract = await getContractInstance(
      swapAddress,
      ABI.quotesV3,
      networkDetails,
    );
    const quoteInstance = quoteContract.contractInstance;
    const swapType = getSwapType(tokenIN, tokenOUT);

    tokenIN = !tokenIN ? wNativeAddress : tokenIN;
    tokenOUT = !tokenOUT ? wNativeAddress : tokenOUT;

    if (tokenIN == tokenOUT) throw new Error("Can't Swap Crypto to Wrapper");

    // const promises = [];
    // promises.push(callContract(quoteInstance, 'fee'));
    // promises.push(callContract(quoteInstance, 'feeDenominator'));
    // promises.push(getUSDPrice(tokenIN, networkDetails));
    // const result = await Promise.all(promises);
    // if (!result[0].success) {
    //   throw { message: 'Failed to fetch the fees' };
    // }

    let quotes = [];

    const getEachDexStats = async (dexInfo, firstFactoryAddress) => {
      try {
        if (dexInfo.length === 0) {
          return null;
        }
        const Id = Number(dexInfo[0]);
        const routerAddress = dexInfo[1];
        const dexType = Number(dexInfo[4]);
        const dexName = dexInfo[5];

        const routerContract = await getContractInstance(
          routerAddress,
          ABI.router,
          networkDetails
        );
        const routerInstance = routerContract.contractInstance;

        const factoryAddress = dexType === DEX_TYPE.UNISWAP_V2
          ? await callContract(routerInstance, 'factory')
          : firstFactoryAddress

        const pathsObject = await getBestPaths(
          networkDetails.chainId,
          amountIN,
          tokenIN,
          tokenOUT,
          networkDetails,
          routerAddress,
          dexType,
          publicAddress,
        );

        if (!pathsObject.success) throw new Error('No paths found');

        const paths = pathsObject.result;

        for (let i = 0; i < paths.length; i += 1) {
          let path = paths[i];

          let walletFee = amountIN.multiply(WALLET_FEE);
          if (tokenOUT == wNativeAddress) {
            walletFee = (new IntegerUnits(path[0])).multiply(WALLET_FEE);
          } else if (tokenIN != wNativeAddress && tokenOUT != wNativeAddress) {
            const quoteContract = await getContractInstance(
              swapAddress,
              ABI.quotesV3,
              networkDetails
            );
            const quoteInstance = quoteContract.contractInstance;
            const feeData = await callContract(
              quoteInstance,
              'getFeeMinimum',
              factoryAddress,
              amountIN.value,
              path[1],
            );
            walletFee = new IntegerUnits(feeData[1]);
          }

          const quote = {
            id: routerAddress,
            swapType,
            dexIndex: Id,
            dexName,
            dexType,
            factoryAddress,
            tokenIN,
            tokenOUT,
            path: path[1],
            dexCalldata: path[2],
            walletFee,
            amountIn: amountIN,
            amountOut: new IntegerUnits(path[0], tokenOUTDecimal),
          }
          const txObject = await getSwapTxnObject(
            swapAddress,
            quote,
            slippage,
            publicAddress,
            networkDetails,
          );
          // Check that the transaction is valid before considering the quote
          if (txObject.success) {
            return {
              dexId: routerAddress,
              dexName,
              dexIndex: Id,
              dexType,
              factoryAddress,
              bestPath: path[1],
              dexCalldata: path[2],
              bestAmountOut: new IntegerUnits(path[0], tokenOUTDecimal),
              walletFee,
            };
          }
        }
        throw new Error('Unable to find a valid swap path');
      } catch (err) {
        Logger.error('Error in getQuotes', {
          swapAddress,
          tokenIN,
          tokenOUT,
          slippage
        });
        console.error(err);
        return null;
      }
    };

    const dexInfoSize = Number(await callContract(quoteInstance, 'dexInfoSize'));

    if (isNaN(dexInfoSize)) {
      throw new Error('Unable to fetch DEX list. The RPC endpoint may be rate-limited. Please try again later.')
    }

    let dexInfos = await Promise.all(
      (new Array(dexInfoSize))
        .fill(0)
        .map((_, dexId) => callContract(quoteInstance, 'dexInfo', dexId))
    );

    const firstRouterAddress = dexInfos[0][1];
    const firstRouterContract = await getContractInstance(
      firstRouterAddress,
      ABI.router,
      networkDetails
    );
    const firstRouterInstance = firstRouterContract.contractInstance;
    const firstFactoryAddress = await callContract(firstRouterInstance, 'factory');

    const dexStats = await Promise.all(
      dexInfos.map(dexInfo => getEachDexStats(dexInfo, firstFactoryAddress))
    );

    for (const dexEx of dexStats) {
      if (dexEx === null) {
        continue;
      }

      const quote = {
        id: dexEx.dexId,
        swapType: swapType,
        dexIndex: dexEx.dexIndex,
        dexName: dexEx.dexName,
        dexType: dexEx.dexType,
        factoryAddress: dexEx.factoryAddress,
        tokenIN: tokenIN,
        tokenOUT: tokenOUT,
        path: dexEx.bestPath,
        dexCalldata: dexEx.dexCalldata,
        walletFee: dexEx.walletFee,
        amountIn: amountIN,
        amountOut: dexEx.bestAmountOut,
      };
      quotes.push(quote);
    }

    if (quotes.length == 0) {
      throw new Error(`No Liquidity Found: Consider increasing the slippage from ${slippage}%`);
    }

    quotes = quotes.sort((quoteA, quoteB) => quoteA.amountOut.lt(quoteB.amountOut) ? 1 : -1);
    return { success: true, quotes };
  } catch (err) {
    Logger.error('Error in getQuotes', {
      swapAddress,
      tokenIN,
      tokenOUT,
      slippage
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

const SWAP_TYPE = {
  UNKNOWN: -1,
  TOKEN_TO_TOKEN: 0,
  TOKEN_TO_NATIVE: 1,
  NATIVE_TO_TOKEN: 2,
};

export const getSwapType = (fromTokenAddress, toTokenAddress) => {
  if (fromTokenAddress && toTokenAddress) {
    return SWAP_TYPE.TOKEN_TO_TOKEN;
  }
  if (fromTokenAddress && !toTokenAddress) {
    return SWAP_TYPE.TOKEN_TO_NATIVE;
  }
  if (!fromTokenAddress && toTokenAddress) {
    return SWAP_TYPE.NATIVE_TO_TOKEN;
  } else {
    return SWAP_TYPE.UNKNOWN;
  }
};

export const getWrapperSwapTxnObject = async (
  fromAddress,
  amountIn,
  wrapperAddress,
  type,
  networkDetails
) => {
  try {
    const wrapperContract = await getContractInstance(
      wrapperAddress,
      ABI.wrapper,
      networkDetails
    );
    let wrapperInstance;

    if (!wrapperContract.success) throw new Error(wrapperContract.error);

    wrapperInstance = wrapperContract.contractInstance;

    let swapObjectData;
    let valueETH;

    // amountIn = BigInt(amountIn).toString();

    switch (type) {
      case WRAPPER_SWAP_TYPE.NATIVE_TO_WRAPPED:
        swapObjectData = wrapperInstance.methods.deposit().encodeABI();
        valueETH = amountIn;
        break;

      case WRAPPER_SWAP_TYPE.WRAPPED_TO_NATIVE:
        swapObjectData = wrapperInstance.methods.withdraw(amountIn.toPrecision(18).value).encodeABI();
        valueETH = 0;
        break;
      default:
        throw new Error('Invalid Wrapper Swap Type!');
    }

    const txnObject = await getSmartContractFunctionTxnObject(
      fromAddress,
      wrapperAddress,
      swapObjectData,
      valueETH,
      networkDetails
    );

    if (!txnObject.success) throw new Error(txnObject.error);

    return txnObject;
  } catch (err) {
    Logger.error('Error in getWrapperSwapTxnObject', {
      wrapperAddress,
      swapType: type,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const getSwapTxnObjectUniswapV3 = async (
  swapAddress,
  quoteObj,
  slippage,
  fromAddress,
  networkDetails
) => {
  try {
    // convert to decimal
    slippage = slippage / 100;

    const swapRouterObject = await getContractInstance(
      swapAddress,
      ABI.quotesV3,
      networkDetails
    );

    if (!swapRouterObject.success) throw new Error(swapRouterObject.error);
    
    const swapRouter = swapRouterObject.contractInstance;

    let swapObjectData;
    let valueETH;

    const MIN_SLIPPAGE = 0.01;
    slippage = Math.max(slippage, MIN_SLIPPAGE);

    switch (quoteObj.swapType) {
      case SWAP_TYPE.TOKEN_TO_TOKEN:
        swapObjectData = swapRouter.methods
          .swapUniswapV3(
            quoteObj.dexIndex,
            {
              path: quoteObj.dexCalldata,
              recipient: fromAddress,
              amountIn: quoteObj.amountIn.value,
              amountOutMinimum: quoteObj.amountOut.multiply(1 - slippage).value,
            },
          )
          .encodeABI();
        valueETH = quoteObj.walletFee;
        break;

      case SWAP_TYPE.TOKEN_TO_NATIVE:
        swapObjectData = swapRouter.methods
          .swapUniswapV3(
            quoteObj.dexIndex,
            {
              path: quoteObj.dexCalldata,
              recipient: fromAddress,
              amountIn: quoteObj.amountIn.value,
              amountOutMinimum: quoteObj.amountOut.multiply(1 - slippage).value,
            }
          );
        swapObjectData = swapObjectData.encodeABI();
        valueETH = new IntegerUnits(0);
        quoteObj.walletFee = quoteObj.amountOut.multiply(WALLET_FEE)
        break;

      case SWAP_TYPE.NATIVE_TO_TOKEN:
        const minimal = quoteObj.amountIn.multiply(WALLET_FEE);
        swapObjectData = swapRouter.methods
          .swapUniswapV3(
            quoteObj.dexIndex,
            {
              path: quoteObj.dexCalldata,
              recipient: fromAddress,
              amountIn: quoteObj.amountIn.subtract(minimal).value,
              amountOutMinimum: quoteObj.amountOut.multiply(1 - slippage).value,
            }
          );
        swapObjectData = swapObjectData.encodeABI();
        valueETH = quoteObj.amountIn;
        quoteObj.walletFee = minimal;
        break;
      default:
        throw new Error('Invalid Swap Type!');
    }

    const txnObject = await getSmartContractFunctionTxnObject(
      fromAddress,
      swapAddress,
      swapObjectData,
      valueETH,
      networkDetails
    );

    if (!txnObject.success) throw new Error(txnObject.error);

    return txnObject;
  } catch (err) {
    Logger.error('Error in getSwapTxnObjectUniswapV3', {
      swapAddress,
      slippage,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const getSwapTxnObjectBalancerV2 = async (
  swapAddress,
  quoteObj,
  slippage,
  fromAddress,
  networkDetails
) => {
  try {
    // convert to decimal
    slippage = slippage / 100;

    const swapRouterObject = await getContractInstance(
      swapAddress,
      ABI.quotesV3,
      networkDetails
    );

    if (!swapRouterObject.success) throw new Error(swapRouterObject.error);
    
    const swapRouter = swapRouterObject.contractInstance;

    let swapObjectData;
    let valueETH;

    const MIN_SLIPPAGE = 0.01;
    slippage = Math.max(slippage, MIN_SLIPPAGE);

    const limits = [
      quoteObj.dexCalldata.swaps[0].amount,
      ...(new Array(quoteObj.path.length - 1)).fill(0n),
    ];

    switch (quoteObj.swapType) {
      case SWAP_TYPE.TOKEN_TO_TOKEN:
        swapObjectData = swapRouter.methods
          .swapBalancerV2(
            quoteObj.dexIndex,
            quoteObj.dexCalldata.swaps,
            quoteObj.path,
            {
              sender: swapRouter.options.address,
              fromInternalBalance: false,
              recipient: fromAddress,
              toInternalBalance: false,
            },
            limits,
            Math.floor(Date.now() / 1000) + (60 * 60),
            0,
            quoteObj.path.length - 1,
            quoteObj.amountIn.value,
          )
          .encodeABI();
        valueETH = quoteObj.walletFee;
        break;

      case SWAP_TYPE.TOKEN_TO_NATIVE:
        swapObjectData = swapRouter.methods
          .swapBalancerV2(
            quoteObj.dexIndex,
            quoteObj.dexCalldata.swaps,
            quoteObj.path,
            {
              sender: swapRouter.options.address,
              fromInternalBalance: false,
              recipient: fromAddress,
              toInternalBalance: false,
            },
            limits,
            Math.floor(Date.now() / 1000) + (60 * 60),
            0,
            quoteObj.path.length - 1,
            quoteObj.amountIn.value,
          );
        swapObjectData = swapObjectData.encodeABI();
        valueETH = new IntegerUnits(0);
        quoteObj.walletFee = quoteObj.amountOut.multiply(WALLET_FEE)
        break;

      case SWAP_TYPE.NATIVE_TO_TOKEN:
        const minimal = quoteObj.amountIn.multiply(WALLET_FEE);
        swapObjectData = swapRouter.methods
          .swapBalancerV2(
            quoteObj.dexIndex,
            quoteObj.dexCalldata.swaps,
            quoteObj.path,
            {
              sender: swapRouter.options.address,
              fromInternalBalance: false,
              recipient: fromAddress,
              toInternalBalance: false,
            },
            limits,
            Math.floor(Date.now() / 1000) + (60 * 60),
            0,
            quoteObj.path.length - 1,
            quoteObj.amountIn.subtract(minimal).value,
          );
        swapObjectData = swapObjectData.encodeABI();
        valueETH = quoteObj.amountIn;
        quoteObj.walletFee = minimal;
        break;
      default:
        throw new Error('Invalid Swap Type!');
    }

    const txnObject = await getSmartContractFunctionTxnObject(
      fromAddress,
      swapAddress,
      swapObjectData,
      valueETH,
      networkDetails
    );

    if (!txnObject.success) throw new Error(txnObject.error);

    return txnObject;
  } catch (err) {
    Logger.error('Error in getSwapTxnObjectBalancerV2', {
      swapAddress,
      slippage,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const getSwapTxnObjectPiteas = async (
  swapAddress,
  quoteObj,
  slippage,
  fromAddress,
  networkDetails
) => {
  try {
    // convert to decimal
    slippage = slippage / 100;

    const swapRouterObject = await getContractInstance(
      swapAddress,
      ABI.quotesV3,
      networkDetails
    );

    if (!swapRouterObject.success) throw new Error(swapRouterObject.error);
    
    const swapRouter = swapRouterObject.contractInstance;

    let swapObjectData;
    let valueETH;

    const MIN_SLIPPAGE = 0.01;
    slippage = Math.max(slippage, MIN_SLIPPAGE);

    switch (quoteObj.swapType) {
      case SWAP_TYPE.TOKEN_TO_TOKEN:
        const feeData = await callContract(
          swapRouter,
          'getFeeMinimum',
          quoteObj.factoryAddress,
          quoteObj.amountIn.value,
          quoteObj.path,
        );

        swapObjectData = swapRouter.methods
          .swapPiteas(
            quoteObj.dexIndex,
            quoteObj.dexCalldata,
          );
        swapObjectData = swapObjectData.encodeABI();
        valueETH = new IntegerUnits(feeData[1], 18);
        quoteObj.walletFee = quoteObj.walletFee;
        break;

      case SWAP_TYPE.TOKEN_TO_NATIVE:
        swapObjectData = swapRouter.methods
          .swapPiteas(
            quoteObj.dexIndex,
            quoteObj.dexCalldata,
          );
        swapObjectData = swapObjectData.encodeABI();
        valueETH = new IntegerUnits(0);
        quoteObj.walletFee = quoteObj.amountOut.multiply(WALLET_FEE)
        break;

      case SWAP_TYPE.NATIVE_TO_TOKEN:
        const minimal = quoteObj.amountIn.multiply(WALLET_FEE);
        swapObjectData = swapRouter.methods
          .swapPiteas(
            quoteObj.dexIndex,
            quoteObj.dexCalldata,
          );
        swapObjectData = swapObjectData.encodeABI();
        valueETH = quoteObj.amountIn;
        quoteObj.walletFee = minimal;
        break;
      default:
        throw new Error('Invalid Swap Type!');
    }

    const txnObject = await getSmartContractFunctionTxnObject(
      fromAddress,
      swapAddress,
      swapObjectData,
      valueETH,
      networkDetails
    );

    if (!txnObject.success) throw new Error(txnObject.error);

    return txnObject;
  } catch (err) {
    Logger.error('Error in getSwapTxnObjectPiteas', {
      swapAddress,
      slippage,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const getSwapTxnObject = async (
  swapAddress,
  quoteObj,
  slippage,
  fromAddress,
  networkDetails,
) => {
  try {
    if (quoteObj.isWrapperSwap) {
      return await getWrapperSwapTxnObject(
        fromAddress,
        quoteObj.amountIn,
        quoteObj.wrapperAddress,
        quoteObj.swapType,
        networkDetails
      );
    }

    if (quoteObj.dexType === DEX_TYPE.UNISWAP_V3) {
      return getSwapTxnObjectUniswapV3(
        swapAddress,
        quoteObj,
        slippage,
        fromAddress,
        networkDetails,
      )
    }

    if (quoteObj.dexType === DEX_TYPE.BALANCER_V2) {
      return getSwapTxnObjectBalancerV2(
        swapAddress,
        quoteObj,
        slippage,
        fromAddress,
        networkDetails,
      )
    }

    if (quoteObj.dexType === DEX_TYPE.PITEAS) {
      return getSwapTxnObjectPiteas(
        swapAddress,
        quoteObj,
        slippage,
        fromAddress,
        networkDetails,
      )
    }

    // convert to decimal
    slippage = slippage / 100;

    const swapRouterObject = await getContractInstance(
      swapAddress,
      ABI.quotesV3,
      networkDetails
    );

    if (!swapRouterObject.success) throw new Error(swapRouterObject.error);
    
    const swapRouter = swapRouterObject.contractInstance;

    let swapObjectData;
    let valueETH;

    const MIN_SLIPPAGE = 0.01;
    slippage = Math.max(slippage, MIN_SLIPPAGE);

    switch (quoteObj.swapType) {
      case SWAP_TYPE.TOKEN_TO_TOKEN:
        swapObjectData = swapRouter.methods
          .swapTokenV2(
            quoteObj.dexIndex,
            fromAddress,
            quoteObj.path,
            quoteObj.amountIn.value,
            quoteObj.amountOut.multiply(1 - slippage).value,
            Math.floor(Date.now() / 1000) + (60 * 60), // 1 hour deadline
          )
          .encodeABI();
        valueETH = new IntegerUnits(0);

        const wNativeAddress = networkDetails.wNativeAddress ?? nativeAddress[networkDetails.chainId];
        if (!wNativeAddress) throw new Error('No Native Address Found');

        const swapIncludesWrappedNative = [
          quoteObj.path[0].toLowerCase(),
          quoteObj.path[quoteObj.path.length - 1].toLowerCase(),
        ].includes(wNativeAddress.toLowerCase());

        if (!swapIncludesWrappedNative) {
          const feeData = await callContract(
            swapRouter,
            'getFeeMinimum',
            quoteObj.factoryAddress,
            quoteObj.amountIn.value,
            quoteObj.path,
          );
          valueETH = new IntegerUnits(feeData[1], 18);
        }

        quoteObj.walletFee = quoteObj.walletFee;
        break;

      case SWAP_TYPE.TOKEN_TO_NATIVE:
        swapObjectData = swapRouter.methods
          .swapToNativeV2(
            quoteObj.dexIndex,
            fromAddress,
            quoteObj.path,
            quoteObj.amountIn.value,
            quoteObj.amountOut.multiply(1 - slippage).value,
            Math.floor(Date.now() / 1000) + (60 * 60), // 1 hour deadline
          );

        swapObjectData = swapObjectData.encodeABI();
        valueETH = new IntegerUnits(0);
        quoteObj.walletFee = quoteObj.amountOut.multiply(WALLET_FEE)
        break;

      case SWAP_TYPE.NATIVE_TO_TOKEN:
        const minimal = quoteObj.amountIn.multiply(WALLET_FEE);
        swapObjectData = swapRouter.methods
          .swapNativeToV2(
            quoteObj.dexIndex,
            fromAddress,
            quoteObj.path,
            quoteObj.amountIn.subtract(minimal).value,
            quoteObj.amountOut.multiply(1 - slippage).value,
            Math.floor(Date.now() / 1000) + (60 * 60), // 1 hour deadline
          );
          
        swapObjectData = swapObjectData.encodeABI();
        valueETH = quoteObj.amountIn;
        quoteObj.walletFee = minimal;
        break;
      default:
        throw new Error('Invalid Swap Type!');
    }

    const txnObject = await getSmartContractFunctionTxnObject(
      fromAddress,
      swapAddress,
      swapObjectData,
      valueETH,
      networkDetails
    );

    if (!txnObject.success) throw new Error(txnObject.error);

    return txnObject;
  } catch (err) {
    Logger.error('Error in getSwapTxnObject', {
      swapAddress,
      slippage,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// *************************************************************
// Display/Read Functions - Transactions
// *************************************************************

export const getTimestamp = async (web3, blockHash) => {
  try {
    const blockDetails = await web3.eth.getBlock(blockHash);
    return {
      success: true,
      // Conversion from BigInt is safe as the MAX_SAFE_INTEGER suffices for more
      // than 200,000 years even when storing milliseconds.
      time: Number(blockDetails.timestamp),
    };
  } catch (err) {
    Logger.error('Error in getTimestamp', {
      blockHash,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const getCorrectDateFormat = (dateObj) => {
  try {
    // const amORpm = parseInt(dateObj.getHours()) < 12 ? 'am' : 'pm';
    // const HH = dateObj.getHours() % 12 == 0 ? 12 : dateObj.getHours() % 12;
    return dateObj.toLocaleDateString('en-US');
  } catch (err) {
    console.error(err);
    return 'Invalid Date';
  }
};

// #36
export const getTransactionDetails = async (
  transactionHash,
  networkDetails
) => {
  try {
    if (!transactionHash.match('^0x[0-9a-fA-F]{64}$')) {
      throw new Error('Invalid TransactionHash');
    }
    const web3 = getWeb3(networkDetails.rpc);

    const promises = [];

    const txnUrl = `${networkDetails.explore}tx/${transactionHash}`;

    promises.push(web3.eth.getTransaction(transactionHash));
    promises.push(web3.eth.getTransactionReceipt(transactionHash));

    const result = await Promise.all(promises);

    let pending = false;

    const tx = result[0];
    const txReceipt = result[1];

    if (!tx.blockNumber) {
      pending = true;
      return { success: true, pending: pending, exploreUrl: txnUrl };
    }

    const timestamp = await getTimestamp(web3, tx.blockHash);
    if (!timestamp.success) throw new Error('Unable to fetch TimeStamp');

    const value = tx.value;

    let type = 0;
    if (tx.maxPriorityFeePerGas) {
      type = 2;
    }

    let txnDetails;
    if (type == 2) {
      txnDetails = {
        type: type,
        status: txReceipt.status,
        nonce: parseInt(tx.nonce),
        date: timestamp.time * 1000,
        value: value,
        gasLimit: new IntegerUnits(tx.gas, 0),
        gasUsed: new IntegerUnits(txReceipt.gasUsed, 0),
        baseFee: new IntegerUnits(tx.gasPrice).subtract(new IntegerUnits(tx.maxPriorityFeePerGas)),
        priorityFee: new IntegerUnits(tx.maxPriorityFeePerGas),
        totalGasFee: new IntegerUnits(txReceipt.effectiveGasPrice),
        maxFeePerGas: new IntegerUnits(tx.maxFeePerGas),
        txnExploreUrl: txnUrl,
      };
    } else {
      txnDetails = {
        type: type,
        status: txReceipt.status,
        nonce: parseInt(tx.nonce),
        date: timestamp.time * 1000,
        value: value,
        gasLimit: new IntegerUnits(tx.gas, 0),
        gasUsed: new IntegerUnits(txReceipt.gasUsed, 0),
        baseFee: new IntegerUnits(tx.gasPrice),
        txnExploreUrl: txnUrl,
      };
    }
    return { success: true, pending: pending, txnDetails: txnDetails };
  } catch (err) {
    Logger.error('Error in getTransactionDetails');
    console.error(err);
    return getErrorResponse(err, {
      pending: true,
      exploreUrl: `${networkDetails.explore}tx/${transactionHash}`,
    });
  }
};

// *************************************************************
// WD Token Functions
// *************************************************************

// #37
export const claimWDTxnObj = async (wdAddress, fromAddress, networkDetails) => {
  try {
    const wdContract = await getContractInstance(
      wdAddress,
      ABI.wdtoken,
      networkDetails
    );
    const wdInstance = wdContract.contractInstance;
    const data = wdInstance.methods.claimDividend(fromAddress, 0).encodeABI();

    const txnObject = await getSmartContractFunctionTxnObject(
      fromAddress,
      wdAddress,
      data,
      0,
      networkDetails
    );

    if (!txnObject.success) throw new Error(txnObject.error);

    return txnObject;
  } catch (err) {
    Logger.error('Error in claimWDTxnObj', {
      wdAddress,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// #38
export const getWDStats = async (wdAddress, networkDetails) => {
  try {
    const wdContract = await getContractInstance(
      wdAddress,
      ABI.wdtoken,
      networkDetails
    );
    const wdInstance = wdContract.contractInstance;

    const promises = [];
    promises.push(callContract(wdInstance, 'name'));
    promises.push(callContract(wdInstance, 'symbol'));
    promises.push(callContract(wdInstance, 'totalSupply'));
    promises.push(callContract(wdInstance, 'decimals'));
    const result = await Promise.all(promises);

    return {
      success: true,
      stats: {
        name: result[0],
        sym: result[1],
        totalSupply: new IntegerUnits(result[2], result[3]),
        decimals: Number(result[3]),
        totalDistributed: new IntegerUnits(0),
      },
    };
  } catch (err) {
    Logger.error('Error in getWDStats', {
      wdAddress,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

// *************************************************************
// TO GET any Smart Contract Send Txn Object
// *************************************************************

// #40
export const getSmartContractFunctionTxnObject = async (
  fromAddress,
  toAddress,
  data,
  value,
  networkDetails
) => {
  try {
    const web3 = getWeb3(networkDetails.rpc);
    const txnType = networkDetails.txnType;

    let txnObject;

    if (txnType == 0) {
      txnObject = await getSendTxnObjectType0(
        web3,
        fromAddress,
        toAddress,
        value,
        data,
        networkDetails
      );
    } else if (txnType == 2) {
      txnObject = await getSendTxnObjectType2(
        web3,
        fromAddress,
        toAddress,
        value,
        data,
        networkDetails
      );
    } else {
      throw new Error('Invalid - txnType');
    }

    if (!txnObject.success) throw new Error(txnObject.error);

    return txnObject;
  } catch (err) {
    Logger.error('Error in getSmartContractFunctionTxnObject');
    console.error(err);
    return getErrorResponse(err);
  }
};

// *************************************************************
// DAPP/Wallet Connect - Handling different Web3 Functions
// *************************************************************

// #41
export const ethSendTransactionPayloadObject = async (
  payload,
  networkDetails
) => {
  try {
    payload = payload.params[0];
    let value = payload.value ? payload.value : '0x0';
    if (value == '0x') {
      value = '0x0';
    }

    value = new IntegerUnits(BigInt(value), 18);

    const dappTxnObject = await getSmartContractFunctionTxnObject(
      payload.from,
      payload.to,
      payload.data,
      value,
      networkDetails,
    );

    if (!dappTxnObject.success) throw new Error(dappTxnObject.error);

    return { ...dappTxnObject, method: 'txn' };
  } catch (err) {
    Logger.error('Error in ethSendTransactionPayloadObject');
    console.error(err);
    return getErrorResponse(err);
  }
};

// First just display & ask for confirmation & then take a sign
// #42
export const ethSignTransactionPayloadObject = async (
  payload,
  walletEncObject,
  password,
  networkDetails,
) => {
  try {
    payload = {
      ...payload.params[0],
      gas: payload.params[0].gas && new IntegerUnits(BigInt(payload.params[0].gas), 0),
      gasLimit: payload.params[0].gasLimit && new IntegerUnits(BigInt(payload.params[0].gasLimit), 0),
      gasPrice: payload.params[0].gasPrice && new IntegerUnits(BigInt(payload.params[0].gasPrice), 18),
      maxPriorityFeePerGas: payload.params[0].maxPriorityFeePerGas && new IntegerUnits(BigInt(payload.params[0]).maxPriorityFeePerGas, 18),
      maxFeePerGas: payload.params[0].maxFeePerGas && new IntegerUnits(BigInt(payload.params[0]).maxFeePerGas, 18),
      value: payload.params[0].value && new IntegerUnits(BigInt(payload.params[0].value), 18),
    };

    let txnObj;
    if (payload.gasPrice == undefined) {
      txnObj = createValidRawTx(2, payload);
    } else {
      txnObj = createValidRawTx(0, payload);
    }

    if (!txnObj.success) throw new Error('Error: Invalid txObject/payload');

    const web3 = getWeb3(networkDetails.rpc);

    const privateKey = await retrievePrivateKeyOf(
      payload.from,
      walletEncObject,
      password
    );

    if (!privateKey.success) throw new Error('Error: Failed to sign. Please close the app and try again.');

    const txn = await web3.eth.accounts.signTransaction(
      txnObj.txnObject,
      privateKey.privateKey
    );

    // TODO: re-enable this check when web3's recover function is fixed
    // const recoverAddress = web3.eth.accounts.recoverTransaction(
    //   txn.rawTransaction
    // );
    // if (recoverAddress.toLowerCase() != payload.from.toLowerCase()) {
    //   throw { message: "Error: Failed to Verify Signature" };
    // }

    return { success: true, signResult: txn.rawTransaction, method: 'sign' };
  } catch (err) {
    Logger.error('Error in ethSignTransactionPayloadObject');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #43
export const ethSignTypedDataPayloadObject = async (
  payload,
  fromAddress,
  walletEncObject,
  password,
  version,
) => {
  try {
    const signMsg = JSON.parse(payload.params[1]);
    const privateKey = await retrievePrivateKeyOf(
      fromAddress,
      walletEncObject,
      password
    );

    if (!privateKey.success) throw new Error('Error: Failed to sign. Please close the app and try again.');

    const sign = signTypedData({
      data: signMsg,
      privateKey: Buffer.from(privateKey.privateKey.slice(2), 'hex'),
      version,
    });

    const recoverAddress = recoverTypedSignature({
      data: signMsg,
      signature: sign,
      version,
    });

    if (recoverAddress.toLowerCase() != fromAddress.toLowerCase()) {
      throw new Error('Error: Failed to Verify Signature');
    }

    return {
      success: true,
      signMsg: signMsg,
      signResult: sign,
      method: 'sign',
    };
  } catch (err) {
    Logger.error('Error in ethSignTypedDataPayloadObject');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #44
export const personalSignPayloadObject = async (
  payload,
  walletEncObject,
  password,
  networkDetails,
) => {
  try {
    const web3 = getWeb3(networkDetails.rpc);
    const privateKey = await retrievePrivateKeyOf(
      payload.params[1],
      walletEncObject,
      password
    );

    if (!privateKey.success) throw new Error('Error: Failed to sign. Please close the app and try again.');

    const signMsg = web3.utils.hexToUtf8(payload.params[0]);

    const sign = web3.eth.accounts.sign(signMsg, privateKey.privateKey);

    // TODO: re-enable this check when web3's recover function is fixed
    // const recoverAddress = web3.eth.accounts.recover(sign);
    // if (recoverAddress.toLowerCase() != payload.params[1].toLowerCase()) {
    //   throw { message: "Error: Failed to Verify Signature" };
    // }

    return {
      success: true,
      signMsg: signMsg,
      signResult: sign.signature,
      method: 'sign',
    };
  } catch (err) {
    Logger.error('Error in personalSignPayloadObject');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #45
export const ethSignPayloadObject = async (
  payload,
  walletEncObject,
  password,
  networkDetails,
) => {
  try {
    const web3 = getWeb3(networkDetails.rpc);

    let signMsg = payload.params[1];

    const privateKey = await retrievePrivateKeyOf(
      payload.params[0],
      walletEncObject,
      password
    );

    if (!privateKey.success) throw new Error('Error: Failed to sign. Please close the app and try again.');

    let signResult;

    try {
      signMsg = web3.utils.hexToUtf8(payload.params[1]);
      signResult = await web3.eth.accounts.sign(signMsg, privateKey.privateKey);
      signResult = signResult.signature;
    } catch (err) {
      console.error(err);
      const signingKey = new ethers.utils.SigningKey(privateKey.privateKey);
      const sigParams = signingKey.signDigest(
        ethers.utils.arrayify(payload.params[1])
      );
      signResult = ethers.utils.joinSignature(sigParams);
    }

    return {
      success: true,
      signMsg: signMsg,
      signResult: signResult,
      method: 'sign',
    };
  } catch (err) {
    Logger.error('Error in ethSignPayloadObject');
    console.error(err);
    return getErrorResponse(err);
  }
};

export const ethSignAuthRequest = async (
  payload,
  walletEncObject,
  password,
  networkDetails,
) => {
  try {
    const web3 = getWeb3(networkDetails.rpc);

    const privateKey = await retrievePrivateKeyOf(
      payload.address,
      walletEncObject,
      password
    );

    if (!privateKey.success) throw new Error('Error: Failed to sign. Please close the app and try again.');

    const signMsg = payload.message;

    const sign = await web3.eth.accounts.sign(signMsg, privateKey.privateKey);

    // TODO: re-enable this check when web3's recover function is fixed
    // const recoverAddress = web3.eth.accounts.recover(sign);
    // if (recoverAddress.toLowerCase() != payload.address.toLowerCase()) {
    //   throw { message: "Error: Failed to Verify Signature" };
    // }

    return {
      success: true,
      signMsg: signMsg,
      signResult: sign.signature,
      method: 'sign',
    };
  } catch (err) {
    Logger.error('Error in ethSignAuthRequest');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #46
export const handleDappTxnObject = async (
  payload,
  fromAddress,
  walletEncObject,
  password,
  networkDetails
) => {
  try {
    if (typeof payload.topic === 'string' && typeof payload.iss !== 'string') {
      payload = payload.params.request;
    }

    const method = payload.method;
    let resultObj;

    switch (method) {
      case "personal_sign":
        resultObj = await personalSignPayloadObject(
          payload,
          walletEncObject,
          password,
          networkDetails
        );
        break;
      case "eth_sign":
        resultObj = await ethSignPayloadObject(
          payload,
          walletEncObject,
          password,
          networkDetails
        );
        break;
      case "eth_signTypedData":
        resultObj = await ethSignTypedDataPayloadObject(
          payload,
          fromAddress,
          walletEncObject,
          password,
          'V3',
        );
        break;
      case "eth_signTypedData_v4":
        resultObj = await ethSignTypedDataPayloadObject(
          payload,
          fromAddress,
          walletEncObject,
          password,
          'V4',
        );
        break;
      case "eth_sendTransaction":
        resultObj = await ethSendTransactionPayloadObject(
          payload,
          networkDetails
        );
        break;
      case "eth_signTransaction":
        resultObj = await ethSignTransactionPayloadObject(
          payload,
          walletEncObject,
          password,
          networkDetails
        );
        break;
      // case "eth_sendRawTransaction":
      //   resultObj = await ethSendRawTransactionObject(
      //     payload,
      //     fromAddress,
      //     walletEncObject,
      //     password,
      //     networkDetails
      //   );
      // break;
      default:
        throw new Error('No transaction methods matched!');
    }
    if (!resultObj.success) throw new Error(resultObj.error);

    return resultObj;
  } catch (err) {
    Logger.error('Error in handleDappTxnObject');
    console.error(err);
    return getErrorResponse(err);
  }
};

// #47
export const getSignMsg = async (payload) => {
  try {
    let resultSign;

    const method = payload.method;

    switch (method) {
      case "personal_sign":
        try {
          resultSign = Web3.utils.hexToUtf8(payload.params[0]);
        } catch {
          resultSign = payload.params[0];
        }
        break;
      case "eth_sign":
        try {
          resultSign = Web3.utils.hexToUtf8(payload.params[1]);
        } catch {
          resultSign = payload.params[1];
        }
        break;
      case "eth_signTypedData":
        resultSign = payload.params[1];
        break;
      case "eth_signTypedData_v4":
        resultSign = payload.params[1];
        break;
      case "eth_signTransaction":
        try {
          payload = {
            ...payload.params[0],
            gas: payload.params[0].gas && new IntegerUnits(BigInt(payload.params[0].gas), 0),
            gasLimit: payload.params[0].gasLimit && new IntegerUnits(BigInt(payload.params[0].gasLimit), 0),
            gasPrice: payload.params[0].gasPrice && new IntegerUnits(BigInt(payload.params[0].gasPrice), 18),
            maxPriorityFeePerGas: payload.params[0].maxPriorityFeePerGas && new IntegerUnits(BigInt(payload.params[0]).maxPriorityFeePerGas, 18),
            maxFeePerGas: payload.params[0].maxFeePerGas && new IntegerUnits(BigInt(payload.params[0]).maxFeePerGas, 18),
            value: payload.params[0].value && new IntegerUnits(BigInt(payload.params[0].value), 18),
          };

          let txnObj;
          if (payload.gasPrice == undefined) {
            txnObj = createValidRawTx(2, payload);
          } else {
            txnObj = createValidRawTx(0, payload);
          }

          if (!txnObj.success) throw new Error('Error: Invalid txObject/payload');

          resultSign = JSON.stringify(txnObj.txnObject);
        } catch (err) {
          throw new Error('Unable to fetch Message!');
        }
        break;
      // case "eth_sendRawTransaction":
      //   resultObj = await ethSendRawTransactionObject(
      //     payload,
      //     fromAddress,
      //     walletEncObject,
      //     password,
      //     networkDetails
      //   );
      // break;
      default:
        throw new Error('No transaction methods matched!');
    }

    return { success: true, signMsg: resultSign };
  } catch (err) {
    Logger.error('Error in getSignMsg');
    console.error(err);
    return getErrorResponse(err);
  }
};

/**
 * This function is used to get receive transactions for addresses in `aReceiverAddresses`,
 * for the tokens `aTokenAddresses` form the network with chain ID `nChainID` from block number `nBlockNumber`.
 *
 * @param {number} nChainID - Chain Id of the network to get receive transactions from
 * @param {number} nBlockNumber - Block number to start query from
 * @param {string[]} aReceiverAddresses - List of receiver addresses to get transactions for
 * @param {string[]} aTokenAddresses - List of token addresses to get transactions for
 *
 * @returns success status and list of transactions
 */
export const getUsersReceiveTransactions = async (
  nChainID,
  nBlockNumber,
  aReceiverAddresses,
  aTokenAddresses
) => {
  return {
    success: true,
    nLatestBlockNumber: 0,
    aReceiveTransactions: [],
  };
};

export const getTokenLogo = async (chainId, tokenAddress) => {
  let iconURI = missingTokenIconIcon;

  try {
    if (tokenAddress == '') {
      tokenAddress = nativeAddress[chainId];
    }

    const body = {
      chainId: chainId,
      tokenAddress: tokenAddress,
    };
    
    const axiosOptions = {
      validateStatus: (status) => {
        return (status >= 200 && status < 300) || status == 404;
      }
    }
    
    const response = await axios.post(config.GET_TOKEN_ICON, body, axiosOptions);

    if (response.data.success) {
      iconURI = response.data.url;
    }
  } catch (err) {
    Logger.error('Error in getTokenLogo', {
      tokenAddress,
    });
    console.error(err);
  }

  try {
    const result = await axios.get(iconURI);
    if (result.status !== 200) {
      return missingTokenIconIcon;
    }
  } catch (err) {
    Logger.error('Error in getTokenLogo', {
      tokenAddress,
    });
    console.error(err);
    return missingTokenIconIcon;
  }

  return iconURI;
};

export const getNetworkLogo = async (chainId) => {
  try {
    let url;
    const body = {
      chainId,
    };
    const response = await axios.post(config.GET_NETWORK_ICON, body);
    if (!response.data.success) {
      throw new Error('Unable to get network icon');
    } else {
      url = response.data.url;
    }
    return url;
  } catch (err) {
    Logger.error('Error in getNetworkLogo', {
      chainId,
    });
    console.error(err);
    return config.BLANK_ICON;
  }
};

/**
 * This helper function is used to format the transaction object.
 *
 * @param {Object} oTransaction - The Transaction object to format
 * @returns Formatted Transaction object
 */
const formatTxObject = async (oTransaction, networkDetails, web3) => {
  try {
    const fromTokenAddress =
      oTransaction.currency.address == '-' ? null : oTransaction.currency.address;
  
    const fromToken = {
      symbol: oTransaction.currency.symbol,
      decimals: oTransaction.currency.decimals,
      address: fromTokenAddress || '',
    };
  
    const usdPrice = await canGetUSDPrice(fromTokenAddress, networkDetails);
    const timestamp = await getTimestamp(web3, oTransaction.block.height);
    if (!timestamp.success) throw new Error('Unable to fetch TimeStamp');

    const oFormattedTransaction = {
      type: 'receive',
      transactionDetail: {
        accountName: '',
        isSupportUSD: usdPrice.success,
        displayAmount: oTransaction.amount * 10 ** oTransaction.currency.decimals,
        displayDecimal: oTransaction.currency.decimals,
        transferedAmount:
          oTransaction.amount * 10 ** oTransaction.currency.decimals,
        usdValue: usdPrice?.price,
        toAddress: oTransaction.receiver.address,
        fromAddress: oTransaction.sender.address,
        networkDetail: networkDetails,
        fromToken,
        utcTime: timestamp.time * 1000,
      },
      receipt: {
        transactionHash: oTransaction.transaction.hash,
        status: true,
      },
      blockNumber: oTransaction.block.height,
    };
  
    return oFormattedTransaction;
  } catch (err) {
    Logger.error('Error in formatTxObject');
    console.error(err);
  }
};

export const getMaxAmount = async (
  type,
  tokenAddress,
  tokenDecimal,
  fullBalance,
  networkDetails,
  accountAddress = '',
) => {
  try {
    let maxBalance = fullBalance;
    let maxGasPrice;
    const gasPriceMultiplier = getGasPriceMultiplier(networkDetails);

    if (!tokenAddress) {
      const fixedGasUsage = {
        1: new IntegerUnits(30_000, 0),
        2: new IntegerUnits(400_000, 0),
      };

      if (networkDetails.txnType == undefined) {
        throw new Error('Invalid networkDetails');
      }

      if (networkDetails.txnType.toString() == '2') {
        maxGasPrice = (await getFeeDetails(networkDetails))
          .range.maxFee.default.high
          .multiply(gasPriceMultiplier);
      } else {
        const web3 = getWeb3(networkDetails.rpc);
        const gasPrice = new IntegerUnits(await web3.eth.getGasPrice());
        maxGasPrice = gasPrice.multiply(gasPriceMultiplier);
      }

      if (!fixedGasUsage[type]) throw new Error('Invalid Type!');

      const maxGasFee = fixedGasUsage[type].multiply(maxGasPrice);

      if (maxBalance.lte(maxGasFee)) {
        throw new Error('Balance too LOW to pay gas fee');
      }
      maxBalance = maxBalance.subtract(maxGasFee);
    } else {
      if (accountAddress !== '') {
        const balanceResult = await getBalanceOf(accountAddress.publicAddress, tokenAddress, tokenDecimal, networkDetails)
        if (balanceResult.success === true) {
          maxBalance = balanceResult.balance;
        }
      }
    }

    return { success: true, maxBalance: maxBalance };
  } catch (err) {
    Logger.error('Error in formatTxObject', {
      type,
      tokenAddress,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

export const getShaHash = (password) => {
  return Web3.utils.keccak256(password);
};
