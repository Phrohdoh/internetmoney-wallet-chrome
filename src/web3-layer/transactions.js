import { STORAGE_KEYS } from 'constants';
import { Strings } from 'resources';
import IntegerUnits from 'utils/IntegerUnits';
import { parseObject } from 'utils/parse';
import { getItem } from 'utils/storage';
import { Validation } from 'utils/validations';
import {
  getHardwareDeviceDetails,
  signTxnInLedger,
  signTxnInTrezor,
} from './hardwareWeb3Layer';
import { createValidRawTx, signTx } from './web3Layer';
import Logger from 'utils/logger';
import { getErrorResponse } from 'utils/errors';

export const WalletTransactionTypes = {
  SWAP: 'swap',
  SEND: 'send',
  CLAIM: 'claim',
  DAPP: 'dapp',
};

/**
 * Create & Send Transaction
 * @param {*} txnObject Transaction data
 * @param {*} wallet Sender's wallet
 * @param {*} fromAccount Sender's account data
 * @param {*} fromAccountBalance Sender's account balance
 * @param {*} selectedNetwork Current network
 * @param {*} additionalParams Extra transaction parameters
 * @returns Transaction receipt
 */
export const createAndSendTxn = async (
  type,
  txnObject,
  wallet,
  fromAccount,
  fromAccountBalance,
  selectedNetwork,
  additionalParams = {}
) => {
  try {
    const { gas, maxPriorityFeePerGas, maxFeePerGas } = txnObject;
    const gasPrice = maxFeePerGas
      ? IntegerUnits.min(txnObject.gasPrice, maxFeePerGas)
      : txnObject.gasPrice;

    let estimateFee =
      selectedNetwork.txnType == 0
        ? gasPrice.multiply(gas)
        : gasPrice.add(maxPriorityFeePerGas).multiply(gas);

    if (
      type === (WalletTransactionTypes.SEND || WalletTransactionTypes.SWAP) &&
      Validation.isEmpty(additionalParams.address)
    ) {
      estimateFee = estimateFee.add(additionalParams.transferAmount);
    }

    const isFeeValid = estimateFee.lte(fromAccountBalance);
    if (!isFeeValid) {
      throw new Error(
        Strings.formatString(Strings.ESTIMATE_FEE_ERROR_MSG, [
          selectedNetwork.sym,
        ])
      )
    }

    let txnReceipt = null;
    if (!fromAccount.isHardware) {
      const validateTx = createValidRawTx(selectedNetwork.txnType, txnObject);

      if (validateTx.error) throw new Error(validateTx.error);

      txnReceipt = await signTx(
        validateTx.txnObject,
        wallet.walletDetail.walletObject,
        wallet.password,
        fromAccount.publicAddress,
        selectedNetwork,
        additionalParams.isApprove
      );
    } else {
      const hardware = getHardwareDeviceDetails(fromAccount.isHardware);
      if (hardware.whichDevice === 'ledger') {
        txnReceipt = await signTxnInLedger(
          selectedNetwork,
          txnObject,
          fromAccount.isHardware
        );
      } else {
        txnReceipt = await signTxnInTrezor(
          selectedNetwork,
          txnObject,
          fromAccount.isHardware
        );
      }
    }

    if (txnReceipt.error) throw new Error(txnReceipt.error);

    return txnReceipt;
  } catch (err) {
    Logger.error('Error in createAndSendTxn', {
      txnType: type,
      isHardwareWallet: fromAccount.isHardware,
    });
    console.error(err);
    return getErrorResponse(err);
  }
};

/**
 * Update Transactions List
 * @param {*} newTxn Transaction to add
 * @param {*} existingTxns Existing transactions list
 * @param {*} selectedNetwork Current network
 * @returns Updated Transactions lists
 */
export const updateTxnLists = (newTxn, existingTxns, selectedNetwork) => {
  const tempTxnsList = [...existingTxns.transactionsList, newTxn];

  const { data } = getItem(STORAGE_KEYS.TRANSACTIONS);
  const parseObj = parseObject(data);

  let parseTransactions;
  if (parseObj.success) {
    parseTransactions = parseObj.data;
  }

  let tempTxns = [];
  if (parseTransactions) {
    tempTxns = parseTransactions;
  }
  const transObj = tempTxns.find(
    (chain) => chain.chainId === selectedNetwork.chainId
  );

  if (transObj) {
    const transIndex = tempTxns.indexOf(transObj);
    transObj['transactions'] = tempTxnsList;
    tempTxns[transIndex] = transObj;
  } else {
    tempTxns.push({
      chainId: selectedNetwork.chainId,
      transactions: tempTxnsList,
    });
  }

  return {
    tempTxns,
    tempTxnsList,
  };
};
