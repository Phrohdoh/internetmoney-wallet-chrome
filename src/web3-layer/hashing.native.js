import argon2RN from "react-native-argon2";
const config = require("./config.json");

import { getShaHash } from "./web3Layer.js";

export const getHash = async (password) => {
  const result = await argon2RN(getShaHash(password), config.ARGON2D_SALT, {});
  const { encodedHash } = result;
  return encodedHash;
};

export const verifyPassword = async (password, passwordHash) => {
  try {
    const result = await argon2RN(
      getShaHash(password),
      config.ARGON2D_SALT,
      {}
    );
    const { encodedHash } = result;
    return encodedHash === passwordHash;
  } catch (error) {
    return false;
  }
};
