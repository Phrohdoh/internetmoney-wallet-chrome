import React, {useEffect, useState} from 'react';
import { useDispatch } from 'react-redux';
import { NetworksAction } from 'redux/slices/networksSlice';
import { MemoryRouter, Routes, Route } from 'react-router-dom';
import UserLayout from 'layouts/UserLayout';
import LockWallet from 'components/LockWallet';
import Splash from 'screens/Splash';
import Onboarding from 'screens/Onboarding';
import LearnStepOne from 'screens/Onboarding/LearnStepOne';
import LearnStepTwo from 'screens/Onboarding/LearnStepTwo';
import LearnStepThree from 'screens/Onboarding/LearnStepThree';
import LearnStepFour from 'screens/Onboarding/LearnStepFour';
import LearnStepFive from 'screens/Onboarding/LearnStepFive';
import PasscodeSetup from 'screens/Onboarding/PasscodeSetup';
import PasswordSetup from 'screens/Onboarding/PasswordSetup';
import CreateWallet from 'screens/Onboarding/CreateWallet';
import SecretRecoveryPhrase from 'screens/Onboarding/SecretRecoveryPhrase';
import YourSecretRecoveryPhrase from 'screens/Onboarding/YourSecretRecoveryPhrase';
import Welcome from 'screens/Onboarding/Welcome';
import LoginPasswordVerification from 'screens/Onboarding/LoginPasswordVerification';
import ExperiencedWithCrypto from 'screens/Onboarding/ExperiencedWithCrypto';
import ImportWallet from 'screens/Onboarding/ImportWallet';
import Wallet from 'screens/Wallet';
import AccountsDetails from 'screens/Wallet/Accounts/AccountsDetails';
import SendToken from 'screens/SendToken';
import EnterAmount from 'screens/SendToken/EnterAmount';
import ConfirmTransaction from 'screens/SendToken/ConfirmTransaction';
import TransferBetweenAccounts from 'screens/SendToken/TransferBetweenAccounts';
import ReceiveToken from 'screens/ReceiveToken';
import SwapToken from 'screens/SwapToken';
import SwapQuotes from 'screens/SwapToken/SwapQuotes';
import SwapConfirmTransaction from 'screens/SwapToken/SwapConfirmTransaction';
import AccountsSettings from 'screens/Wallet/Accounts/AccountsSettings';
import RevealPrivateKeyVerification from 'screens/Wallet/Accounts/AccountsSettings/RevealPrivateKeyVerification';
import RevealPrivateKey from 'screens/Wallet/Accounts/AccountsSettings/RevealPrivateKey';
import TransactionsDetails from 'screens/Wallet/Transactions/TransactionsDetails';
import Networks from 'screens/Networks';
import AddNetwork from 'screens/Networks/AddNetwork';
import Settings from 'screens/Settings';
import PasswordSettings from 'screens/Settings/PasswordSettings';
import ChangePassword from 'screens/Settings/ChangePassword';
import SettingsSecretRecoveryPhrase from 'screens/Settings/SecretRecoveryPhrase';
import TermsAndConditions from 'screens/Settings/TermsAndConditions';
import PrivacyPolicy from 'screens/Settings/PrivacyPolicy';
import General from 'screens/Settings/General';
import ChooseLanguage from 'screens/Settings/General/ChooseLanguage';
import Support from 'screens/Settings/Support';
import WalletConnected from 'screens/Settings/WalletConnected';
import SignatureRequest from 'screens/Settings/SignatureRequest';
import DappTransactions from 'screens/Settings/DappTransactions';
import YourSecretSeedPhrase from 'screens/Settings/YourSecretSeedPhrase';
import DataCollection from 'screens/Settings/DataCollection';
import WDFlow from 'screens/WDFlow';
import ClaimWD from 'screens/WDFlow/ClaimWD';
import ApproveTransaction from 'screens/SwapToken/ApproveTransaction';
import RevealSeedPhraseVerification from 'screens/Settings/RevealSeedPhraseVerification';
import About from 'screens/Settings/General/About';
import { getItem } from 'utils/storage';
import { STORAGE_KEYS } from 'constants';
import { parseObject } from 'utils/parse';
import GlobalAlertModal from 'components/GlobalAlertModal';


export default function Navigation () {

  const dispatch = useDispatch();
  const [initialScreen, setInitialScreen]= useState(null);

  useEffect(() => {
    const screen= getQueryVariable('screen');

    console.log('screen:', screen)

    if(screen && !isUserLoggedIn()){
      dispatch(NetworksAction.initNetworks());
      setInitialScreen(screen);
    }
      
    else
      setInitialScreen('/');  
  },[]);

  const isUserLoggedIn = () => {
    const walletObject = getItem(STORAGE_KEYS.WALLET_OBJECT);
    const loginData = getItem(STORAGE_KEYS.IS_LOGIN);

    if (!loginData.error && !walletObject.error) {
      const parseObj = parseObject(walletObject.data);
      if (parseObj.success)
        return true;
    }

    return false;
  };

  
  const getQueryVariable= (variable) => {
    const query = window.location.search.substring(1);
    const vars = query.split('&');
    for (let i=0;i<vars.length;i++) {
      const pair = vars[i].split('=');
      if(pair[0] === variable){return pair[1];}
    }
    return(false);
  };

  const renderNavigation= () => {
    return (
      <React.Fragment>
        <MemoryRouter initialEntries={ [initialScreen] }>
          <Routes>
            <Route path='/' element={ <UserLayout Component={ Splash } /> } />
            <Route path='/on-boarding' element={ <UserLayout Component={ Onboarding } /> } />
            <Route path='/learn-step-one' element={ <UserLayout Component={ LearnStepOne } /> } />
            <Route path='/learn-step-two' element={ <UserLayout Component={ LearnStepTwo } /> } />
            <Route path='/learn-step-three' element={ <UserLayout Component={ LearnStepThree } /> } />
            <Route path='/learn-step-four' element={ <UserLayout Component={ LearnStepFour } /> } />
            <Route path='/learn-step-five' element={ <UserLayout Component={ LearnStepFive } /> } />
            <Route path='/passcode-setup' element={ <UserLayout Component={ PasscodeSetup } /> } />          
            <Route path='/password-setup' element={ <UserLayout Component={ PasswordSetup } /> } />
            <Route path='/create-wallet' element={ <UserLayout Component={ CreateWallet } /> } />
            <Route path='/secret-recovery-phrase' element={ <UserLayout Component={ SecretRecoveryPhrase } /> } />
            <Route path='/your-secret-recovery-phrase' element={ <UserLayout Component={ YourSecretRecoveryPhrase } /> } />
            <Route path='/welcome' element={ <UserLayout Component={ Welcome } /> } />
            <Route path='/login-password-verification' element={ <UserLayout Component={ LoginPasswordVerification } /> } />
            <Route path='/experienced-crypto' element={ <UserLayout Component={ ExperiencedWithCrypto } /> } />
            <Route path='/import-wallet' element={ <UserLayout Component={ ImportWallet } /> } />
            <Route path='/wallet' element={ <UserLayout Component={ Wallet } /> } />
            <Route path='/accounts-details' element={ <UserLayout Component={ AccountsDetails } /> } />
            <Route path='/send-token' element={ <UserLayout Component={ SendToken } /> } />
            <Route path='/enter-amount' element={ <UserLayout Component={ EnterAmount } /> } />
            <Route path='/confirm-transaction' element={ <UserLayout Component={ ConfirmTransaction } /> } />
            <Route path='/transfer-between-accounts' element={ <UserLayout Component={ TransferBetweenAccounts } /> } />
            <Route path='/receive-token' element={ <UserLayout Component={ ReceiveToken } /> } />
            <Route path='/swap-token' element={ <UserLayout Component={ SwapToken } /> } />
            <Route path='/swap-quotes' element={ <UserLayout Component={ SwapQuotes } /> } />
            <Route path='/swap-confirm-transaction' element={ <UserLayout Component={ SwapConfirmTransaction } /> } />
            <Route path='/accounts-settings' element={ <UserLayout Component={ AccountsSettings } /> } />
            <Route path='/reveal-private-key-verification' element={ <UserLayout Component={ RevealPrivateKeyVerification } /> } />
            <Route path='/reveal-private-key' element={ <UserLayout Component={ RevealPrivateKey } /> } />
            <Route path='/transactions-details' element={ <UserLayout Component={ TransactionsDetails } /> } />
            <Route path='/networks' element={ <UserLayout Component={ Networks } /> } />
            <Route path='/add-network' element={ <UserLayout Component={ AddNetwork } /> } />
            <Route path='/settings' element={ <UserLayout Component={ Settings } /> } />
            <Route path='/password-settings' element={ <UserLayout Component={ PasswordSettings } /> } />
            <Route path='/lock-wallet' element={ <UserLayout Component={ LockWallet } /> } />
            <Route path='/change-password' element={ <UserLayout Component={ ChangePassword } /> } />
            <Route path='/settings-secret-recovery-phrase' element={ <UserLayout Component={ SettingsSecretRecoveryPhrase } /> } />
            <Route path='/terms-conditions' element={ <UserLayout Component={ TermsAndConditions } /> } />
            <Route path='/privacy-policy' element={ <UserLayout Component={ PrivacyPolicy } /> } />
            <Route path='/general' element={ <UserLayout Component={ General } /> } />
            <Route path='/support' element={ <UserLayout Component={ Support } /> } />
            <Route path='/wallet-connected' element={ <UserLayout Component={ WalletConnected } /> } />
            <Route path='/signature-request' element={ <UserLayout Component= { SignatureRequest } /> } />
            <Route path='/dapp-transactions' element={ <UserLayout Component= { DappTransactions } /> } />
            <Route path='/data-collection' element={ <UserLayout Component={ DataCollection } /> } />
            <Route path='/wd-flow' element={ <UserLayout Component={ WDFlow } /> } />
            <Route path='/claim-WD' element={ <UserLayout Component={ ClaimWD } /> } />
            <Route path='/approve-transaction' element={ <UserLayout Component={ ApproveTransaction } /> } />  
            <Route path='/your-secret-seed-phrase' element={ <UserLayout Component={ YourSecretSeedPhrase } /> } /> 
            <Route path='/choose-language' element={ <UserLayout Component={ ChooseLanguage } /> } /> 
            <Route path='/reveal-seed-phrase-verification' element={ <UserLayout Component={ RevealSeedPhraseVerification } /> } />
            <Route path='/about' element={ <UserLayout Component={ About } /> } />          
          </Routes>
          <GlobalAlertModal />
        </MemoryRouter>
      </React.Fragment>
    );
  };

  return initialScreen ? renderNavigation() : null;

}
