const USB_PERMISSIONS_CLOSE = 'usb-permissions-close';
const USB_PERMISSIONS_INIT = 'usb-permissions-init';

const switchTab = () => {
  window.removeEventListener('beforeunload', switchTab);
  chrome.tabs.query({currentWindow: true, active: true}, (currentTabs) => {
    if (currentTabs.length > 0) {
      const prevTabIndex = currentTabs[0].index - 1;
      chrome.tabs.query({index: prevTabIndex}, (prevTabs) => {
        if (prevTabs.length > 0) {
          chrome.tabs.update(prevTabs[0].id, { active: true });
        }
      });
      chrome.tabs.remove(currentTabs[0].id);
    }
  });
};

const postInitMessage = () => {
  const permissionsElement = document.getElementById('trezor-usb-permissions');
  const message = {
    type: USB_PERMISSIONS_INIT,
    extension: chrome.runtime.id,
  };
  permissionsElement.contentWindow.postMessage(message, '*');
};

window.addEventListener('message', event => {
  if (event.data === USB_PERMISSIONS_INIT) {
    postInitMessage();
  }
  if (event.data === USB_PERMISSIONS_CLOSE) {
    switchTab();
  }
});

window.addEventListener('beforeunload', switchTab);