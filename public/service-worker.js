chrome.webRequest.onBeforeRequest.addListener(
  (details) => {
    const url = details.url;
    
    if (url && url?.startsWith('https://internetmoney.io/wc?')) {

      chrome.tabs.remove(details.tabId);

      chrome.tabs.query({ url: chrome.runtime.getURL('index.html') + '*' }, (tabs) => {
        const extensionUrl = url.replace(
          'https://internetmoney.io/wc',
          chrome.runtime.getURL('index.html')
        );
        if (tabs.length > 0) {
          chrome.tabs.update(tabs[0].id, { active: true });
          chrome.tabs.sendMessage(tabs[0].id, {
            type: 'walletConnectSessionRequest',
            extensionUrl,
          });
        } else {
          chrome.tabs.create({ url: extensionUrl });
        }
      });

      return { cancel: true };
    }
  },
  { urls: ["*://internetmoney.io/*"] }
);

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.action === 'returnToPreviousTab') {
    chrome.tabs.query({ currentWindow: true }, (tabs) => {
      let previousTabId = null;

      for (let i = 0; i < tabs.length; i++) {
        const tab = tabs[i];

        if (i === tabs.length - 2) { // Assuming the previous tab is right before the current tab
          previousTabId = tab.id;
        }
      }

      if (previousTabId !== null) {
        chrome.tabs.update(previousTabId, { active: true });
      }
    });
  }
});

const Constants = {
  WALLET_PASS: 'walletPass',
  WALLET_LAST_OPEN: 'walletLastOpen',
  CLEAR_PASSWORD_ALARM_NAME: 'clearPassword',
  PASSWORD_CHECK_INTERVAL_MINUTES: 10,
};

chrome.alarms.onAlarm.addListener(async (alarm) => {
  if (alarm.name === Constants.CLEAR_PASSWORD_ALARM_NAME) {
    const walletLastOpenResponse = await chrome.storage.local.get(
      Constants.WALLET_LAST_OPEN
    );
    if (!walletLastOpenResponse) return;

    const walletLastOpen = walletLastOpenResponse[Constants.WALLET_LAST_OPEN];
    if (!walletLastOpen) return;

    if (
      Date.now() - walletLastOpen >
      Constants.PASSWORD_CHECK_INTERVAL_MINUTES * 60 * 1000
    ) {
      chrome.storage.local.remove(Constants.WALLET_PASS);
    }
  }
});
