# Internet Money Chrome Extension

## Publishing a new build

### Preparation

First, ensure that you have the lastest code from the `development` branch:

```bash
git checkout development
git fetch
git reset origin/development --hard
git checkout -b build-xx
```

Install the dependencies:

```bash
yarn
```

### Version Bumping

After a public release (and only after a public release), you will need to bump the public release version number (e.g. `1.16` => `1.17`):
* `version_name` in `public/manifest.json`

For every build, bump the external build version number (e.g. `1.16.2` => `1.17.0` after a public release, otherwise `1.16.2` => `1.16.3`) in two places:
* `version` in `package.json`
* `version` in `public/manifest.json`

### Bundle and Publish

Build the bundle:

```bash
yarn build
```

This will create a `build` directory of the bundled extension. Next, zip this directory into `build.zip`.

Log into the developer account and open the [Internet Money | Crypto Wallet](https://chrome.google.com/u/2/webstore/devconsole/ff0a41f3-d3ed-4cc6-9721-755c87cd585b/ckklhkaabbmdjkahiaaplikpdddkenic/edit). From there, upload the `build.zip` file as the extension bundle and submit for review.