const sharp = require('sharp');
const fs = require('fs');
const path = require('path');

const resizeAndReplace = async (newImage, imageToReplace) => {
  try {
    // Get the dimensions of the target image
    const { width, height } = await sharp(imageToReplace).metadata();

    // Resize the original image to the target dimensions
    await sharp(newImage)
      .resize(width, height)
      .toFile('temporaryImage.png');

    // Replace the imageToReplace file with the resized original image
    fs.rename('temporaryImage.png', imageToReplace, (err) => {
      if (err) throw err;
      console.log('Image successfully resized and replaced');
    });
  } catch (err) {
    console.error('Error:', err);
  }
};

const main = async () => {
  const icon = path.join(__dirname, '../public/IM-logo.png');
  const newIcon = path.join(__dirname, './internet-money-wallet-logo.png');
  console.log(`Resizing ${newIcon} and replacing ${icon}...`);
  await resizeAndReplace(newIcon, icon);
  console.log('Done!');
}

main();